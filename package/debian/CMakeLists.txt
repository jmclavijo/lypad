set(DEBIAN_PACKAGE_FOLDER ${CMAKE_CURRENT_BINARY_DIR}/lypad-${PROJECT_VERSION})

configure_file(control.in ${DEBIAN_PACKAGE_FOLDER}/DEBIAN/control)
configure_file(postinst.in ${DEBIAN_PACKAGE_FOLDER}/DEBIAN/postinst
               FILE_PERMISSIONS OWNER_READ GROUP_READ WORLD_READ OWNER_WRITE OWNER_EXECUTE GROUP_EXECUTE WORLD_EXECUTE)

add_custom_target(debian-preinstall
                  COMMAND ${CMAKE_COMMAND} --install ${CMAKE_BINARY_DIR} --prefix ${DEBIAN_PACKAGE_FOLDER}/usr --strip
                  COMMAND ${CMAKE_COMMAND} --install ${CMAKE_BINARY_DIR} --prefix ${DEBIAN_PACKAGE_FOLDER}/usr --component DEBIAN
                  DEPENDS lypad)

set(DEBIAN_PACKAGE ${PROJECT_BINARY_DIR}/lypad_${PROJECT_VERSION}_amd64.deb)

find_program(GZIP_EXECUTABLE gzip)
if (${GZIP_EXECUTABLE} STREQUAL "GZIP_EXECUTABLE-NOTFOUND")
    message(WARNING "gzip not found")
else()
    configure_file(${PROJECT_SOURCE_DIR}/Changelog ${CMAKE_CURRENT_BINARY_DIR}/changelog COPYONLY
                     FILE_PERMISSIONS OWNER_READ GROUP_READ WORLD_READ OWNER_WRITE)

    add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/changelog.gz
                       COMMAND ${GZIP_EXECUTABLE} -kn9f ${CMAKE_CURRENT_BINARY_DIR}/changelog
                       DEPENDS
                           ${PROJECT_SOURCE_DIR}/Changelog)

    add_custom_target(debian-changelog DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/changelog.gz)
    add_dependencies(debian-preinstall debian-changelog)

    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/changelog.gz DESTINATION share/doc/lypad COMPONENT DEBIAN EXCLUDE_FROM_ALL)
endif()

install(FILES copyright DESTINATION share/doc/lypad COMPONENT DEBIAN EXCLUDE_FROM_ALL)
install(FILES lypad-archive-keyring.gpg DESTINATION share/keyrings COMPONENT DEBIAN EXCLUDE_FROM_ALL)

file(CHMOD_RECURSE ${DEBIAN_PACKAGE_FOLDER}
     DIRECTORY_PERMISSIONS
         OWNER_READ OWNER_WRITE OWNER_EXECUTE
         GROUP_READ GROUP_EXECUTE
         WORLD_READ WORLD_EXECUTE)

add_custom_command(OUTPUT ${DEBIAN_PACKAGE}
                   COMMAND ${FAKEROOT_EXECUTABLE} ${DPKG_DEB_EXECUTABLE}
                   ARGS
                       -b ${DEBIAN_PACKAGE_FOLDER}
                       ${DEBIAN_PACKAGE}
                   DEPENDS
                       lypad
                       debian-preinstall
                       ${DEBIAN_PACKAGE_FOLDER}/DEBIAN/control
                       ${DEBIAN_PACKAGE_FOLDER}/DEBIAN/postinst
                   COMMENT "Generating lypad_${PROJECT_VERSION}_amd64.deb")

add_custom_target(debian-package DEPENDS ${DEBIAN_PACKAGE})

find_program(LINTIAN_EXECUTABLE lintian)
if (${LINTIAN_EXECUTABLE} STREQUAL LINTIAN_EXECUTABLE-NOTFOUND)
    message(WARNING "Can't check debian package: lintian not found")
else()
    add_custom_target(check-debian-package
                      COMMAND ${LINTIAN_EXECUTABLE} ${DEBIAN_PACKAGE} --no-tag-display-limit
                      DEPENDS ${DEBIAN_PACKAGE}
                      COMMENT "Checking packaging errors")
    add_dependencies(debian-package check-debian-package)
endif()
