% LYPAD(1) lypad @PROJECT_VERSION@
% José M. Clavijo <jmclavijo94@gmail.com>
% June 2021

# NAME
lypad - Note taking application

# SYNOPSIS
**lypad** [OPTION]... [FILE]...

# DESCRIPTION
Application to take notes and optionally pin them to the desktop, resembling the Post-it Notes appearance. Currently, sticking notes to the desktop is only supported on X11.

If FILE(s) are provided they are opened in the lypad's note editor.

# OPTIONS
**-h**, **--help**
: Show command help

**-b**, **--background**
: Start in background: the notes pinned to the desktop will be shown, and the tray icon, if enabled, will be appended the the tray panel, but the application main window won't be opened.

**-v**, **--version**
: Show the version information

**-q**, **--quit**
: Exit the application, closing all the visible notes and removing the tray icon.

# BUGS
Bug reports can be found and filled at https://gitlab.gnome.org/jmclavijo/lypad/issues

# COPYRIGHT
Copyright © 2021 José M. Clavijo\
Distributed under the GNU General Public License v3+
