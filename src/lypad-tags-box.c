/*
 *  lypad-tags-box.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-tags-box.h"

struct _LypadTagsBox
{
    GtkContainer  parent;
    GtkWidget    *expander;
    GList        *children;
    int           spacing;
    int           hborder;
    int           vborder;
    gboolean      interactive;
    LypadTagMode  mode;
    GtkWidget    *popover;
    GtkWidget    *popover_box;
};

G_DEFINE_TYPE (LypadTagsBox, lypad_tags_box, GTK_TYPE_CONTAINER)

enum
{
    SIGNAL_REMOVE_TAG,
    SIGNAL_LAST
};

#define MAX_VISIBLE_CHILDREN (3)
static guint signals[SIGNAL_LAST] = {0};
static GQuark sibling_quark = 0;

static void on_expander_toggled (GtkButton *button, gpointer user_data)
{
    LypadTagsBox *box = LYPAD_TAGS_BOX (user_data);
    gtk_popover_popup (GTK_POPOVER (box->popover));
}

static void on_popover_closed (GtkPopover *popover, gpointer user_data)
{
    LypadTagsBox *box = LYPAD_TAGS_BOX (user_data);
    g_signal_handlers_block_by_func (box->expander, on_expander_toggled, box);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (box->expander), FALSE);
    g_signal_handlers_unblock_by_func (box->expander, on_expander_toggled, box);
}

static void on_child_remove_tag (LypadTagWidget *widget, gpointer user_data)
{
    LypadTagsBox *box = LYPAD_TAGS_BOX (user_data);
    g_signal_emit (box, signals[SIGNAL_REMOVE_TAG], 0, lypad_tag_widget_get_tag (widget));
}

static void lypad_tags_box_init (LypadTagsBox *box)
{
    gtk_widget_set_has_window (GTK_WIDGET (box), FALSE);
    gtk_widget_set_valign (GTK_WIDGET (box), GTK_ALIGN_CENTER);
    GtkStyleContext *context = gtk_widget_get_style_context (GTK_WIDGET (box));
    gtk_style_context_add_class (context, "LypadTagsBox");

    box->expander = gtk_toggle_button_new ();
    GtkWidget *image = gtk_image_new_from_icon_name  ("pan-down-symbolic", GTK_ICON_SIZE_BUTTON);
    gtk_button_set_image (GTK_BUTTON (box->expander), image);
    gtk_button_set_relief (GTK_BUTTON (box->expander), GTK_RELIEF_NONE);
    gtk_widget_set_parent (box->expander, GTK_WIDGET (box));
    gtk_widget_show (box->expander);

    box->popover = gtk_popover_new (box->expander);
    box->popover_box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_add (GTK_CONTAINER (box->popover), box->popover_box);
    gtk_container_set_border_width (GTK_CONTAINER (box->popover_box), 8);
    gtk_widget_show (box->popover_box);

    box->children = NULL;
    box->spacing = 0;
    box->hborder = 4;
    box->vborder = 2;
    box->interactive = FALSE;
    box->mode = LYPAD_TAG_DEFAULT_SIZE;
    g_signal_connect (box->expander, "toggled", G_CALLBACK (on_expander_toggled), box);
    g_signal_connect (box->popover, "closed", G_CALLBACK (on_popover_closed), box);
}

void lypad_tags_box_set_interactive (LypadTagsBox *box, gboolean interactive)
{
    g_return_if_fail (LYPAD_IS_TAGS_BOX (box));
    if (interactive == box->interactive)
        return;

    box->interactive = interactive;
    for (GList *l = box->children; l != NULL; l = l->next)
    {
        lypad_tag_widget_set_interactive (LYPAD_TAG_WIDGET (l->data), interactive);
        GtkWidget *sibling = g_object_get_qdata (G_OBJECT (l->data), sibling_quark);
        lypad_tag_widget_set_interactive (LYPAD_TAG_WIDGET (sibling), interactive);
    }
}

void lypad_tags_box_set_mode (LypadTagsBox *box, LypadTagMode mode)
{
    g_return_if_fail (LYPAD_IS_TAGS_BOX (box));
    if (mode == box->mode)
        return;

    box->mode = mode;
    for (GList *l = box->children; l != NULL; l = l->next)
        lypad_tag_widget_set_mode (LYPAD_TAG_WIDGET (l->data), mode);
}

void lypad_tags_box_add_tag (LypadTagsBox *box, LypadTag *tag)
{
    g_return_if_fail (LYPAD_IS_TAGS_BOX (box));
    GtkWidget *child = lypad_tag_widget_new (tag);
    lypad_tag_widget_set_interactive (LYPAD_TAG_WIDGET (child), box->interactive);
    lypad_tag_widget_set_mode (LYPAD_TAG_WIDGET (child), box->mode);
    box->children = g_list_append (box->children, child);
    gtk_widget_set_parent (child, GTK_WIDGET (box));
    gtk_widget_queue_resize (GTK_WIDGET (box));
    g_signal_connect (child, "remove-tag", G_CALLBACK (on_child_remove_tag), box);

    GtkWidget *sibling = lypad_tag_widget_new (tag);
    lypad_tag_widget_set_interactive (LYPAD_TAG_WIDGET (sibling), box->interactive);
    lypad_tag_widget_set_mode (LYPAD_TAG_WIDGET (sibling), LYPAD_TAG_FULL_SIZE);
    g_object_ref_sink (sibling);
    g_object_set_qdata_full (G_OBJECT (child), sibling_quark, sibling, g_object_unref);
    gtk_container_add (GTK_CONTAINER (box->popover_box), sibling);
    g_signal_connect (sibling, "remove-tag", G_CALLBACK (on_child_remove_tag), box);
}

void lypad_tags_box_remove_tag (LypadTagsBox *box, LypadTag *tag)
{
    g_return_if_fail (LYPAD_IS_TAGS_BOX (box));
    for (GList *l = box->children; l != NULL; l = l->next)
    {
        if (lypad_tag_widget_get_tag (l->data) == tag)
        {
            GtkWidget *sibling = g_object_steal_qdata (l->data, sibling_quark);
            gtk_widget_destroy (sibling);
            g_object_unref (sibling);
            gtk_widget_unparent (l->data);
            box->children = g_list_delete_link (box->children, l);
            break;
        }
    }
}

void lypad_tags_box_clear (LypadTagsBox *box)
{
    g_return_if_fail (LYPAD_IS_TAGS_BOX (box));
    for (GList *l = box->children; l != NULL; l = l->next)
    {
        GtkWidget *sibling = g_object_steal_qdata (l->data, sibling_quark);
        gtk_widget_destroy (sibling);
        g_object_unref (sibling);
        gtk_widget_unparent (l->data);
    }
    g_list_free (box->children);
    box->children = NULL;
}

static void lypad_tags_box_get_preferred_width (GtkWidget *widget, int *minimum_width, int *natural_width)
{
    LypadTagsBox *box = LYPAD_TAGS_BOX (widget);
    int minimum = 0, natural = 0;
    int child_minimum, child_natural;
    int expander_minimum;
    int n_children = g_list_length (box->children);

    gtk_widget_get_preferred_width (box->expander, &expander_minimum, NULL);

    if (box->children)
    {
        gtk_widget_get_preferred_width (box->children->data, &child_minimum, NULL);
        minimum += child_minimum;
        if (n_children > 1)
        {
            minimum += box->spacing;
            minimum += expander_minimum;
        }
    }

    int i = 0;
    for (GList *l = box->children; l != NULL && i < MAX_VISIBLE_CHILDREN; l = l->next, ++i)
    {
        gtk_widget_get_preferred_width (l->data, NULL, &child_natural);
        natural += child_natural;
    }
    if (n_children > 1)
    {
        int n_vis_children = MIN (n_children, MAX_VISIBLE_CHILDREN);
        natural += (n_vis_children - 1) * box->spacing;
        if (n_children > MAX_VISIBLE_CHILDREN)
        {
            natural += box->spacing;
            natural += expander_minimum;
        }
    }

    minimum += 2 * box->hborder;
    natural += 2 * box->hborder;
    minimum = MIN (minimum, natural);

    *minimum_width = minimum;
    *natural_width = natural;
}

static void lypad_tags_box_get_preferred_height (GtkWidget *widget, int *minimum_height, int *natural_height)
{
    LypadTagsBox *box = LYPAD_TAGS_BOX (widget);
    int minimum = 0, natural = 0;
    int child_minimum, child_natural;
    int i = 0;

    gtk_widget_get_preferred_height (box->expander, &minimum, NULL);
    for (GList *l = box->children; l != NULL && i < MAX_VISIBLE_CHILDREN; l = l->next, ++i)
    {
        gtk_widget_get_preferred_height (l->data, &child_minimum, &child_natural);
        minimum = MAX (minimum, child_minimum);
        natural = MAX (natural, child_natural);
    }
    natural = MAX (minimum, natural);
    *minimum_height = minimum + 2 * box->vborder;
    *natural_height = natural + 2 * box->vborder;
}

static void lypad_tags_box_size_allocate (GtkWidget *widget, GtkAllocation *allocation)
{
    LypadTagsBox *box = LYPAD_TAGS_BOX (widget);
    gtk_widget_set_allocation (widget, allocation);

    int remaining_space = allocation->width - 2 * box->hborder;
    int n_children = g_list_length (box->children);
    GtkRequestedSize sizes[MAX_VISIBLE_CHILDREN];
    int n_vis_children = 0;
    int i = 0;
    for (GList *l = box->children; l != NULL && i < MAX_VISIBLE_CHILDREN; l = l->next, ++i)
    {
        GtkWidget *child = l->data;
        gtk_widget_get_preferred_width (child, &sizes[i].minimum_size, &sizes[i].natural_size);
        if (sizes[i].minimum_size <= remaining_space)
        {
            remaining_space -= sizes[i].minimum_size;
            remaining_space -= box->spacing;
            n_vis_children += 1;
        }
        else
            break;
    }

    remaining_space += box->spacing;
    if (n_vis_children < n_children)
    {
        int width, height;
        gtk_widget_get_preferred_width (box->expander, &width, NULL);
        gtk_widget_get_preferred_height (box->expander, &height, NULL);
        remaining_space -= width;
        remaining_space -= box->spacing;
        while (remaining_space < 0)
        {
            n_vis_children -= 1;
            g_return_if_fail (n_vis_children >= 0);
            remaining_space += sizes[n_vis_children].minimum_size;
            remaining_space += box->spacing;
        }

        GtkAllocation expander_allocation;
        expander_allocation.x = allocation->x + allocation->width - width - box->hborder;
        expander_allocation.y = allocation->y + allocation->height * 0.5 - height * 0.5;
        expander_allocation.width = width;
        expander_allocation.height = height;
        gtk_widget_size_allocate (box->expander, &expander_allocation);
        gtk_widget_set_child_visible (box->expander, TRUE);
    }
    else
        gtk_widget_set_child_visible (box->expander, FALSE);

    gtk_distribute_natural_allocation (remaining_space, n_vis_children, sizes);

    GtkAllocation child_allocation;
    child_allocation.y = allocation->y + box->vborder;
    child_allocation.height = allocation->height - 2 * box->vborder;

    int x = allocation->x + box->hborder;
    i = 0;
    for (GList *l = box->children; l != NULL; l = l->next, ++i)
    {
        if (i < n_vis_children)
        {
            child_allocation.x = x;
            child_allocation.width = sizes[i].minimum_size;
            x += child_allocation.width + box->spacing;
            gtk_widget_size_allocate (l->data, &child_allocation);
            gtk_widget_set_child_visible (l->data, TRUE);
            GtkWidget *sibling = g_object_get_qdata (l->data, sibling_quark);
            gtk_widget_hide (sibling);
        }
        else
        {
            gtk_widget_set_child_visible (l->data, FALSE);
            GtkWidget *sibling = g_object_get_qdata (l->data, sibling_quark);
            gtk_widget_show (sibling);
        }
    }
}

static void lypad_tags_box_remove (GtkContainer *container, GtkWidget *widget)
{
    g_return_if_fail (LYPAD_IS_TAG_WIDGET (widget));
    lypad_tags_box_remove_tag (LYPAD_TAGS_BOX (container), lypad_tag_widget_get_tag (LYPAD_TAG_WIDGET (widget)));
}

static void lypad_tags_box_forall (GtkContainer *container, gboolean include_internals, GtkCallback callback, gpointer callback_data)
{
    LypadTagsBox *box = LYPAD_TAGS_BOX (container);
    if (include_internals)
    {
        GList *l = box->children;
        while (l != NULL)
        {
            GList *next_child = l->next;
            callback (l->data, callback_data);
            l = next_child;
        }
        callback (box->expander, callback_data);
    }
}

static gboolean lypad_tags_box_draw (GtkWidget *widget, cairo_t *cr)
{
    if (LYPAD_TAGS_BOX (widget)->children)
    {
        int width = gtk_widget_get_allocated_width (widget);
        int height = gtk_widget_get_allocated_height (widget);
        double radius = height * 0.5;

        cairo_set_line_width (cr, 1);
        cairo_set_source_rgba (cr, 0, 0, 0, 0.1);
        cairo_arc (cr, radius, height * 0.5, radius, G_PI * 0.5, G_PI * 1.5);
        cairo_arc (cr, width - radius, height * 0.5, radius, G_PI * 1.5, G_PI * 0.5);
        cairo_fill (cr);
    }
    return GTK_WIDGET_CLASS (lypad_tags_box_parent_class)->draw (widget, cr);
}

static void lypad_tags_box_destroy (GtkWidget *widget)
{
    lypad_tags_box_clear (LYPAD_TAGS_BOX (widget));
    GTK_WIDGET_CLASS (lypad_tags_box_parent_class)->destroy (widget);
}

static void lypad_tags_box_class_init (LypadTagsBoxClass *klass)
{
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
    GtkContainerClass *container_class = GTK_CONTAINER_CLASS (klass);

    widget_class->get_preferred_width  = lypad_tags_box_get_preferred_width;
    widget_class->get_preferred_height = lypad_tags_box_get_preferred_height;
    widget_class->size_allocate        = lypad_tags_box_size_allocate;
    widget_class->draw                 = lypad_tags_box_draw;
    widget_class->destroy              = lypad_tags_box_destroy;
    container_class->remove            = lypad_tags_box_remove;
    container_class->forall            = lypad_tags_box_forall;

    signals[SIGNAL_REMOVE_TAG] = g_signal_new ("remove-tag", G_TYPE_FROM_CLASS (klass),
                                               G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                               g_cclosure_marshal_VOID__OBJECT,
                                               G_TYPE_NONE, 1, LYPAD_TYPE_TAG);

    sibling_quark = g_quark_from_string ("lypad-tabs-box-sibling");
}

void lypad_tags_box_set_spacing (LypadTagsBox *box, int spacing)
{
    box->spacing = spacing;
    gtk_widget_queue_resize (GTK_WIDGET (box));
}

GtkWidget *lypad_tags_box_new ()
{
    return g_object_new (LYPAD_TYPE_TAGS_BOX, NULL);
}
