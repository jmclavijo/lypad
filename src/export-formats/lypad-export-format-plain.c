/*
 *  lypad-export-format-plain.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-export-format-plain.h"
#include "src/lypad-text-buffer.h"

struct _LypadExportFormatPlain
{
    GObject parent;
};

static void export_format_iface_init (LypadExportFormatInterface *iface);

G_DEFINE_TYPE_WITH_CODE (LypadExportFormatPlain, lypad_export_format_plain, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (LYPAD_TYPE_EXPORT_FORMAT, export_format_iface_init))

static gboolean lypad_export_format_plain_export_note (LypadExportFormat *format, LypadNote *note, const char *filename)
{
    FILE *file = fopen (filename, "w");

    GtkTextIter start, end;
    GtkTextBuffer *buffer = lypad_note_get_title_buffer (note);
    gtk_text_buffer_get_start_iter (buffer, &start);
    if (!gtk_text_iter_ends_line (&start))
    {
        end = start;
        gtk_text_iter_forward_to_line_end (&end);
        char *text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
        fprintf (file, "%s\n", text);
        g_free (text);
    }

    buffer = lypad_note_get_content_buffer (note);
    gtk_text_buffer_get_start_iter (buffer, &start);

    end = start;
    while (!gtk_text_iter_is_end (&start))
    {
        gtk_text_iter_forward_line (&end);

        GSList *tags = gtk_text_iter_get_tags (&start);
        for (GSList *l = tags; l != NULL; l = l->next)
        {
            int tag_type = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (l->data), "lypad-text-tag-type"));
            if (tag_type == TAG_TYPE_LIST)
            {
                LypadTextListInfo *info = g_object_get_data (G_OBJECT (l->data), "lypad-text-tag-value");
                switch (lypad_text_list_info_get_type (info))
                {
                    case LYPAD_TEXT_LIST_TYPE_BULLET:
                        // Always use the bullet character
                        fprintf (file, " \u2022 ");
                        break;

                    default:
                        g_warn_if_reached ();
                }

                gtk_text_iter_forward_to_tag_toggle (&start, l->data);
                break;
            }
        }

        char *text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
        fprintf (file, "%s", text);
        g_free (text);
        start = end;
    }

/*
    gtk_text_buffer_get_bounds (buffer, &start, &end);
    char *text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
    fprintf (file, "%s\n", text);
    g_free (text);*/
    fclose (file);
    return TRUE;
}

static void lypad_export_format_plain_init (LypadExportFormatPlain *format)
{

}

static const char *lypad_export_format_plain_get_name (LypadExportFormat *format)
{
    static const char *name = "Plain text";
    return name;
}

static const char *lypad_export_format_plain_get_extension (LypadExportFormat *format)
{
    static const char *extension = "txt";
    return extension;
}

static const char *lypad_export_format_plain_get_mime_type (LypadExportFormat *format)
{
    static const char *mime_type = "text/plain";
    return mime_type;
}

static void export_format_iface_init (LypadExportFormatInterface *iface)
{
    iface->get_name      = lypad_export_format_plain_get_name;
    iface->get_extension = lypad_export_format_plain_get_extension;
    iface->get_mime_type = lypad_export_format_plain_get_mime_type;
    iface->export_note   = lypad_export_format_plain_export_note;
}

static void lypad_export_format_plain_class_init (LypadExportFormatPlainClass *klass)
{

}
