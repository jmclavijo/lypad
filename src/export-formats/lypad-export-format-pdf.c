/*
 *  lypad-export-format-pdf.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-export-format-pdf.h"
#include "src/lypad-text-buffer.h"
#include <cairo-pdf.h>
#include "src/color-selection/lypad-color-widget.h"

struct _LypadExportFormatPDF
{
    GObject parent;
};

#define PAGE_A4_WIDTH           595
#define PAGE_A4_HEIGHT          842
#define PAGE_A4_MARGIN_LEFT      32
#define PAGE_A4_MARGIN_RIGHT     32
#define PAGE_A4_MARGIN_TOP       32
#define PAGE_A4_MARGIN_BOTTOM    32
#define TITLE_LINE_MARGIN_TOP     4
#define TITLE_LINE_MARGIN_BOTTOM 10

static void export_format_iface_init (LypadExportFormatInterface *iface);

G_DEFINE_TYPE_WITH_CODE (LypadExportFormatPDF, lypad_export_format_pdf, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (LYPAD_TYPE_EXPORT_FORMAT, export_format_iface_init));

struct TagStartIndex
{
    GtkTextTag *tag;
    int         index;
};

static PangoAttribute *lypad_tag_to_pango_attribute (GtkTextTag *tag)
{
    int tag_type = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (tag), "lypad-text-tag-type"));
    if (tag_type == TAG_TYPE_TOGGLE)
    {
        switch (GPOINTER_TO_INT (g_object_get_data (G_OBJECT (tag), "lypad-text-tag-value")))
        {
            case BOLD_TAG:
                return pango_attr_weight_new (PANGO_WEIGHT_BOLD);

            case ITALIC_TAG:
                return pango_attr_style_new (PANGO_STYLE_ITALIC);

            case UNDERLINE_TAG:
                return pango_attr_underline_new (PANGO_UNDERLINE_SINGLE);

            case STRIKETHROUGH_TAG:
                return pango_attr_strikethrough_new (TRUE);
        }
    }
    else if (tag_type == TAG_TYPE_FONT)
    {
        const char *family = g_object_get_data (G_OBJECT (tag), "lypad-text-tag-value");
        return pango_attr_family_new (family);
    }
    else if (tag_type == TAG_TYPE_SIZE)
    {
        int size = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (tag), "lypad-text-tag-value"));
        return pango_attr_size_new_absolute (size * PANGO_SCALE);
    }
    else if (tag_type == TAG_TYPE_COLOR)
    {
        GdkRGBA *color = g_object_get_data (G_OBJECT (tag), "lypad-text-tag-value");
        guint16 r, g, b;
        if (gdk_rgba_equal (color, &LYPAD_AUTOMATIC_TEXT_COLOR))
        {
            r = 0.;
            g = 0.;
            b = 0.;
        }
        else
        {
            r = color->red * G_MAXUINT16;
            g = color->green * G_MAXUINT16;
            b = color->blue * G_MAXUINT16;
        }
        return pango_attr_foreground_new (r, g, b);
    }
    return NULL;
}

static double get_layout_height (PangoLayout *layout)
{
	PangoRectangle rect;
	pango_layout_get_extents (layout, NULL, &rect);
	return (double) rect.height / (double) PANGO_SCALE;
}

static double get_layout_line_height (PangoLayoutIter *iter)
{
    PangoRectangle rect;
    pango_layout_iter_get_line_extents (iter, NULL, &rect);
	return (double) rect.height / (double) PANGO_SCALE;
}


static PangoAttrList *get_pango_attributes (GtkTextBuffer *buffer, GtkTextIter *start, GtkTextIter *end)
{
    PangoAttrList *attributes = pango_attr_list_new ();
    int index_offset = gtk_text_iter_get_line_index (start);

    GtkTextIter iter = *start;
    GList *open_tags = NULL;
    GSList *tag_list = gtk_text_iter_get_tags (&iter);
    for (GSList *l = tag_list; l != NULL; l = l->next)
    {
        struct TagStartIndex *item = g_new (struct TagStartIndex, 1);
        item->tag = l->data;
        item->index = gtk_text_iter_get_line_index (&iter) - index_offset;
        open_tags = g_list_prepend (open_tags, item);
    }
    g_slist_free (tag_list);

    gtk_text_iter_forward_to_tag_toggle (&iter, NULL);
    while (gtk_text_iter_compare (&iter, end) < 0)
    {
        // Close tags
        GList *l = open_tags;
        while (l != NULL)
        {
            GList *next = l->next;
            struct TagStartIndex *item = l->data;
            if (gtk_text_iter_ends_tag (&iter, item->tag))
            {
                PangoAttribute *attr = lypad_tag_to_pango_attribute (item->tag);
                if (attr)
                {
                    attr->start_index = item->index;
                    attr->end_index = gtk_text_iter_get_line_index (&iter) - index_offset;
                    pango_attr_list_insert (attributes, attr);
                }

                open_tags = g_list_delete_link (open_tags, l);
                g_free (item);
            }
            l = next;
        }

        // Append new open tags
        tag_list = gtk_text_iter_get_toggled_tags (&iter, TRUE);
        for (GSList *sl = tag_list; sl != NULL; sl = sl->next)
        {
            struct TagStartIndex *item = g_new (struct TagStartIndex, 1);
            item->tag = sl->data;
            item->index = gtk_text_iter_get_line_index (&iter) - index_offset;
            open_tags = g_list_prepend (open_tags, item);
        }
        g_slist_free (tag_list);

        gtk_text_iter_forward_to_tag_toggle (&iter, NULL);
    }

    // Close remaining tags
    for (GList *l = open_tags; l != NULL; l = l->next)
    {
        struct TagStartIndex *item = l->data;

        PangoAttribute *attr = lypad_tag_to_pango_attribute (item->tag);
        if (attr)
        {
            attr->start_index = item->index;
            attr->end_index = gtk_text_iter_get_line_index (end) - index_offset;
            pango_attr_list_insert (attributes, attr);
        }
    }
    g_list_free_full (open_tags, g_free);

    return attributes;
}

static PangoAttrList *get_linebreak_pango_attributes (GtkTextBuffer *buffer, GtkTextIter *iter)
{
    PangoAttrList *attributes = pango_attr_list_new ();
    GSList *tag_list = gtk_text_iter_get_tags (iter);
    for (GSList *l = tag_list; l != NULL; l = l->next)
    {
        int tag_type = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (l->data), "lypad-text-tag-type"));
        if (tag_type == TAG_TYPE_SIZE)
        {
            int size = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (l->data), "lypad-text-tag-value"));
            PangoAttribute *attr = pango_attr_size_new_absolute (size * PANGO_SCALE);
            attr->start_index = 0;
            attr->end_index = 1;
            pango_attr_list_insert (attributes, attr);
        }
    }
    g_slist_free (tag_list);
    return attributes;
}

static void fill_layout (PangoLayout *layout, GtkTextBuffer *buffer, GtkTextIter *start, GtkTextIter *end)
{
	if (gtk_text_iter_ends_line (start))
	{
		pango_layout_set_text (layout, " ", 1);
        PangoAttrList *attributes = get_linebreak_pango_attributes (buffer, start);
        pango_layout_set_attributes (layout, attributes);
        pango_attr_list_unref (attributes);
		return;
	}

    char *text = gtk_text_buffer_get_slice (buffer, start, end, TRUE);
    pango_layout_set_text (layout, text, -1);
    g_free (text);

    PangoAttrList *attributes = get_pango_attributes (buffer, start, end);
    pango_layout_set_attributes (layout, attributes);
    pango_attr_list_unref (attributes);
}

static void print_note_to_cairo (LypadNote *note, cairo_t *cr)
{
    PangoContext *pango_context = pango_font_map_create_context (pango_cairo_font_map_get_default ());
    PangoLayout *layout = pango_layout_new (pango_context);

    int page_width = PAGE_A4_WIDTH - PAGE_A4_MARGIN_LEFT - PAGE_A4_MARGIN_RIGHT;
    int page_height = PAGE_A4_HEIGHT - PAGE_A4_MARGIN_TOP - PAGE_A4_MARGIN_BOTTOM;
    pango_layout_set_width (layout, page_width * PANGO_SCALE);
	pango_layout_set_wrap (layout, PANGO_WRAP_WORD_CHAR);

    double y = PAGE_A4_MARGIN_TOP;
    int current_height = 0;
    int line_height;
    GtkTextIter range_start, range_end, buffer_end;

    GtkTextBuffer *buffer = lypad_note_get_title_buffer (note);
    gtk_text_buffer_get_start_iter (buffer, &range_start);
    if (!gtk_text_iter_ends_line (&range_start))
    {
        range_end = range_start;
        gtk_text_iter_forward_to_line_end (&range_end);
        fill_layout (layout, buffer, &range_start, &range_end);
        line_height = get_layout_height (layout);
        if (line_height < page_height)
        {
            cairo_move_to (cr, PAGE_A4_MARGIN_LEFT, y);
            pango_cairo_show_layout (cr, layout);
            y += line_height + TITLE_LINE_MARGIN_TOP;
            current_height += line_height + TITLE_LINE_MARGIN_TOP;

            cairo_set_line_width (cr, 1);
            cairo_set_source_rgb (cr, 0, 0, 0);
            cairo_move_to (cr, PAGE_A4_MARGIN_LEFT, y);
            cairo_line_to (cr, PAGE_A4_MARGIN_LEFT + page_width, y);
            cairo_stroke (cr);

            y += TITLE_LINE_MARGIN_BOTTOM;
            current_height += TITLE_LINE_MARGIN_BOTTOM;
        }
        else
            g_warning ("Note title is too big to be printed on a single page. Skipping.");
    }

    buffer = lypad_note_get_content_buffer (note);
    gtk_text_buffer_get_bounds (buffer, &range_start, &buffer_end);

    range_end = range_start;
    gtk_text_iter_forward_to_line_end (&range_end);
    while (gtk_text_iter_compare (&range_start, &buffer_end) < 0)
    {
        fill_layout (layout, buffer, &range_start, &range_end);
        line_height = get_layout_height (layout);

        if (current_height + line_height > page_height)
        {
            PangoLayoutIter *layout_iter = pango_layout_get_iter (layout);
            line_height = 0;

            do
            {
                line_height += get_layout_line_height (layout_iter);
                if (current_height + line_height > page_height)
                    break;
            } while (pango_layout_iter_next_line (layout_iter));
            range_end = range_start;
            int index = gtk_text_iter_get_line_index (&range_end);
            index += pango_layout_iter_get_index (layout_iter);
            gtk_text_iter_set_line_index (&range_end, index);
            pango_layout_iter_free (layout_iter);

            fill_layout (layout, buffer, &range_start, &range_end);

            if (gtk_text_iter_ends_line (&range_end))
            {
                gtk_text_iter_forward_line (&range_start);
                range_end = range_start;
                gtk_text_iter_forward_to_line_end (&range_end);
            }
            else
            {
                range_start = range_end;
                gtk_text_iter_forward_to_line_end (&range_end);
            }

            cairo_move_to (cr, PAGE_A4_MARGIN_LEFT, y);
            pango_cairo_show_layout (cr, layout);
            cairo_show_page (cr);
            y = PAGE_A4_MARGIN_TOP;
            current_height = 0;
        }
        else
        {
            cairo_move_to (cr, PAGE_A4_MARGIN_LEFT, y);
            pango_cairo_show_layout (cr, layout);
            y += line_height;
            current_height += line_height;
            gtk_text_iter_forward_line (&range_start);
            range_end = range_start;
            gtk_text_iter_forward_to_line_end (&range_end);
        }
    }
    if (current_height > 0)
        cairo_show_page (cr);

    g_object_unref (layout);
    g_object_unref (pango_context);
}

static gboolean lypad_export_format_pdf_export_note (LypadExportFormat *format, LypadNote *note, const char *filename)
{
    cairo_surface_t *surface = cairo_pdf_surface_create (filename, PAGE_A4_WIDTH, PAGE_A4_HEIGHT);
    cairo_t *cr = cairo_create (surface);
    cairo_pdf_surface_set_metadata (surface, CAIRO_PDF_METADATA_CREATOR, "Lypad");
    print_note_to_cairo (note, cr);
    cairo_surface_destroy (surface);
    cairo_destroy(cr);
    return TRUE;
}

static gboolean lypad_export_format_pdf_export_notes (LypadExportFormat *format, GSList *notes, const char *filename)
{
    cairo_surface_t *surface = cairo_pdf_surface_create (filename, PAGE_A4_WIDTH, PAGE_A4_HEIGHT);
    cairo_t *cr = cairo_create (surface);
    cairo_pdf_surface_set_metadata (surface, CAIRO_PDF_METADATA_CREATOR, "Lypad");
    for (GSList *l = notes; l != NULL; l = l->next)
        print_note_to_cairo (l->data, cr);
    cairo_surface_destroy (surface);
    cairo_destroy(cr);
    return TRUE;
}

static void lypad_export_format_pdf_init (LypadExportFormatPDF *format)
{

}

static const char *lypad_export_format_pdf_get_name (LypadExportFormat *format)
{
    static const char *name = "Portable document format";
    return name;
}

static const char *lypad_export_format_pdf_get_extension (LypadExportFormat *format)
{
    static const char *extension = "pdf";
    return extension;
}

static const char *lypad_export_format_pdf_get_mime_type (LypadExportFormat *format)
{
    static const char *mime_type = "application/pdf";
    return mime_type;
}

static void export_format_iface_init (LypadExportFormatInterface *iface)
{
    iface->get_name      = lypad_export_format_pdf_get_name;
    iface->get_extension = lypad_export_format_pdf_get_extension;
    iface->get_mime_type = lypad_export_format_pdf_get_mime_type;
    iface->export_note   = lypad_export_format_pdf_export_note;
    iface->export_notes  = lypad_export_format_pdf_export_notes;
}

static void lypad_export_format_pdf_class_init (LypadExportFormatPDFClass *klass)
{

}
