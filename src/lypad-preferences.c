/*
 *  lypad-preferences.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-preferences.h"
#include "lypad-utils.h"
#include "lypad-timer.h"

struct _LypadPreferences
{
    GObject     parent;
    char       *data_path;
    char       *config_path;
    GKeyFile   *key_file;
    gboolean    modified;
    LypadTimer *autosave_timer;

    // Interface
    char       *default_note_path;
};

G_DEFINE_TYPE (LypadPreferences, lypad_preferences, G_TYPE_OBJECT)

static void on_autosave_timeout (LypadTimer *time, gpointer user_data)
{
    LypadPreferences *preferences = user_data;
    lypad_preferences_save (preferences);
}

static void lypad_preferences_init (LypadPreferences *preferences)
{
    preferences->data_path = g_strdup_printf ("%s/lypad", g_get_user_data_dir ());
    preferences->config_path = g_strdup_printf ("%s/lypad", g_get_user_config_dir ());

    if (g_mkdir_with_parents (preferences->data_path, 0775) < 0)
        g_warning ("Error creating data folder");
    if (g_mkdir_with_parents (preferences->config_path, 0775) < 0)
        g_warning ("Error creating config folder");

    preferences->default_note_path = g_strdup_printf ("%s/notes", preferences->data_path);
    if (g_mkdir_with_parents (preferences->default_note_path, 0775) < 0)
        g_warning ("Error creating default folder for notes");

    preferences->key_file = g_key_file_new ();
    char *config_file_name = g_strdup_printf ("%s/lypad.conf", preferences->config_path);
    g_key_file_load_from_file (preferences->key_file, config_file_name, G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS, NULL);
    g_free (config_file_name);
    preferences->modified = FALSE;
    preferences->autosave_timer = lypad_timer_new ();
    lypad_timer_set_delay (preferences->autosave_timer, 60, 40);
    g_signal_connect (preferences->autosave_timer, "timeout", G_CALLBACK (on_autosave_timeout), preferences);
}

const char *lypad_preferences_get_data_path (LypadPreferences *preferences)
{
    return preferences->data_path;
}

char **lypad_preferences_get_note_paths (LypadPreferences *preferences)
{
    char **path_list = g_new (char *, 3);
    GSettings *settings = g_settings_new ("org.gnome.lypad");

    path_list[0] = g_settings_get_string (settings, "notes-folder");
    path_list[1] = NULL;
    path_list[2] = NULL;
    if (path_list[0] && path_list[0][0] == '~')
    {
        char *str = lypad_utils_path_expand_tilde (path_list[0]);
        g_free (path_list[0]);
        path_list[0] = str;
    }

    if (!path_list[0] || strcmp (path_list[0], "") == 0)
        path_list[0] = g_strdup (preferences->default_note_path);
    else if (strcmp (path_list[0], preferences->default_note_path) != 0)
        path_list[1] = g_strdup (preferences->default_note_path);

    g_object_unref (settings);
    return path_list;
}

void lypad_preferences_save (LypadPreferences *preferences)
{
    if (preferences->modified)
    {
        char *config_file_name = g_strdup_printf ("%s/lypad.conf", preferences->config_path);
        g_key_file_save_to_file (preferences->key_file, config_file_name, NULL);
        g_free (config_file_name);
        preferences->modified = FALSE;
    }
}

GKeyFile *lypad_preferences_get_key_file (LypadPreferences *preferences)
{
    return preferences->key_file;
}

void lypad_preferences_set_modified (LypadPreferences *preferences)
{
    preferences->modified = TRUE;
    lypad_timer_start (preferences->autosave_timer);
}

static void lypad_preferences_finalize (GObject *object)
{
    LypadPreferences *preferences = LYPAD_PREFERENCES (object);
    g_free (preferences->data_path);
    g_free (preferences->config_path);
    g_key_file_free (preferences->key_file);
    G_OBJECT_CLASS (lypad_preferences_parent_class)->finalize (object);
}

static void lypad_preferences_class_init (LypadPreferencesClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    object_class->finalize = lypad_preferences_finalize;
}

LypadPreferences *lypad_preferences_new ()
{
    return g_object_new (LYPAD_TYPE_PREFERENCES, NULL);
}
