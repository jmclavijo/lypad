/*
 *  lypad-tag-chooser.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include "lypad-tag.h"

G_BEGIN_DECLS

#define LYPAD_TYPE_TAG_CHOOSER (lypad_tag_chooser_get_type ())
G_DECLARE_FINAL_TYPE (LypadTagChooser, lypad_tag_chooser, LYPAD, TAG_CHOOSER, GtkDialog)

GtkWidget *lypad_tag_chooser_new     (GtkWindow *parent);
LypadTag  *lypad_tag_chooser_get_tag (LypadTagChooser *chooser);              // [transfer full]

G_END_DECLS
