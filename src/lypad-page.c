/*
 *  lypad-page.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-page.h"

typedef struct _LypadPagePrivate LypadPagePrivate;

struct _LypadPagePrivate
{
    GtkWidget *side_panel;
    GtkWidget *list_box;
    GtkWidget *go_back_row;
};

G_DEFINE_TYPE_WITH_PRIVATE (LypadPage, lypad_page, GTK_TYPE_GRID)

enum
{
    SIGNAL_GO_BACK,
    SIGNAL_LAST
};

static guint signals[SIGNAL_LAST] = {0};
static GQuark data_quark = 0;

#define LYPAD_PAGE_ROW(x) ((LypadPageRow *) (x))

GtkWidget *lypad_page_get_side_panel (LypadPage *page)
{
    g_return_val_if_fail (LYPAD_IS_PAGE (page), NULL);
    LypadPagePrivate *priv = lypad_page_get_instance_private (page);
    return priv->side_panel;
}

static int lypad_page_row_sort_func (GtkListBoxRow *row1, GtkListBoxRow *row2, gpointer user_data)
{
    LypadPageRowSortFunc row_sort_func = user_data;
    int idx1 = gtk_list_box_row_get_index (row1);
    int idx2 = gtk_list_box_row_get_index (row2);
    if (idx1 == 0 && idx2 == 0)
        return 0;
    else if (idx1 == 0)
        return -1;
    else if (idx2 == 0)
        return 1;
    else
        return row_sort_func (row1, row2);
}

void lypad_page_set_row_sort_func (LypadPage *page, LypadPageRowSortFunc sort_func)
{
    g_return_if_fail (LYPAD_IS_PAGE (page));
    LypadPagePrivate *priv = lypad_page_get_instance_private (page);
    if (sort_func)
    {
        gtk_list_box_set_sort_func (GTK_LIST_BOX (priv->list_box), lypad_page_row_sort_func, sort_func, NULL);
    }
    else
        gtk_list_box_set_sort_func (GTK_LIST_BOX (priv->list_box), NULL, NULL, NULL);
}

void lypad_page_go_back (LypadPage *page)
{
    g_return_if_fail (LYPAD_IS_PAGE (page));
    if (LYPAD_PAGE_GET_CLASS (page)->go_back)
        LYPAD_PAGE_GET_CLASS (page)->go_back (page);
    g_signal_emit (page, signals[SIGNAL_GO_BACK], 0);
}

static void on_panel_row_destroy (GtkWidget *widget, gpointer user_data)
{
    LypadPage *page = LYPAD_PAGE (user_data);
    if (LYPAD_PAGE_GET_CLASS (page)->release_row)
        LYPAD_PAGE_GET_CLASS (page)->release_row (page, LYPAD_PAGE_ROW (widget));
}

LypadPageRow *lypad_page_add_panel_row (LypadPage *page, gpointer user_data)
{
    g_return_val_if_fail (LYPAD_IS_PAGE (page), NULL);
    LypadPagePrivate *priv = lypad_page_get_instance_private (page);
    GtkWidget *row = gtk_list_box_row_new ();
    GtkStyleContext *context = gtk_widget_get_style_context (row);
    gtk_style_context_add_class (context, "LypadPanelRow");
    if (LYPAD_PAGE_GET_CLASS (page)->init_row)
        LYPAD_PAGE_GET_CLASS (page)->init_row (page, LYPAD_PAGE_ROW (row), user_data);
    gtk_list_box_insert (GTK_LIST_BOX (priv->list_box), row, -1);
    gtk_widget_show_all (row);
    g_signal_connect (row, "destroy", G_CALLBACK (on_panel_row_destroy), page);
    return LYPAD_PAGE_ROW (row);
}

void lypad_page_remove_panel_row (LypadPage *page, LypadPageRow *row)
{
    g_return_if_fail (LYPAD_IS_PAGE (page));
    gtk_widget_destroy (GTK_WIDGET (row));
}

GList *lypad_page_get_panel_rows (LypadPage *page)
{
    g_return_val_if_fail (LYPAD_IS_PAGE (page), NULL);
    LypadPagePrivate *priv = lypad_page_get_instance_private (page);
    if (priv->list_box)
    {
        GList *children = gtk_container_get_children (GTK_CONTAINER (priv->list_box));
        children = g_list_remove (children, priv->go_back_row);
        return children;
    }
    else
        return NULL;
}

void lypad_page_select_panel_row (LypadPage *page, LypadPageRow *row)
{
    g_return_if_fail (LYPAD_IS_PAGE (page));
    LypadPagePrivate *priv = lypad_page_get_instance_private (page);
    if (priv->list_box)
        gtk_list_box_select_row (GTK_LIST_BOX (priv->list_box), row);
}

LypadPageRow *lypad_page_get_selected_panel_row (LypadPage *page)
{
    g_return_val_if_fail (LYPAD_IS_PAGE (page), NULL);
    LypadPagePrivate *priv = lypad_page_get_instance_private (page);
    if (priv->list_box)
        return LYPAD_PAGE_ROW (gtk_list_box_get_selected_row (GTK_LIST_BOX (priv->list_box)));
    return NULL;
}

void lypad_page_row_set_data (LypadPageRow *row, gpointer data, GDestroyNotify destroy_func)
{
    g_object_set_qdata_full (G_OBJECT (row), data_quark, data, destroy_func);
}

gpointer lypad_page_row_get_data (LypadPageRow *row)
{
    return g_object_get_qdata (G_OBJECT (row), data_quark);
}

static GtkWidget *lypad_page_panel_add_go_back (GtkListBox *list_box)
{
    GtkWidget *box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 12);
    gtk_box_pack_start (GTK_BOX (box), gtk_image_new_from_icon_name ("go-previous", GTK_ICON_SIZE_BUTTON), FALSE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX (box), gtk_label_new ("Back"), FALSE, TRUE, 0);

    GtkWidget *row = gtk_list_box_row_new ();
    gtk_container_add (GTK_CONTAINER (row), box);

    GtkStyleContext *context = gtk_widget_get_style_context (row);
    gtk_style_context_add_class (context, "LypadPanelRow");
    gtk_list_box_insert (list_box, row, -1);
    return row;
}

static void on_panel_row_activated (GtkListBox *panel, GtkListBoxRow *row, gpointer user_data)
{
    LypadPage *page = LYPAD_PAGE (user_data);
    int index = gtk_list_box_row_get_index (row);
    if (index == 0)
        lypad_page_go_back (page);
    else
        if (LYPAD_PAGE_GET_CLASS (page)->row_activated)
            LYPAD_PAGE_GET_CLASS (page)->row_activated (page, row);
}

static void lypad_page_init (LypadPage *page)
{
    LypadPagePrivate *priv = lypad_page_get_instance_private (page);
    priv->side_panel = g_object_ref_sink (gtk_scrolled_window_new (NULL, NULL));
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (priv->side_panel), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
    priv->list_box = g_object_ref_sink (gtk_list_box_new ());
    gtk_container_add (GTK_CONTAINER (priv->side_panel), priv->list_box);
    priv->go_back_row = lypad_page_panel_add_go_back (GTK_LIST_BOX (priv->list_box));
    gtk_widget_show_all (priv->side_panel);

    g_signal_connect (priv->list_box, "row-activated", G_CALLBACK (on_panel_row_activated), page);
}

static void lypad_page_destroy (GtkWidget *widget)
{
    LypadPage *page = LYPAD_PAGE (widget);
    LypadPagePrivate *priv = lypad_page_get_instance_private (page);
    if (priv->side_panel)
    {
        gtk_widget_destroy (priv->side_panel);
        g_object_unref (priv->side_panel);
        priv->side_panel = NULL;
    }
    g_clear_object (&priv->list_box);
    GTK_WIDGET_CLASS (lypad_page_parent_class)->destroy (widget);
}

static void lypad_page_class_init (LypadPageClass *klass)
{
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

    klass->init_row       = NULL;
    klass->release_row    = NULL;
    klass->row_activated  = NULL;
    klass->go_back        = NULL;
    widget_class->destroy = lypad_page_destroy;

    signals[SIGNAL_GO_BACK] = g_signal_new ("go-back", G_TYPE_FROM_CLASS (klass),
                                            G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                            g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

    data_quark = g_quark_from_string ("lypad-row-data");
}