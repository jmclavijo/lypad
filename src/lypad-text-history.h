/*
 *  lypad-text-history.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

typedef struct _LypadTextHistoryFuncs LypadTextHistoryFuncs;

#define LYPAD_TYPE_TEXT_HISTORY (lypad_text_history_get_type ())
G_DECLARE_FINAL_TYPE (LypadTextHistory, lypad_text_history, LYPAD, TEXT_HISTORY, GObject)

struct _LypadTextHistoryFuncs
{
    void (*insert_text) (GtkTextBuffer *buffer, GtkTextIter *location, const char  *text, int len);
    void (*delete_text) (GtkTextBuffer *buffer, GtkTextIter *start, GtkTextIter *end);
    void (*apply_tag)   (GtkTextBuffer *buffer, GtkTextTag *tag, GtkTextIter *start, GtkTextIter *end);
    void (*remove_tag)  (GtkTextBuffer *buffer, GtkTextTag *tag, GtkTextIter *start, GtkTextIter *end);
};

/* LypadTextHistory doesn't keep a reference to buffer to avoid reference loops */
LypadTextHistory *lypad_text_history_new               (GtkTextBuffer *buffer, LypadTextHistoryFuncs *funcs);
void              lypad_text_history_clear             (LypadTextHistory *history);
gboolean          lypad_text_history_can_undo          (LypadTextHistory *history);
gboolean          lypad_text_history_can_redo          (LypadTextHistory *history);
void              lypad_text_history_undo              (LypadTextHistory *history);
void              lypad_text_history_redo              (LypadTextHistory *history);

void              lypad_text_history_lock              (LypadTextHistory *history);
void              lypad_text_history_unlock            (LypadTextHistory *history);

void              lypad_text_history_begin_user_action (LypadTextHistory *history);
void              lypad_text_history_end_user_action   (LypadTextHistory *history);

void              lypad_text_history_insert_text       (LypadTextHistory *history, GtkTextIter *start, GtkTextIter *end, const char *text, int len);
void              lypad_text_history_delete_text       (LypadTextHistory *history, GtkTextIter *start, GtkTextIter *end);
void              lypad_text_history_apply_tag         (LypadTextHistory *history, GtkTextTag *tag, const GtkTextIter *start, const GtkTextIter *end);
void              lypad_text_history_remove_tag        (LypadTextHistory *history, GtkTextTag *tag, const GtkTextIter *start, const GtkTextIter *end);

G_END_DECLS
