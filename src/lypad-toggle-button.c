/*
 *  lypad-toggle-button.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-toggle-button.h"

typedef struct _LypadToggleButtonPrivate LypadToggleButtonPrivate;

struct _LypadToggleButtonPrivate
{
    gboolean   is_active;
    GtkWidget *base_image;
    GtkWidget *active_image;
};

G_DEFINE_TYPE_WITH_PRIVATE (LypadToggleButton, lypad_toggle_button, LYPAD_TYPE_BUTTON)

enum
{
	SIGNAL_TOGGLED,
	SIGNAL_LAST
};

static guint signals[SIGNAL_LAST] = {0};

static void lypad_toggle_button_init (LypadToggleButton *button)
{
    LypadToggleButtonPrivate *priv = lypad_toggle_button_get_instance_private (button);
    GtkStyleContext *context = gtk_widget_get_style_context (GTK_WIDGET (button));
    gtk_style_context_add_class (context, "LypadToggleButton");

    priv->is_active = FALSE;
    priv->base_image = NULL;
    priv->active_image = NULL;
}

gboolean lypad_toggle_button_get_active (LypadToggleButton *button)
{
    LypadToggleButtonPrivate *priv = lypad_toggle_button_get_instance_private (button);
    return priv->is_active;
}

static inline void lypad_toggle_button_update_image (LypadToggleButton *button, LypadToggleButtonPrivate *priv)
{
    if (priv->active_image)
    {
        gtk_container_remove (GTK_CONTAINER (button), gtk_bin_get_child (GTK_BIN (button)));
        gtk_container_add (GTK_CONTAINER (button), priv->is_active ? priv->active_image : priv->base_image);
    }
}

void lypad_toggle_button_set_active (LypadToggleButton *button, gboolean active)
{
    g_return_if_fail (LYPAD_IS_TOGGLE_BUTTON (button));
    LypadToggleButtonPrivate *priv = lypad_toggle_button_get_instance_private (button);
    active = (active != FALSE);
    if (priv->is_active != active)
    {
        if (active)
            gtk_widget_set_state_flags (GTK_WIDGET (button), GTK_STATE_FLAG_CHECKED, FALSE);
        else
            gtk_widget_unset_state_flags (GTK_WIDGET (button), GTK_STATE_FLAG_CHECKED);
        priv->is_active = active;
        lypad_toggle_button_update_image (button, priv);
        g_signal_emit (button, signals[SIGNAL_TOGGLED], 0, active);
    }
}

void lypad_toggle_button_set_images (LypadToggleButton *button, GtkWidget *base_image, GtkWidget *active_image)
{
    g_return_if_fail (LYPAD_IS_TOGGLE_BUTTON (button));
    LypadToggleButtonPrivate *priv = lypad_toggle_button_get_instance_private (button);
    g_return_if_fail (priv->base_image == NULL && priv->active_image == NULL);
    priv->base_image = g_object_ref_sink (base_image);
    priv->active_image = g_object_ref_sink (active_image);
    gtk_widget_show (base_image);
    gtk_widget_show (active_image);
    gtk_container_add (GTK_CONTAINER (button), priv->is_active ? priv->active_image : priv->base_image);
}

static void lypad_toggle_button_destroy (GtkWidget *widget)
{
    LypadToggleButtonPrivate *priv = lypad_toggle_button_get_instance_private (LYPAD_TOGGLE_BUTTON (widget));
    g_clear_object (&priv->base_image);
    g_clear_object (&priv->active_image);
    GTK_WIDGET_CLASS (lypad_toggle_button_parent_class)->destroy (widget);
}

static void lypad_toggle_button_clicked (LypadButton *button)
{
    LypadToggleButton *toggle_button = LYPAD_TOGGLE_BUTTON (button);
    LypadToggleButtonPrivate *priv = lypad_toggle_button_get_instance_private (toggle_button);
    lypad_toggle_button_set_active (toggle_button, !priv->is_active);
    LYPAD_BUTTON_CLASS (lypad_toggle_button_parent_class)->clicked (button);
}

static void lypad_toggle_button_class_init (LypadToggleButtonClass *klass)
{
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
    LypadButtonClass *button_class = LYPAD_BUTTON_CLASS (klass);

    widget_class->destroy = lypad_toggle_button_destroy;
    button_class->clicked = lypad_toggle_button_clicked;

	signals[SIGNAL_TOGGLED] = g_signal_new ("toggled", G_TYPE_FROM_CLASS (klass),
										    G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
										    g_cclosure_marshal_VOID__BOOLEAN, G_TYPE_NONE, 1, G_TYPE_BOOLEAN);
}

GtkWidget *lypad_toggle_button_new ()
{
    return g_object_new (LYPAD_TYPE_TOGGLE_BUTTON, NULL);
}
