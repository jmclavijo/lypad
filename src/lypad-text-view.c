/*
 *  lypad-text-view.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-text-view.h"
#include "lypad-text-buffer.h"
#include "lypad-placeholder-buffer.h"

typedef struct _LypadTextViewPrivate LypadTextViewPrivate;

enum
{
    CONTEXT_MENU_ITEM_UNDO,
    CONTEXT_MENU_ITEM_REDO,
    CONTEXT_MENU_ITEM_CUT,
    CONTEXT_MENU_ITEM_COPY,
    CONTEXT_MENU_ITEM_PASTE,
    N_CONTEXT_MENU_ITEMS
};

struct _LypadTextViewPrivate
{
    GtkTextBuffer *buffer;
    GtkTextBuffer *placeholder_buffer;
    gboolean       placeholder_set;

    GtkMenu       *context_menu;

    char *text_to_insert;
};

G_DEFINE_TYPE_WITH_PRIVATE (LypadTextView, lypad_text_view, GTK_TYPE_TEXT_VIEW)

enum
{
    SIGNAL_UNDO,
    SIGNAL_REDO,
    SIGNAL_LAST
};

static guint signals[SIGNAL_LAST] = {0};
static GQuark menu_items_quark = 0;

#define LYPAD_TEXT_VIEW_SET_BUFFER(text_view, buffer) \
    LYPAD_TEXT_VIEW_GET_CLASS (text_view)->set_buffer (text_view, buffer);

void lypad_text_view_set_placeholder (LypadTextView *text_view, const char *placeholder)
{
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
    lypad_placeholder_buffer_set_text (LYPAD_PLACEHOLDER_BUFFER (priv->placeholder_buffer), placeholder);
}

static void on_buffer_end_user_action (GtkTextBuffer *buffer, gpointer user_data)
{
    LypadTextView *text_view = LYPAD_TEXT_VIEW (user_data);
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
    GtkTextIter start, end;
    gtk_text_buffer_get_bounds (buffer, &start, &end);
    if (gtk_text_iter_equal (&start, &end))
    {
        LYPAD_TEXT_VIEW_SET_BUFFER (text_view, priv->placeholder_buffer);
        priv->placeholder_set = TRUE;
    }
    else if (priv->placeholder_set)
    {
        LYPAD_TEXT_VIEW_SET_BUFFER (text_view, priv->buffer);
        priv->placeholder_set = FALSE;
    }
}

static void on_placeholder_buffer_insert_tex (GtkTextBuffer *placeholder_buffer, GtkTextIter *location, gchar *text, gint len, gpointer user_data)
{
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (LYPAD_TEXT_VIEW (user_data));
    if (!priv->text_to_insert)
    {
        if (len == -1)
            priv->text_to_insert = g_strdup (text);
        else
            priv->text_to_insert = g_strndup (text, len);
    }
    else
        g_warn_if_reached ();
}

static void on_placeholder_buffer_mark_set (GtkTextBuffer *placeholder_buffer, GtkTextIter *location, GtkTextMark *mark, gpointer user_data)
{
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (LYPAD_TEXT_VIEW (user_data));
    if (priv->buffer && mark == gtk_text_buffer_get_insert (placeholder_buffer))
    {
        GtkTextIter start;
        gtk_text_buffer_get_start_iter (priv->buffer, &start);
        gtk_text_buffer_move_mark (priv->buffer, gtk_text_buffer_get_insert (priv->buffer), &start);
    }
}

static void on_placeholder_buffer_end_user_action (GtkTextBuffer *placeholder_buffer, gpointer user_data)
{
    LypadTextView *text_view = LYPAD_TEXT_VIEW (user_data);
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
    if (priv->placeholder_set)
    {
        LYPAD_TEXT_VIEW_SET_BUFFER (text_view, priv->buffer);
        priv->placeholder_set = FALSE;
        if (priv->text_to_insert)
        {
            LYPAD_TEXT_VIEW_GET_CLASS (text_view)->insert_text (text_view, priv->text_to_insert);
            g_free (priv->text_to_insert);
            priv->text_to_insert = NULL;
        }
    }
}

static void lypad_text_view_init (LypadTextView *text_view)
{
    GtkStyleContext *context = gtk_widget_get_style_context (GTK_WIDGET (text_view));
    gtk_style_context_add_class (context, "LypadTextView");

    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
    priv->buffer = NULL;
    priv->placeholder_buffer = lypad_placeholder_buffer_new ();
    priv->placeholder_set = FALSE;

    g_signal_connect_after (priv->placeholder_buffer, "insert-text", G_CALLBACK (on_placeholder_buffer_insert_tex), text_view);
    g_signal_connect_after (priv->placeholder_buffer, "mark_set", G_CALLBACK (on_placeholder_buffer_mark_set), text_view);
    g_signal_connect_after (priv->placeholder_buffer, "end-user-action", G_CALLBACK (on_placeholder_buffer_end_user_action), text_view);

    priv->context_menu = NULL;

    priv->text_to_insert = NULL;
}

void lypad_text_view_release_buffer (LypadTextView *text_view)
{
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
    if (priv->buffer)
    {
        g_signal_handlers_disconnect_by_func (priv->buffer, on_buffer_end_user_action, text_view);
        g_object_unref (priv->buffer);
        priv->buffer = NULL;
    }
    if (LYPAD_TEXT_VIEW_GET_CLASS (text_view)->release_buffer)
        LYPAD_TEXT_VIEW_GET_CLASS (text_view)->release_buffer (text_view);
}

void lypad_text_view_set_buffer (LypadTextView *text_view, GtkTextBuffer *buffer)
{
    g_return_if_fail (LYPAD_IS_TEXT_VIEW (text_view));
    g_return_if_fail (LYPAD_IS_TEXT_BUFFER (buffer));
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
    lypad_text_view_release_buffer (text_view);
    priv->buffer = g_object_ref (buffer);

    GtkTextIter start, end;
    gtk_text_buffer_get_bounds (buffer, &start, &end);
    if (gtk_text_iter_equal (&start, &end))
    {
        LYPAD_TEXT_VIEW_SET_BUFFER (text_view, priv->placeholder_buffer);
        priv->placeholder_set = TRUE;
    }
    else
        LYPAD_TEXT_VIEW_SET_BUFFER (text_view, buffer);
    g_signal_connect_after (buffer, "end-user-action", G_CALLBACK (on_buffer_end_user_action), text_view);
}

GtkTextBuffer *lypad_text_view_get_buffer (LypadTextView *text_view)
{
    return LYPAD_TEXT_VIEW_GET_CLASS (text_view)->get_buffer (text_view);
}

static void on_menu_item_undo_activate (GtkMenuItem *item, gpointer user_data)
{
    LypadTextView *text_view = LYPAD_TEXT_VIEW (user_data);
    if (gtk_text_view_get_editable (GTK_TEXT_VIEW (text_view)))
    {
        LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
        gtk_text_buffer_begin_user_action (priv->buffer);
        lypad_text_buffer_undo (LYPAD_TEXT_BUFFER (priv->buffer));
        gtk_text_buffer_end_user_action (priv->buffer);
    }
}

static void on_menu_item_redo_activate (GtkMenuItem *item, gpointer user_data)
{
    LypadTextView *text_view = LYPAD_TEXT_VIEW (user_data);
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
    if (gtk_text_view_get_editable (GTK_TEXT_VIEW (text_view)))
    {
        gtk_text_buffer_begin_user_action (priv->buffer);
        lypad_text_buffer_redo (LYPAD_TEXT_BUFFER (priv->buffer));
        gtk_text_buffer_end_user_action (priv->buffer);
    }
}

static void on_menu_item_cut_activate (GtkMenuItem *item, gpointer user_data)
{
    LypadTextView *text_view = LYPAD_TEXT_VIEW (user_data);
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
    gtk_text_buffer_cut_clipboard (priv->buffer,
                                   gtk_widget_get_clipboard (GTK_WIDGET (text_view), GDK_SELECTION_CLIPBOARD),
                                   gtk_text_view_get_editable (GTK_TEXT_VIEW (text_view)));
}

static void on_menu_item_copy_activate (GtkMenuItem *item, gpointer user_data)
{
    LypadTextView *text_view = LYPAD_TEXT_VIEW (user_data);
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
    gtk_text_buffer_copy_clipboard (priv->buffer, gtk_widget_get_clipboard (GTK_WIDGET (text_view), GDK_SELECTION_CLIPBOARD));
}

static void on_menu_item_paste_activate (GtkMenuItem *item, gpointer user_data)
{
    LypadTextView *text_view = LYPAD_TEXT_VIEW (user_data);
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
    gtk_text_buffer_paste_clipboard (priv->buffer,
                                     gtk_widget_get_clipboard (GTK_WIDGET (text_view), GDK_SELECTION_CLIPBOARD),
                                     NULL, gtk_text_view_get_editable (GTK_TEXT_VIEW (text_view)));
}

static void lypad_text_view_update_context_menu (LypadTextView *text_view)
{
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
    g_return_if_fail (priv->context_menu != NULL);
    GtkWidget **menu_items = g_object_get_qdata (G_OBJECT (priv->context_menu), menu_items_quark);
    if (menu_items)
    {
        gboolean editable = gtk_text_view_get_editable (GTK_TEXT_VIEW (text_view));

        gtk_widget_set_sensitive (menu_items[CONTEXT_MENU_ITEM_UNDO], editable && lypad_text_buffer_can_undo (LYPAD_TEXT_BUFFER (priv->buffer)));
        gtk_widget_set_sensitive (menu_items[CONTEXT_MENU_ITEM_REDO], editable && lypad_text_buffer_can_redo (LYPAD_TEXT_BUFFER (priv->buffer)));

        gboolean has_selection = gtk_text_buffer_get_selection_bounds (priv->buffer, NULL, NULL);
        gtk_widget_set_sensitive (menu_items[CONTEXT_MENU_ITEM_CUT], editable && has_selection);
        gtk_widget_set_sensitive (menu_items[CONTEXT_MENU_ITEM_COPY], has_selection);

        gboolean can_paste = gtk_clipboard_wait_is_text_available (gtk_widget_get_clipboard (GTK_WIDGET (text_view), GDK_SELECTION_CLIPBOARD));
        gtk_widget_set_sensitive (menu_items[CONTEXT_MENU_ITEM_PASTE], editable && can_paste);
    }
}

GtkWidget *_lypad_text_view_build_context_menu (LypadTextView *text_view)
{
    GtkWidget *menu = gtk_menu_new ();
    GtkWidget *menu_item;
    GtkWidget *child;

    GtkWidget **context_menu_items = g_new (GtkWidget*, N_CONTEXT_MENU_ITEMS);
    g_object_set_qdata_full (G_OBJECT (menu), menu_items_quark, context_menu_items, g_free);

    menu_item = gtk_menu_item_new_with_mnemonic ("_Undo");
    context_menu_items[CONTEXT_MENU_ITEM_UNDO] = menu_item;
    child = gtk_bin_get_child (GTK_BIN (menu_item));
    gtk_accel_label_set_accel (GTK_ACCEL_LABEL (child), GDK_KEY_Z, GDK_CONTROL_MASK);
    g_signal_connect (menu_item, "activate", G_CALLBACK (on_menu_item_undo_activate), text_view);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

    menu_item = gtk_menu_item_new_with_mnemonic ("_Redo");
    context_menu_items[CONTEXT_MENU_ITEM_REDO] = menu_item;
    child = gtk_bin_get_child (GTK_BIN (menu_item));
    gtk_accel_label_set_accel (GTK_ACCEL_LABEL (child), GDK_KEY_Z, GDK_CONTROL_MASK | GDK_SHIFT_MASK);
    g_signal_connect (menu_item, "activate", G_CALLBACK (on_menu_item_redo_activate), text_view);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

    menu_item = gtk_separator_menu_item_new ();
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

    menu_item = gtk_menu_item_new_with_mnemonic ("Cu_t");
    context_menu_items[CONTEXT_MENU_ITEM_CUT] = menu_item;
    child = gtk_bin_get_child (GTK_BIN (menu_item));
    gtk_accel_label_set_accel (GTK_ACCEL_LABEL (child), GDK_KEY_X, GDK_CONTROL_MASK);
    g_signal_connect (menu_item, "activate", G_CALLBACK (on_menu_item_cut_activate), text_view);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

    menu_item = gtk_menu_item_new_with_mnemonic ("_Copy");
    context_menu_items[CONTEXT_MENU_ITEM_COPY] = menu_item;
    child = gtk_bin_get_child (GTK_BIN (menu_item));
    gtk_accel_label_set_accel (GTK_ACCEL_LABEL (child), GDK_KEY_C, GDK_CONTROL_MASK);
    g_signal_connect (menu_item, "activate", G_CALLBACK (on_menu_item_copy_activate), text_view);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

    menu_item = gtk_menu_item_new_with_mnemonic ("_Paste");
    context_menu_items[CONTEXT_MENU_ITEM_PASTE] = menu_item;
    child = gtk_bin_get_child (GTK_BIN (menu_item));
    gtk_accel_label_set_accel (GTK_ACCEL_LABEL (child), GDK_KEY_V, GDK_CONTROL_MASK);
    g_signal_connect (menu_item, "activate", G_CALLBACK (on_menu_item_paste_activate), text_view);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

    gtk_widget_show_all (menu);
    return menu;
}

static gboolean lypad_text_view_popup_menu (GtkWidget *widget)
{
    LypadTextView *text_view = LYPAD_TEXT_VIEW (widget);
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);

    if (G_UNLIKELY (!priv->context_menu))
    {
        priv->context_menu = GTK_MENU (_lypad_text_view_build_context_menu (text_view));
        g_object_ref_sink (priv->context_menu);
        gtk_menu_attach_to_widget (priv->context_menu, widget, NULL);
    }
    lypad_text_view_update_context_menu (LYPAD_TEXT_VIEW (widget));

    GdkEvent *event = gtk_get_current_event ();
    if (event && gdk_event_triggers_context_menu (event))
        gtk_menu_popup_at_pointer (priv->context_menu, event);
    else
        gtk_menu_popup_at_widget (priv->context_menu, widget, GDK_GRAVITY_CENTER, GDK_GRAVITY_CENTER, event);
    return TRUE;
}

void lypad_text_view_set_context_menu (LypadTextView *text_view, GtkMenu *menu)
{
    g_return_if_fail (LYPAD_IS_TEXT_VIEW (text_view));
    g_return_if_fail (menu == NULL || GTK_IS_MENU (menu));

    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
    if (priv->context_menu)
    {
        gtk_widget_destroy (GTK_WIDGET (priv->context_menu));
        g_object_unref (priv->context_menu);
        priv->context_menu = NULL;
    }
    if (menu)
    {
        priv->context_menu = g_object_ref_sink (menu);
        gtk_menu_attach_to_widget (GTK_MENU (menu), GTK_WIDGET (text_view), NULL);
    }
}

void lypad_text_view_notify_bkg_color (LypadTextView *text_view, const GdkRGBA *color)
{
    g_return_if_fail (LYPAD_IS_TEXT_VIEW (text_view));
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
    lypad_placeholder_buffer_notify_bkg_color (LYPAD_PLACEHOLDER_BUFFER (priv->placeholder_buffer), color);
}

static void lypad_text_view_set_buffer_real (LypadTextView *text_view, GtkTextBuffer *buffer)
{
    gtk_text_view_set_buffer (GTK_TEXT_VIEW (text_view), buffer);
}

static GtkTextBuffer *lypad_text_view_get_buffer_real (LypadTextView *text_view)
{
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
    return priv->buffer;
}

static void lypad_text_view_insert_text_real (LypadTextView *text_view, const char *text)
{
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
    gtk_text_buffer_begin_user_action (priv->buffer);
    gtk_text_buffer_insert_at_cursor (priv->buffer, text, -1);
    gtk_text_buffer_end_user_action (priv->buffer);
}

static void lypad_text_view_destroy (GtkWidget *widget)
{
    LypadTextView *text_view = LYPAD_TEXT_VIEW (widget);
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
    lypad_text_view_release_buffer (text_view);
    g_clear_pointer (&priv->text_to_insert, g_free);

    if (priv->placeholder_buffer)
    {
        g_signal_handlers_disconnect_by_func (priv->placeholder_buffer, on_placeholder_buffer_insert_tex, text_view);
        g_signal_handlers_disconnect_by_func (priv->placeholder_buffer, on_placeholder_buffer_mark_set, text_view);
        g_signal_handlers_disconnect_by_func (priv->placeholder_buffer, on_placeholder_buffer_end_user_action, text_view);
        g_object_unref (priv->placeholder_buffer);
        priv->placeholder_buffer = NULL;
    }

    lypad_text_view_set_context_menu (text_view, NULL);

    GTK_WIDGET_CLASS (lypad_text_view_parent_class)->destroy (widget);
}

static gboolean lypad_text_view_button_press (GtkWidget *widget, GdkEventButton *event)
{
    if (gdk_event_triggers_context_menu ((GdkEvent*) event))
    {
        lypad_text_view_popup_menu (widget);
        return TRUE;
    }
    else
        return GTK_WIDGET_CLASS (lypad_text_view_parent_class)->button_press_event (widget, event);
}

static void lypad_text_view_undo (LypadTextView *text_view)
{
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
    gtk_text_buffer_begin_user_action (priv->buffer);
    lypad_text_buffer_undo (LYPAD_TEXT_BUFFER (priv->buffer));
    gtk_text_buffer_end_user_action (priv->buffer);
}

static void lypad_text_view_redo (LypadTextView *text_view)
{
    LypadTextViewPrivate *priv = lypad_text_view_get_instance_private (text_view);
    gtk_text_buffer_begin_user_action (priv->buffer);
    lypad_text_buffer_redo (LYPAD_TEXT_BUFFER (priv->buffer));
    gtk_text_buffer_end_user_action (priv->buffer);
}

static void lypad_text_view_class_init (LypadTextViewClass *klass)
{
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

    widget_class->destroy            = lypad_text_view_destroy;
    widget_class->button_press_event = lypad_text_view_button_press;
    widget_class->popup_menu         = lypad_text_view_popup_menu;
    klass->undo                      = lypad_text_view_undo;
    klass->redo                      = lypad_text_view_redo;
    klass->set_buffer                = lypad_text_view_set_buffer_real;
    klass->release_buffer            = NULL;
    klass->get_buffer                = lypad_text_view_get_buffer_real;
    klass->insert_text               = lypad_text_view_insert_text_real;

    signals[SIGNAL_UNDO] = g_signal_new ("undo", G_TYPE_FROM_CLASS (klass),
		                                 G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
		                                 G_STRUCT_OFFSET (LypadTextViewClass, undo),
                                         NULL, NULL, NULL,
                                         G_TYPE_NONE, 0);

    signals[SIGNAL_REDO] = g_signal_new ("redo", G_TYPE_FROM_CLASS (klass),
		                                 G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
		                                 G_STRUCT_OFFSET (LypadTextViewClass, redo),
                                         NULL, NULL, NULL,
                                         G_TYPE_NONE, 0);

    GtkBindingSet *binding_set = gtk_binding_set_by_class (klass);
    gtk_binding_entry_add_signal (binding_set, GDK_KEY_z, GDK_CONTROL_MASK, "undo", 0);
    gtk_binding_entry_add_signal (binding_set, GDK_KEY_z, GDK_SHIFT_MASK | GDK_CONTROL_MASK, "redo", 0);

    menu_items_quark = g_quark_from_string ("lypad-text-view-menu-items");
}

GtkWidget *lypad_text_view_new ()
{
    return g_object_new (LYPAD_TYPE_TEXT_VIEW, NULL);
}
