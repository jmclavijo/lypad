/*
 *  lypad-button.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-button.h"

typedef struct _LypadButtonPrivate LypadButtonPrivate;

struct _LypadButtonPrivate
{
    GtkGesture *gesture;
};

G_DEFINE_TYPE_WITH_PRIVATE (LypadButton, lypad_button, GTK_TYPE_EVENT_BOX)

enum
{
    SIGNAL_CLICKED,
    SIGNAL_LAST
};

static guint signals[SIGNAL_LAST] = {0};

static void lypad_button_clicked (LypadButton *button)
{
}

static void lypad_button_press (LypadButton *button)
{
    gtk_widget_set_state_flags (GTK_WIDGET (button), GTK_STATE_FLAG_ACTIVE, FALSE);
}

static void lypad_button_release (LypadButton *button, gboolean cancelled)
{
    gtk_widget_unset_state_flags (GTK_WIDGET (button), GTK_STATE_FLAG_ACTIVE);
    if (!cancelled)
        g_signal_emit (button, signals[SIGNAL_CLICKED], 0);
}

static void on_gesture_pressed (GtkGestureMultiPress *gesture, guint n_press, gdouble x, gdouble y, gpointer user_data)
{
    LypadButton *button = LYPAD_BUTTON (user_data);
    if (gtk_gesture_set_state (GTK_GESTURE (gesture), GTK_EVENT_SEQUENCE_CLAIMED))
    {
        if (gtk_widget_get_focus_on_click (GTK_WIDGET (button)) && !gtk_widget_has_focus (GTK_WIDGET (button)))
            gtk_widget_grab_focus (GTK_WIDGET (button));
        lypad_button_press (button);
    }
}

static void on_gesture_released (GtkGestureMultiPress *gesture, guint n_press, gdouble x, gdouble y, gpointer user_data)
{
    LypadButton *button = LYPAD_BUTTON (user_data);
    lypad_button_release (button, FALSE);
}

static void on_gesture_update (GtkGesture *gesture, GdkEventSequence *sequence, gpointer user_data)
{
    LypadButton *button = LYPAD_BUTTON (user_data);
    GtkAllocation allocation;
    gdouble x, y;

    gtk_widget_get_allocation (GTK_WIDGET (button), &allocation);
    gtk_gesture_get_point (gesture, sequence, &x, &y);

    gboolean pointer_in_rect = (x >= 0 && y >= 0 && x < allocation.width && y < allocation.height);
    if (!pointer_in_rect)
    {
        gtk_gesture_set_state (GTK_GESTURE (gesture), GTK_EVENT_SEQUENCE_DENIED);
        lypad_button_release (button, TRUE);
    }
}

static void on_gesture_cancel (GtkGesture *gesture, GdkEventSequence *sequence, gpointer user_data)
{
    LypadButton *button = LYPAD_BUTTON (user_data);
    lypad_button_release (button, TRUE);
}

static void lypad_button_init (LypadButton *button)
{
    LypadButtonPrivate *priv = lypad_button_get_instance_private (button);
    GtkStyleContext *context = gtk_widget_get_style_context (GTK_WIDGET (button));
    gtk_style_context_add_class (context, "LypadButton");

    gtk_widget_add_events (GTK_WIDGET (button), GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_TOUCH_MASK);

    priv->gesture = gtk_gesture_multi_press_new (GTK_WIDGET (button));
    gtk_gesture_single_set_exclusive (GTK_GESTURE_SINGLE (priv->gesture), TRUE);
    g_signal_connect (priv->gesture, "pressed", G_CALLBACK (on_gesture_pressed), button);
    g_signal_connect (priv->gesture, "released", G_CALLBACK (on_gesture_released), button);
    g_signal_connect (priv->gesture, "update", G_CALLBACK (on_gesture_update), button);
    g_signal_connect (priv->gesture, "cancel", G_CALLBACK (on_gesture_cancel), button);
}

static void lypad_button_finalize (GObject *object)
{
    LypadButtonPrivate *priv = lypad_button_get_instance_private (LYPAD_BUTTON (object));
    g_clear_object (&priv->gesture);
    G_OBJECT_CLASS (lypad_button_parent_class)->finalize (object);
}

static gboolean lypad_button_enter_notify (GtkWidget *widget, GdkEventCrossing *event)
{
    gtk_widget_set_state_flags (widget, GTK_STATE_FLAG_PRELIGHT, FALSE);
    return FALSE;
}

static gboolean lypad_button_leave_notify (GtkWidget *widget, GdkEventCrossing *event)
{
    gtk_widget_unset_state_flags (widget, GTK_STATE_FLAG_PRELIGHT);
    return FALSE;
}

static void lypad_button_class_init (LypadButtonClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

    object_class->finalize = lypad_button_finalize;
    widget_class->enter_notify_event = lypad_button_enter_notify;
    widget_class->leave_notify_event = lypad_button_leave_notify;
    klass->clicked = lypad_button_clicked;

    signals[SIGNAL_CLICKED] = g_signal_new ("clicked", G_TYPE_FROM_CLASS (klass),
                                            G_SIGNAL_RUN_FIRST,
		                                    G_STRUCT_OFFSET (LypadButtonClass, clicked),
                                            NULL, NULL,
                                            g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
}

GtkWidget *lypad_button_new ()
{
    return g_object_new (LYPAD_TYPE_BUTTON, NULL);
}
