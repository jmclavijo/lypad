/*
 *  lypad-notes-list.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-notes-list.h"
#include "lypad-note.h"
#include "lypad-note-widget.h"
#include "lypad-selection-widget.h"
#include "lypad-main-window.h"
#include "lypad-utils.h"
#include "lypad-export.h"

typedef struct _RowData RowData;

enum
{
    NOTE_MENU_ITEM_EDIT,
    NOTE_MENU_ITEM_ADD_TAG,
    NOTE_MENU_ITEM_DELETE,
    NOTE_MENU_ITEM_EXPORT,
    NOTE_MENU_ITEM_SELECT_ALL,
    NOTE_MENU_ITEM_CLEAR_SELECTION,
    N_NOTE_MENU_ITEMS
};

struct _LypadNotesList
{
    GtkListBox             parent;
    //GSequence             *children;
    LypadSortMode          sort_mode;
    gboolean               sort_ascending;

    LypadAnimationHandler *animation_handler;
    RowData               *selected_row_data;
    int                    n_selected_rows;
    GtkWidget             *select_toggle_button;
    gulong                 select_toggle_handler;

    GtkMenu               *note_menu;
    GtkWidget             *note_menu_item[N_NOTE_MENU_ITEMS];
    gboolean               menu_accels_set;

    GSettings             *settings;
};

G_DEFINE_TYPE (LypadNotesList, lypad_notes_list, GTK_TYPE_LIST_BOX)

static GQuark row_data_quark = 0;

struct _RowData
{
    LypadNotesList  *parent;
    GtkListBoxRow   *row;
    LypadNote       *note;
    GtkWidget       *selection_widget;
    gulong           clicked_handler;
    gulong           date_changed_handler;
    gulong           title_changed_handler;
    gulong           note_destroy_handler;
    gboolean         selected;
};

static void lypad_notes_list_update_menu_items (LypadNotesList *list)
{
    if (list->n_selected_rows == 0)
    {
        gtk_widget_set_sensitive (list->note_menu_item[NOTE_MENU_ITEM_EDIT], FALSE);
        gtk_widget_set_sensitive (list->note_menu_item[NOTE_MENU_ITEM_ADD_TAG], FALSE);
        gtk_widget_set_sensitive (list->note_menu_item[NOTE_MENU_ITEM_DELETE], FALSE);
        gtk_widget_set_sensitive (list->note_menu_item[NOTE_MENU_ITEM_EXPORT], FALSE);
        gtk_widget_set_sensitive (list->note_menu_item[NOTE_MENU_ITEM_CLEAR_SELECTION], FALSE);
    }
    else
    {
        gtk_widget_set_sensitive (list->note_menu_item[NOTE_MENU_ITEM_EDIT], TRUE);
        gtk_widget_set_sensitive (list->note_menu_item[NOTE_MENU_ITEM_ADD_TAG], TRUE);
        gtk_widget_set_sensitive (list->note_menu_item[NOTE_MENU_ITEM_DELETE], TRUE);
        gtk_widget_set_sensitive (list->note_menu_item[NOTE_MENU_ITEM_EXPORT], TRUE);

        if (list->selected_row_data)
            gtk_widget_set_sensitive (list->note_menu_item[NOTE_MENU_ITEM_CLEAR_SELECTION], FALSE);
        else
            gtk_widget_set_sensitive (list->note_menu_item[NOTE_MENU_ITEM_CLEAR_SELECTION], TRUE);
    }
}

static void lypad_notes_list_row_release_selected (RowData *data)
{
    LypadNotesList *list = data->parent;
    if (list->selected_row_data == data)
    {
        list->selected_row_data = NULL;
        g_return_if_fail (data->selected);
        gtk_widget_unset_state_flags (lypad_selection_widget_get_child (LYPAD_SELECTION_WIDGET (data->selection_widget)), GTK_STATE_FLAG_CHECKED);
        data->selected = FALSE;
        list->n_selected_rows -= 1;
        lypad_notes_list_update_menu_items (list);
    }
}

static void lypad_notes_list_row_data_free (gpointer user_data)
{
    RowData *data = user_data;
    lypad_notes_list_row_release_selected (data);
    if (data->selected)
    {
        data->parent->n_selected_rows -= 1;
        data->selected = FALSE;
        lypad_notes_list_update_menu_items (data->parent);
    }

    g_signal_handler_disconnect (data->note, data->date_changed_handler);
    g_signal_handler_disconnect (data->note, data->note_destroy_handler);

    GtkTextBuffer *title_buffer = lypad_note_get_title_buffer (data->note);
    g_signal_handler_disconnect (title_buffer, data->title_changed_handler);
    g_object_unref (data->note);
    g_free (data);
}


static int lypad_notes_list_sort_function (GtkListBoxRow *row1, GtkListBoxRow *row2, gpointer user_data)
{
    LypadNotesList *list = LYPAD_NOTES_LIST (user_data);
    const RowData *data1 = g_object_get_qdata (G_OBJECT (row1), row_data_quark);
    const RowData *data2 = g_object_get_qdata (G_OBJECT (row2), row_data_quark);
    g_return_val_if_fail (data1 != NULL && data2 != NULL, 0);

    switch (list->sort_mode)
    {
        case LYPAD_SORT_MODIFIED:
        {
            time_t date1 = lypad_note_get_modification_date (data1->note);
            time_t date2 = lypad_note_get_modification_date (data2->note);
            if (date1 < date2)
            {
                if (list->sort_ascending)
                    return -1;
                else
                    return 1;
            }
            else if (date2 < date1)
            {
                if (list->sort_ascending)
                    return 1;
                else
                    return -1;
            }
            else
                return 0;
        }

        case LYPAD_SORT_CREATED:
        {
            time_t date1 = lypad_note_get_creation_date (data1->note);
            time_t date2 = lypad_note_get_creation_date (data2->note);
            if (date1 < date2)
            {
                if (list->sort_ascending)
                    return -1;
                else
                    return 1;
            }
            else if (date2 < date1)
            {
                if (list->sort_ascending)
                    return 1;
                else
                    return -1;
            }
            else
                return 0;
        }

        case LYPAD_SORT_TITLE:
        {
            GtkTextBuffer *buffer1 = lypad_note_get_title_buffer (data1->note);
            GtkTextBuffer *buffer2 = lypad_note_get_title_buffer (data2->note);
            GtkTextIter start, end;
            gtk_text_buffer_get_bounds (buffer1, &start, &end);
            char *title1 = gtk_text_buffer_get_text (buffer1, &start, &end, FALSE);
            gtk_text_buffer_get_bounds (buffer2, &start, &end);
            char *title2 = gtk_text_buffer_get_text (buffer2, &start, &end, FALSE);
            char *title1_folded = g_utf8_casefold (title1, -1);
            char *title2_folded = g_utf8_casefold (title2, -1);
            int cmp = g_utf8_collate (title1_folded, title2_folded);

            g_free (title1);
            g_free (title2);
            g_free (title1_folded);
            g_free (title2_folded);

            if (cmp == 0)
            {
                time_t date1 = lypad_note_get_modification_date (data1->note);
                time_t date2 = lypad_note_get_modification_date (data2->note);
                if (date1 < date2)
                    return 1;
                else if (date2 < date1)
                    return -1;
                else
                    return 0;
            }

            if (list->sort_ascending)
                return cmp;
            else
                return -cmp;
        }

        default:
            g_warn_if_reached ();
            return 0;
    }
}

static void get_list_children_callback (GtkWidget *widget, gpointer user_data)
{
    GSList **children = user_data;
    *children = g_slist_prepend (*children, gtk_bin_get_child (GTK_BIN (widget)));
}

static void lypad_notes_list_set_selectable (LypadNotesList *list, gboolean selectable)
{
    GSList *child_widgets = NULL;
    gtk_container_foreach (GTK_CONTAINER (list), get_list_children_callback, &child_widgets);
    lypad_selection_widget_set_selectable (child_widgets, selectable, list->animation_handler);
    g_slist_free (child_widgets);
    if (list->select_toggle_button)
    {
        g_signal_handler_block (list->select_toggle_button, list->select_toggle_handler);
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (list->select_toggle_button), selectable);
        g_signal_handler_unblock (list->select_toggle_button, list->select_toggle_handler);
    }
}

static void on_note_clicked (LypadButton *button, gpointer user_data)
{
    RowData *data = user_data;
    LypadNotesList *list = data->parent;
    GdkModifierType state = 0;
    if (gtk_get_current_event_state (&state))
        if (state & (GDK_CONTROL_MASK | GDK_SHIFT_MASK))
        {
            lypad_notes_list_set_selectable (list, TRUE);
            lypad_selection_widget_set_selected (LYPAD_SELECTION_WIDGET (data->selection_widget), TRUE, list->animation_handler);
            return;
        }

    GSList *notes = g_slist_prepend (NULL, data->note);
    lypad_main_window_edit_notes (LYPAD_MAIN_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (list))), notes);
    g_slist_free (notes);
}

static void on_note_date_changed (LypadNote *note, gpointer user_data)
{
    RowData *data = user_data;
    gtk_list_box_row_changed (data->row);
}

static void on_note_title_changed (GtkTextBuffer *buffer, gpointer user_data)
{
    RowData *data = user_data;
    LypadNotesList *list = LYPAD_NOTES_LIST (data->parent);
    if (list->sort_mode == LYPAD_SORT_TITLE)
        gtk_list_box_row_changed (data->row);
}

static gboolean on_note_widget_button_press_event (GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    if (gdk_event_triggers_context_menu ((GdkEvent *) event))
    {
        RowData *data = user_data;
        if (!lypad_selection_widget_get_selectable (LYPAD_SELECTION_WIDGET (data->selection_widget)))
        {
            LypadNotesList *list = data->parent;
            list->selected_row_data = data;
            if (!data->selected)
            {
                data->selected = TRUE;
                list->n_selected_rows += 1;
                lypad_notes_list_update_menu_items (list);
            }
            gtk_widget_set_state_flags (widget, GTK_STATE_FLAG_CHECKED, FALSE);
            gtk_menu_popup_at_pointer (list->note_menu, (GdkEvent*) event);
            return TRUE;
        }
    }
    return FALSE;
}

static void on_note_destroy (LypadNote *note, gpointer user_data)
{
    RowData *data = user_data;
    GtkWidget *row = GTK_WIDGET (data->row);
    gtk_widget_destroy (row);
}

static gboolean on_button_press_event (GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    if (gdk_event_triggers_context_menu ((GdkEvent *) event))
    {
        LypadNotesList *list = LYPAD_NOTES_LIST(widget);
        gtk_menu_popup_at_pointer (list->note_menu, (GdkEvent*) event);
        return TRUE;
    }
    return FALSE;
}

static void on_selection_widget_toggled (LypadSelectionWidget *widget, gboolean active, gpointer user_data)
{
    RowData *data = user_data;
    if (active != data->selected)
    {
        LypadNotesList *list = data->parent;
        data->selected = active;
        if (active)
        {
            list->n_selected_rows += 1;
            if (list->n_selected_rows == 1)
                lypad_notes_list_update_menu_items (list);
        }
        else
        {
            list->n_selected_rows -= 1;
            if (list->n_selected_rows == 0)
                lypad_notes_list_update_menu_items (list);
        }
    }
}

static void on_note_menu_selection_done (GtkMenuShell *menu, gpointer user_data)
{
    LypadNotesList *list = LYPAD_NOTES_LIST (user_data);
    if (list->selected_row_data)
        lypad_notes_list_row_release_selected (list->selected_row_data);
}

void lypad_notes_list_add (LypadNotesList *list, GtkWidget *note_widget)
{
    RowData *data = g_new (RowData, 1);
    data->parent = list;
    data->selection_widget = lypad_selection_widget_new ();
    data->row = GTK_LIST_BOX_ROW (gtk_list_box_row_new ());
    data->note = g_object_ref (lypad_note_widget_get_note (LYPAD_NOTE_WIDGET (note_widget)));
    g_object_set_qdata_full (G_OBJECT (data->row), row_data_quark, data, lypad_notes_list_row_data_free);
    lypad_selection_widget_add_child (LYPAD_SELECTION_WIDGET (data->selection_widget), note_widget);
    gtk_container_add (GTK_CONTAINER (data->row), data->selection_widget);
    gtk_container_add (GTK_CONTAINER (list), GTK_WIDGET (data->row));
    gtk_widget_show_all (GTK_WIDGET (data->row));

    g_signal_connect (note_widget, "clicked", G_CALLBACK (on_note_clicked), data);
    g_signal_connect (note_widget, "button-press-event", G_CALLBACK (on_note_widget_button_press_event), data);
    g_signal_connect (data->selection_widget, "toggled", G_CALLBACK (on_selection_widget_toggled), data);
    data->selected = FALSE;

    data->date_changed_handler = g_signal_connect (data->note, "date-changed", G_CALLBACK (on_note_date_changed), data);
    GtkTextBuffer *title_buffer = lypad_note_get_title_buffer (data->note);
    data->title_changed_handler = g_signal_connect (title_buffer, "changed", G_CALLBACK (on_note_title_changed), data);
    data->note_destroy_handler = g_signal_connect (data->note, "destroy", G_CALLBACK (on_note_destroy), data);
}

void lypad_notes_list_remove_by_note (LypadNotesList *list, LypadNote *note)
{
    GList *children = gtk_container_get_children (GTK_CONTAINER (list));
    for (GList *l = children; l != NULL; l = l->next)
    {
        RowData *data = g_object_get_qdata (G_OBJECT (l->data), row_data_quark);
        if (data && data->note == note)
        {
            gtk_widget_destroy (l->data);
            break;
        }
    }
    g_list_free (children);
}

static void on_select_toggle_button_toggled (GtkToggleButton *button, gpointer user_data)
{
    LypadNotesList *list = LYPAD_NOTES_LIST (user_data);
    gboolean active = gtk_toggle_button_get_active (button);
    lypad_notes_list_set_selectable (list, active);
}

void lypad_notes_list_set_toggle (LypadNotesList *list, GtkWidget *toggle_button)
{
    if (list->select_toggle_button)
    {
        lypad_notes_list_set_selectable (list, FALSE);
        g_signal_handler_disconnect (list->select_toggle_button, list->select_toggle_handler);
        g_object_unref (list->select_toggle_button);
        list->select_toggle_handler = 0;
        list->select_toggle_button = NULL;
    }

    if (toggle_button)
    {
        list->select_toggle_button = g_object_ref (toggle_button);
        list->select_toggle_handler = g_signal_connect (toggle_button, "toggled", G_CALLBACK (on_select_toggle_button_toggled), list);
    }
}

static void get_selected_notes_callback (GtkWidget *widget, gpointer user_data)
{
    GSList **selected_notes = user_data;
    const RowData *data = g_object_get_qdata (G_OBJECT (widget), row_data_quark);
    g_return_if_fail (data != NULL);

    if (lypad_selection_widget_get_selected (LYPAD_SELECTION_WIDGET (data->selection_widget)))
        *selected_notes = g_slist_prepend (*selected_notes, data->note);
}

static GSList *lypad_notes_list_get_selected_notes (LypadNotesList *list)
{
    GSList *selected = NULL;
    if (list->selected_row_data)
        selected = g_slist_prepend (selected, list->selected_row_data->note);
    else
        gtk_container_foreach (GTK_CONTAINER (list), get_selected_notes_callback, &selected);
    selected = g_slist_reverse (selected);
    return selected;
}

static void on_edit_activated (GtkMenuItem *menu_item, gpointer user_data)
{
    LypadNotesList *list = LYPAD_NOTES_LIST (user_data);
    GSList *selected = lypad_notes_list_get_selected_notes (list);
    lypad_main_window_edit_notes (LYPAD_MAIN_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (list))), selected);
    g_slist_free (selected);
}

static void on_add_tag_activated (GtkMenuItem *menu_item, gpointer user_data)
{
    LypadNotesList *list = LYPAD_NOTES_LIST (user_data);
    GSList *selected = lypad_notes_list_get_selected_notes (list);
    lypad_utils_prompt_add_tags (selected, GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (list))));
    g_slist_free (selected);
}

static void on_delete_activated (GtkMenuItem *menu_item, gpointer user_data)
{
    LypadNotesList *list = LYPAD_NOTES_LIST (user_data);
    GSList *selected = lypad_notes_list_get_selected_notes (list);
    lypad_utils_prompt_delete_notes (selected, GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (list))));
    g_slist_free (selected);
}

static void on_export_activated (GtkMenuItem *menu_item, gpointer user_data)
{
    LypadNotesList *list = LYPAD_NOTES_LIST (user_data);
    GSList *selected = lypad_notes_list_get_selected_notes (list);
    lypad_export_dialog_launch (GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (list))), selected);
    g_slist_free (selected);
}

static void on_select_all_activated (GtkMenuItem *menu_item, gpointer user_data)
{
    LypadNotesList *list = LYPAD_NOTES_LIST (user_data);
    GSList *child_widgets = NULL;
    gtk_container_foreach (GTK_CONTAINER (list), get_list_children_callback, &child_widgets);
    if (child_widgets)
    {
        if (!lypad_selection_widget_get_selectable (LYPAD_SELECTION_WIDGET (child_widgets->data)))
            lypad_notes_list_set_selectable (list, TRUE);

        for (GSList *l = child_widgets; l != NULL; l = l->next)
            lypad_selection_widget_set_selected (LYPAD_SELECTION_WIDGET (l->data), TRUE, list->animation_handler);
    }
    g_slist_free (child_widgets);
}

static void on_clear_selection_activated (GtkMenuItem *menu_item, gpointer user_data)
{
    LypadNotesList *list = LYPAD_NOTES_LIST (user_data);
    GSList *child_widgets = NULL;
    gtk_container_foreach (GTK_CONTAINER (list), get_list_children_callback, &child_widgets);
    for (GSList *l = child_widgets; l != NULL; l = l->next)
        lypad_selection_widget_set_selected (LYPAD_SELECTION_WIDGET (l->data), FALSE, list->animation_handler);
    g_slist_free (child_widgets);
}

static GtkMenu *lypad_notes_list_build_note_menu (LypadNotesList *list)
{
    GtkWidget *menu = gtk_menu_new ();
    GtkWidget *menu_item;
    GtkWidget *child;

    menu_item = gtk_menu_item_new_with_mnemonic ("_New note");
    gtk_actionable_set_action_name (GTK_ACTIONABLE (menu_item), "win.new-note");
    child = gtk_bin_get_child (GTK_BIN (menu_item));
    gtk_accel_label_set_accel (GTK_ACCEL_LABEL (child), GDK_KEY_N, GDK_CONTROL_MASK);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

    menu_item = gtk_separator_menu_item_new ();
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

    menu_item = gtk_menu_item_new_with_mnemonic ("_Edit");
    list->note_menu_item[NOTE_MENU_ITEM_EDIT] = menu_item;
    g_signal_connect (menu_item, "activate", G_CALLBACK (on_edit_activated), list);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

    menu_item = gtk_menu_item_new_with_mnemonic ("Add _tag");
    list->note_menu_item[NOTE_MENU_ITEM_ADD_TAG] = menu_item;
    g_signal_connect (menu_item, "activate", G_CALLBACK (on_add_tag_activated), list);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);
    gtk_widget_show_all (menu);

    menu_item = gtk_menu_item_new_with_mnemonic ("_Delete");
    list->note_menu_item[NOTE_MENU_ITEM_DELETE] = menu_item;
    g_signal_connect (menu_item, "activate", G_CALLBACK (on_delete_activated), list);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);
    gtk_widget_show_all (menu);

    menu_item = gtk_menu_item_new_with_mnemonic ("E_xport");
    list->note_menu_item[NOTE_MENU_ITEM_EXPORT] = menu_item;
    g_signal_connect (menu_item, "activate", G_CALLBACK (on_export_activated), list);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);
    gtk_widget_show_all (menu);

    menu_item = gtk_separator_menu_item_new ();
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

    menu_item = gtk_menu_item_new_with_mnemonic ("Select _all");
    list->note_menu_item[NOTE_MENU_ITEM_SELECT_ALL] = menu_item;
    g_signal_connect (menu_item, "activate", G_CALLBACK (on_select_all_activated), list);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);
    gtk_widget_show_all (menu);

    menu_item = gtk_menu_item_new_with_mnemonic ("Clea_r selection");
    list->note_menu_item[NOTE_MENU_ITEM_CLEAR_SELECTION] = menu_item;
    g_signal_connect (menu_item, "activate", G_CALLBACK (on_clear_selection_activated), list);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);
    gtk_widget_show_all (menu);

    lypad_notes_list_update_menu_items (list);
    return GTK_MENU (menu);
}

static void on_sort_by_changed (GSettings *settings, char *key, gpointer user_data)
{
    LypadNotesList *list = LYPAD_NOTES_LIST (user_data);
    list->sort_mode = g_settings_get_enum (settings, "sort-by");
    gtk_list_box_invalidate_sort (GTK_LIST_BOX (list));
}

static void on_sort_ascending_changed (GSettings *settings, char *key, gpointer user_data)
{
    LypadNotesList *list = LYPAD_NOTES_LIST (user_data);
    list->sort_ascending = g_settings_get_boolean (list->settings, "sort-ascending");
    gtk_list_box_invalidate_sort (GTK_LIST_BOX (list));
}

static void lypad_notes_list_init (LypadNotesList *list)
{
    list->settings = g_settings_new ("org.gnome.lypad");
    list->sort_mode = g_settings_get_enum (list->settings, "sort-by");
    list->sort_ascending = g_settings_get_boolean (list->settings, "sort-ascending");

    g_signal_connect (list->settings, "changed::sort-by", G_CALLBACK (on_sort_by_changed), list);
    g_signal_connect (list->settings, "changed::sort-ascending", G_CALLBACK (on_sort_ascending_changed), list);

    list->menu_accels_set = FALSE;
    list->note_menu = lypad_notes_list_build_note_menu (list);
    g_object_ref_sink (list->note_menu);
    gtk_menu_attach_to_widget (list->note_menu, GTK_WIDGET (list) , NULL);

    g_signal_connect (list->note_menu, "selection-done", G_CALLBACK (on_note_menu_selection_done), list);
    g_signal_connect (list, "button-press-event", G_CALLBACK (on_button_press_event), NULL);

    list->selected_row_data = NULL;
    list->animation_handler = lypad_animation_handler_new (GTK_WIDGET (list));
    list->n_selected_rows = 0;

    list->select_toggle_handler = 0;
    list->select_toggle_button = NULL;

    gtk_style_context_add_class (gtk_widget_get_style_context (GTK_WIDGET (list)), "LypadNotesList");
    gtk_list_box_set_sort_func (GTK_LIST_BOX (list), lypad_notes_list_sort_function, list, NULL);
}

static void lypad_notes_list_dispose (GObject *object)
{
    LypadNotesList *list = LYPAD_NOTES_LIST (object);
    g_clear_object (&list->settings);
    lypad_notes_list_set_toggle (list, NULL);
    g_clear_pointer (&list->animation_handler, lypad_animation_handler_free);
    if (list->note_menu)
    {
        gtk_widget_destroy (GTK_WIDGET (list->note_menu));
        g_object_unref (list->note_menu);
        list->note_menu = NULL;
    }
    G_OBJECT_CLASS (lypad_notes_list_parent_class)->dispose (object);
}

static void lypad_notes_list_map (GtkWidget *widget)
{
    LypadNotesList *list = LYPAD_NOTES_LIST (widget);
    if (!list->menu_accels_set)
    {
        GtkAccelGroup *accel_group = lypad_main_window_get_accel_group (LYPAD_MAIN_WINDOW (gtk_widget_get_toplevel (widget)));
        gtk_widget_add_accelerator (list->note_menu_item[NOTE_MENU_ITEM_SELECT_ALL], "activate",
                                    accel_group, GDK_KEY_A, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
        gtk_widget_add_accelerator (list->note_menu_item[NOTE_MENU_ITEM_CLEAR_SELECTION], "activate",
                                    accel_group, GDK_KEY_A, GDK_SHIFT_MASK | GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
        gtk_widget_add_accelerator (list->note_menu_item[NOTE_MENU_ITEM_DELETE], "activate",
                                    accel_group, GDK_KEY_Delete, 0, GTK_ACCEL_VISIBLE);
        list->menu_accels_set = TRUE;
    }
    GTK_WIDGET_CLASS (lypad_notes_list_parent_class)->map (widget);
}

static void lypad_notes_list_class_init (LypadNotesListClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

    object_class->dispose   = lypad_notes_list_dispose;
    widget_class->map       = lypad_notes_list_map;

    row_data_quark = g_quark_from_string ("lypad-notes-list-row-data");
}

GtkWidget *lypad_notes_list_new ()
{
    return g_object_new (LYPAD_TYPE_NOTES_LIST, "selection-mode", GTK_SELECTION_NONE, NULL);
}
