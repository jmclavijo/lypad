/*
 *  lypad-note-editor.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-note-editor.h"
#include "lypad-toolbar.h"
#include "lypad-text-view.h"
#include "lypad-title-view.h"
#include "lypad-utils.h"
#include "lypad-tag-widget.h"
#include "lypad-tags-box.h"
#include "lypad-application.h"
#include "lypad-export.h"
#include "color-selection/lypad-color-utils.h"

typedef struct _PanelRowData PanelRowData;

struct _LypadNoteEditor
{
    LypadPage        parent;
    LypadTextView   *title_view;
    LypadTextView   *content_view;
    LypadToolbar    *toolbar;
    GtkColorChooser *color_chooser;
    GtkCssProvider  *color_provider;
    GtkLabel        *date_created_label;
    GtkBox          *header_box;
    GtkWidget       *tags_box;
    LypadPageRow    *active_row;
    guint            timestep;

    GtkWidget       *menu;
    GtkWidget       *pin_button;
};

G_DEFINE_TYPE (LypadNoteEditor, lypad_note_editor, LYPAD_TYPE_PAGE)

//TODO: Use a custom palette for the color chooser dialog. Use two tones for the same colors: palid and vivid.
// All the default colors should have some amount of transparency (alpha ~ 0.9)
// Add also a set of dark transparent colors. Combine it with white font color.
//TODO: Use acelerators for setting toggle tags, e.g. CTRL + B for bold, CTRL + I for italic, etc.

//TODO: Check cleanup and memory release

static void lypad_note_editor_set_color (LypadNoteEditor *editor, const GdkRGBA *color)
{
    GdkRGBA fg_color;
    lypad_color_utils_get_text_color_for_background (color, &fg_color);

    int color_r = (int) (0.5 + 255. * color->red);
    int color_g = (int) (0.5 + 255. * color->green);
    int color_b = (int) (0.5 + 255. * color->blue);

    int fg_r = (int) (0.5 + 255. * fg_color.red);
    int fg_g = (int) (0.5 + 255. * fg_color.green);
    int fg_b = (int) (0.5 + 255. * fg_color.blue);

    char *style_data = lypad_utils_strdup_printf (
        "grid {\n"
        "    background-color: rgba(%d,%d,%d,%.3f);\n"
        "}\n"
        ".LypadDateLabel {\n"
        "    color: rgba(%d, %d, %d, 0.7);\n"
        "}\n"
        ".LypadTextView {\n"
        "    caret-color: rgb(%d, %d, %d);\n"
        "}\n",
        color_r, color_g, color_b, color->alpha,
        fg_r, fg_g, fg_b,
        fg_r, fg_g, fg_b
    );
    gtk_css_provider_load_from_data (editor->color_provider, style_data, -1, NULL);
}

struct _PanelRowData
{
    LypadNoteEditor *parent;
    LypadNote       *note;
    GtkWidget       *label;
    gulong           note_color_changed_handler;
    gulong           note_title_changed_handler;
    gulong           note_tags_changed_handler;
    gulong           note_destroy_handler;
    gulong           note_pin_toggled_handler;
    guint            timestamp;
};

static inline guint lypad_note_editor_get_timestamp (LypadNoteEditor *editor)
{
    return ++editor->timestep;
}

static void panel_row_data_free (gpointer row_data)
{
    PanelRowData *data = row_data;
    GtkTextBuffer *title_buffer = lypad_note_get_title_buffer (data->note);
    g_signal_handler_disconnect (title_buffer, data->note_title_changed_handler);
    g_signal_handler_disconnect (data->note, data->note_color_changed_handler);
    g_signal_handler_disconnect (data->note, data->note_tags_changed_handler);
    g_signal_handler_disconnect (data->note, data->note_destroy_handler);
    g_signal_handler_disconnect (data->note, data->note_pin_toggled_handler);
    g_object_unref (data->label);
    lypad_note_save (data->note);
    g_object_unref (data->note);
    g_free (data);
}

static void on_panel_row_close_clicked (GtkButton *button, gpointer user_data)
{
    LypadPageRow *row = user_data;
    PanelRowData *data = lypad_page_row_get_data (row);
    lypad_page_remove_panel_row (LYPAD_PAGE (data->parent), row);
}

static void on_remove_tag_activated (LypadTagsBox *box, LypadTag *tag, gpointer user_data)
{
    LypadNoteEditor *editor = LYPAD_NOTE_EDITOR (user_data);
    if (editor->active_row)
    {
        PanelRowData *data = lypad_page_row_get_data (editor->active_row);
        lypad_note_remove_tag (data->note, tag);
    }
}

static void on_note_color_changed (LypadNote *note, gpointer user_data)
{
    LypadPageRow *row = user_data;
    PanelRowData *data = lypad_page_row_get_data (row);
    if (data->parent->active_row == row)
    {
        const GdkRGBA *color = lypad_note_get_color (note);
        lypad_note_editor_set_color (data->parent, color);
        gtk_color_chooser_set_rgba (data->parent->color_chooser, color);
        lypad_toolbar_notify_bkg_color (data->parent->toolbar, color);
        lypad_text_view_notify_bkg_color (data->parent->title_view, color);
        lypad_text_view_notify_bkg_color (data->parent->content_view, color);
    }
}

static void on_note_tags_changed (LypadNote *note, gpointer user_data)
{
    LypadPageRow *row = user_data;
    PanelRowData *data = lypad_page_row_get_data (row);
    if (data->parent->active_row == row)
    {
        lypad_tags_box_clear (LYPAD_TAGS_BOX (data->parent->tags_box));

        GList *tags = lypad_note_get_tags (note);
        for (GList *l = tags; l != NULL; l = l->next)
            lypad_tags_box_add_tag (LYPAD_TAGS_BOX (data->parent->tags_box), l->data);
    }
}

static void on_note_pin_toggled (LypadNote *note, gboolean active, gpointer user_data)
{
    LypadPageRow *row = user_data;
    PanelRowData *data = lypad_page_row_get_data (row);
    if (data->parent->active_row == row)
        g_object_set (data->parent->pin_button, "active", active, NULL);
}

static void on_note_title_changed (GtkTextBuffer *buffer, gpointer user_data)
{
    LypadPageRow *row = user_data;
    PanelRowData *data = lypad_page_row_get_data (row);
    GtkTextIter start, end;
    gtk_text_buffer_get_bounds (buffer, &start, &end);
    gchar *text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
    g_strstrip (text);
    if (strlen (text) == 0)
        gtk_label_set_label (GTK_LABEL (data->label), "(untitled)");
    else
        gtk_label_set_label (GTK_LABEL (data->label), text);
    g_free (text);
}

static void on_note_destroy (LypadNote *note, gpointer user_data)
{
    LypadPageRow *row = user_data;
    PanelRowData *data = lypad_page_row_get_data (row);
    lypad_page_remove_panel_row (LYPAD_PAGE (data->parent), row);
}

static void lypad_note_editor_init_row (LypadPage *page, LypadPageRow *row, gpointer user_data)
{
    LypadNoteEditor *editor = LYPAD_NOTE_EDITOR (page);
    LypadNote *note = LYPAD_NOTE (user_data);
    PanelRowData *data = g_new (PanelRowData, 1);

    lypad_page_row_set_data (row, data, panel_row_data_free);
    data->parent = editor;
    data->note = g_object_ref (note);
    data->note_color_changed_handler = g_signal_connect (note, "color-changed", G_CALLBACK (on_note_color_changed), row);
    data->note_tags_changed_handler = g_signal_connect (note, "tags-changed", G_CALLBACK (on_note_tags_changed), row);
    data->note_destroy_handler = g_signal_connect (note, "destroy", G_CALLBACK (on_note_destroy), row);
    data->note_pin_toggled_handler = g_signal_connect (note, "pin-toggled", G_CALLBACK (on_note_pin_toggled), row);

    /* Set text */
    GtkTextIter start, end;
    GtkTextBuffer *buffer = lypad_note_get_title_buffer (note);
    gtk_text_buffer_get_bounds (buffer, &start, &end);
    gchar *text = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
    g_strstrip (text);
    if (strlen (text) == 0)
        data->label = gtk_label_new ("(untitled)");
    else
        data->label = gtk_label_new (text);
    gtk_label_set_xalign (GTK_LABEL (data->label), 0);
    g_object_ref_sink (data->label);
    g_free (text);
    /************/

    data->note_title_changed_handler = g_signal_connect (buffer, "changed", G_CALLBACK (on_note_title_changed), row);

    GtkWidget *scrolled_window = gtk_scrolled_window_new (NULL, NULL);
    gtk_container_add (GTK_CONTAINER (scrolled_window), data->label);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window), GTK_POLICY_EXTERNAL, GTK_POLICY_NEVER);

    GtkWidget *box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 4);
    gtk_box_pack_start (GTK_BOX (box), scrolled_window, TRUE, TRUE, 0);

    GtkWidget *close_button = gtk_button_new_from_icon_name ("window-close-symbolic", GTK_ICON_SIZE_SMALL_TOOLBAR);
    gtk_button_set_relief (GTK_BUTTON (close_button), GTK_RELIEF_NONE);
    gtk_widget_set_focus_on_click (close_button, FALSE);
    gtk_box_pack_end (GTK_BOX (box), close_button, FALSE, TRUE, 0);

    g_signal_connect (close_button, "clicked", G_CALLBACK (on_panel_row_close_clicked), row);

    gtk_container_add (GTK_CONTAINER (row), box);
}

static void on_color_set (GtkColorButton *button, gpointer user_data)
{
    LypadNoteEditor *editor = LYPAD_NOTE_EDITOR (user_data);
    GdkRGBA color;
    g_return_if_fail (editor->active_row);

    PanelRowData *data = lypad_page_row_get_data (editor->active_row);
    gtk_color_chooser_get_rgba (GTK_COLOR_CHOOSER (button), &color);
    lypad_note_set_color (data->note, &color);
}

static void lypad_note_editor_set_active_row (LypadNoteEditor *editor, LypadPageRow *row)
{
    if (editor->active_row == row)
        return;

    if (editor->active_row)
    {
        PanelRowData *data = lypad_page_row_get_data (editor->active_row);
        lypad_note_save (data->note);

        LypadTextBuffer *buffer = LYPAD_TEXT_BUFFER (lypad_text_view_get_buffer (editor->title_view));
        lypad_text_buffer_clear_selection (buffer);
        lypad_text_buffer_set_edit_mode (buffer, FALSE);
        buffer = LYPAD_TEXT_BUFFER (lypad_text_view_get_buffer (editor->content_view));
        lypad_text_buffer_clear_selection (buffer);
        lypad_text_buffer_set_edit_mode (buffer, FALSE);

        lypad_toolbar_release_buffer (editor->toolbar);
        lypad_text_view_release_buffer (editor->title_view);
        lypad_text_view_release_buffer (editor->content_view);

        g_clear_object (&editor->active_row);

        lypad_tags_box_clear (LYPAD_TAGS_BOX (editor->tags_box));
    }

    if (row)
    {
        editor->active_row = g_object_ref (row);
        PanelRowData *data = lypad_page_row_get_data (editor->active_row);
        data->timestamp = lypad_note_editor_get_timestamp (editor);

        GtkTextBuffer *buffer = lypad_note_get_content_buffer (data->note);
        lypad_text_view_set_buffer (editor->content_view, buffer);

        lypad_toolbar_set_buffer (editor->toolbar, LYPAD_TEXT_BUFFER (buffer));
        lypad_toolbar_set_text_view (editor->toolbar, GTK_TEXT_VIEW (editor->content_view));
        GtkTextIter start;
        gtk_text_buffer_get_start_iter (buffer, &start);
        gtk_text_buffer_place_cursor (buffer, &start);
        lypad_text_buffer_set_edit_mode (LYPAD_TEXT_BUFFER (buffer), TRUE);

        buffer = lypad_note_get_title_buffer (data->note);
        lypad_text_view_set_buffer (editor->title_view, buffer);
        lypad_text_buffer_set_edit_mode (LYPAD_TEXT_BUFFER (buffer), TRUE);

        on_note_color_changed (data->note, row);

        char *date_created = lypad_utils_format_time (lypad_note_get_creation_date (data->note));
        char *full_label = g_strdup_printf("Created: %s", date_created);
        gtk_label_set_label (editor->date_created_label, full_label);
        g_free (full_label);
        g_free (date_created);

        GList *tags = lypad_note_get_tags (data->note);
        for (GList *l = tags; l != NULL; l = l->next)
            lypad_tags_box_add_tag (LYPAD_TAGS_BOX (editor->tags_box), l->data);

        gtk_widget_grab_focus (GTK_WIDGET (editor->content_view));

        lypad_page_select_panel_row (LYPAD_PAGE (editor), row);

        g_object_set (editor->pin_button, "active", lypad_note_get_show_on_desktop (data->note), NULL);
    }
}

static gboolean set_active_row (gpointer user_data)
{
    LypadNoteEditor *editor = LYPAD_NOTE_EDITOR (user_data);
    if (!editor->active_row)
    {
        GList *row_list = lypad_page_get_panel_rows (LYPAD_PAGE (editor));
        guint last_timestamp = 0;
        LypadPageRow *last_row = NULL;
        for (GList *l = row_list; l != NULL; l = l->next)
        {
            PanelRowData *data = lypad_page_row_get_data (l->data);
            if (data->timestamp > last_timestamp)
            {
                last_timestamp = data->timestamp;
                last_row = l->data;
            }
        }
        g_list_free (row_list);
        if (last_row)
            lypad_note_editor_set_active_row (editor, last_row);
        else
            lypad_page_go_back (LYPAD_PAGE (editor));
    }
    return G_SOURCE_REMOVE;
}

static void lypad_note_editor_release_row (LypadPage *page, LypadPageRow *row)
{
    LypadNoteEditor *editor = LYPAD_NOTE_EDITOR (page);
    if (row == editor->active_row)
    {
        lypad_note_editor_set_active_row (editor, NULL);
        g_idle_add_full (G_PRIORITY_HIGH_IDLE, set_active_row, g_object_ref (editor), g_object_unref);
    }
}

static void lypad_note_editor_close_all (LypadNoteEditor *editor)
{
    lypad_note_editor_set_active_row (editor, NULL);
    GList *row_list = lypad_page_get_panel_rows (LYPAD_PAGE (editor));
    for (GList *l = row_list; l != NULL; l = l->next)
        lypad_page_remove_panel_row (LYPAD_PAGE (editor), l->data);
    editor->timestep = 0;
}

static void lypad_note_editor_go_back (LypadPage *page)
{
    LypadNoteEditor *editor = LYPAD_NOTE_EDITOR (page);
    lypad_note_editor_close_all (editor);
}

void lypad_note_editor_edit_notes (LypadNoteEditor *editor, GSList *notes)
{
    GSList *new_rows = NULL;
    for (GSList *n = notes; n != NULL; n = n->next)
    {
        LypadPageRow *row = NULL;
        GList *open_rows = lypad_page_get_panel_rows (LYPAD_PAGE (editor));
        for (GList *l = open_rows; l != NULL; l = l->next)
        {
            PanelRowData *data = lypad_page_row_get_data (l->data);
            if (lypad_note_equal (n->data, data->note))
            {
                row = l->data;
                break;
            }
        }
        if (!row)
            row = lypad_page_add_panel_row (LYPAD_PAGE (editor), n->data);
        new_rows = g_slist_prepend (new_rows, row);
    }

    for (GSList *l = new_rows; l != NULL; l = l->next)
    {
        PanelRowData *data = lypad_page_row_get_data (l->data);
        if (l->next)
            data->timestamp = lypad_note_editor_get_timestamp (editor);
        else
            lypad_note_editor_set_active_row (editor, l->data);
    }
    g_slist_free (new_rows);
}

static gboolean on_title_view_focus_in (GtkWidget *widget, GdkEventFocus *event, gpointer user_data)
{
    LypadNoteEditor *editor = LYPAD_NOTE_EDITOR (user_data);
    lypad_text_buffer_clear_selection (LYPAD_TEXT_BUFFER (lypad_text_view_get_buffer (editor->content_view)));

    GtkTextBuffer *buffer = lypad_text_view_get_buffer (LYPAD_TEXT_VIEW (widget));
    lypad_toolbar_set_buffer (editor->toolbar, LYPAD_TEXT_BUFFER (buffer));
    lypad_toolbar_set_text_view (editor->toolbar, GTK_TEXT_VIEW (widget));

    return FALSE;
}

static gboolean on_content_view_focus_in (GtkWidget *widget, GdkEventFocus *event, gpointer user_data)
{
    LypadNoteEditor *editor = LYPAD_NOTE_EDITOR (user_data);
    lypad_text_buffer_clear_selection (LYPAD_TEXT_BUFFER (lypad_text_view_get_buffer (editor->title_view)));

    GtkTextBuffer *buffer = lypad_text_view_get_buffer (LYPAD_TEXT_VIEW (widget));
    lypad_toolbar_set_buffer (editor->toolbar, LYPAD_TEXT_BUFFER (buffer));
    lypad_toolbar_set_text_view (editor->toolbar, GTK_TEXT_VIEW (widget));

    return FALSE;
}

static void lypad_note_editor_row_activated (LypadPage *page, LypadPageRow *row)
{
    lypad_note_editor_set_active_row (LYPAD_NOTE_EDITOR (page), row);
}

static void on_menu_pin_toggled_clicked (GtkButton *button, gpointer user_data)
{
    LypadNoteEditor *editor = LYPAD_NOTE_EDITOR (user_data);
    if (editor->active_row)
    {
        PanelRowData *data = lypad_page_row_get_data (editor->active_row);
        gboolean active;
        g_object_get (editor->pin_button, "active", &active, NULL);
        lypad_note_set_show_on_desktop (data->note, !active, GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (editor))));
    }
}

static void on_menu_add_tag_clicked (GtkButton *button, gpointer user_data)
{
    LypadNoteEditor *editor = LYPAD_NOTE_EDITOR (user_data);
    if (editor->active_row)
    {
        PanelRowData *data = lypad_page_row_get_data (editor->active_row);
        GSList *notes = g_slist_prepend (NULL, data->note);
        lypad_utils_prompt_add_tags (notes, GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (editor))));
        g_slist_free (notes);
    }
}

static void on_menu_delete_note_clicked (GtkButton *button, gpointer user_data)
{
    LypadNoteEditor *editor = LYPAD_NOTE_EDITOR (user_data);
    if (editor->active_row)
    {
        PanelRowData *data = lypad_page_row_get_data (editor->active_row);
        GSList *notes = g_slist_prepend (NULL, data->note);
        lypad_utils_prompt_delete_notes (notes, GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (editor))));
        g_slist_free (notes);
    }
}

static void on_menu_export_clicked (GtkButton *button, gpointer user_data)
{
    LypadNoteEditor *editor = LYPAD_NOTE_EDITOR (user_data);
    if (editor->active_row)
    {
        PanelRowData *data = lypad_page_row_get_data (editor->active_row);
        GSList *notes = g_slist_prepend (NULL, data->note);
        lypad_export_dialog_launch (GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (editor))), notes);
        g_slist_free (notes);
    }
}

static void on_menu_about_clicked (GtkButton *button, gpointer user_data)
{
    GtkWidget *editor = GTK_WIDGET (user_data);
    lypad_application_show_about_dialog (LYPAD_APPLICATION_DEFAULT, GTK_WINDOW (gtk_widget_get_toplevel (editor)));
}

static inline void lypad_note_editor_build_menu (LypadNoteEditor *editor)
{
    editor->menu = gtk_popover_menu_new ();
    g_object_ref_sink (editor->menu);

    GtkWidget *box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
    gtk_widget_set_margin_start (box, 10);
    gtk_widget_set_margin_end (box, 10);
    gtk_widget_set_margin_top (box, 10);
    gtk_widget_set_margin_bottom (box, 10);
    gtk_container_add (GTK_CONTAINER (editor->menu), box);

    GtkWidget *item;

    item = gtk_model_button_new ();
    g_object_set (item, "text", "Add tag", NULL);
    g_signal_connect (item, "clicked", G_CALLBACK (on_menu_add_tag_clicked), editor);
    gtk_box_pack_start (GTK_BOX (box), item, FALSE, TRUE, 0);

    item = gtk_model_button_new ();
    editor->pin_button = item;
    g_object_set (item, "text", "Pin to desktop", "role", GTK_BUTTON_ROLE_CHECK, "active", TRUE, NULL);
    g_signal_connect (item, "clicked", G_CALLBACK (on_menu_pin_toggled_clicked), editor);
    gtk_box_pack_start (GTK_BOX (box), item, FALSE, TRUE, 0);

    item = gtk_model_button_new ();
    g_object_set (item, "text", "Delete note", NULL);
    g_signal_connect (item, "clicked", G_CALLBACK (on_menu_delete_note_clicked), editor);
    gtk_box_pack_start (GTK_BOX (box), item, FALSE, TRUE, 0);

    item = gtk_model_button_new ();
    g_object_set (item, "text", "Export", NULL);
    g_signal_connect (item, "clicked", G_CALLBACK (on_menu_export_clicked), editor);
    gtk_box_pack_start (GTK_BOX (box), item, FALSE, TRUE, 0);

    item = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);
    gtk_box_pack_start (GTK_BOX (box), item, FALSE, TRUE, 0);

    item = gtk_model_button_new ();
    g_object_set (item, "text", "About", NULL);
    g_signal_connect (item, "clicked", G_CALLBACK (on_menu_about_clicked), editor);
    gtk_box_pack_start (GTK_BOX (box), item, FALSE, TRUE, 0);

    item = gtk_model_button_new ();
    g_object_set (item, "text", "Quit", NULL);
    gtk_actionable_set_action_name (GTK_ACTIONABLE (item), "app.quit");
    gtk_box_pack_start (GTK_BOX (box), item, FALSE, TRUE, 0);

    gtk_widget_show_all (box);
}

static void lypad_note_editor_init (LypadNoteEditor *editor)
{
    gtk_widget_init_template (GTK_WIDGET (editor));
    editor->toolbar = lypad_toolbar_new ();
    lypad_toolbar_show (editor->toolbar);
    gtk_grid_attach (GTK_GRID (editor), GTK_WIDGET (editor->toolbar), 0, -1, 3, 1);

    lypad_text_view_set_placeholder (editor->title_view, "Title");
    lypad_text_view_set_placeholder (editor->content_view, "Text");

    editor->color_provider = gtk_css_provider_new ();
    GtkStyleContext *style_context = gtk_widget_get_style_context (GTK_WIDGET (editor));
    gtk_style_context_add_provider (style_context, GTK_STYLE_PROVIDER (editor->color_provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

    style_context = gtk_widget_get_style_context (GTK_WIDGET (editor->date_created_label));
    gtk_style_context_add_provider (style_context, GTK_STYLE_PROVIDER (editor->color_provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

    style_context = gtk_widget_get_style_context (GTK_WIDGET (editor->title_view));
    gtk_style_context_add_provider (style_context, GTK_STYLE_PROVIDER (editor->color_provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

    style_context = gtk_widget_get_style_context (GTK_WIDGET (editor->content_view));
    gtk_style_context_add_provider (style_context, GTK_STYLE_PROVIDER (editor->color_provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

    g_signal_connect (editor->title_view, "focus-in-event", G_CALLBACK (on_title_view_focus_in), editor);
    g_signal_connect (editor->content_view, "focus-in-event", G_CALLBACK (on_content_view_focus_in), editor);

    editor->active_row = NULL;
    editor->tags_box = lypad_tags_box_new ();
    lypad_tags_box_set_spacing (LYPAD_TAGS_BOX (editor->tags_box), 6);
    lypad_tags_box_set_interactive (LYPAD_TAGS_BOX (editor->tags_box), TRUE);
    gtk_box_pack_end (editor->header_box, editor->tags_box, FALSE, FALSE, 0);
    g_signal_connect (editor->tags_box, "remove-tag", G_CALLBACK (on_remove_tag_activated), editor);

    lypad_note_editor_build_menu (editor);
}

GtkWidget *lypad_note_editor_get_menu (LypadNoteEditor *editor)
{
    return editor->menu;
}

static void lypad_note_editor_destroy (GtkWidget *widget)
{
    LypadNoteEditor *editor = LYPAD_NOTE_EDITOR (widget);
    lypad_note_editor_close_all (editor);
    g_clear_object (&editor->color_provider);
    g_clear_object (&editor->menu);
    GTK_WIDGET_CLASS (lypad_note_editor_parent_class)->destroy (widget);
}

static void lypad_note_editor_class_init (LypadNoteEditorClass *klass)
{
    LypadPageClass *page_class = LYPAD_PAGE_CLASS (klass);
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

    page_class->init_row      = lypad_note_editor_init_row;
    page_class->release_row   = lypad_note_editor_release_row;
    page_class->row_activated = lypad_note_editor_row_activated;
    page_class->go_back       = lypad_note_editor_go_back;
    widget_class->destroy     = lypad_note_editor_destroy;

    g_type_ensure (LYPAD_TYPE_TEXT_VIEW);
    g_type_ensure (LYPAD_TYPE_TITLE_VIEW);
    gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/lypad/ui/lypad-note-editor.glade");
    gtk_widget_class_bind_template_child (widget_class, LypadNoteEditor, title_view);
    gtk_widget_class_bind_template_child (widget_class, LypadNoteEditor, content_view);
    gtk_widget_class_bind_template_child (widget_class, LypadNoteEditor, color_chooser);
    gtk_widget_class_bind_template_child (widget_class, LypadNoteEditor, date_created_label);
    gtk_widget_class_bind_template_child (widget_class, LypadNoteEditor, header_box);
    gtk_widget_class_bind_template_callback (widget_class, on_color_set);
}

GtkWidget *lypad_note_editor_new ()
{
    return g_object_new (LYPAD_TYPE_NOTE_EDITOR, NULL);
}
