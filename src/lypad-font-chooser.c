/*
 *  lypad-font-chooser.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-font-chooser.h"
#include "lypad-popover.h"
#include <stdlib.h>

struct _LypadFontChooser
{
    GtkBox        parent;
    LypadPopover *popover;
    GtkEntry     *entry;
    GtkWidget    *button;
    GtkWidget    *tree_view;
    char         *current_font;

    gulong        insert_text_handler;
};

static GtkTreeModel *font_store = NULL;

G_DEFINE_TYPE (LypadFontChooser, lypad_font_chooser, GTK_TYPE_BOX)

#define N_RECENT_FONTS (5)

enum
{
    COLUMN_FONT_FAMILY,
    COLUMN_IS_SEPARATOR,
    COLUMN_IS_RECENT,
    N_STORE_COLUMNS
};

enum
{
	SIGNAL_FONT_CHANGED,
	SIGNAL_LAST
};

static guint signals[SIGNAL_LAST] = {0};

static gboolean row_separator_func (GtkTreeModel *model, GtkTreeIter *iter, gpointer data)
{
    gboolean is_separator_row = FALSE;
    gtk_tree_model_get (model, iter, COLUMN_IS_SEPARATOR, &is_separator_row, -1);
    return is_separator_row;
}

static void on_button_clicked (GtkButton *button, gpointer user_data)
{
    LypadFontChooser *chooser = LYPAD_FONT_CHOOSER (user_data);
    GtkTreeIter iter;
    gtk_tree_model_get_iter_first (font_store, &iter);
    GtkTreePath *path = gtk_tree_model_get_path (font_store, &iter);
    gtk_tree_view_scroll_to_cell (GTK_TREE_VIEW (chooser->tree_view), path, NULL, FALSE, 0, 0);
    gtk_tree_path_free (path);
    lypad_popover_popup (chooser->popover);
}

static void lypad_font_chooser_set_entry_text (LypadFontChooser *chooser, const char *text)
{
    g_signal_handler_block (chooser->entry, chooser->insert_text_handler);
    gtk_entry_set_text (chooser->entry, text);
    g_signal_handler_unblock (chooser->entry, chooser->insert_text_handler);
}

typedef struct
{
    const char *text;
    gboolean    found;
} FindTextData;

static void add_recent_font (const char *font)
{
    GtkTreeIter start, last_recent, iter;
    int n_recent = 0;
    if (G_UNLIKELY (!gtk_tree_model_get_iter_first (font_store, &iter)))
        return;
    start = iter;
    do
    {
        int is_recent;
        char *family;
        gtk_tree_model_get (font_store, &iter, COLUMN_FONT_FAMILY, &family, COLUMN_IS_RECENT, &is_recent, -1);
        if (is_recent)
        {
            n_recent += 1;
            if (strcmp (font, family) == 0)
            {
                gtk_list_store_move_before (GTK_LIST_STORE (font_store), &iter, &start);
                g_free (family);
                return;
            }
            last_recent = iter;
        }
        g_free (family);
    } while (gtk_tree_model_iter_next (font_store, &iter));

    if (n_recent >= N_RECENT_FONTS)
        gtk_list_store_remove (GTK_LIST_STORE (font_store), &last_recent);
    gtk_list_store_insert_with_values (GTK_LIST_STORE (font_store), NULL, 0, COLUMN_FONT_FAMILY, font, COLUMN_IS_RECENT, TRUE, -1);
}

static gboolean find_font (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data)
{
    FindTextData *data = user_data;
    char *family;
    gboolean is_recent, is_separator;
    gtk_tree_model_get (model, iter, COLUMN_FONT_FAMILY, &family, COLUMN_IS_SEPARATOR, &is_separator, COLUMN_IS_RECENT, &is_recent, -1);
    if (is_recent || is_separator)
        return FALSE;

    if (g_ascii_strcasecmp (family, data->text) == 0)
    {
        data->found = TRUE;
        g_free (family);
        return TRUE;
    }
    g_free (family);
    return FALSE;
}

static void lypad_font_chooser_font_set (LypadFontChooser *chooser, const char *font, gboolean emit_signal)
{
    g_clear_pointer (&chooser->current_font, g_free);
    chooser->current_font = g_strdup (font);
    FindTextData data = {font, FALSE};
    if (font[0] == '\0')
        data.found = TRUE;
    else
        gtk_tree_model_foreach (font_store, find_font, &data);
    if (data.found)
    {
        PangoAttrList *attributes = gtk_entry_get_attributes (chooser->entry);
        if (attributes)
            pango_attr_list_ref (attributes);
        else
            attributes = pango_attr_list_new ();

        PangoAttribute *attr = pango_attr_style_new (PANGO_STYLE_NORMAL);
        pango_attr_list_insert (attributes, attr);
        gtk_entry_set_attributes (chooser->entry, attributes);
        pango_attr_list_unref (attributes);
    }
    else
    {
        PangoAttrList *attributes = gtk_entry_get_attributes (chooser->entry);
        if (attributes)
            pango_attr_list_ref (attributes);
        else
            attributes = pango_attr_list_new ();

        PangoAttribute *attr = pango_attr_style_new (PANGO_STYLE_ITALIC);
        pango_attr_list_insert (attributes, attr);
        gtk_entry_set_attributes (chooser->entry, attributes);
        pango_attr_list_unref (attributes);
    }
    if (emit_signal)
    {
        g_signal_emit (chooser, signals[SIGNAL_FONT_CHANGED], 0, font);
        add_recent_font (font);
    }
}

static void on_tree_view_row_activated (GtkTreeView *tree_view, GtkTreePath *path, GtkTreeViewColumn *column, gpointer user_data)
{
    LypadFontChooser *chooser = LYPAD_FONT_CHOOSER (user_data);
    lypad_popover_popdown (chooser->popover);

    GtkTreeIter iter;
    GtkTreeModel *model = gtk_tree_view_get_model (tree_view);

    if (!gtk_tree_model_get_iter (model, &iter, path))
        return;

    GValue value = G_VALUE_INIT;
    gtk_tree_model_get_value (model, &iter, COLUMN_FONT_FAMILY, &value);
    lypad_font_chooser_set_entry_text (chooser, g_value_get_string (&value));
    lypad_font_chooser_font_set (chooser, g_value_get_string (&value), TRUE);
    g_value_unset (&value);
}

static void on_popover_closed (GtkPopover *popover, gpointer user_data)
{
    LypadFontChooser *chooser = LYPAD_FONT_CHOOSER (user_data);
    g_signal_handlers_block_by_func (chooser->button, on_button_clicked, chooser);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (chooser->button), FALSE);
    g_signal_handlers_unblock_by_func (chooser->button, on_button_clicked, chooser);
}

typedef struct
{
    char *query_text;
    char *matched_text;
} MatchTextData;

static gboolean find_font_match (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer user_data)
{
    //FIXME: Skip the first 6 rows: recent fonts and the separator. The recent fonts would not be sorted.
    MatchTextData *data = user_data;
    char *family;
    gboolean is_recent, is_separator;
    gtk_tree_model_get (model, iter, COLUMN_FONT_FAMILY, &family, COLUMN_IS_SEPARATOR, &is_separator, COLUMN_IS_RECENT, &is_recent, -1);
    if (is_separator || is_recent)
        return FALSE;

    char *casefold_str = g_utf8_casefold (family, -1);
    if (g_str_has_prefix (casefold_str, data->query_text))
    {
        data->matched_text = family;
        g_free (casefold_str);
        return TRUE;
    }
    g_free (family);
    g_free (casefold_str);
    return FALSE;
}

static gboolean complete_entry (gpointer user_data);

static void on_entry_insert_text (GtkEditable *editable, char *new_text, int new_text_length, gpointer position, gpointer user_data)
{
    g_idle_add_full (G_PRIORITY_HIGH, complete_entry, user_data, NULL);
}

static gboolean complete_entry (gpointer user_data)
{
    LypadFontChooser *chooser = LYPAD_FONT_CHOOSER (user_data);
    MatchTextData data;

    data.query_text = g_utf8_casefold (gtk_entry_get_text (chooser->entry), -1);
    data.matched_text = NULL;
    gtk_tree_model_foreach (font_store, find_font_match, &data);

    if (data.matched_text != NULL)
    {
        lypad_font_chooser_set_entry_text (chooser, data.matched_text);
        int query_len = g_utf8_strlen (data.query_text, -1);
        gtk_editable_select_region (GTK_EDITABLE (chooser->entry), query_len, -1);
        g_free (data.matched_text);
    }
    g_free (data.query_text);
    return G_SOURCE_REMOVE;
}

static void on_entry_activate (GtkEntry *entry, gpointer user_data)
{
    LypadFontChooser *chooser = LYPAD_FONT_CHOOSER (user_data);
    gtk_editable_set_position (GTK_EDITABLE (entry), 0);
    lypad_font_chooser_font_set (chooser, gtk_entry_get_text (entry), TRUE);
}

static gboolean on_entry_focus_out (GtkWidget *widget, GdkEventFocus *event, gpointer user_data)
{
    LypadFontChooser *chooser = LYPAD_FONT_CHOOSER (user_data);
    lypad_font_chooser_set_entry_text (chooser, chooser->current_font);
    gtk_editable_set_position (GTK_EDITABLE (chooser->entry), 0);
    return FALSE;
}

static int cmp_families (const void *a, const void *b)
{
    const char *a_name = pango_font_family_get_name (*(PangoFontFamily **)a);
    const char *b_name = pango_font_family_get_name (*(PangoFontFamily **)b);
    char *a_case_name = g_utf8_casefold (a_name, -1);
    char *b_case_name = g_utf8_casefold (b_name, -1);
    int ret = g_utf8_collate (a_case_name, b_case_name);
    g_free (a_case_name);
    g_free (b_case_name);
    return ret;
}

static inline void init_font_store ()
{
    if (G_UNLIKELY (!font_store))
    {
        GtkListStore *store = gtk_list_store_new (N_STORE_COLUMNS, G_TYPE_STRING, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN);
        PangoFontMap *font_map =  pango_cairo_font_map_get_default ();
        PangoFontFamily **families;
        int n_families;

        pango_font_map_list_families (font_map, &families, &n_families);
        qsort (families, n_families, sizeof (PangoFontFamily *), cmp_families);

        gtk_list_store_insert_with_values (store, NULL, -1, COLUMN_IS_SEPARATOR, TRUE, -1);

        for (int i = 0; i < n_families; ++i)
        {
            const char* font_name = pango_font_family_get_name (families[i]);
            if (g_strcmp0 (font_name, "Lypad Symbols") != 0)
                gtk_list_store_insert_with_values (store, NULL, -1, COLUMN_FONT_FAMILY, font_name, -1);
        }
        g_free (families);
        font_store = GTK_TREE_MODEL (store);
    }
}

static void lypad_font_chooser_init_popup (LypadFontChooser *chooser)
{
    init_font_store ();
    chooser->tree_view = gtk_tree_view_new_with_model (font_store);
    GtkCellRenderer *renderer = gtk_cell_renderer_text_new ();
    GtkTreeViewColumn *column = gtk_tree_view_column_new_with_attributes ("Font", renderer, "text", 0, "family", 0, NULL);
    gtk_tree_view_append_column (GTK_TREE_VIEW (chooser->tree_view), column);
    gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (chooser->tree_view), FALSE);
    gtk_tree_view_set_row_separator_func (GTK_TREE_VIEW (chooser->tree_view), row_separator_func, NULL, NULL);
    gtk_tree_view_set_hover_selection (GTK_TREE_VIEW (chooser->tree_view), TRUE);
    gtk_tree_view_set_enable_search (GTK_TREE_VIEW (chooser->tree_view), FALSE);
    gtk_tree_view_set_activate_on_single_click (GTK_TREE_VIEW (chooser->tree_view), TRUE);

    chooser->popover = lypad_popover_new (GTK_WIDGET (chooser));
    GtkWidget *scrolled_window = gtk_scrolled_window_new (NULL, NULL);
    gtk_widget_set_size_request (GTK_WIDGET (chooser->popover), -1, 300);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
    gtk_scrolled_window_set_overlay_scrolling (GTK_SCROLLED_WINDOW (scrolled_window), FALSE);

    gtk_container_add (GTK_CONTAINER (chooser->popover), scrolled_window);
    gtk_container_add (GTK_CONTAINER (scrolled_window), chooser->tree_view);
    gtk_widget_show_all (scrolled_window);

    g_signal_connect (chooser->tree_view, "row-activated", G_CALLBACK (on_tree_view_row_activated), chooser);
    g_signal_connect (chooser->popover, "closed", G_CALLBACK (on_popover_closed), chooser);
}

static void lypad_font_chooser_init (LypadFontChooser *chooser)
{
    gtk_style_context_add_class (gtk_widget_get_style_context (GTK_WIDGET (chooser)), "linked");
    lypad_font_chooser_init_popup (chooser);

    chooser->entry = GTK_ENTRY (gtk_entry_new ());
    chooser->button = gtk_toggle_button_new ();
    gtk_widget_set_can_focus (chooser->button, FALSE);

    GtkWidget *arrow = gtk_image_new_from_icon_name ("pan-down-symbolic", GTK_ICON_SIZE_BUTTON);
    gtk_container_add (GTK_CONTAINER (chooser->button), arrow);
    gtk_container_add (GTK_CONTAINER (chooser), GTK_WIDGET (chooser->entry));
    gtk_container_add (GTK_CONTAINER (chooser), chooser->button);

    g_signal_connect (chooser->button, "clicked", G_CALLBACK (on_button_clicked), chooser);
    g_signal_connect (chooser->entry, "activate", G_CALLBACK (on_entry_activate), chooser);
    chooser->insert_text_handler = g_signal_connect_after (chooser->entry, "insert-text", G_CALLBACK (on_entry_insert_text), chooser);
    g_signal_connect (chooser->entry, "focus-out-event", G_CALLBACK (on_entry_focus_out), chooser);
}

static void lypad_font_chooser_finalize (GObject *object)
{
    LypadFontChooser *chooser = LYPAD_FONT_CHOOSER (object);
    g_clear_pointer (&chooser->current_font, g_free);
    G_OBJECT_CLASS (lypad_font_chooser_parent_class)->finalize (object);
}

static void lypad_font_chooser_class_init (LypadFontChooserClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = lypad_font_chooser_finalize;

	signals[SIGNAL_FONT_CHANGED] = g_signal_new ("font-changed", G_TYPE_FROM_CLASS (klass),
											G_SIGNAL_RUN_LAST, 0, NULL, NULL,
											g_cclosure_marshal_VOID__STRING, G_TYPE_NONE, 1, G_TYPE_STRING);
}

void lypad_font_chooser_set_font (LypadFontChooser *chooser, const char *font)
{
    if (font)
    {
        lypad_font_chooser_set_entry_text (chooser, font);
        lypad_font_chooser_font_set (chooser, font, FALSE);
    }
    else
    {
        lypad_font_chooser_set_entry_text (chooser, "");
        lypad_font_chooser_font_set (chooser, "", FALSE);
    }
}

LypadFontChooser *lypad_font_chooser_new ()
{
    LypadFontChooser *chooser = g_object_new (LYPAD_TYPE_FONT_CHOOSER, "orientation", GTK_ORIENTATION_HORIZONTAL, NULL);
    return chooser;
}
