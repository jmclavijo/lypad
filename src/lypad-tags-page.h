/*
 *  lypad-tags-page.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "lypad-page.h"
#include "lypad-tag.h"

G_BEGIN_DECLS

#define LYPAD_TYPE_TAGS_PAGE (lypad_tags_page_get_type ())
G_DECLARE_FINAL_TYPE (LypadTagsPage, lypad_tags_page, LYPAD, TAGS_PAGE, LypadPage)

GtkWidget *lypad_tags_page_new        (void);
void       lypad_tags_page_set_tag    (LypadTagsPage *tags_page, LypadTag *tag);
LypadTag  *lypad_tags_page_get_tag    (LypadTagsPage *tags_page);
void       lypad_tags_page_set_toggle (LypadTagsPage *tags_page, GtkWidget *toggle_button);

G_END_DECLS
