/*
 *  lypad-toolbar.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-toolbar.h"
#include "lypad-font-chooser.h"
#include "lypad-size-chooser.h"
#include "lypad-text-buffer.h"
#include "color-selection/lypad-text-color-chooser.h"
#include "color-selection/lypad-color-widget.h"
#include "lypad-list-chooser.h"
#include <string.h>

struct _LypadToolbar
{
    GtkRevealer          parent;
    GtkToolbar          *toolbar;
    LypadTextBuffer     *text_buffer;
    GtkToolItem         *button_bold;
    GtkToolItem         *button_italic;
    GtkToolItem         *button_underline;
    GtkToolItem         *button_strikethrough;
    GtkToggleToolButton *toggle_buttons[LYPAD_N_TOGGLE_TAGS];
    LypadFontChooser    *font_chooser;
    LypadSizeChooser    *size_chooser;
    LypadColorWidget    *text_color_chooser;
    LypadListChooser    *bullet_chooser;
    LypadListChooser    *enum_chooser;

    GtkTextView         *text_view;

    // Signal handlers
    gulong              sh_style_changed;
};

G_DEFINE_TYPE (LypadToolbar, lypad_toolbar, GTK_TYPE_REVEALER)

void lypad_toolbar_show (LypadToolbar *toolbar)
{
    gtk_revealer_set_reveal_child (GTK_REVEALER (toolbar), TRUE);
}

void lypad_toolbar_hide (LypadToolbar *toolbar)
{
    gtk_revealer_set_reveal_child (GTK_REVEALER (toolbar), FALSE);
}

static void on_button_toggled (GtkToggleToolButton *button, gpointer user_data)
{
    LypadToolbar *toolbar = LYPAD_TOOLBAR (user_data);
    for (int tag = 0; tag < LYPAD_N_TOGGLE_TAGS; ++tag)
        if (toolbar->toggle_buttons[tag] == button)
            lypad_text_buffer_set_toggle_tag (toolbar->text_buffer, tag, gtk_toggle_tool_button_get_active (button));
}

static void on_buffer_style_changed (LypadTextBuffer *buffer, gpointer user_data)
{
    LypadToolbar *toolbar = LYPAD_TOOLBAR (user_data);
    const char *font = lypad_text_buffer_get_font (buffer);
    int size = lypad_text_buffer_get_size (buffer);
    const GdkRGBA *color = lypad_text_buffer_get_color (buffer);

    lypad_font_chooser_set_font (toolbar->font_chooser, font);
    lypad_size_chooser_set_size (toolbar->size_chooser, size);
    if (color)
        lypad_color_widget_set_color (toolbar->text_color_chooser, color);

    for (int tag = 0; tag < LYPAD_N_TOGGLE_TAGS; ++tag)
    {
        g_signal_handlers_block_by_func (toolbar->toggle_buttons[tag], on_button_toggled, toolbar);
        gtk_toggle_tool_button_set_active (toolbar->toggle_buttons[tag], lypad_text_buffer_get_toggle_tag (toolbar->text_buffer, tag));
        g_signal_handlers_unblock_by_func (toolbar->toggle_buttons[tag], on_button_toggled, toolbar);
    }
}

void lypad_toolbar_set_buffer (LypadToolbar *toolbar, LypadTextBuffer *text_buffer)
{
    lypad_toolbar_release_buffer (toolbar);
    toolbar->text_buffer = g_object_ref (text_buffer);
    //FIXME: Initialize the selected font and size to the defaults in preferences
    toolbar->sh_style_changed = g_signal_connect (text_buffer, "style-changed", G_CALLBACK (on_buffer_style_changed), toolbar);
}

void lypad_toolbar_set_text_view (LypadToolbar *toolbar, GtkTextView *text_view)
{
    g_clear_object (&toolbar->text_view);
    if (text_view)
        toolbar->text_view = g_object_ref (text_view);
}

void lypad_toolbar_notify_bkg_color (LypadToolbar *toolbar, const GdkRGBA *color)
{
    g_return_if_fail (LYPAD_IS_TOOLBAR (toolbar));
    lypad_text_color_chooser_set_bkg_color (LYPAD_TEXT_COLOR_CHOOSER (toolbar->text_color_chooser), color);
}

void lypad_toolbar_release_buffer (LypadToolbar *toolbar)
{
    if (toolbar->sh_style_changed)
    {
        g_signal_handler_disconnect (toolbar->text_buffer, toolbar->sh_style_changed);
        toolbar->sh_style_changed = 0;
    }
    g_clear_object (&toolbar->text_buffer);
    g_clear_pointer (&toolbar->text_view, g_object_unref);
}

static void on_font_changed (LypadFontChooser *chooser, char *font, gpointer user_data)
{
    LypadToolbar *toolbar = LYPAD_TOOLBAR (user_data);
    lypad_text_buffer_set_font (toolbar->text_buffer, font);
    if (toolbar->text_view)
        gtk_widget_grab_focus (GTK_WIDGET (toolbar->text_view));
}

static void on_size_changed (LypadFontChooser *chooser, int size, gpointer user_data)
{
    LypadToolbar *toolbar = LYPAD_TOOLBAR (user_data);
    lypad_text_buffer_set_size (toolbar->text_buffer, size);
    if (toolbar->text_view)
        gtk_widget_grab_focus (GTK_WIDGET (toolbar->text_view));
}

static void on_text_color_changed (LypadColorWidget *widget, gpointer user_data)
{
    LypadToolbar *toolbar = LYPAD_TOOLBAR (user_data);
    GdkRGBA color;
    lypad_color_widget_get_color (widget, &color);
    lypad_text_buffer_set_color (toolbar->text_buffer, &color);
    if (toolbar->text_view)
        gtk_widget_grab_focus (GTK_WIDGET (toolbar->text_view));
}

static void on_list_changed (LypadListChooser *chooser, LypadTextListType type, gpointer user_data)
{
    LypadToolbar *toolbar = LYPAD_TOOLBAR (user_data);
    if (type == LYPAD_TEXT_LIST_TYPE_NONE)
        lypad_text_buffer_unset_list (toolbar->text_buffer);
    else if (type == LYPAD_TEXT_LIST_TYPE_BULLET)
    {
        gunichar bullet = lypad_list_chooser_get_bullet (chooser);
        lypad_text_buffer_set_bullet (toolbar->text_buffer, bullet);
    }
    // else if (type == LYPAD_TEXT_LIST_TYPE_ENUM)
    //     lypad_text_buffer_set_enum (toolbar->text_buffer);
}

static void lypad_toolbar_init (LypadToolbar *toolbar)
{
    gtk_widget_init_template (GTK_WIDGET (toolbar));
    toolbar->text_buffer = NULL;
    toolbar->text_view = NULL;
    toolbar->sh_style_changed = 0;

    GtkToolItem *tool_item = gtk_tool_item_new ();
    toolbar->font_chooser = lypad_font_chooser_new ();
    gtk_container_add (GTK_CONTAINER (tool_item), GTK_WIDGET (toolbar->font_chooser));
    gtk_toolbar_insert (toolbar->toolbar, tool_item, 0);

    tool_item = gtk_tool_item_new ();
    toolbar->size_chooser = lypad_size_chooser_new ();
    gtk_container_add (GTK_CONTAINER (tool_item), GTK_WIDGET (toolbar->size_chooser));
    gtk_toolbar_insert (toolbar->toolbar, tool_item, 1);

    tool_item = gtk_tool_item_new ();
    toolbar->text_color_chooser = LYPAD_COLOR_WIDGET (lypad_text_color_chooser_new ());
    gtk_container_add (GTK_CONTAINER (tool_item), GTK_WIDGET (toolbar->text_color_chooser));
    gtk_toolbar_insert (toolbar->toolbar, tool_item, 2);
    gtk_widget_set_margin_start (GTK_WIDGET (tool_item), 8);

    tool_item = gtk_tool_item_new ();
    toolbar->bullet_chooser = LYPAD_LIST_CHOOSER (lypad_list_chooser_new (LYPAD_TEXT_LIST_TYPE_BULLET));
    gtk_container_add (GTK_CONTAINER (tool_item), GTK_WIDGET (toolbar->bullet_chooser));
    gtk_toolbar_insert (toolbar->toolbar, tool_item, -1);

    tool_item = gtk_tool_item_new ();
    toolbar->enum_chooser = LYPAD_LIST_CHOOSER (lypad_list_chooser_new (LYPAD_TEXT_LIST_TYPE_ENUM));
    gtk_container_add (GTK_CONTAINER (tool_item), GTK_WIDGET (toolbar->enum_chooser));
    gtk_toolbar_insert (toolbar->toolbar, tool_item, -1);
    gtk_widget_set_margin_start (GTK_WIDGET (tool_item), 1);

    gtk_widget_show_all (GTK_WIDGET (toolbar));

    g_signal_connect (toolbar->font_chooser, "font-changed", G_CALLBACK (on_font_changed), toolbar);
    g_signal_connect (toolbar->size_chooser, "size-changed", G_CALLBACK (on_size_changed), toolbar);
    g_signal_connect (toolbar->text_color_chooser, "color-changed", G_CALLBACK (on_text_color_changed), toolbar);
    g_signal_connect (toolbar->bullet_chooser, "list-changed", G_CALLBACK (on_list_changed), toolbar);
    g_signal_connect (toolbar->enum_chooser, "list-changed", G_CALLBACK (on_list_changed), toolbar);

    //FIXME: Use lypad_text_buffer_set_freeze_selection in the signal handlers for the "focus-in" and "focus-out" of text_view
}

static void lypad_toolbar_destroy (GtkWidget *widget)
{
    LypadToolbar *toolbar = LYPAD_TOOLBAR (widget);
    lypad_toolbar_set_text_view (toolbar, NULL);
    lypad_toolbar_release_buffer (toolbar);
    g_clear_object (&toolbar->text_view);
    GTK_WIDGET_CLASS (lypad_toolbar_parent_class)->destroy (widget);
}

static void lypad_toolbar_class_init (LypadToolbarClass *klass)
{
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

    widget_class->destroy = lypad_toolbar_destroy;

    gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/lypad/ui/lypad-toolbar.glade");

    gtk_widget_class_bind_template_child (widget_class, LypadToolbar, toolbar);
    gtk_widget_class_bind_template_child_full (widget_class, "button_bold", FALSE, G_STRUCT_OFFSET (LypadToolbar, toggle_buttons[0]));
    gtk_widget_class_bind_template_child_full (widget_class, "button_italic", FALSE, G_STRUCT_OFFSET (LypadToolbar, toggle_buttons[1]));
    gtk_widget_class_bind_template_child_full (widget_class, "button_underline", FALSE, G_STRUCT_OFFSET (LypadToolbar, toggle_buttons[2]));
    gtk_widget_class_bind_template_child_full (widget_class, "button_strikethrough", FALSE, G_STRUCT_OFFSET (LypadToolbar, toggle_buttons[3]));
    gtk_widget_class_bind_template_callback (widget_class, on_button_toggled);
}

LypadToolbar *lypad_toolbar_new ()
{
    return g_object_new (LYPAD_TYPE_TOOLBAR, NULL);
}
