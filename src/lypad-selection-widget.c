/*
 *  lypad-selection-widget.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-selection-widget.h"
#include <math.h>

struct _LypadSelectionWidget
{
    GtkOverlay     parent;
    GtkWidget     *child_container;
    GtkWidget     *check_button;
};

G_DEFINE_TYPE (LypadSelectionWidget, lypad_selection_widget, GTK_TYPE_OVERLAY)

enum
{
    SIGNAL_TOGGLED,
    SIGNAL_LAST
};

static guint signals[SIGNAL_LAST] = {0};

struct _LypadAnimationHandler
{
    GtkWidget *parent;
    guint      cb_handler;
    gint64     time_start;
    gint64     time_current;
    gboolean   to_selectable;

    GSList    *widgets;
};

LypadAnimationHandler *lypad_animation_handler_new (GtkWidget *widget)
{
    LypadAnimationHandler *handler = g_new0 (LypadAnimationHandler, 1);
    handler->parent = g_object_ref (widget);
    return handler;
}

void lypad_animation_handler_free (LypadAnimationHandler *handler)
{
    g_slist_free_full (handler->widgets, g_object_unref);
    g_object_unref (handler->parent);
    g_free (handler);
}

static gboolean on_check_button_button_press (GtkWidget *check_button, GdkEventButton *event, gpointer user_data)
{
    if (event->button == GDK_BUTTON_SECONDARY)
    {
        LypadSelectionWidget *widget = LYPAD_SELECTION_WIDGET (user_data);
        GtkWidget *child = gtk_bin_get_child (GTK_BIN (widget->child_container));
        if (child)
            gtk_propagate_event (child, (GdkEvent*) event);
        return TRUE;
    }
    return FALSE;
}

static void on_check_button_toggled (GtkToggleButton *button, gpointer widget)
{
    GtkWidget *child = gtk_bin_get_child (GTK_BIN (LYPAD_SELECTION_WIDGET (widget)->child_container));
    gboolean active = gtk_toggle_button_get_active (button);
    if (active)
        gtk_widget_set_state_flags (child, GTK_STATE_FLAG_CHECKED, FALSE);
    else
        gtk_widget_unset_state_flags (child, GTK_STATE_FLAG_CHECKED);
    g_signal_emit (widget, signals[SIGNAL_TOGGLED], 0, gtk_toggle_button_get_active (button));
}

static void lypad_selection_widget_init (LypadSelectionWidget *widget)
{
    widget->child_container = gtk_event_box_new ();
    GTK_CONTAINER_CLASS (lypad_selection_widget_parent_class)->add (GTK_CONTAINER (widget), widget->child_container);
    widget->check_button = gtk_check_button_new ();
    gtk_widget_hide (widget->check_button);
    gtk_widget_set_focus_on_click (widget->check_button, FALSE);
    gtk_widget_set_no_show_all (widget->check_button, TRUE);

    gtk_overlay_add_overlay (GTK_OVERLAY (widget), widget->check_button);
    gtk_widget_set_halign (widget->check_button, GTK_ALIGN_FILL);
    gtk_widget_set_valign (widget->check_button, GTK_ALIGN_FILL);
    gtk_widget_show_all (GTK_WIDGET (widget));

    g_signal_connect (widget->check_button, "button-press-event", G_CALLBACK (on_check_button_button_press), widget);
    g_signal_connect (widget->check_button, "toggled", G_CALLBACK (on_check_button_toggled), widget);
}

#define ANIMATION_LENGTH (0.23e+6f)
#define ANIMATION_MAX_MARGIN (32)

static inline float ease_animation (float t)
{
    return ANIMATION_MAX_MARGIN * 0.5f * (1.f - cos(M_PI * t));
}

static gboolean update_animation_callback (GtkWidget *toplevel, GdkFrameClock *frame_clock, gpointer user_data)
{
    LypadAnimationHandler *animation_handler = user_data;
    animation_handler->time_current = gdk_frame_clock_get_frame_time (frame_clock);
    float t = (animation_handler->time_current - animation_handler->time_start) * (1.f / ANIMATION_LENGTH);
    if (t < 1)
    {
        int margin;
        if (animation_handler->to_selectable)
            margin = ease_animation (t);
        else
            margin = ease_animation (1.f - t);

        for (GSList *l = animation_handler->widgets; l != NULL; l = l->next)
        {
            LypadSelectionWidget *widget = l->data;
            gtk_widget_set_margin_start (widget->child_container, margin);
        }

        return G_SOURCE_CONTINUE;
    }
    else
    {
        for (GSList *l = animation_handler->widgets; l != NULL; l = l->next)
        {
            LypadSelectionWidget *widget = l->data;
            if (animation_handler->to_selectable)
            {
                gtk_widget_set_margin_start (widget->child_container, ANIMATION_MAX_MARGIN);
                gtk_widget_show (widget->check_button);
                gtk_event_box_set_above_child (GTK_EVENT_BOX (widget->child_container), TRUE);
            }
            else
            {
                gtk_widget_set_margin_start (widget->child_container, 0);
            }
        }
        animation_handler->cb_handler = 0;
        return G_SOURCE_REMOVE;
    }
}

void lypad_selection_widget_set_selectable (GSList *widgets, gboolean selectable, LypadAnimationHandler *animation_handler)
{
    g_return_if_fail (animation_handler != NULL);
    if (selectable == animation_handler->to_selectable)
        return;

    g_clear_slist (&animation_handler->widgets, g_object_unref);
    for (GSList *l = widgets; l != NULL; l = l->next)
        animation_handler->widgets = g_slist_prepend (animation_handler->widgets, g_object_ref (l->data));

    if (selectable)
    {
        if (animation_handler->cb_handler == 0)
        {
            animation_handler->time_start = g_get_monotonic_time ();
            animation_handler->time_current = animation_handler->time_start;
            animation_handler->cb_handler = gtk_widget_add_tick_callback (animation_handler->parent, update_animation_callback, animation_handler, NULL);
        }
        else
        {
            animation_handler->time_current = g_get_monotonic_time ();
            if (animation_handler->time_current - animation_handler->time_start > ANIMATION_LENGTH)
                animation_handler->time_start = animation_handler->time_current;
            else
                animation_handler->time_start = 2 * animation_handler->time_current - ANIMATION_LENGTH - animation_handler->time_start;
        }
        animation_handler->to_selectable = TRUE;
    }
    else
    {
        if (animation_handler->cb_handler == 0)
        {
            animation_handler->time_start = g_get_monotonic_time ();
            animation_handler->time_current = animation_handler->time_start;
            animation_handler->cb_handler = gtk_widget_add_tick_callback (animation_handler->parent, update_animation_callback, animation_handler, NULL);
        }
        else
        {
            animation_handler->time_current = g_get_monotonic_time ();
            if (animation_handler->time_current - animation_handler->time_start > ANIMATION_LENGTH)
                animation_handler->time_start = animation_handler->time_current;
            else
                animation_handler->time_start = 2 * animation_handler->time_current - ANIMATION_LENGTH - animation_handler->time_start;
        }
        animation_handler->to_selectable = FALSE;

        for (GSList *l = animation_handler->widgets; l != NULL; l = l->next)
        {
            LypadSelectionWidget *widget = l->data;
            gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget->check_button), FALSE);
            gtk_widget_hide (widget->check_button);
            gtk_event_box_set_above_child (GTK_EVENT_BOX (widget->child_container), FALSE);
        }
    }
}

gboolean lypad_selection_widget_get_selectable (LypadSelectionWidget *widget)
{
    return gtk_widget_is_visible (widget->check_button);
}

void lypad_selection_widget_set_selected (LypadSelectionWidget *widget, gboolean selected, LypadAnimationHandler *animation_handler)
{
    if ((animation_handler && animation_handler->to_selectable) || gtk_widget_is_visible (widget->check_button))
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget->check_button), selected);
}

gboolean lypad_selection_widget_get_selected (LypadSelectionWidget *widget)
{
    return gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget->check_button));
}

static void on_child_destroy (GtkWidget *child, gpointer user_data)
{
    GtkWidget *widget = GTK_WIDGET (user_data);
    if (!gtk_widget_in_destruction (widget))
        gtk_widget_destroy (widget);
}

void lypad_selection_widget_add_child (LypadSelectionWidget *widget, GtkWidget *child)
{
    gtk_container_add (GTK_CONTAINER (widget->child_container), child);
    g_signal_connect (child, "destroy", G_CALLBACK (on_child_destroy), widget);
}

GtkWidget *lypad_selection_widget_get_child (LypadSelectionWidget *widget)
{
    return gtk_bin_get_child (GTK_BIN (widget->child_container));
}

static void lypad_selection_widget_class_init (LypadSelectionWidgetClass *klass)
{
    signals[SIGNAL_TOGGLED] = g_signal_new ("toggled", G_TYPE_FROM_CLASS (klass),
                                            G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                            g_cclosure_marshal_VOID__BOOLEAN,
                                            G_TYPE_NONE, 1, G_TYPE_BOOLEAN);
}

GtkWidget *lypad_selection_widget_new ()
{
    return g_object_new (LYPAD_TYPE_SELECTION_WIDGET, NULL);
}
