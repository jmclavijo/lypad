/*
 *  lypad-preferences-editor.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-preferences-editor.h"
#include "lypad-preferences.h"
#include "lypad-utils.h"
#include "color-selection/lypad-color-store.h"
#include "lypad-theme.h"
#include "lypad-notes-list.h"

struct _LypadPreferencesEditor
{
    LypadPage         parent;
    GtkStack         *stack;

    // Interface
    GtkSwitch        *tray_icon_switch;
    GtkSwitch        *autostart_switch;

    GtkWidget        *sort_by_title;
    GtkWidget        *sort_by_creation;
    GtkWidget        *sort_by_modification;
    GtkWidget        *sort_ascending;
    GtkWidget        *sort_descending;

    // Note defaults
    GtkWidget        *bg_color_fixed;
    GtkWidget        *bg_color_chooser;
    GtkWidget        *bg_color_random;
    GtkWidget        *bg_color_pool_combo;

    GtkWidget        *fg_color_automatic;
    GtkWidget        *fg_color_custom;
    GtkWidget        *fg_color_chooser;

    GtkWidget        *text_font;
    GtkWidget        *theme_combo;

    // Advanced
    GtkWidget        *notes_folder_button;
    GtkWidget        *notes_folder_label;

    GSettings        *settings;
    GSettings        *default_note_settings;
};

G_DEFINE_TYPE (LypadPreferencesEditor, lypad_preferences_editor, LYPAD_TYPE_PAGE)

static const char* const ROW_LABELS[]  = {"Interface", "Note defaults", "Advanced"};
static const char* const CHILD_NAMES[] = {"interface", "note-defaults", "advanced"};

static void lypad_preferences_editor_init_row (LypadPage *page, LypadPageRow *row, gpointer user_data)
{
    lypad_page_row_set_data (row, user_data, NULL);
    GtkWidget *label = gtk_label_new (ROW_LABELS[GPOINTER_TO_INT (user_data)]);
    gtk_widget_set_halign (label, GTK_ALIGN_START);
    gtk_container_add (GTK_CONTAINER (row), label);
}

static void lypad_preferences_editor_row_activated (LypadPage *page, LypadPageRow *row)
{
    int idx = GPOINTER_TO_INT (lypad_page_row_get_data (row));
    gtk_stack_set_visible_child_name (LYPAD_PREFERENCES_EDITOR (page)->stack, CHILD_NAMES[idx]);
}

void lypad_preferences_editor_focus (LypadPreferencesEditor *editor)
{
    GList *rows = lypad_page_get_panel_rows (LYPAD_PAGE (editor));
    if (rows)
        lypad_page_select_panel_row (LYPAD_PAGE (editor), rows->data);
    g_list_free (rows);
    gtk_stack_set_visible_child_name (editor->stack, CHILD_NAMES[0]);
}

static gboolean on_autostart_switch_state_set (GtkSwitch *switch_widget, gboolean state, gpointer user_data)
{
    gtk_switch_set_state (switch_widget, lypad_utils_set_autostart_enabled (state));
    return TRUE;
}

static void on_notes_folder_dialog_response (GtkDialog *dialog, int response_id, gpointer user_data)
{
    if (response_id == GTK_RESPONSE_ACCEPT)
    {
        LypadPreferencesEditor *editor = LYPAD_PREFERENCES_EDITOR (user_data);
        char *tmp_path = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
        char *path = lypad_utils_path_shrink_tilde (tmp_path);
        g_free (tmp_path);
        g_settings_set_string (editor->settings, "notes-folder", path);
        g_free (path);
    }
    gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void on_notes_folder_button_clicked (GtkButton *button, gpointer user_data)
{
    LypadPreferencesEditor *editor = LYPAD_PREFERENCES_EDITOR (user_data);
    GtkWidget *dialog = gtk_file_chooser_dialog_new ("Select folder",
                                                     GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (editor))),
                                                     GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                                     "Cancel", GTK_RESPONSE_CANCEL, NULL);
    GtkWidget *accept_button = gtk_dialog_add_button (GTK_DIALOG (dialog), "Select", GTK_RESPONSE_ACCEPT);
    gtk_style_context_add_class (gtk_widget_get_style_context (accept_button), GTK_STYLE_CLASS_SUGGESTED_ACTION);

    gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
    g_signal_connect (dialog, "response", G_CALLBACK (on_notes_folder_dialog_response), editor);
    gtk_window_present (GTK_WINDOW (dialog));
}

static void on_bg_color_toggled (GtkToggleButton *button, gpointer user_data)
{
    if (!gtk_toggle_button_get_active (button))
        return;

    g_signal_handlers_block_by_func (button, on_bg_color_toggled, user_data);
    LypadPreferencesEditor *editor = LYPAD_PREFERENCES_EDITOR (user_data);
    if (button == GTK_TOGGLE_BUTTON (editor->bg_color_fixed))
    {
        gtk_widget_set_sensitive (editor->bg_color_chooser, TRUE);
        gtk_widget_set_sensitive (editor->bg_color_pool_combo, FALSE);
        g_settings_set_boolean (editor->default_note_settings, "bg-color-fixed", TRUE);
    }
    else if (button == GTK_TOGGLE_BUTTON (editor->bg_color_random))
    {
        gtk_widget_set_sensitive (editor->bg_color_chooser, FALSE);
        gtk_widget_set_sensitive (editor->bg_color_pool_combo, TRUE);
        g_settings_set_boolean (editor->default_note_settings, "bg-color-fixed", FALSE);
    }
    g_signal_handlers_unblock_by_func (button, on_bg_color_toggled, user_data);
}

static void on_fg_color_toggled (GtkToggleButton *button, gpointer user_data)
{
    if (!gtk_toggle_button_get_active (button))
        return;

    g_signal_handlers_block_by_func (button, on_fg_color_toggled, user_data);
    LypadPreferencesEditor *editor = LYPAD_PREFERENCES_EDITOR (user_data);
    if (button == GTK_TOGGLE_BUTTON (editor->fg_color_automatic))
    {
        gtk_widget_set_sensitive (editor->fg_color_chooser, FALSE);
        g_settings_set_boolean (editor->default_note_settings, "fg-automatic", TRUE);
    }
    else if (button == GTK_TOGGLE_BUTTON (editor->fg_color_custom))
    {
        gtk_widget_set_sensitive (editor->fg_color_chooser, TRUE);
        g_settings_set_boolean (editor->default_note_settings, "fg-automatic", FALSE);
    }
    g_signal_handlers_unblock_by_func (button, on_fg_color_toggled, user_data);
}

static void on_bg_color_fixed_changed (GSettings *settings, char *key, gpointer user_data)
{
    LypadPreferencesEditor *editor = LYPAD_PREFERENCES_EDITOR (user_data);
    gboolean active = g_settings_get_boolean (settings, key);
    if (active)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (editor->bg_color_fixed), TRUE);
    else
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (editor->bg_color_random), TRUE);
}

static void on_fg_color_automatic_changed (GSettings *settings, char *key, gpointer user_data)
{
    LypadPreferencesEditor *editor = LYPAD_PREFERENCES_EDITOR (user_data);
    gboolean active = g_settings_get_boolean (settings, key);
    if (active)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (editor->fg_color_automatic), TRUE);
    else
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (editor->fg_color_custom), TRUE);
}

static void on_sort_by_toggled (GtkToggleButton *button, gpointer user_data)
{
    if (!gtk_toggle_button_get_active (button))
        return;

    g_signal_handlers_block_by_func (button, on_sort_by_toggled, user_data);
    LypadPreferencesEditor *editor = LYPAD_PREFERENCES_EDITOR (user_data);
    if (button == GTK_TOGGLE_BUTTON (editor->sort_by_title))
        g_settings_set_enum (editor->settings, "sort-by", LYPAD_SORT_TITLE);
    else if (button == GTK_TOGGLE_BUTTON (editor->sort_by_creation))
        g_settings_set_enum (editor->settings, "sort-by", LYPAD_SORT_CREATED);
    else if (button == GTK_TOGGLE_BUTTON (editor->sort_by_modification))
        g_settings_set_enum (editor->settings, "sort-by", LYPAD_SORT_MODIFIED);
    g_signal_handlers_unblock_by_func (button, on_sort_by_toggled, user_data);
}

static void on_sort_by_changed (GSettings *settings, char *key, gpointer user_data)
{
    LypadPreferencesEditor *editor = LYPAD_PREFERENCES_EDITOR (user_data);
    int sort_by = g_settings_get_enum (settings, "sort-by");
    switch (sort_by)
    {
        case LYPAD_SORT_TITLE:
            gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (editor->sort_by_title), TRUE);
            break;

        case LYPAD_SORT_CREATED:
            gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (editor->sort_by_creation), TRUE);
            break;

        case LYPAD_SORT_MODIFIED:
            gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (editor->sort_by_modification), TRUE);
            break;
    }
}

static void on_sort_ascending_toggled (GtkToggleButton *button, gpointer user_data)
{
    if (!gtk_toggle_button_get_active (button))
        return;

    g_signal_handlers_block_by_func (button, on_sort_ascending_toggled, user_data);
    LypadPreferencesEditor *editor = LYPAD_PREFERENCES_EDITOR (user_data);
    if (button == GTK_TOGGLE_BUTTON (editor->sort_ascending))
        g_settings_set_boolean (editor->settings, "sort-ascending", TRUE);
    else if (button == GTK_TOGGLE_BUTTON (editor->sort_descending))
        g_settings_set_boolean (editor->settings, "sort-ascending", FALSE);
    g_signal_handlers_unblock_by_func (button, on_sort_ascending_toggled, user_data);
}

static void on_sort_ascending_changed (GSettings *settings, char *key, gpointer user_data)
{
    LypadPreferencesEditor *editor = LYPAD_PREFERENCES_EDITOR (user_data);
    gboolean sort_ascending = g_settings_get_boolean (settings, "sort-ascending");
    if (sort_ascending)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (editor->sort_ascending), TRUE);
    else
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (editor->sort_descending), TRUE);
}

GVariant *set_mapping_color (const GValue *value, const GVariantType *expected_type, gpointer user_data)
{
    if (g_variant_type_equal (expected_type, G_VARIANT_TYPE_STRING))
    {
        char *color_str = gdk_rgba_to_string (g_value_get_boxed (value));
        GVariant *variant = g_variant_new_string (color_str);
        g_free (color_str);
        return variant;
    }
    return NULL;
}

gboolean get_mapping_color (GValue *value, GVariant *variant, gpointer user_data)
{
    if (G_VALUE_HOLDS_BOXED (value))
    {
        GdkRGBA color;
        if (gdk_rgba_parse (&color, g_variant_get_string (variant, NULL)))
        {
            g_value_set_boxed (value, &color);
            return TRUE;
        }
    }
    return FALSE;
}

GVariant *set_mapping_font (const GValue *value, const GVariantType *expected_type, gpointer user_data)
{
    if (g_variant_type_equal (expected_type, G_VARIANT_TYPE_STRING))
    {
        char *font_desc = pango_font_description_to_string (g_value_get_boxed (value));
        GVariant *variant = g_variant_new_string (font_desc);
        g_free (font_desc);
        return variant;
    }
    return NULL;
}

gboolean get_mapping_font (GValue *value, GVariant *variant, gpointer user_data)
{
    if (G_VALUE_HOLDS_BOXED (value))
    {
        PangoFontDescription *font_desc = pango_font_description_from_string (g_variant_get_string (variant, NULL));
        const char *family = pango_font_description_get_family (font_desc);
        int size = pango_font_description_get_size (font_desc);
        if (!family || !size)
        {
            GtkSettings *settings = gtk_settings_get_default ();
            if (settings)
            {
                char *font_default;
                g_object_get (settings, "gtk-font-name", &font_default, NULL);
                PangoFontDescription *font_desc_default = pango_font_description_from_string (font_default);
                g_free (font_default);

                if (!family)
                    pango_font_description_set_family (font_desc, pango_font_description_get_family (font_desc_default));
                if (!size)
                {
                    int size_default = pango_font_description_get_size (font_desc_default);
                    if (pango_font_description_get_size_is_absolute (font_desc_default))
                        pango_font_description_set_absolute_size (font_desc, size_default);
                    else
                        pango_font_description_set_size (font_desc, size_default);
                }
                pango_font_description_free (font_desc_default);
            }
        }
        g_value_set_boxed (value, font_desc);
        pango_font_description_free (font_desc);
        return TRUE;
    }
    return FALSE;
}

static void lypad_preferences_editor_init (LypadPreferencesEditor *editor)
{
    gtk_widget_init_template (GTK_WIDGET (editor));

    for (int i = 0; i < (int)G_N_ELEMENTS (ROW_LABELS); ++i)
        lypad_page_add_panel_row (LYPAD_PAGE (editor), GINT_TO_POINTER (i));

    // Interface
    editor->settings = g_settings_new ("org.gnome.lypad");
    g_settings_bind (editor->settings, "show-tray-icon", editor->tray_icon_switch, "state", G_SETTINGS_BIND_DEFAULT | G_SETTINGS_BIND_NO_SENSITIVITY);

    gtk_switch_set_active (editor->autostart_switch, lypad_utils_get_autostart_enabled ());
    g_signal_connect (editor->autostart_switch, "state-set", G_CALLBACK (on_autostart_switch_state_set), editor);

    int sort_by = g_settings_get_enum (editor->settings, "sort-by");
    switch (sort_by)
    {
        case LYPAD_SORT_TITLE:
            gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (editor->sort_by_title), TRUE);
            break;

        case LYPAD_SORT_CREATED:
            gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (editor->sort_by_creation), TRUE);
            break;

        case LYPAD_SORT_MODIFIED:
            gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (editor->sort_by_modification), TRUE);
            break;
    }
    g_signal_connect (editor->sort_by_title, "toggled", G_CALLBACK (on_sort_by_toggled), editor);
    g_signal_connect (editor->sort_by_creation, "toggled", G_CALLBACK (on_sort_by_toggled), editor);
    g_signal_connect (editor->sort_by_modification, "toggled", G_CALLBACK (on_sort_by_toggled), editor);
    g_signal_connect (editor->settings, "changed::sort-by", G_CALLBACK (on_sort_by_changed), editor);

    gboolean sort_ascending = g_settings_get_boolean (editor->settings, "sort-ascending");
    if (sort_ascending)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (editor->sort_ascending), TRUE);
    else
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (editor->sort_descending), TRUE);
    g_signal_connect (editor->sort_ascending, "toggled", G_CALLBACK (on_sort_ascending_toggled), editor);
    g_signal_connect (editor->sort_descending, "toggled", G_CALLBACK (on_sort_ascending_toggled), editor);
    g_signal_connect (editor->settings, "changed::sort-ascending", G_CALLBACK (on_sort_ascending_changed), editor);

    // Note defaults
    editor->default_note_settings = g_settings_new ("org.gnome.lypad.defaults");
    LypadColorPalette *palette = lypad_color_store_get_palette (LYPAD_NOTE_COLOR_SET);
    for (int i = 0; palette->row_names[i] != NULL; ++i)
    {
        char *id = g_utf8_strdown (palette->row_names[i], -1);
        gtk_combo_box_text_append (GTK_COMBO_BOX_TEXT (editor->bg_color_pool_combo), id, palette->row_names[i]);
        g_free (id);
    }
    g_settings_bind (editor->default_note_settings, "bg-color-pool",
                     editor->bg_color_pool_combo, "active-id", G_SETTINGS_BIND_DEFAULT | G_SETTINGS_BIND_NO_SENSITIVITY);

    gboolean bg_fixed_active = g_settings_get_boolean (editor->default_note_settings, "bg-color-fixed");
    if (bg_fixed_active)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (editor->bg_color_fixed), TRUE);
    else
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (editor->bg_color_random), TRUE);
    gtk_widget_set_sensitive (editor->bg_color_chooser, bg_fixed_active);
    gtk_widget_set_sensitive (editor->bg_color_pool_combo, !bg_fixed_active);

    gboolean fg_auto_active = g_settings_get_boolean (editor->default_note_settings, "fg-automatic");
    if (fg_auto_active)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (editor->fg_color_automatic), TRUE);
    else
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (editor->fg_color_custom), TRUE);
    gtk_widget_set_sensitive (editor->fg_color_chooser, !fg_auto_active);

    g_settings_bind_with_mapping (editor->default_note_settings, "bg-color",
                                  editor->bg_color_chooser, "rgba", G_SETTINGS_BIND_DEFAULT | G_SETTINGS_BIND_NO_SENSITIVITY,
                                  get_mapping_color, set_mapping_color, NULL, NULL);
    g_settings_bind_with_mapping (editor->default_note_settings, "fg-color",
                                  editor->fg_color_chooser, "rgba", G_SETTINGS_BIND_DEFAULT | G_SETTINGS_BIND_NO_SENSITIVITY,
                                  get_mapping_color, set_mapping_color, NULL, NULL);

    g_signal_connect (editor->bg_color_fixed, "toggled", G_CALLBACK (on_bg_color_toggled), editor);
    g_signal_connect (editor->bg_color_random, "toggled", G_CALLBACK (on_bg_color_toggled), editor);
    g_signal_connect (editor->default_note_settings, "changed::bg-color-fixed", G_CALLBACK (on_bg_color_fixed_changed), editor);

    g_signal_connect (editor->fg_color_automatic, "toggled", G_CALLBACK (on_fg_color_toggled), editor);
    g_signal_connect (editor->fg_color_custom, "toggled", G_CALLBACK (on_fg_color_toggled), editor);
    g_signal_connect (editor->default_note_settings, "changed::fg-automatic", G_CALLBACK (on_fg_color_automatic_changed), editor);

    g_settings_bind_with_mapping (editor->default_note_settings, "font",
                                  editor->text_font, "font-desc", G_SETTINGS_BIND_DEFAULT | G_SETTINGS_BIND_NO_SENSITIVITY,
                                  get_mapping_font, set_mapping_font, NULL, NULL);

    GSList *themes = lypad_list_themes ();
    for (GSList *l = themes; l != NULL; l = l->next)
    {
        const char *id = lypad_theme_get_name (l->data);
        const char *name = lypad_theme_get_display_name (l->data);
        gtk_combo_box_text_append (GTK_COMBO_BOX_TEXT (editor->theme_combo), id, name);
    }
    g_slist_free (themes);
    g_settings_bind (editor->default_note_settings, "theme",
                     editor->theme_combo, "active-id", G_SETTINGS_BIND_DEFAULT | G_SETTINGS_BIND_NO_SENSITIVITY);

    // Advanced settings
    g_settings_bind (editor->settings, "notes-folder", editor->notes_folder_label, "label", G_SETTINGS_BIND_DEFAULT | G_SETTINGS_BIND_NO_SENSITIVITY);
    g_signal_connect (editor->notes_folder_button, "clicked", G_CALLBACK (on_notes_folder_button_clicked), editor);
}

static void lypad_preferences_editor_destroy (GtkWidget *widget)
{
    LypadPreferencesEditor *editor = LYPAD_PREFERENCES_EDITOR (widget);
    g_clear_object (&editor->settings);
    g_clear_object (&editor->default_note_settings);
    GTK_WIDGET_CLASS (lypad_preferences_editor_parent_class)->destroy (widget);
}

static void lypad_preferences_editor_class_init (LypadPreferencesEditorClass *klass)
{
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
    LypadPageClass *page_class = LYPAD_PAGE_CLASS (klass);

    widget_class->destroy     = lypad_preferences_editor_destroy;
    page_class->init_row      = lypad_preferences_editor_init_row;
    page_class->row_activated = lypad_preferences_editor_row_activated;

    gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/lypad/ui/lypad-preferences-editor.glade");
    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, stack);
    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, autostart_switch);
    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, tray_icon_switch);
    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, sort_by_title);
    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, sort_by_creation);
    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, sort_by_modification);
    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, sort_ascending);
    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, sort_descending);

    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, bg_color_fixed);
    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, bg_color_chooser);
    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, bg_color_random);
    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, bg_color_pool_combo);
    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, fg_color_automatic);
    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, fg_color_custom);
    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, fg_color_chooser);
    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, text_font);
    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, theme_combo);

    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, notes_folder_button);
    gtk_widget_class_bind_template_child (widget_class, LypadPreferencesEditor, notes_folder_label);
}

GtkWidget *lypad_preferences_editor_new ()
{
    return g_object_new (LYPAD_TYPE_PREFERENCES_EDITOR, NULL);
}
