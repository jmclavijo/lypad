/*
 *  lypad-theme.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-theme.h"
#include "themes/lypad-theme-default.h"
#include "lypad-toggle-button.h"
#include "lypad-title-view.h"

G_DEFINE_TYPE (LypadTheme, lypad_theme, G_TYPE_OBJECT)

static GList *registered_themes = NULL;

static void lypad_theme_init (LypadTheme *theme)
{
}

//TODO: Check that either none or both of the "build_layout" and "destroy_layout" functions are overridden.

static void lypad_theme_class_init (LypadThemeClass *klass)
{
    klass->get_display_name = NULL;
    klass->init_layout      = NULL;
    klass->destroy_layout   = NULL;
}

static inline GtkWidget *lypad_theme_build_lock_button ()
{
    GtkWidget *button = lypad_toggle_button_new ();
    gtk_widget_set_size_request (button, 32, 32);

    GtkWidget *image_on = gtk_image_new_from_icon_name ("lypad-lock-on-symbolic", GTK_ICON_SIZE_LARGE_TOOLBAR);
    GtkWidget *image_off = gtk_image_new_from_icon_name ("lypad-lock-off-symbolic", GTK_ICON_SIZE_LARGE_TOOLBAR);
    lypad_toggle_button_set_images (LYPAD_TOGGLE_BUTTON (button), image_off, image_on);

    GtkStyleContext *context = gtk_widget_get_style_context (button);
    gtk_style_context_add_class (context, "LypadHeaderButton");

    return GTK_WIDGET (button);
}

static void lypad_theme_default_destroy_layout (LypadTheme *theme, LayoutData *layout)
{
    g_object_unref (layout->layout_base);
}

static inline GtkWidget *lypad_theme_build_drag_button ()
{
    GtkWidget *button = lypad_button_new ();
    gtk_widget_set_size_request (button, 32, 32);

    GtkWidget *image = gtk_image_new_from_icon_name ("lypad-drag-symbolic", GTK_ICON_SIZE_LARGE_TOOLBAR);
    gtk_image_set_pixel_size (GTK_IMAGE (image), 24);
    gtk_container_add (GTK_CONTAINER (button), image);

    GtkStyleContext *context = gtk_widget_get_style_context (button);
    gtk_style_context_add_class (context, "LypadHeaderButton");

    return button;
}

static void lypad_theme_default_init_layout (LypadTheme *theme, LayoutData *layout, LypadNote *note)
{
    g_type_ensure (LYPAD_TYPE_TOGGLE_BUTTON);
    GtkBuilder *builder = gtk_builder_new_from_resource ("/org/gnome/lypad/themes/default-layout.glade");
    layout->layout_base = GTK_WIDGET (g_object_ref_sink (gtk_builder_get_object (builder, "layout_base")));
    layout->menu_button = LYPAD_TOGGLE_BUTTON (gtk_builder_get_object (builder, "menu_button"));
    GtkWidget *content_container = GTK_WIDGET (gtk_builder_get_object (builder, "content_container"));
    GtkWidget *header_box = GTK_WIDGET (gtk_builder_get_object (builder, "header_box"));

    layout->lock_button = LYPAD_TOGGLE_BUTTON (lypad_theme_build_lock_button ());
    layout->drag_widget = lypad_theme_build_drag_button ();
    gtk_box_pack_start (GTK_BOX (header_box), GTK_WIDGET (layout->lock_button), FALSE, FALSE, 0);
    gtk_box_pack_start (GTK_BOX (header_box), GTK_WIDGET (layout->drag_widget), FALSE, FALSE, 0);

    layout->title_view = LYPAD_TEXT_VIEW (lypad_title_view_new ());
    lypad_text_view_set_buffer (layout->title_view, lypad_note_get_title_buffer (note));
    gtk_text_view_set_justification (GTK_TEXT_VIEW (layout->title_view), GTK_JUSTIFY_CENTER);
    gtk_widget_set_valign (GTK_WIDGET (layout->title_view), GTK_ALIGN_CENTER);

    GtkWidget *title_container = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (title_container), GTK_POLICY_EXTERNAL, GTK_POLICY_NEVER);
    gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (title_container), GTK_SHADOW_NONE);
    gtk_container_add (GTK_CONTAINER (title_container), GTK_WIDGET (layout->title_view));
    gtk_box_pack_start (GTK_BOX (header_box), title_container, TRUE, TRUE, 0);

    layout->content_view = LYPAD_TEXT_VIEW (lypad_text_view_new ());
    lypad_text_view_set_buffer (LYPAD_TEXT_VIEW (layout->content_view), lypad_note_get_content_buffer (note));
    gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (layout->content_view), GTK_WRAP_WORD_CHAR);
    gtk_widget_set_margin_start  (GTK_WIDGET (layout->content_view), 10);
    gtk_widget_set_margin_end    (GTK_WIDGET (layout->content_view), 10);
    gtk_widget_set_margin_top    (GTK_WIDGET (layout->content_view), 10);
    gtk_widget_set_margin_bottom (GTK_WIDGET (layout->content_view), 10);
    gtk_container_add (GTK_CONTAINER (content_container), GTK_WIDGET (layout->content_view));

    g_object_unref (builder);
}

void lypad_theme_destroy_layout (LypadTheme *theme, LayoutData *layout)
{
    g_return_if_fail (LYPAD_IS_THEME (theme));
    LypadThemeClass *klass = LYPAD_THEME_GET_CLASS (theme);
    if (klass->destroy_layout)
        klass->destroy_layout (theme, layout);
    else
        lypad_theme_default_destroy_layout (theme, layout);
    g_free (layout);
}

LayoutData *lypad_theme_build_layout (LypadTheme *theme, LypadNote *note)
{
    g_return_val_if_fail (LYPAD_IS_THEME (theme), NULL);
    g_return_val_if_fail (LYPAD_IS_NOTE (note), NULL);
    LypadThemeClass *klass = LYPAD_THEME_GET_CLASS (theme);
    LayoutData *layout = g_new0 (LayoutData, 1);
    if (klass->init_layout)
        klass->init_layout (theme, layout, note);
    else
        lypad_theme_default_init_layout (theme, layout, note);

    if (!layout->layout_base)
        g_error ("Theme \"%s\" ::init_layout did not set the layout_base member of layout", lypad_theme_get_display_name (theme));
    if (!layout->content_view)
        g_error ("Theme \"%s\" ::init_layout did not set the content_view member of layout", lypad_theme_get_display_name (theme));
    
    GtkStyleContext *context = gtk_widget_get_style_context (layout->layout_base);
    gtk_style_context_add_class (context, "LypadLayoutBase");
    return layout;
}

gboolean lypad_theme_equal (LypadTheme *theme1, LypadTheme *theme2)
{
    const char *name1 = lypad_theme_get_name (theme1);
    const char *name2 = lypad_theme_get_name (theme2);
    return g_strcmp0 (name1, name2) == 0;
}

const char *lypad_theme_get_name (LypadTheme *theme)
{
    g_return_val_if_fail (LYPAD_IS_THEME (theme), NULL);
    LypadThemeClass *klass = LYPAD_THEME_GET_CLASS (theme);
    if (klass->get_name)
        return klass->get_name (theme);
    else
        g_error ("LypadTheme::get_name not implemented for \"%s\".", G_OBJECT_TYPE_NAME (theme));
}

const char *lypad_theme_get_display_name (LypadTheme *theme)
{
    g_return_val_if_fail (LYPAD_IS_THEME (theme), NULL);
    LypadThemeClass *klass = LYPAD_THEME_GET_CLASS (theme);
    if (klass->get_display_name)
        return klass->get_display_name (theme);
    else
        return NULL;
}

LypadTheme *lypad_theme_get_for_name (const char *name)
{
    LypadTheme *default_theme = lypad_theme_get_default ();
    if (g_strcmp0 (lypad_theme_get_name (default_theme), name) == 0)
        return default_theme;

    for (GList *l = registered_themes; l != NULL; l = l->next)
        if (g_strcmp0 (lypad_theme_get_name (l->data), name) == 0)
            return l->data;
    return NULL;
}

LypadTheme *lypad_theme_get_default ()
{
    static LypadTheme *default_theme = NULL;
    if (!default_theme)
        default_theme = lypad_theme_default_new ();
    return default_theme;
}

GSList *lypad_list_themes ()
{
    GSList *list = g_slist_prepend (NULL, lypad_theme_get_default ());
    for (GList *l = registered_themes; l != NULL; l = l->next)
        list = g_slist_append (list, l->data);
    return list;
}

void lypad_theme_register (GType type)
{
    GSList *themes = lypad_list_themes ();
    for (GSList *l = themes; l != NULL; l = l->next)
        if (G_TYPE_CHECK_INSTANCE_TYPE (l->data, type))
        {
            g_warning ("LypadTheme::register error: Type '%s' is already registered.", g_type_name (type));
            return;
        }
    g_slist_free (themes);
    LypadTheme *theme = g_object_new (type, NULL);
    registered_themes = g_list_append (registered_themes, theme);
}
