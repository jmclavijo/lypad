/*
 *  lypad-size-chooser.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-size-chooser.h"
#include "lypad-popover.h"
#include <stdlib.h>
#include <stdio.h>
#include <strings.h>

struct _LypadSizeChooser
{
    GtkBox        parent;
    GtkListStore *size_store;
    LypadPopover *popover;
    GtkWidget    *entry;
    GtkWidget    *button;
    int           current_size;
};

G_DEFINE_TYPE (LypadSizeChooser, lypad_size_chooser, GTK_TYPE_BOX)

enum
{
	SIGNAL_SIZE_CHANGED,
	SIGNAL_LAST
};

static guint signals[SIGNAL_LAST] = {0};
#define MIN_SIZE 4
#define MAX_SIZE 128

static int parse_size_to_pt (const char *size_str)
{
    int size = -1;
    if (sscanf (size_str, "%d", &size) < 1)
        size = -1;

    if (size < MIN_SIZE && size != -1)
        size = MIN_SIZE;
    else if (size > MAX_SIZE)
        size = MAX_SIZE;
    return size;
}

static void on_button_clicked (GtkButton *button, gpointer user_data)
{
    LypadSizeChooser *chooser = LYPAD_SIZE_CHOOSER (user_data);
    lypad_popover_popup (chooser->popover);
}

static void on_entry_activate (GtkEntry *entry, gpointer user_data)
{
    LypadSizeChooser *chooser = LYPAD_SIZE_CHOOSER (user_data);
    const char *size_str = gtk_entry_get_text (entry);
    int size = parse_size_to_pt (size_str);
    if (size != -1)
    {
        char *text = g_strdup_printf ("%d pt", size);
        gtk_entry_set_text (entry, text);
        g_free (text);
        chooser->current_size = size;
        g_signal_emit (chooser, signals[SIGNAL_SIZE_CHANGED], 0, size);
    }
    else
    {
        char *text = g_strdup_printf ("%d pt", chooser->current_size);
        gtk_entry_set_text (entry, text);
        g_free (text);
    }
}

static gboolean on_entry_focus_out (GtkWidget *widget, GdkEventFocus *event, gpointer user_data)
{
    LypadSizeChooser *chooser = LYPAD_SIZE_CHOOSER (user_data);
    if (chooser->current_size)
    {
        char *size_str = g_strdup_printf ("%d pt", chooser->current_size);
        gtk_entry_set_text (GTK_ENTRY (chooser->entry), size_str);
        g_free (size_str);
    }
    else
        gtk_entry_set_text (GTK_ENTRY (chooser->entry), "");
    gtk_editable_set_position (GTK_EDITABLE (chooser->entry), 0);
    return FALSE;
}

static void on_tree_view_row_activated (GtkTreeView *tree_view, GtkTreePath *path, GtkTreeViewColumn *column, gpointer user_data)
{
    LypadSizeChooser *chooser = LYPAD_SIZE_CHOOSER (user_data);
    lypad_popover_popdown (chooser->popover);

    GtkTreeIter iter;
    GtkTreeModel *model = gtk_tree_view_get_model (tree_view);

    if (!gtk_tree_model_get_iter (model, &iter, path))
        return;

    GValue value = G_VALUE_INIT;
    gtk_tree_model_get_value (model, &iter, 0, &value);
    gtk_entry_set_text (GTK_ENTRY (chooser->entry), g_value_get_string (&value));
    g_value_unset (&value);
    gtk_tree_model_get_value (model, &iter, 1, &value);
    chooser->current_size = g_value_get_int (&value);
    g_value_unset (&value);
    g_signal_emit (chooser, signals[SIGNAL_SIZE_CHANGED], 0, chooser->current_size);
}

static void on_popover_closed (GtkPopover *popover, gpointer user_data)
{
    LypadSizeChooser *chooser = LYPAD_SIZE_CHOOSER (user_data);
    g_signal_handlers_block_by_func (chooser->button, on_button_clicked, chooser);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (chooser->button), FALSE);
    g_signal_handlers_unblock_by_func (chooser->button, on_button_clicked, chooser);
}

static GtkTreeModel *lypad_size_chooser_init_popup (LypadSizeChooser *chooser)
{
    GtkListStore *store = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
    const char *size_list_str[] = {"6 pt", "8 pt", "10 pt", "12 pt", "14 pt", "16 pt", "20 pt", "24 pt", "28 pt", "32 pt", "36 pt", "48 pt", "64 pt"};
    const int   size_list_int[] = {6, 8, 10, 12, 14, 16, 20, 24, 28, 32, 36, 48, 64};
    for (guint i = 0; i < G_N_ELEMENTS (size_list_int); ++i)
        gtk_list_store_insert_with_values (store, NULL, -1, 0, size_list_str[i], 1, size_list_int[i], -1);

    GtkWidget *tree_view = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
    GtkCellRenderer *renderer = gtk_cell_renderer_text_new ();
    GtkTreeViewColumn *column = gtk_tree_view_column_new_with_attributes ("Size", renderer, "text", 0, NULL);
    gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view), column);
    gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (tree_view), FALSE);
    gtk_tree_view_set_hover_selection (GTK_TREE_VIEW (tree_view), TRUE);
    gtk_tree_view_set_enable_search (GTK_TREE_VIEW (tree_view), FALSE);
    gtk_tree_view_set_activate_on_single_click (GTK_TREE_VIEW (tree_view), TRUE);

    chooser->popover = lypad_popover_new (GTK_WIDGET (chooser));
    GtkWidget *scrolled_window = gtk_scrolled_window_new (NULL, NULL);
    gtk_widget_set_size_request (GTK_WIDGET (chooser->popover), -1, 300);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);

    gtk_container_add (GTK_CONTAINER (chooser->popover), scrolled_window);
    gtk_container_add (GTK_CONTAINER (scrolled_window), tree_view);
    gtk_widget_show_all (scrolled_window);

    g_signal_connect (tree_view, "row-activated", G_CALLBACK (on_tree_view_row_activated), chooser);
    g_signal_connect (chooser->popover, "closed", G_CALLBACK (on_popover_closed), chooser);

    return GTK_TREE_MODEL (store);
}

static void lypad_size_chooser_init (LypadSizeChooser *chooser)
{
    gtk_style_context_add_class (gtk_widget_get_style_context (GTK_WIDGET (chooser)), "linked");
    GtkTreeModel *model = lypad_size_chooser_init_popup (chooser);
    g_object_unref (model);

    chooser->current_size = 12;
    chooser->entry = gtk_entry_new ();
    gtk_entry_set_width_chars (GTK_ENTRY (chooser->entry), 5);

    chooser->button = gtk_toggle_button_new ();
    gtk_widget_set_can_focus (chooser->button, FALSE);

    GtkWidget *arrow = gtk_image_new_from_icon_name ("pan-down-symbolic", GTK_ICON_SIZE_BUTTON);
    gtk_container_add (GTK_CONTAINER (chooser->button), arrow);
    gtk_container_add (GTK_CONTAINER (chooser), chooser->entry);
    gtk_container_add (GTK_CONTAINER (chooser), chooser->button);

    g_signal_connect (chooser->button, "clicked", G_CALLBACK (on_button_clicked), chooser);
    g_signal_connect (chooser->entry, "activate", G_CALLBACK (on_entry_activate), chooser);
    g_signal_connect (chooser->entry, "focus-out-event", G_CALLBACK (on_entry_focus_out), chooser);
}

static void lypad_size_chooser_class_init (LypadSizeChooserClass *klass)
{
	signals[SIGNAL_SIZE_CHANGED] = g_signal_new ("size-changed", G_TYPE_FROM_CLASS (klass),
											     G_SIGNAL_RUN_LAST, 0, NULL, NULL,
											     g_cclosure_marshal_VOID__INT, G_TYPE_NONE, 1, G_TYPE_INT);
}

void lypad_size_chooser_set_size (LypadSizeChooser *chooser, int size)
{
    if (size)
    {
        char *size_str = g_strdup_printf ("%d pt", size);
        gtk_entry_set_text (GTK_ENTRY (chooser->entry), size_str);
        g_free (size_str);
    }
    else
        gtk_entry_set_text (GTK_ENTRY (chooser->entry), "");
    chooser->current_size = size;
}

LypadSizeChooser *lypad_size_chooser_new ()
{
    LypadSizeChooser *chooser = g_object_new (LYPAD_TYPE_SIZE_CHOOSER, "orientation", GTK_ORIENTATION_HORIZONTAL, NULL);
    return chooser;
}
