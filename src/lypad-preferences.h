/*
 *  lypad-preferences.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define LYPAD_TYPE_PREFERENCES (lypad_preferences_get_type ())
G_DECLARE_FINAL_TYPE (LypadPreferences, lypad_preferences, LYPAD, PREFERENCES, GObject)

LypadPreferences  *lypad_preferences_new               (void);
char             **lypad_preferences_get_note_paths    (LypadPreferences *preferences);
void               lypad_preferences_save              (LypadPreferences *preferences);
GKeyFile          *lypad_preferences_get_key_file      (LypadPreferences *preferences);
void               lypad_preferences_set_modified      (LypadPreferences *preferences);
const char        *lypad_preferences_get_data_path     (LypadPreferences *preferences);

G_END_DECLS
