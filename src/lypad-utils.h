/*
 *  lypad-utils.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

char     *lypad_utils_format_time           (time_t time) G_GNUC_WARN_UNUSED_RESULT;

gboolean  lypad_utils_get_autostart_enabled ();
gboolean  lypad_utils_set_autostart_enabled (gboolean enable);

void      lypad_utils_prompt_delete_notes   (GSList *notes, GtkWindow *window);
void      lypad_utils_prompt_add_tags       (GSList *notes, GtkWindow *window);

char     *lypad_utils_path_expand_tilde     (const char *path) G_GNUC_WARN_UNUSED_RESULT;
char     *lypad_utils_path_shrink_tilde     (const char *path) G_GNUC_WARN_UNUSED_RESULT;

gboolean  lypad_utils_magic_file_check      (GFile *file);

char     *lypad_utils_strdup_printf         (const char *format, ...) G_GNUC_WARN_UNUSED_RESULT;

G_END_DECLS
