/*
 *  lypad-tag-widget.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include "lypad-tag.h"

G_BEGIN_DECLS

#define LYPAD_TYPE_TAG_WIDGET (lypad_tag_widget_get_type ())
G_DECLARE_FINAL_TYPE (LypadTagWidget, lypad_tag_widget, LYPAD, TAG_WIDGET, GtkBin)

typedef enum
{
    LYPAD_TAG_MINIMAL_SIZE,
    LYPAD_TAG_DEFAULT_SIZE,
    LYPAD_TAG_FULL_SIZE
} LypadTagMode;

GtkWidget *lypad_tag_widget_new             (LypadTag *tag);
LypadTag  *lypad_tag_widget_get_tag         (LypadTagWidget *widget);                           // [transfer none]
void       lypad_tag_widget_set_mode        (LypadTagWidget *widget, LypadTagMode mode);
void       lypad_tag_widget_set_interactive (LypadTagWidget *widget, gboolean interactive);

G_END_DECLS
