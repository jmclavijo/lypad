/*
 *  lypad-title-buffer.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-title-buffer.h"
#include <string.h>

struct _LypadTitleBuffer
{
    LypadTextBuffer  parent;
};

G_DEFINE_TYPE (LypadTitleBuffer, lypad_title_buffer, LYPAD_TYPE_TEXT_BUFFER)

static void lypad_title_buffer_init (LypadTitleBuffer *buffer)
{
}

static char *remove_new_lines (const char *str, int *len)
{
    char *str2 = g_new (char, *len);
    memcpy (str2, str, *len);
    for (int i = 0; i < *len; ++i)
    {
        if (str2[i] == '\n')
        {
            *len -= 1;
            for (int j = i; j < *len; ++j)
                str2[j] = str2[j + 1];
        }
    }
    return str2;
}

static void lypad_title_buffer_insert_text (GtkTextBuffer *text_buffer, GtkTextIter *location, const char *text, gint len)
{
    if (len < 0)
        len = strlen (text);
    char *text_modified = remove_new_lines (text, &len);
    if (len > 0)
        GTK_TEXT_BUFFER_CLASS (lypad_title_buffer_parent_class)->insert_text (text_buffer, location, text_modified, len);
    g_free (text_modified);
}

static void lypad_title_buffer_class_init (LypadTitleBufferClass *klass)
{
    GtkTextBufferClass *buffer_class = GTK_TEXT_BUFFER_CLASS (klass);

    buffer_class->insert_text = lypad_title_buffer_insert_text;
}

LypadTextBuffer *lypad_title_buffer_new ()
{
    return g_object_new (LYPAD_TYPE_TITLE_BUFFER, NULL);
}
