/*
 *  lypad-notes-manager.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib.h>
#include "lypad-note.h"

G_BEGIN_DECLS

#define LYPAD_TYPE_NOTES_MANAGER (lypad_notes_manager_get_type ())
G_DECLARE_FINAL_TYPE (LypadNotesManager, lypad_notes_manager, LYPAD, NOTES_MANAGER, GObject)

LypadNotesManager *lypad_notes_manager_new             ();
GSList            *lypad_notes_manager_get_notes       (LypadNotesManager *manager);                     // [transfer container]
void               lypad_notes_manager_save_all        (LypadNotesManager *manager);
void               lypad_notes_manager_import          (LypadNotesManager *manager, LypadNote *note);
int                lypad_notes_manager_get_note_id     (LypadNotesManager *manager, LypadNote *note);
LypadNote         *lypad_notes_manager_get_note_for_id (LypadNotesManager *manager, int id);

G_END_DECLS
