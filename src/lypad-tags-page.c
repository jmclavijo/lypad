/*
 *  lypad-tags-page.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-tags-page.h"
#include "lypad-application.h"
#include "lypad-tag-widget.h"
#include "lypad-note-widget.h"
#include "lypad-notes-list.h"
#include "lypad-notes-manager.h"

typedef struct _PanelRowData PanelRowData;

struct _LypadTagsPage
{
    LypadPage       parent;
    GtkWidget      *viewport;
    LypadPageRow   *active_row;
    gulong          tag_created_handler;
    GtkWidget      *select_toggle_button;
};

G_DEFINE_TYPE (LypadTagsPage, lypad_tags_page, LYPAD_TYPE_PAGE)

enum
{
    SIGNAL_TAG_SET,
    SIGNAL_LAST
};

static guint signals[SIGNAL_LAST] = {0};

struct _PanelRowData
{
    LypadTagsPage *parent;
    GtkWidget     *notes_list;
    LypadTag      *tag;
    gulong         tag_destroy_handler;
    gulong         tag_added_handler;
    gulong         tag_removed_handler;
};

static void panel_row_data_free (gpointer row_data)
{
    PanelRowData *data = row_data;
    gtk_widget_destroy (data->notes_list);
    g_object_unref (data->notes_list);
    g_signal_handler_disconnect (data->tag, data->tag_destroy_handler);
    g_signal_handler_disconnect (data->tag, data->tag_added_handler);
    g_signal_handler_disconnect (data->tag, data->tag_removed_handler);
    g_object_unref (data->tag);
    g_free (data);
}

static void on_tag_destroy (LypadTag *tag, gpointer user_data)
{
    LypadPageRow *row = user_data;
    PanelRowData *data = lypad_page_row_get_data (row);
    lypad_page_remove_panel_row (LYPAD_PAGE (data->parent), row);
}

static void on_tag_added (LypadTag *tag, LypadNote *note, gpointer user_data)
{
    LypadPageRow *row = user_data;
    PanelRowData *data = lypad_page_row_get_data (row);
    lypad_notes_list_add (LYPAD_NOTES_LIST (data->notes_list), lypad_note_widget_new (note));
}

static void on_tag_removed (LypadTag *tag, LypadNote *note, gpointer user_data)
{
    LypadPageRow *row = user_data;
    PanelRowData *data = lypad_page_row_get_data (row);
    lypad_notes_list_remove_by_note (LYPAD_NOTES_LIST (data->notes_list), note);
}

static void lypad_tags_page_init_row (LypadPage *page, LypadPageRow *row, gpointer user_data)
{
    LypadTagsPage *tags_page = LYPAD_TAGS_PAGE (page);
    PanelRowData *data = g_new (PanelRowData, 1);
    LypadTag *tag = LYPAD_TAG (user_data);

    lypad_page_row_set_data (row, data, panel_row_data_free);
    data->parent = tags_page;
    data->tag = g_object_ref (tag);
    data->tag_destroy_handler = g_signal_connect (tag, "destroy", G_CALLBACK (on_tag_destroy), row);
    data->tag_added_handler = g_signal_connect (tag, "added", G_CALLBACK (on_tag_added), row);
    data->tag_removed_handler = g_signal_connect (tag, "removed", G_CALLBACK (on_tag_removed), row);

    data->notes_list = lypad_notes_list_new ();
    g_object_ref_sink (data->notes_list);
    /*gtk_box_set_spacing (GTK_BOX (data->notes_list), 15);
    gtk_widget_set_margin_start (data->notes_list, 15);
    gtk_widget_set_margin_end (data->notes_list, 15);
    gtk_widget_set_margin_top (data->notes_list, 15);
    gtk_widget_set_margin_bottom (data->notes_list, 15);
    gtk_widget_set_valign (data->notes_list, GTK_ALIGN_START);*/

    LypadNotesManager *manager = lypad_application_get_notes_manager (LYPAD_APPLICATION_DEFAULT);
    GSList *notes = lypad_notes_manager_get_notes (manager);
    for (GSList *l = notes; l != NULL; l = l->next)
    {
        if (lypad_note_has_tag (l->data, tag))
            lypad_notes_list_add (LYPAD_NOTES_LIST (data->notes_list), lypad_note_widget_new (l->data));
    }

    GtkWidget *tag_widget = lypad_tag_widget_new (tag);
    gtk_container_add (GTK_CONTAINER (row), tag_widget);

    if (tags_page->select_toggle_button)
        lypad_notes_list_set_toggle (LYPAD_NOTES_LIST (data->notes_list), tags_page->select_toggle_button);
}

static void lypad_tags_page_release_row (LypadPage *page, LypadPageRow *row)
{
    LypadTagsPage *tags_page = LYPAD_TAGS_PAGE (page);
    if (row == tags_page->active_row)
    {
        tags_page->active_row = NULL;
        lypad_page_go_back (page);
    }
}

static void lypad_tags_page_go_back (LypadPage *page)
{
    LypadTagsPage *tags_page = LYPAD_TAGS_PAGE (page);
    tags_page->active_row = NULL;
    GtkWidget *child = gtk_bin_get_child (GTK_BIN (tags_page->viewport));
    if (child)
        gtk_container_remove (GTK_CONTAINER (tags_page->viewport), child);
}

static void lypad_tags_page_row_activated (LypadPage *page, LypadPageRow *row)
{
    LypadTagsPage *tags_page = LYPAD_TAGS_PAGE (page);
    if (tags_page->active_row == row)
        return;

    GtkWidget *child = gtk_bin_get_child (GTK_BIN (tags_page->viewport));
    if (child)
        gtk_container_remove (GTK_CONTAINER (tags_page->viewport), child);
    PanelRowData *data = lypad_page_row_get_data (row);

    tags_page->active_row = row;
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (tags_page->select_toggle_button), FALSE);

    gtk_container_add (GTK_CONTAINER (tags_page->viewport), data->notes_list);
    gtk_widget_show_all (tags_page->viewport);

    g_signal_emit (tags_page, signals[SIGNAL_TAG_SET], 0, data->tag);
}

void lypad_tags_page_set_tag (LypadTagsPage *tags_page, LypadTag *tag)
{
    g_return_if_fail (LYPAD_IS_TAGS_PAGE (tags_page));
    GList *rows = lypad_page_get_panel_rows (LYPAD_PAGE (tags_page));
    for (GList *l = rows; l != NULL; l = l->next)
    {
        PanelRowData *data = lypad_page_row_get_data (l->data);
        if (data->tag == tag)
        {
            lypad_tags_page_row_activated (LYPAD_PAGE (tags_page), l->data);
            lypad_page_select_panel_row (LYPAD_PAGE (tags_page), l->data);
            break;
        }
    }
}

LypadTag *lypad_tags_page_get_tag (LypadTagsPage *tags_page)
{
    g_return_val_if_fail (LYPAD_IS_TAGS_PAGE (tags_page), NULL);
    LypadPageRow *row = lypad_page_get_selected_panel_row (LYPAD_PAGE (tags_page));
    if (row)
    {
        PanelRowData *data = lypad_page_row_get_data (row);
        if (data)
            return data->tag;
    }
    return NULL;
}

void lypad_tags_page_set_toggle (LypadTagsPage *tags_page, GtkWidget *toggle_button)
{
    g_return_if_fail (LYPAD_IS_TAGS_PAGE (tags_page));
    GList *rows = lypad_page_get_panel_rows (LYPAD_PAGE (tags_page));
    if (tags_page->select_toggle_button)
    {
        for (GList *l = rows; l != NULL; l = l->next)
        {
            PanelRowData *data = lypad_page_row_get_data (l->data);
            lypad_notes_list_set_toggle (LYPAD_NOTES_LIST (data->notes_list), NULL);
        }

        g_object_unref (tags_page->select_toggle_button);
        tags_page->select_toggle_button = NULL;
    }

    if (toggle_button)
    {
        tags_page->select_toggle_button = g_object_ref (toggle_button);
        for (GList *l = rows; l != NULL; l = l->next)
        {
            PanelRowData *data = lypad_page_row_get_data (l->data);
            lypad_notes_list_set_toggle (LYPAD_NOTES_LIST (data->notes_list), toggle_button);
        }
    }
    g_list_free (rows);
}

static void on_tag_created (GObject *signaler, LypadTag *tag, gpointer user_data)
{
    lypad_page_add_panel_row (LYPAD_PAGE (user_data), tag);
}

static int row_sort_function (LypadPageRow *row1, LypadPageRow *row2)
{
    PanelRowData *data1 = lypad_page_row_get_data (row1);
    PanelRowData *data2 = lypad_page_row_get_data (row2);
    return g_ascii_strcasecmp (lypad_tag_get_name (data1->tag), lypad_tag_get_name (data2->tag));
}

static void lypad_tags_page_init (LypadTagsPage *tags_page)
{
    GtkWidget *scrolled_window = gtk_scrolled_window_new (NULL, NULL);
    gtk_widget_set_hexpand (scrolled_window, TRUE);
    gtk_widget_set_vexpand (scrolled_window, TRUE);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);

    tags_page->viewport = gtk_viewport_new (NULL, NULL);
    gtk_container_add (GTK_CONTAINER (scrolled_window), tags_page->viewport);
    gtk_container_add (GTK_CONTAINER (tags_page), scrolled_window);

    tags_page->active_row = NULL;
    lypad_page_set_row_sort_func (LYPAD_PAGE (tags_page), row_sort_function);

    GSList *tags = lypad_tag_get_all ();
    for (GSList *l = tags; l != NULL; l = l->next)
        lypad_page_add_panel_row (LYPAD_PAGE (tags_page), l->data);
    g_slist_free (tags);
    tags_page->tag_created_handler = g_signal_connect (lypad_tag_get_signaler (), "tag-created", G_CALLBACK (on_tag_created), tags_page);

    tags_page->select_toggle_button = NULL;
}

static void lypad_tags_page_destroy (GtkWidget *widget)
{
    LypadTagsPage *page = LYPAD_TAGS_PAGE (widget);
    if (page->tag_created_handler)
    {
        g_signal_handler_disconnect (lypad_tag_get_signaler (), page->tag_created_handler);
        page->tag_created_handler = 0;
    }
    GTK_WIDGET_CLASS (lypad_tags_page_parent_class)->destroy (widget);
}

static void lypad_tags_page_class_init (LypadTagsPageClass *klass)
{
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
    LypadPageClass *page_class = LYPAD_PAGE_CLASS (klass);

    widget_class->destroy     = lypad_tags_page_destroy;
    page_class->init_row      = lypad_tags_page_init_row;
    page_class->release_row   = lypad_tags_page_release_row;
    page_class->row_activated = lypad_tags_page_row_activated;
    page_class->go_back       = lypad_tags_page_go_back;

    signals[SIGNAL_TAG_SET] = g_signal_new ("tag-set", G_TYPE_FROM_CLASS (klass),
                                                 G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                                 g_cclosure_marshal_VOID__OBJECT,
                                                 G_TYPE_NONE, 1, LYPAD_TYPE_TAG);
}

GtkWidget *lypad_tags_page_new ()
{
    return g_object_new (LYPAD_TYPE_TAGS_PAGE, NULL);
}
