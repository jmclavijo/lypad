/*
 *  lypad-notes-manager.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-notes-manager.h"
#include "lypad-application.h"
#include "lypad-preferences.h"
#include "lypad-utils.h"

struct _LypadNotesManager
{
    GObject    parent;
    GPtrArray *notes;
    GList     *available_ids;
};

G_DEFINE_TYPE (LypadNotesManager, lypad_notes_manager, G_TYPE_OBJECT)

enum
{
    SIGNAL_NOTE_ADDED,
    SIGNAL_LAST
};

static GQuark id_quark = 0;
static guint signals[SIGNAL_LAST] = {0};

static void on_note_destroy (LypadNote *note, gpointer user_data);

static GList *load_notes ()
{
    GList *notes = NULL;
    char **note_paths = lypad_preferences_get_note_paths (lypad_application_get_preferences (LYPAD_APPLICATION_DEFAULT));
    for (char **path = note_paths; *path != NULL; ++path)
    {
        GFile *folder = g_file_new_for_path (*path);
        GFileEnumerator *enumerator = g_file_enumerate_children (folder, G_FILE_ATTRIBUTE_STANDARD_NAME, G_FILE_QUERY_INFO_NONE, NULL, NULL);
        if (enumerator)
        {
            GFile *file;
            GFileInfo *info;
            while (TRUE)
            {
                if (!g_file_enumerator_iterate (enumerator, &info, &file, NULL, NULL))
                    break;
                if (!info)
                    break;
                if (lypad_utils_magic_file_check (file))
                {
                    LypadNote *note = lypad_note_load_from_file (file);
                    if (note)
                        notes = g_list_prepend (notes, note);
                }
            }
            g_object_unref (enumerator);
        }
    }
    g_strfreev (note_paths);
    return notes;
}

void lypad_notes_manager_import (LypadNotesManager *manager, LypadNote *note)
{
    g_return_if_fail (LYPAD_IS_NOTES_MANAGER (manager));
    g_return_if_fail (LYPAD_IS_NOTE (note));

    if (g_object_get_qdata (G_OBJECT (note), id_quark))
        return;

    if (manager->available_ids)
    {
        int id = GPOINTER_TO_INT (manager->available_ids->data);
        manager->available_ids = g_list_delete_link (manager->available_ids, manager->available_ids);
        g_object_set_qdata (G_OBJECT (note), id_quark, GINT_TO_POINTER (id));
        manager->notes->pdata[id - 1] = g_object_ref (note);
        g_signal_connect (note, "destroy", G_CALLBACK (on_note_destroy), manager);
    }
    else
    {
        g_ptr_array_add (manager->notes, g_object_ref (note));
        g_object_set_qdata (G_OBJECT (note), id_quark, GINT_TO_POINTER (manager->notes->len));
        g_signal_connect (note, "destroy", G_CALLBACK (on_note_destroy), manager);
    }
    g_signal_emit (manager, signals[SIGNAL_NOTE_ADDED], 0, note);
}

static void lypad_notes_manager_release_note (LypadNotesManager *manager, LypadNote *note)
{
    g_signal_handlers_disconnect_by_func (note, on_note_destroy, manager);
    g_object_unref (note);
}

static void on_note_destroy (LypadNote *note, gpointer user_data)
{
    LypadNotesManager *manager = LYPAD_NOTES_MANAGER (user_data);
    gpointer data = g_object_get_qdata (G_OBJECT (note), id_quark);
    g_return_if_fail (data != NULL);

    int id = GPOINTER_TO_INT (data);
    g_assert (manager->notes->pdata[id - 1] == note);
    manager->notes->pdata[id - 1] = NULL;
    g_object_set_qdata (G_OBJECT (note), id_quark, NULL);
    manager->available_ids = g_list_append (manager->available_ids, GINT_TO_POINTER (id));
    lypad_notes_manager_release_note (manager, note);
}

static void lypad_notes_manager_init (LypadNotesManager *manager)
{
    GList *loaded_notes = load_notes ();
    manager->notes = g_ptr_array_sized_new (g_list_length (loaded_notes));
    for (GList *l = loaded_notes; l != NULL; l = l->next)
    {
        g_ptr_array_add (manager->notes, l->data);
        g_object_set_qdata (l->data, id_quark, GINT_TO_POINTER (manager->notes->len));
        g_signal_connect (l->data, "destroy", G_CALLBACK (on_note_destroy), manager);
        lypad_note_show (l->data, NULL);
    }
    g_list_free (loaded_notes);
    manager->available_ids = NULL;
}

void lypad_notes_manager_save_all (LypadNotesManager *manager)
{
    g_return_if_fail (LYPAD_IS_NOTES_MANAGER (manager));
    for (guint i = 0; i < manager->notes->len; ++i)
        if (manager->notes->pdata[i] != NULL)
            lypad_note_save (manager->notes->pdata[i]);
}

GSList *lypad_notes_manager_get_notes (LypadNotesManager *manager)
{
    GSList *notes = NULL;
    for (guint i = 0; i < manager->notes->len; ++i)
        if (manager->notes->pdata[i] != NULL)
            notes = g_slist_append (notes, manager->notes->pdata[i]);
    return notes;
}

static void lypad_notes_manager_dispose (GObject *object)
{
    LypadNotesManager *manager = LYPAD_NOTES_MANAGER (object);
    if (manager->notes->len > 0)
    {
        for (guint i = 0; i < manager->notes->len; ++i)
            if (manager->notes->pdata[i] != NULL)
                lypad_notes_manager_release_note (manager, manager->notes->pdata[i]);
        g_ptr_array_remove_range (manager->notes, 0, manager->notes->len);
    }
    if (manager->available_ids)
    {
        g_list_free (manager->available_ids);
        manager->available_ids = NULL;
    }
    G_OBJECT_CLASS (lypad_notes_manager_parent_class)->dispose (object);
}

int lypad_notes_manager_get_note_id (LypadNotesManager *manager, LypadNote *note)
{
    g_return_val_if_fail (LYPAD_IS_NOTES_MANAGER (manager), -1);
    gpointer data = g_object_get_qdata (G_OBJECT (note), id_quark);
    if (data == NULL)
        return -1;
    else
        return GPOINTER_TO_INT (data);
}

LypadNote *lypad_notes_manager_get_note_for_id (LypadNotesManager *manager, int id)
{
    g_return_val_if_fail (LYPAD_IS_NOTES_MANAGER (manager), NULL);
    g_return_val_if_fail (id <= (int) manager->notes->len, NULL);
    return manager->notes->pdata[id - 1];
}

static void lypad_notes_manager_class_init (LypadNotesManagerClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->dispose = lypad_notes_manager_dispose;

    signals[SIGNAL_NOTE_ADDED] = g_signal_new ("note-added", G_TYPE_FROM_CLASS (klass),
                                               G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                               g_cclosure_marshal_VOID__OBJECT, G_TYPE_NONE, 1, G_TYPE_OBJECT);

    id_quark = g_quark_from_string ("lypad-notes-manager-id");
}

LypadNotesManager *lypad_notes_manager_new ()
{
    return g_object_new (LYPAD_TYPE_NOTES_MANAGER, NULL);
}
