/*
 *  lypad-placeholder-buffer.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-placeholder-buffer.h"
#include "color-selection/lypad-color-utils.h"
#include <string.h>

struct _LypadPlaceholderBuffer
{
    GtkTextBuffer  parent;
    GtkTextTag    *text_tag;
};

G_DEFINE_TYPE (LypadPlaceholderBuffer, lypad_placeholder_buffer, GTK_TYPE_TEXT_BUFFER)

static void lypad_placeholder_buffer_init (LypadPlaceholderBuffer *buffer)
{
}

void lypad_placeholder_buffer_set_text (LypadPlaceholderBuffer *buffer, const char *text)
{
    g_return_if_fail (LYPAD_IS_PLACEHOLDER_BUFFER (buffer));
    GtkTextIter start, end;
    GtkTextBuffer *text_buffer = GTK_TEXT_BUFFER (buffer);
    gtk_text_buffer_get_bounds (text_buffer, &start, &end);
    GTK_TEXT_BUFFER_CLASS (lypad_placeholder_buffer_parent_class)->delete_range (text_buffer, &start, &end);
    GTK_TEXT_BUFFER_CLASS (lypad_placeholder_buffer_parent_class)->insert_text (text_buffer, &start, text, -1);
    gtk_text_buffer_get_bounds (text_buffer, &start, &end);
    gtk_text_buffer_apply_tag (text_buffer, buffer->text_tag, &start, &end);
    gtk_text_buffer_place_cursor (text_buffer, &start);
}

void lypad_placeholder_buffer_notify_bkg_color (LypadPlaceholderBuffer *buffer, const GdkRGBA *color)
{
    g_return_if_fail (LYPAD_IS_PLACEHOLDER_BUFFER (buffer));
    GdkRGBA text_color;
    if (LYPAD_RGB_LUMINANCE (color->red, color->green, color->blue) > 0.55)
    {
        text_color.red   = 0.4;
        text_color.green = 0.4;
        text_color.blue  = 0.4;
        text_color.alpha = 1.0;
    }
    else
    {
        text_color.red   = 0.7;
        text_color.green = 0.7;
        text_color.blue  = 0.7;
        text_color.alpha = 1.0;
    }
    g_object_set (G_OBJECT (buffer->text_tag), "foreground-rgba", &text_color, NULL);
}

static void lypad_placeholder_buffer_insert_text (GtkTextBuffer *text_buffer, GtkTextIter *location, const char *text, gint len)
{
}

static void lypad_placeholder_buffer_delete_range (GtkTextBuffer *buffer, GtkTextIter *start, GtkTextIter *end)
{
}

static void lypad_placeholder_buffer_mark_set (GtkTextBuffer *buffer, const GtkTextIter *location, GtkTextMark *mark)
{
    if (mark == gtk_text_buffer_get_insert (buffer) || mark == gtk_text_buffer_get_selection_bound (buffer))
    {
        GtkTextIter start;
        gtk_text_buffer_get_start_iter (buffer, &start);
        if (!gtk_text_iter_equal (location, &start))
            gtk_text_buffer_move_mark (buffer, mark, &start);
    }
}

static void lypad_placeholder_buffer_constructed (GObject *object)
{
    G_OBJECT_CLASS (lypad_placeholder_buffer_parent_class)->constructed (object);

    LypadPlaceholderBuffer *buffer = LYPAD_PLACEHOLDER_BUFFER (object);
    GdkRGBA color = {0.4, 0.4, 0.4, 1.0};
    buffer->text_tag = gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (buffer), NULL, "foreground-rgba", &color, NULL);
}

static void lypad_placeholder_buffer_class_init (LypadPlaceholderBufferClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GtkTextBufferClass *buffer_class = GTK_TEXT_BUFFER_CLASS (klass);

    object_class->constructed  = lypad_placeholder_buffer_constructed;
    buffer_class->insert_text  = lypad_placeholder_buffer_insert_text;
    buffer_class->delete_range = lypad_placeholder_buffer_delete_range;
    buffer_class->mark_set     = lypad_placeholder_buffer_mark_set;
}

GtkTextBuffer *lypad_placeholder_buffer_new ()
{
    return g_object_new (LYPAD_TYPE_PLACEHOLDER_BUFFER, NULL);
}