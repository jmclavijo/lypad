/*
 *  lypad-main-window.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-main-window.h"
#include "lypad-note-widget.h"
#include "lypad-note-editor.h"
#include "lypad-notes-list.h"
#include "lypad-notes-manager.h"
#include "lypad-tags-list.h"
#include "lypad-tags-page.h"
#include "lypad-preferences-editor.h"

struct _LypadMainWindow
{
    GtkApplicationWindow    parent;
    GtkWidget              *header_bar;
    LypadNotesList         *notes_list;
    LypadTagsList          *tags_list;
    GtkWidget              *note_editor;
    GtkWidget              *preferences_editor;
    GtkWidget              *tags_page;

    GtkWidget              *last_page;
    char                   *last_page_data;

    GtkButton              *new_note_button;
    GtkToggleButton        *select_button;
    GtkMenuButton          *menu_button;

    GtkWidget              *notes_tab;
    GtkWidget              *tags_tab;

    GtkStack               *view_panel;
    GtkStack               *left_panel;
    GtkListBox             *left_panel_list;
    GtkListBoxRow          *left_panel_notes_row;
    GtkListBoxRow          *left_panel_tags_row;
    GtkListBoxRow          *left_panel_preferences_row;

    GtkAccelGroup          *accel_group;
    GtkWidget              *base_menu;
    GtkWidget              *notes_list_menu;

    gulong                  note_added_handler;
};

G_DEFINE_TYPE (LypadMainWindow, lypad_main_window, GTK_TYPE_APPLICATION_WINDOW)

#define PREFERENCES_GROUP_NAME "Main window"

static void on_note_added (LypadNotesManager *manager, LypadNote *note, gpointer user_data)
{
    LypadMainWindow *win = LYPAD_MAIN_WINDOW (user_data);
    GtkWidget *widget = lypad_note_widget_new (note);
    lypad_notes_list_add (win->notes_list, widget);
}

static void on_new_note_action_activated (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
    LypadMainWindow *win = LYPAD_MAIN_WINDOW (user_data);
    LypadNote *note = lypad_note_new ();
    LypadNotesManager *manager = lypad_application_get_notes_manager (LYPAD_APPLICATION_DEFAULT);

    if (gtk_stack_get_visible_child (win->view_panel) == win->tags_page)
    {
        LypadTag *tag = lypad_tags_page_get_tag (LYPAD_TAGS_PAGE (win->tags_page));
        if (tag)
            lypad_note_add_tag (note, tag);
    }

    lypad_notes_manager_import (manager, note);
    GSList *notes = g_slist_prepend (NULL, note);
    lypad_main_window_edit_notes (win, notes);
    g_slist_free (notes);

    g_object_unref (note);
}

static GActionEntry action_entries[] = {
	{ "new-note", on_new_note_action_activated, NULL, NULL}
};

static void lypad_main_window_set_last_page (LypadMainWindow *win, GtkWidget *page, const char *data)
{
    if (win->last_page != page || win->last_page_data != data)
    {
        g_free (win->last_page_data);

        win->last_page = page;
        if (data)
            win->last_page_data = g_strdup (data);
        else
            win->last_page_data = NULL;

        LypadPreferences *preferences = lypad_application_get_preferences (LYPAD_APPLICATION_DEFAULT);
        GKeyFile *key_file = lypad_preferences_get_key_file (preferences);
        if (win->last_page == win->notes_tab)
        {
            g_key_file_set_string (key_file, PREFERENCES_GROUP_NAME, "last-page", "notes-tab");
            g_key_file_set_string (key_file, PREFERENCES_GROUP_NAME, "last-page-data", "");
        }
        else if (win->last_page == win->tags_tab)
        {
            g_key_file_set_string (key_file, PREFERENCES_GROUP_NAME, "last-page", "tags-tab");
            g_key_file_set_string (key_file, PREFERENCES_GROUP_NAME, "last-page-data", "");
        }
        else if (win->last_page == win->tags_page)
        {
            g_key_file_set_string (key_file, PREFERENCES_GROUP_NAME, "last-page", "tags-page");
            g_key_file_set_string (key_file, PREFERENCES_GROUP_NAME, "last-page-data", win->last_page_data ? win->last_page_data : "");
        }
        lypad_preferences_set_modified (preferences);
    }
}

static void lypad_main_window_go_to_page (LypadMainWindow *win, GtkWidget *page, gpointer data, gboolean go_in)
{
    if (page == win->notes_tab)
    {
        gtk_stack_set_visible_child (win->view_panel, win->notes_tab);
        gtk_stack_set_visible_child_full (win->left_panel, "default", GTK_STACK_TRANSITION_TYPE_SLIDE_RIGHT);

        GtkListBoxRow *row = gtk_list_box_get_row_at_index (win->left_panel_list, 0);
        gtk_list_box_select_row (win->left_panel_list, row);
        gtk_widget_grab_focus (GTK_WIDGET (row));

        lypad_main_window_set_last_page (win, win->notes_tab, NULL);
    }
    else if (page == win->note_editor)
    {
        lypad_note_editor_edit_notes (LYPAD_NOTE_EDITOR (win->note_editor), (GSList *) data);
        gtk_stack_set_visible_child (win->view_panel, win->note_editor);
        gtk_stack_set_visible_child_full (win->left_panel, "note-editor", GTK_STACK_TRANSITION_TYPE_SLIDE_LEFT);
    }
    else if (page == win->tags_tab)
    {
        gtk_stack_set_visible_child (win->view_panel, win->tags_tab);
        gtk_stack_set_visible_child_full (win->left_panel, "default", GTK_STACK_TRANSITION_TYPE_SLIDE_RIGHT);

        gtk_list_box_select_row (GTK_LIST_BOX (win->tags_list), NULL);
        GtkListBoxRow *row = gtk_list_box_get_row_at_index (win->left_panel_list, 1);
        gtk_list_box_select_row (win->left_panel_list, row);
        gtk_widget_grab_focus (GTK_WIDGET (row));

        lypad_main_window_set_last_page (win, win->tags_tab, NULL);
    }
    else if (page == win->tags_page)
    {
        gtk_stack_set_visible_child (win->view_panel, win->tags_page);
        lypad_tags_page_set_tag (LYPAD_TAGS_PAGE (win->tags_page), LYPAD_TAG (data));
        gtk_stack_set_visible_child_full (win->left_panel, "tags-page",
                                          go_in ? GTK_STACK_TRANSITION_TYPE_SLIDE_LEFT : GTK_STACK_TRANSITION_TYPE_SLIDE_RIGHT);

        lypad_main_window_set_last_page (win, win->tags_page, lypad_tag_get_name (LYPAD_TAG (data)));
    }
    else if (page == win->preferences_editor)
    {
        gtk_stack_set_visible_child (win->view_panel, win->preferences_editor);
        gtk_stack_set_visible_child_full (win->left_panel, "pref-editor", GTK_STACK_TRANSITION_TYPE_SLIDE_LEFT);
        lypad_preferences_editor_focus (LYPAD_PREFERENCES_EDITOR (win->preferences_editor));
    }
}

static inline void lypad_main_window_go_to_last_page (LypadMainWindow *win)
{
    if (G_LIKELY (win->last_page != NULL))
    {
        if (win->last_page == win->tags_page && win->last_page_data)
            lypad_main_window_go_to_page (win, win->last_page, lypad_tag_find (win->last_page_data), FALSE);
        else
            lypad_main_window_go_to_page (win, win->last_page, NULL, FALSE);
    }
    else
        lypad_main_window_go_to_page (win, win->notes_tab, NULL, FALSE);
}

GtkAccelGroup *lypad_main_window_get_accel_group (LypadMainWindow *win)
{
    return win->accel_group;
}


void lypad_main_window_edit_notes (LypadMainWindow *win, GSList *notes)
{
    lypad_main_window_go_to_page (win, win->note_editor, notes, TRUE);
}

static void on_preferences_editor_go_back (LypadPreferencesEditor *editor, gpointer user_data)
{
    LypadMainWindow *win = LYPAD_MAIN_WINDOW (user_data);
    if (gtk_stack_get_visible_child (win->view_panel) == win->preferences_editor)
        lypad_main_window_go_to_last_page (win);
}

static void on_tags_page_go_back (LypadTagsPage *page, gpointer user_data)
{
    LypadMainWindow *win = LYPAD_MAIN_WINDOW (user_data);
    if (gtk_stack_get_visible_child (win->view_panel) == win->tags_page)
        lypad_main_window_go_to_page (win, win->tags_tab, NULL, FALSE);
}

static void on_note_editor_go_back (LypadNoteEditor *editor, gpointer user_data)
{
    LypadMainWindow *win = LYPAD_MAIN_WINDOW (user_data);
    if (gtk_stack_get_visible_child (win->view_panel) == win->note_editor)
        lypad_main_window_go_to_last_page (win);
}

static void on_tags_page_tag_set (LypadTagsPage *page, LypadTag *tag, gpointer user_data)
{
    LypadMainWindow *win = LYPAD_MAIN_WINDOW (user_data);
    if (gtk_stack_get_visible_child (win->view_panel) == win->tags_page)
        lypad_main_window_set_last_page (win, win->tags_page, lypad_tag_get_name (tag));
}

static void on_view_panel_visible_child_changed (GObject *stack, GParamSpec *pspec, gpointer user_data)
{
    LypadMainWindow *win = LYPAD_MAIN_WINDOW (user_data);
    GtkWidget *visible_child = gtk_stack_get_visible_child (GTK_STACK (stack));

    if (visible_child == win->notes_tab || visible_child == win->tags_page)
        gtk_widget_set_visible (GTK_WIDGET (win->select_button), TRUE);
    else
    {
        gtk_toggle_button_set_active (win->select_button, FALSE);
        gtk_widget_set_visible (GTK_WIDGET (win->select_button), FALSE);
    }

    if (visible_child == win->note_editor)
    {
        GtkWidget *menu = lypad_note_editor_get_menu (LYPAD_NOTE_EDITOR (win->note_editor));
        gtk_menu_button_set_popover (win->menu_button, menu);
    }
    else
        gtk_menu_button_set_popover (win->menu_button, win->base_menu);
}

static void on_tags_list_tag_clicked (LypadTagsList *list, LypadTag *tag, gpointer user_data)
{
    LypadMainWindow *win = LYPAD_MAIN_WINDOW (user_data);
    lypad_main_window_go_to_page (win, win->tags_page, tag, TRUE);
}

static void on_menu_about_clicked (GtkButton *button, gpointer user_data)
{
    lypad_application_show_about_dialog (LYPAD_APPLICATION_DEFAULT, GTK_WINDOW (user_data));
}

static inline void lypad_main_window_build_base_menu (LypadMainWindow *win)
{
    win->base_menu = gtk_popover_menu_new ();
    g_object_ref_sink (win->base_menu);

    GtkWidget *box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
    gtk_widget_set_margin_start (box, 10);
    gtk_widget_set_margin_end (box, 10);
    gtk_widget_set_margin_top (box, 10);
    gtk_widget_set_margin_bottom (box, 10);
    gtk_container_add (GTK_CONTAINER (win->base_menu), box);

    GtkWidget *item;

    item = gtk_model_button_new ();
    g_object_set (item, "text", "About", NULL);
    g_signal_connect (item, "clicked", G_CALLBACK (on_menu_about_clicked), win);
    gtk_box_pack_start (GTK_BOX (box), item, FALSE, TRUE, 0);

    item = gtk_model_button_new ();
    g_object_set (item, "text", "Quit", NULL);
    gtk_actionable_set_action_name (GTK_ACTIONABLE (item), "app.quit");
    gtk_box_pack_start (GTK_BOX (box), item, FALSE, TRUE, 0);

    gtk_widget_show_all (box);
}

static void lypad_main_window_init (LypadMainWindow *win)
{
    gtk_widget_init_template (GTK_WIDGET (win));
    gtk_window_set_default_size (GTK_WINDOW (win), 800, 500);

    win->last_page = NULL;
    win->last_page_data = NULL;

    LypadNotesManager *manager = lypad_application_get_notes_manager (LYPAD_APPLICATION_DEFAULT);
    GSList *notes = lypad_notes_manager_get_notes (manager);
    for (GSList *l = notes; l != NULL; l = l->next)
    {
        GtkWidget *note_widget = lypad_note_widget_new (l->data);
        gtk_widget_set_hexpand (note_widget, TRUE);
        gtk_widget_set_size_request (note_widget, -1, 100);
        lypad_notes_list_add (win->notes_list, note_widget);
    }

    win->note_editor = lypad_note_editor_new ();
    gtk_widget_show_all (win->note_editor);
    gtk_stack_add_named (win->view_panel, win->note_editor, "note-editor");
    gtk_stack_add_named (win->left_panel, lypad_page_get_side_panel (LYPAD_PAGE (win->note_editor)), "note-editor");
    g_signal_connect (win->note_editor, "go-back", G_CALLBACK (on_note_editor_go_back), win);

    win->preferences_editor = lypad_preferences_editor_new ();
    gtk_widget_show_all (win->preferences_editor);
    gtk_stack_add_named (win->view_panel, win->preferences_editor, "pref-editor");
    gtk_stack_add_named (win->left_panel, lypad_page_get_side_panel (LYPAD_PAGE (win->preferences_editor)), "pref-editor");
    g_signal_connect (win->preferences_editor, "go-back", G_CALLBACK (on_preferences_editor_go_back), win);

    win->tags_page = lypad_tags_page_new ();
    gtk_widget_show_all (win->tags_page);
    gtk_stack_add_named (win->view_panel, win->tags_page, "tags-page");
    gtk_stack_add_named (win->left_panel, lypad_page_get_side_panel (LYPAD_PAGE (win->tags_page)), "tags-page");
    g_signal_connect (win->tags_page, "go-back", G_CALLBACK (on_tags_page_go_back), win);
    g_signal_connect (win->tags_page, "tag-set", G_CALLBACK (on_tags_page_tag_set), win);

    lypad_notes_list_set_toggle (win->notes_list, GTK_WIDGET (win->select_button));
    lypad_tags_page_set_toggle (LYPAD_TAGS_PAGE (win->tags_page), GTK_WIDGET (win->select_button));

    g_signal_connect (win->tags_list, "tag-clicked", G_CALLBACK (on_tags_list_tag_clicked), win);
    g_signal_connect (win->view_panel, "notify::visible-child", G_CALLBACK (on_view_panel_visible_child_changed), win);

    win->note_added_handler = g_signal_connect (manager, "note-added", G_CALLBACK (on_note_added), win);

    g_action_map_add_action_entries (G_ACTION_MAP (win), action_entries, G_N_ELEMENTS (action_entries), win);

    win->accel_group = gtk_accel_group_new ();
    gtk_window_add_accel_group (GTK_WINDOW (win), win->accel_group);

    lypad_main_window_build_base_menu (win);
    gtk_menu_button_set_popover (win->menu_button, win->base_menu);


    LypadPreferences *preferences = lypad_application_get_preferences (LYPAD_APPLICATION_DEFAULT);
    GKeyFile *key_file = lypad_preferences_get_key_file (preferences);
    char *last_page = g_key_file_get_string (key_file, PREFERENCES_GROUP_NAME, "last-page", NULL);
    if (last_page)
    {
        if (strcmp (last_page, "notes-tab") == 0)
            lypad_main_window_go_to_page (win, win->notes_tab, NULL, TRUE);
        else if (strcmp (last_page, "tags-tab") == 0)
            lypad_main_window_go_to_page (win, win->tags_tab, NULL, TRUE);
        else if (strcmp (last_page, "tags-page") == 0)
        {
            char *tag_name = g_key_file_get_string (key_file, PREFERENCES_GROUP_NAME, "last-page-data", NULL);
            LypadTag *tag = NULL;
            if (tag_name)
            {
                tag = lypad_tag_find (tag_name);
                g_free (tag_name);
            }
            lypad_main_window_go_to_page (win, win->tags_page, tag, TRUE);
        }
        g_free (last_page);
    }
}

static void on_left_panel_row_activated (GtkListBox *box, GtkListBoxRow *row, gpointer user_data)
{
    LypadMainWindow *win = LYPAD_MAIN_WINDOW (user_data);

    if (row == win->left_panel_preferences_row)
        lypad_main_window_go_to_page (win, win->preferences_editor, NULL, TRUE);
    else if (row == win->left_panel_tags_row)
        lypad_main_window_go_to_page (win, win->tags_tab, NULL, TRUE);
    else if (row == win->left_panel_notes_row)
        lypad_main_window_go_to_page (win, win->notes_tab, NULL, TRUE);
}

static gboolean lypad_main_window_key_press (GtkWidget *widget, GdkEventKey *event)
{
    LypadMainWindow *win = LYPAD_MAIN_WINDOW (widget);
    if (event->keyval == GDK_KEY_Escape && gtk_toggle_button_get_active (win->select_button))
    {
        gtk_toggle_button_set_active (win->select_button, FALSE);
        return TRUE;
    }
    return GTK_WIDGET_CLASS (lypad_main_window_parent_class)->key_press_event (widget, event);
}

static void lypad_main_window_destroy (GtkWidget *widget)
{
    LypadMainWindow *win = LYPAD_MAIN_WINDOW (widget);
    LypadNotesManager *manager = lypad_application_get_notes_manager (LYPAD_APPLICATION_DEFAULT);
    g_clear_signal_handler (&win->note_added_handler, manager);
    g_clear_object (&win->base_menu);
    GTK_WIDGET_CLASS (lypad_main_window_parent_class)->destroy (widget);
}

static void lypad_main_window_finalize (GObject *object)
{
    LypadMainWindow *win = LYPAD_MAIN_WINDOW (object);
    g_object_unref (win->accel_group);
    G_OBJECT_CLASS (lypad_main_window_parent_class)->finalize (object);
}

static void lypad_main_window_class_init (LypadMainWindowClass *klass)
{
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    widget_class->key_press_event = lypad_main_window_key_press;
    widget_class->destroy         = lypad_main_window_destroy;
    object_class->finalize        = lypad_main_window_finalize;

    g_type_ensure (LYPAD_TYPE_NOTES_LIST);
    g_type_ensure (LYPAD_TYPE_TAGS_LIST);
    gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/lypad/ui/lypad-main-window.glade");
    gtk_widget_class_bind_template_child (widget_class, LypadMainWindow, header_bar);
    gtk_widget_class_bind_template_child (widget_class, LypadMainWindow, notes_list);
    gtk_widget_class_bind_template_child (widget_class, LypadMainWindow, new_note_button);
    gtk_widget_class_bind_template_child (widget_class, LypadMainWindow, select_button);
    gtk_widget_class_bind_template_child (widget_class, LypadMainWindow, menu_button);
    gtk_widget_class_bind_template_child (widget_class, LypadMainWindow, tags_list);
    gtk_widget_class_bind_template_child (widget_class, LypadMainWindow, view_panel);
    gtk_widget_class_bind_template_child (widget_class, LypadMainWindow, left_panel);
    gtk_widget_class_bind_template_child (widget_class, LypadMainWindow, left_panel_list);
    gtk_widget_class_bind_template_child (widget_class, LypadMainWindow, left_panel_notes_row);
    gtk_widget_class_bind_template_child (widget_class, LypadMainWindow, left_panel_tags_row);
    gtk_widget_class_bind_template_child (widget_class, LypadMainWindow, left_panel_preferences_row);
    gtk_widget_class_bind_template_child (widget_class, LypadMainWindow, notes_tab);
    gtk_widget_class_bind_template_child (widget_class, LypadMainWindow, tags_tab);

    gtk_widget_class_bind_template_callback (widget_class, on_left_panel_row_activated);
}

LypadMainWindow *lypad_main_window_new (LypadApplication *app)
{
    LypadMainWindow *win = g_object_new (LYPAD_TYPE_MAIN_WINDOW, "application", app, NULL);
    return win;
}
