/*
 *  lypad-tag-widget.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-tag-widget.h"

struct _LypadTagWidget
{
    GtkBin        parent;
    GtkWidget    *label;
    GdkRGBA       color;
    LypadTag     *tag;
    gulong        color_changed_handler;
    gulong        tag_destroy_handler;
    LypadTagMode  mode;

    double        shape_radius;
    double        shape_width;
    int           child_hpadding;
    int           child_vpadding;

    gboolean      button_pressed;
    gboolean      interactive;
    GtkWidget    *menu;
};

G_DEFINE_TYPE (LypadTagWidget, lypad_tag_widget, GTK_TYPE_BIN)

enum
{
    SIGNAL_REMOVE,
    SIGNAL_LAST
};
static guint signals[SIGNAL_LAST] = {0};

static void on_tag_color_changed (LypadTag *tag, GParamSpec *pspec, gpointer user_data)
{
    LypadTagWidget *widget = LYPAD_TAG_WIDGET (user_data);
    lypad_tag_get_color (tag, &widget->color);
    gtk_widget_queue_draw (GTK_WIDGET (widget));
}

static void on_tag_destroy (LypadTag *tag, gpointer user_data)
{
    gtk_widget_destroy (user_data);
}

void lypad_tag_widget_set_mode (LypadTagWidget *widget, LypadTagMode mode)
{
    if (widget->mode != mode)
    {
        if (mode == LYPAD_TAG_MINIMAL_SIZE)
            gtk_label_set_label (GTK_LABEL (widget->label), "");
        else if (widget->mode == LYPAD_TAG_MINIMAL_SIZE)
            gtk_label_set_label (GTK_LABEL (widget->label), lypad_tag_get_name (widget->tag));

        widget->mode = mode;
        gtk_widget_queue_resize (GTK_WIDGET (widget));
        if (mode == LYPAD_TAG_MINIMAL_SIZE)
        {
            char *tooltip = g_strdup_printf ("<b>Tag:</b> %s", lypad_tag_get_name (widget->tag));
            gtk_widget_set_tooltip_markup (GTK_WIDGET (widget), tooltip);
            g_free (tooltip);
        }
        else
            gtk_widget_set_tooltip_text (GTK_WIDGET (widget), NULL);
    }
}

static void lypad_tag_widget_init (LypadTagWidget *widget)
{
    gtk_widget_set_size_request (GTK_WIDGET (widget), 24, 24);
    gtk_widget_set_has_window (GTK_WIDGET (widget), TRUE);

    widget->label = gtk_label_new ("");
    gtk_widget_set_margin_start (widget->label, 7);
    gtk_widget_set_margin_end (widget->label, 7);
    gtk_label_set_ellipsize (GTK_LABEL (widget->label), PANGO_ELLIPSIZE_END);

    PangoAttrList *attributes = gtk_label_get_attributes (GTK_LABEL (widget->label));
    if (!attributes)
        attributes = pango_attr_list_new ();
    PangoAttribute *foreground = pango_attr_foreground_new (2560, 2560, 2560);
    pango_attr_list_change (attributes, foreground);
    gtk_label_set_attributes (GTK_LABEL (widget->label), attributes);

    gtk_container_add (GTK_CONTAINER (widget), widget->label);
    gtk_widget_set_valign (widget->label, GTK_ALIGN_CENTER);

    widget->mode = LYPAD_TAG_DEFAULT_SIZE;
    widget->shape_radius = 0;
    widget->shape_width = 0;
    widget->child_hpadding = 0;
    widget->child_vpadding = 4;

    widget->button_pressed = FALSE;
    widget->interactive = FALSE;

    widget->menu = NULL;

    gtk_widget_show_all (GTK_WIDGET (widget));
}

static void lypad_tag_widget_set_tag (LypadTagWidget *widget, LypadTag *tag)
{
    g_return_if_fail (tag != NULL && widget->tag == NULL);
    widget->tag = g_object_ref (tag);

    if (widget->mode != LYPAD_TAG_MINIMAL_SIZE)
        gtk_label_set_label (GTK_LABEL (widget->label), lypad_tag_get_name (tag));

    if (widget->mode == LYPAD_TAG_MINIMAL_SIZE)
    {
        char *tooltip = g_strdup_printf ("<b>Tag:</b> %s", lypad_tag_get_name (widget->tag));
        gtk_widget_set_tooltip_markup (GTK_WIDGET (widget), tooltip);
        g_free (tooltip);
    }

    widget->color_changed_handler = g_signal_connect (tag, "notify::color", G_CALLBACK (on_tag_color_changed), widget);
    widget->tag_destroy_handler = g_signal_connect (tag, "destroy", G_CALLBACK (on_tag_destroy), widget);
    on_tag_color_changed (tag, NULL, widget);
}

LypadTag *lypad_tag_widget_get_tag (LypadTagWidget *widget)
{
    g_return_val_if_fail (LYPAD_IS_TAG_WIDGET (widget), NULL);
    return widget->tag;
}


static void on_tag_color_dialog_response (GtkDialog *dialog, gint response_id, gpointer tag_widget)
{
    if (response_id == GTK_RESPONSE_OK)
    {
        GdkRGBA color;
        gtk_color_chooser_get_rgba (GTK_COLOR_CHOOSER (dialog), &color);
        lypad_tag_set_color (lypad_tag_widget_get_tag (tag_widget), &color);
    }
    gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void on_change_color_clicked (GtkButton *button, gpointer user_data)
{
    GtkWidget *dialog = gtk_color_chooser_dialog_new ("Select color", GTK_WINDOW (gtk_widget_get_toplevel (user_data)));
    gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
    gtk_color_chooser_set_use_alpha (GTK_COLOR_CHOOSER (dialog), FALSE);
    g_signal_connect (dialog, "response", G_CALLBACK (on_tag_color_dialog_response), user_data);
    gtk_window_present (GTK_WINDOW (dialog));
}

static void on_remove_tag_clicked (GtkButton *button, gpointer user_data)
{
    g_signal_emit (LYPAD_TAG_WIDGET (user_data), signals[SIGNAL_REMOVE], 0);
}

static void lypad_tag_widget_activate (LypadTagWidget *widget)
{
    if (!widget->menu)
    {
        widget->menu = gtk_popover_new (GTK_WIDGET (widget));
        GtkWidget *box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
        gtk_container_set_border_width (GTK_CONTAINER (box), 10);

        GtkWidget *button = gtk_model_button_new ();
        GValue value = G_VALUE_INIT;
        g_value_init (&value, G_TYPE_STRING);
        g_value_set_string (&value, "Change color");
        g_object_set_property (G_OBJECT (button), "text", &value);
        gtk_box_pack_start (GTK_BOX (box), button, FALSE, TRUE, 0);
        g_signal_connect (button, "clicked", G_CALLBACK (on_change_color_clicked), widget);

        button = gtk_model_button_new ();
        g_value_set_string (&value, "Remove tag");
        g_object_set_property (G_OBJECT (button), "text", &value);
        g_value_unset (&value);
        gtk_box_pack_start (GTK_BOX (box), button, FALSE, TRUE, 0);
        g_signal_connect (button, "clicked", G_CALLBACK (on_remove_tag_clicked), widget);

        gtk_container_add (GTK_CONTAINER (widget->menu), box);
        gtk_widget_show_all (box);
    }

    gtk_popover_popup (GTK_POPOVER (widget->menu));
}

void lypad_tag_widget_set_interactive (LypadTagWidget *widget, gboolean interactive)
{
    g_return_if_fail (LYPAD_IS_TAG_WIDGET (widget));
    widget->interactive = interactive;
}

static gboolean lypad_tag_widget_draw (GtkWidget *widget, cairo_t *cr)
{
    LypadTagWidget *tag_widget = LYPAD_TAG_WIDGET (widget);
    if (gtk_widget_get_has_window (widget))
    {
        GtkAllocation allocation;
        if (tag_widget->mode == LYPAD_TAG_MINIMAL_SIZE)
            allocation.width = allocation.height;

        cairo_set_line_width (cr, 1);

        if (tag_widget->mode == LYPAD_TAG_MINIMAL_SIZE)
        {
            gtk_widget_get_allocation (widget, &allocation);
            cairo_arc (cr, allocation.width * 0.5, allocation.height * 0.5, tag_widget->shape_radius, 0, G_PI * 2);
        }
        else
        {
            gtk_widget_get_allocation (tag_widget->label, &allocation);
            cairo_arc (cr, allocation.x + allocation.width * 0.5 - tag_widget->shape_width * 0.5,
                       allocation.y + allocation.height * 0.5, tag_widget->shape_radius, G_PI * 0.5, G_PI * 1.5);
            cairo_arc (cr, allocation.x + allocation.width * 0.5 + tag_widget->shape_width * 0.5,
                       allocation.y + allocation.height * 0.5, tag_widget->shape_radius, G_PI * 1.5, G_PI * 0.5);
            cairo_line_to (cr, allocation.x + allocation.width * 0.5 - tag_widget->shape_width * 0.5,
                           allocation.y + allocation.height * 0.5 + tag_widget->shape_radius);
        }
        cairo_set_source_rgba (cr, 0, 0, 0, 1);
        cairo_stroke_preserve (cr);
        gdk_cairo_set_source_rgba (cr, &tag_widget->color);
        cairo_fill (cr);
    }

    GTK_WIDGET_CLASS (lypad_tag_widget_parent_class)->draw (widget, cr);
    return FALSE;
}

static void lypad_tag_widget_get_preferred_height (GtkWidget *widget, int *minimum, int *natural)
{
    LypadTagWidget *tag_widget = LYPAD_TAG_WIDGET (widget);
    GtkWidget *child = tag_widget->label;
    gtk_widget_get_preferred_height (child, minimum, NULL);
    tag_widget->shape_radius = *minimum * 0.5 + tag_widget->child_vpadding - 2;
    *minimum += tag_widget->child_vpadding;
    *natural = *minimum;
}

static void lypad_tag_widget_get_preferred_width (GtkWidget *widget, int *minimum, int *natural)
{
    LypadTagWidget *tag_widget = LYPAD_TAG_WIDGET (widget);
    GtkWidget *child = tag_widget->label;
    int child_height;
    gtk_widget_get_preferred_height (child, &child_height, NULL);
    tag_widget->shape_radius = child_height * 0.5 + tag_widget->child_vpadding - 2;

    gtk_widget_get_preferred_width (child, minimum, natural);
    if (LYPAD_TAG_WIDGET (widget)->mode == LYPAD_TAG_MINIMAL_SIZE)
    {
        tag_widget->shape_width = 0;
        tag_widget->child_hpadding = tag_widget->shape_radius - *minimum * 0.5 + 0.5 + 2;
        if (tag_widget->child_hpadding < 0)
            tag_widget->child_hpadding = 0;
        *minimum = *minimum + 2 * tag_widget->child_hpadding;
        *natural = *minimum;
    }
    else
    {
        tag_widget->child_hpadding = 0.3 * tag_widget->shape_radius + 2;
        *minimum += 2 * tag_widget->child_hpadding;
        *natural += 2 * tag_widget->child_hpadding;

        int min_width = 2 * tag_widget->shape_radius + 2;
        if (*minimum < min_width)
            *minimum = min_width;
        if (*natural < min_width)
            *natural = min_width;

        if (LYPAD_TAG_WIDGET (widget)->mode == LYPAD_TAG_DEFAULT_SIZE)
        {
            int max_width = MAX (*minimum, 92);
            if (*natural > max_width)
                *natural = max_width;
        }
    }
}

static void lypad_tag_widget_realize (GtkWidget *widget)
{
    GtkAllocation allocation;
    GdkWindow *window;
    GdkWindowAttr attributes;
    gint attributes_mask;

    gtk_widget_get_allocation (widget, &allocation);

    attributes.x = allocation.x;
    attributes.y = allocation.y;
    attributes.width = allocation.width;
    attributes.height = allocation.height;
    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.event_mask = gtk_widget_get_events (widget) | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_LEAVE_NOTIFY_MASK;
    attributes.visual = gtk_widget_get_visual (widget);
    attributes.wclass = GDK_INPUT_OUTPUT;

    attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL;

    window = gdk_window_new (gtk_widget_get_parent_window (widget), &attributes, attributes_mask);
    gtk_widget_set_window (widget, window);
    gtk_widget_register_window (widget, window);
    gtk_widget_set_realized (widget, TRUE);
}

static void lypad_tag_widget_size_allocate (GtkWidget *widget, GtkAllocation *allocation)
{
    LypadTagWidget *tag_widget = LYPAD_TAG_WIDGET (widget);
    GtkAllocation child_allocation;
    gtk_widget_set_allocation (widget, allocation);

    if (gtk_widget_get_realized (widget))
        gdk_window_move_resize (gtk_widget_get_window (widget),
                                allocation->x,
                                allocation->y,
                                allocation->width,
                                allocation->height);

    child_allocation.x = tag_widget->child_hpadding;
    child_allocation.y = tag_widget->child_vpadding;
    child_allocation.width = allocation->width - 2 * tag_widget->child_hpadding;
    child_allocation.height = allocation->height - 2 * tag_widget->child_vpadding;

    GtkWidget *child = tag_widget->label;
    gtk_widget_size_allocate (child, &child_allocation);

    if (tag_widget->mode == LYPAD_TAG_MINIMAL_SIZE)
        tag_widget->shape_width = 0;
    else
    {
        tag_widget->shape_width = allocation->width - 2 * tag_widget->shape_radius - 2;
        if (tag_widget->shape_width < 0)
            tag_widget->shape_width = 0;
    }
}

static void lypad_tag_widget_destroy (GtkWidget *widget)
{
    LypadTagWidget *tag_widget = LYPAD_TAG_WIDGET (widget);
    if (tag_widget->tag)
    {
        g_signal_handler_disconnect (tag_widget->tag, tag_widget->color_changed_handler);
        g_signal_handler_disconnect (tag_widget->tag, tag_widget->tag_destroy_handler);
        g_clear_object (&tag_widget->tag);
    }

    GTK_WIDGET_CLASS (lypad_tag_widget_parent_class)->destroy (widget);
}



static gboolean lypad_tag_widget_button_press (GtkWidget *widget, GdkEventButton *event)
{
    LypadTagWidget *tag_widget = LYPAD_TAG_WIDGET (widget);
    if (tag_widget->interactive)
    {
        if (event->button == GDK_BUTTON_PRIMARY)
        {
            tag_widget->button_pressed = TRUE;
            return TRUE;
        }
        else if (event->button == GDK_BUTTON_SECONDARY)
        {
            lypad_tag_widget_activate (tag_widget);
            return TRUE;
        }
    }
    return FALSE;
}

static gboolean lypad_tag_widget_button_release (GtkWidget *widget, GdkEventButton *event)
{
    LypadTagWidget *tag_widget = LYPAD_TAG_WIDGET (widget);
    if (tag_widget->button_pressed)
    {
        tag_widget->button_pressed = FALSE;
        if (tag_widget->interactive)
        {
            lypad_tag_widget_activate (tag_widget);
            return TRUE;
        }
    }
    return FALSE;
}

static gboolean lypad_tag_widget_enter_notify (GtkWidget *widget, GdkEventCrossing *event)
{
    return FALSE;
}

static gboolean lypad_tag_widget_leave_notify (GtkWidget *widget, GdkEventCrossing *event)
{
    if (LYPAD_TAG_WIDGET (widget)->button_pressed)
        LYPAD_TAG_WIDGET (widget)->button_pressed = FALSE;
    return FALSE;
}

static void lypad_tag_widget_class_init (LypadTagWidgetClass *klass)
{
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

    widget_class->destroy              = lypad_tag_widget_destroy;
    widget_class->draw                 = lypad_tag_widget_draw;
    widget_class->realize              = lypad_tag_widget_realize;
    widget_class->size_allocate        = lypad_tag_widget_size_allocate;
    widget_class->get_preferred_width  = lypad_tag_widget_get_preferred_width;
    widget_class->get_preferred_height = lypad_tag_widget_get_preferred_height;

    widget_class->button_press_event   = lypad_tag_widget_button_press;
    widget_class->button_release_event = lypad_tag_widget_button_release;
    widget_class->enter_notify_event   = lypad_tag_widget_enter_notify;
    widget_class->leave_notify_event   = lypad_tag_widget_leave_notify;

    signals[SIGNAL_REMOVE] = g_signal_new ("remove-tag", G_TYPE_FROM_CLASS (klass),
                                            G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                            g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
}

GtkWidget *lypad_tag_widget_new (LypadTag *tag)
{
    g_return_val_if_fail (LYPAD_IS_TAG (tag), NULL);
    LypadTagWidget *widget = g_object_new (LYPAD_TYPE_TAG_WIDGET, NULL);
    lypad_tag_widget_set_tag (widget, tag);
    return GTK_WIDGET (widget);
}
