/*
 *  lypad-tag-chooser.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-tag-chooser.h"
#include "lypad-tag-widget.h"

struct _LypadTagChooser
{
    GtkDialog  parent;
    GSList    *tags;
    GtkWidget *tags_box;
    GtkWidget *name_entry;
    GtkWidget *ok_button;
    GtkWidget *color_button;
    GdkRGBA    selected_color;
};

G_DEFINE_TYPE (LypadTagChooser, lypad_tag_chooser, GTK_TYPE_DIALOG)

LypadTag *lypad_tag_chooser_get_tag (LypadTagChooser *chooser)
{
    g_return_val_if_fail (LYPAD_IS_TAG_CHOOSER (chooser), NULL);
    g_return_val_if_fail (!gtk_widget_in_destruction (GTK_WIDGET (chooser)), NULL);
    char *text = g_strdup (gtk_entry_get_text (GTK_ENTRY (chooser->name_entry)));
    g_strstrip (text);
    g_return_val_if_fail (strlen (text) > 0, NULL);
    LypadTag *tag = lypad_tag_new (text);
    GdkRGBA color;
    gtk_color_chooser_get_rgba (GTK_COLOR_CHOOSER (chooser->color_button), &color);
    lypad_tag_set_color (tag, &color);
    return tag;
}

static void on_name_entry_focus (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
    LypadTagChooser *chooser = LYPAD_TAG_CHOOSER (user_data);
    gtk_list_box_select_row (GTK_LIST_BOX (chooser->tags_box), NULL);
}

static void on_tags_box_row_selected (GtkListBox *box, GtkListBoxRow *row, gpointer user_data)
{
    if (row != NULL)
    {
        LypadTagChooser *chooser = LYPAD_TAG_CHOOSER (user_data);
        GtkWidget *child = gtk_bin_get_child (GTK_BIN (row));
        LypadTag *tag = lypad_tag_widget_get_tag (LYPAD_TAG_WIDGET (child));
        gtk_entry_set_text (GTK_ENTRY (chooser->name_entry), lypad_tag_get_name (tag));
    }
}

static void on_buffer_deleted_text (GtkEntryBuffer *buffer, guint position, guint n_chars, gpointer user_data)
{
    LypadTagChooser *chooser = LYPAD_TAG_CHOOSER (user_data);
    char *text = g_strdup (gtk_entry_buffer_get_text (buffer));
    g_strstrip (text);
    if (strlen (text) == 0)
        gtk_widget_set_sensitive (chooser->ok_button, FALSE);
    else
    {
        gboolean found = FALSE;
        for (GSList *l = chooser->tags; l != NULL; l = l->next)
        {
            const char *name = lypad_tag_get_name (l->data);
            if (g_strcmp0 (name, text) == 0)
            {
                GdkRGBA color;
                lypad_tag_get_color (l->data, &color);
                gtk_color_chooser_set_rgba (GTK_COLOR_CHOOSER (chooser->color_button), &color);
                gtk_widget_set_sensitive (chooser->color_button, FALSE);
                gtk_widget_set_visible (chooser->color_button, FALSE);
                found = TRUE;
                break;
            }
        }
        if (!found && !gtk_widget_get_sensitive (chooser->color_button))
        {
            gtk_widget_set_sensitive (chooser->color_button, TRUE);
            gtk_widget_set_visible (chooser->color_button, TRUE);
            gtk_color_chooser_set_rgba (GTK_COLOR_CHOOSER (chooser->color_button), &chooser->selected_color);
        }
    }
    g_free (text);
}

static void on_selected_color_changed (GtkColorButton *button, gpointer user_data)
{
    LypadTagChooser *chooser = LYPAD_TAG_CHOOSER (user_data);
    gtk_color_chooser_get_rgba (GTK_COLOR_CHOOSER (button), &chooser->selected_color);
}

static void on_buffer_inserted_text (GtkEntryBuffer *buffer, guint position, gchar *chars, guint n_chars, gpointer user_data)
{
    LypadTagChooser *chooser = LYPAD_TAG_CHOOSER (user_data);

    char *text = g_strdup (gtk_entry_buffer_get_text (buffer));
    g_strstrip (text);
    if (strlen (text) > 0)
    {
        if (!gtk_widget_get_sensitive (chooser->ok_button))
        {
            gtk_widget_set_sensitive (chooser->ok_button, TRUE);
            gtk_widget_grab_default (chooser->ok_button);
        }

        gboolean found = FALSE;
        for (GSList *l = chooser->tags; l != NULL; l = l->next)
        {
            const char *name = lypad_tag_get_name (l->data);
            if (g_strcmp0 (name, text) == 0)
            {
                GdkRGBA color;
                lypad_tag_get_color (l->data, &color);
                gtk_color_chooser_set_rgba (GTK_COLOR_CHOOSER (chooser->color_button), &color);
                gtk_widget_set_sensitive (chooser->color_button, FALSE);
                gtk_widget_set_visible (chooser->color_button, FALSE);
                found = TRUE;
                break;
            }
        }
        if (!found && !gtk_widget_get_sensitive (chooser->color_button))
        {
            gtk_widget_set_sensitive (chooser->color_button, TRUE);
            gtk_widget_set_visible (chooser->color_button, TRUE);
            gtk_color_chooser_set_rgba (GTK_COLOR_CHOOSER (chooser->color_button), &chooser->selected_color);
        }
    }
    g_free (text);
}

static void lypad_tag_chooser_init (LypadTagChooser *chooser)
{
    gtk_widget_init_template (GTK_WIDGET (chooser));
    gtk_window_set_default_size (GTK_WINDOW (chooser), 300, 250);

    chooser->tags = lypad_tag_get_all ();
    for (GSList *l = chooser->tags; l != NULL; l = l->next)
    {
        GtkWidget *widget = lypad_tag_widget_new (l->data);
        lypad_tag_widget_set_mode (LYPAD_TAG_WIDGET (widget), LYPAD_TAG_FULL_SIZE);
        gtk_list_box_insert (GTK_LIST_BOX (chooser->tags_box), widget, -1);
    }
    gtk_widget_grab_focus (chooser->name_entry);

    g_signal_connect (chooser->name_entry, "focus-in-event", G_CALLBACK (on_name_entry_focus), chooser);
    g_signal_connect (chooser->tags_box, "row-selected", G_CALLBACK (on_tags_box_row_selected), chooser);

    GtkEntryBuffer *buffer = gtk_entry_get_buffer (GTK_ENTRY (chooser->name_entry));
    g_signal_connect (buffer, "deleted-text", G_CALLBACK (on_buffer_deleted_text), chooser);
    g_signal_connect (buffer, "inserted-text", G_CALLBACK (on_buffer_inserted_text), chooser);

    chooser->selected_color.red   = 0.9;
    chooser->selected_color.green = 0.9;
    chooser->selected_color.blue  = 0.9;
    chooser->selected_color.alpha = 1;
    gtk_color_chooser_set_rgba (GTK_COLOR_CHOOSER (chooser->color_button), &chooser->selected_color);
    g_signal_connect (chooser->color_button, "color-set", G_CALLBACK (on_selected_color_changed), chooser);
}

static void lypad_tags_chooser_destroy (GtkWidget *widget)
{
    LypadTagChooser *chooser = LYPAD_TAG_CHOOSER (widget);
    if (chooser->tags)
    {
        g_slist_free (chooser->tags);
        chooser->tags = NULL;
    }
    GTK_WIDGET_CLASS (lypad_tag_chooser_parent_class)->destroy (widget);
}

static void lypad_tag_chooser_class_init (LypadTagChooserClass *klass)
{
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
    widget_class->destroy = lypad_tags_chooser_destroy;

    gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/lypad/ui/lypad-tag-chooser.glade");
    gtk_widget_class_bind_template_child (widget_class, LypadTagChooser, name_entry);
    gtk_widget_class_bind_template_child (widget_class, LypadTagChooser, tags_box);
    gtk_widget_class_bind_template_child (widget_class, LypadTagChooser, ok_button);
    gtk_widget_class_bind_template_child (widget_class, LypadTagChooser, color_button);
}

GtkWidget *lypad_tag_chooser_new (GtkWindow *parent)
{
    return g_object_new (LYPAD_TYPE_TAG_CHOOSER, "transient-for", parent, NULL);
}
