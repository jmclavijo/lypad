/*
 *  lypad-utils.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-utils.h"
#include "lypad-note.h"
#include "lypad-tag-chooser.h"
#include "lypad.h"
#include <locale.h>

char *lypad_utils_format_time (time_t time_val)
{
    GDateTime *time = g_date_time_new_from_unix_local (time_val);
    char *time_str = g_date_time_format (time, "%c");
    return time_str;
}

static gboolean check_autostart_file ()
{
    char *path = g_build_filename (g_get_user_config_dir (), "autostart", "lypad.desktop", NULL);
    GKeyFile *key_file = g_key_file_new ();
    gboolean loaded = g_key_file_load_from_file (key_file, path, G_KEY_FILE_NONE, NULL);

    if (!loaded)
        goto file_disabled;

    char *str;

    GError *error = NULL;
    if (!g_key_file_get_boolean (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_HIDDEN, &error))
    {
        if (error)
        {
            if (error->code != G_KEY_FILE_ERROR_KEY_NOT_FOUND)
            {
                g_error_free (error);
                goto file_invalid;
            }
            else
                g_error_free (error);
        }
    }
    else
        goto file_disabled;

    str = g_key_file_get_string (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_NAME, NULL);
    if (g_strcmp0 (str, "Lypad") != 0)
    {
        g_free (str);
        goto file_invalid;
    }
    g_free (str);

    str = g_key_file_get_string (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_EXEC, NULL);
    if (g_strcmp0 (str, "lypad -b") != 0)
    {
        g_free (str);
        goto file_invalid;
    }
    g_free (str);

    str = g_key_file_get_string (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_TYPE, NULL);
    if (g_strcmp0 (str, G_KEY_FILE_DESKTOP_TYPE_APPLICATION) != 0)
    {
        g_free (str);
        goto file_invalid;
    }
    g_free (str);

    error = NULL;
    if (!g_key_file_get_boolean (key_file, G_KEY_FILE_DESKTOP_GROUP, "X-GNOME-Autostart-enabled", &error))
    {
        if (error)
        {
            if (error->code == G_KEY_FILE_ERROR_KEY_NOT_FOUND)
                g_error_free (error);
            else
            {
                g_error_free (error);
                goto file_invalid;
            }
        }
        else
            goto file_disabled;
    }

    error = NULL;
    if (!g_key_file_get_boolean (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_TERMINAL, &error))
    {
        if (error)
        {
            g_error_free (error);
            goto file_invalid;
        }
    }
    else
        goto file_invalid;

    g_key_file_free (key_file);
    g_free (path);
    return TRUE;

file_disabled:
    g_key_file_free (key_file);
    g_free (path);
    return FALSE;

file_invalid:
    g_key_file_free (key_file);
    GFile *file = g_file_new_for_path (path);
    g_file_delete (file, NULL, NULL);
    g_object_unref (file);
    g_free (path);
    return FALSE;
}

gboolean lypad_utils_get_autostart_enabled ()
{
    return check_autostart_file ();
}

gboolean lypad_utils_set_autostart_enabled (gboolean enable)
{
    if (enable)
    {
        char *autostart_folder = g_build_filename (g_get_user_config_dir (), "autostart", NULL);
        if (g_mkdir_with_parents (autostart_folder, 0775) < 0)
        {
            g_warning ("Couldn't enable autostart: Error creating autostart folder");
            g_free (autostart_folder);
            return FALSE;
        }

        GError *error = NULL;
        char *autostart_file = g_build_filename (autostart_folder, "lypad.desktop", NULL);
        g_free (autostart_folder);

        GKeyFile *key_file = g_key_file_new ();
        g_key_file_set_string (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_NAME, "Lypad");
        g_key_file_set_string (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_COMMENT, "Sticky notes app");
        g_key_file_set_string (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_EXEC, "lypad -b");
        g_key_file_set_string (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_ICON, "lypad");
        g_key_file_set_string (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_TYPE, G_KEY_FILE_DESKTOP_TYPE_APPLICATION);
        g_key_file_set_boolean (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_TERMINAL, FALSE);    
        g_key_file_set_boolean (key_file, G_KEY_FILE_DESKTOP_GROUP, G_KEY_FILE_DESKTOP_KEY_STARTUP_NOTIFY, FALSE);
        if (!g_key_file_save_to_file (key_file, autostart_file, &error))
        {
            g_warning ("Couldn't enable autostart: %s", error->message);
            g_error_free (error);
            g_key_file_free (key_file);
            return FALSE;
        }
        g_key_file_free (key_file);
        return TRUE;
    }
    else
    {
        char *autostart_file = g_build_filename (g_get_user_config_dir (), "autostart", "lypad.desktop", NULL);
        GFile *file = g_file_new_for_path (autostart_file);
        g_free (autostart_file);
        g_file_delete (file, NULL, NULL);
        g_object_unref (file);
        return FALSE;
    }
}

static void on_delete_notes_dialog_response (GtkDialog *dialog, gint response_id, gpointer user_data)
{
    GSList *notes = user_data;
    if (response_id == GTK_RESPONSE_OK)
    {
        for (GSList *l = notes; l != NULL; l = l->next)
            lypad_note_delete (l->data);
    }
    g_slist_free_full (notes, g_object_unref);
    gtk_widget_destroy (GTK_WIDGET (dialog));
}

void lypad_utils_prompt_delete_notes (GSList *in_notes, GtkWindow *window)
{
    GSList *notes = NULL;
    int n_notes = 0;
    for (GSList *l = in_notes; l != NULL; l = l->next)
    {
        notes = g_slist_prepend (notes, g_object_ref (l->data));
        n_notes += 1;
    }

    char *message;
    if (n_notes == 1)
    {
        GtkTextIter start, end;
        GtkTextBuffer *buffer = lypad_note_get_title_buffer (notes->data);
        gtk_text_buffer_get_bounds (buffer, &start, &end);
        gchar *note_title = gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
        g_strstrip (note_title);
        if (strlen (note_title) == 0)
            message = g_strdup ("Are you sure you want to premanently delete the selected note?");
        else
            message = g_strdup_printf ("Are you sure you want to premanently delete “%s”?", note_title);
        g_free (note_title);
    }
    else
        message = g_strdup_printf ("Are you sure you want to premanently delete the %d selected notes?", n_notes);

    GtkWidget *dialog = gtk_message_dialog_new (window,
                                                GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                                GTK_MESSAGE_WARNING,
                                                GTK_BUTTONS_NONE,
                                                "%s", message);
    g_free (message);

    gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog), "Deleted notes will be permanently lost.");

    gtk_dialog_add_button (GTK_DIALOG (dialog), "Cancel", GTK_RESPONSE_CANCEL);
    GtkWidget *delete_button = gtk_dialog_add_button (GTK_DIALOG (dialog), "Delete", GTK_RESPONSE_OK);
    GtkStyleContext *context = gtk_widget_get_style_context (delete_button);
    gtk_style_context_add_class (context, GTK_STYLE_CLASS_DESTRUCTIVE_ACTION);
    g_signal_connect (dialog, "response", G_CALLBACK (on_delete_notes_dialog_response), notes);
    gtk_widget_show (dialog);
}

static void on_add_tags_dialog_response (GtkDialog *dialog, gint response_id, gpointer user_data)
{
    GSList *notes = user_data;
    if (response_id == GTK_RESPONSE_OK)
    {
        LypadTag *tag = lypad_tag_chooser_get_tag (LYPAD_TAG_CHOOSER (dialog));
        for (GSList *l = notes; l != NULL; l = l->next)
        {
            lypad_note_add_tag (l->data, tag);
            g_object_unref (l->data);
        }
        g_object_unref (tag);
    }
    g_slist_free (notes);
    gtk_widget_destroy (GTK_WIDGET (dialog));
}

void lypad_utils_prompt_add_tags (GSList *in_notes, GtkWindow *window)
{
    GSList *notes = NULL;
    for (GSList *l = in_notes; l != NULL; l = l->next)
        notes = g_slist_prepend (notes, g_object_ref (l->data));

    GtkWidget *chooser = lypad_tag_chooser_new (window);
    g_signal_connect (chooser, "response", G_CALLBACK (on_add_tags_dialog_response), notes);
    gtk_widget_show (chooser);
}

char *lypad_utils_path_expand_tilde (const char *path)
{
    if (path[0] == '~')
        return g_strconcat (g_get_home_dir (), path + 1, NULL);
    else
        return g_strdup (path);
}

char *lypad_utils_path_shrink_tilde (const char *path)
{
    const char *home = g_get_home_dir ();
    size_t len = strlen (home);
    if (strncmp (path, home, len) == 0 && strlen (path) > len)
        return g_strconcat ("~", path + len, NULL);
    else
        return g_strdup (path);
}

gboolean lypad_utils_magic_file_check (GFile *file)
{
    const int BUFFER_SIZE = sizeof LYPAD_MAGIC_HEADER - 1;
    char buffer[BUFFER_SIZE];
    GInputStream *stream = (GInputStream *)g_file_read (file, NULL, NULL);
    if (!stream)
        return FALSE;

    if (g_input_stream_read (stream, buffer, BUFFER_SIZE, NULL, NULL) != BUFFER_SIZE)
    {
        g_input_stream_close (stream, NULL, NULL);
        g_object_unref (stream);
        return FALSE;
    }

    if (memcmp (buffer, LYPAD_MAGIC_HEADER, BUFFER_SIZE) != 0)
    {
        g_input_stream_close (stream, NULL, NULL);
        g_object_unref (stream);
        return FALSE;
    }

    g_input_stream_close (stream, NULL, NULL);
    g_object_unref (stream);
    return TRUE;
}

char *lypad_utils_strdup_printf(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    char *locale = setlocale(LC_NUMERIC, NULL);
    setlocale(LC_NUMERIC, "C");
    char *str = g_strdup_vprintf(format, args);
    setlocale(LC_NUMERIC, locale);
    va_end(args);
    return str;
}
