/*
 *  lypad-color-wheel.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-color-wheel.h"
#include "lypad-color-widget.h"
#include <math.h>

struct _LypadColorWheel
{
    GtkWidget   parent;
    GtkGesture *gesture;
    int         radius;

    double      hue;
    double      saturation;
    double      value;
    double      alpha;

    gboolean    focus_on_triangle;
    GdkCursor  *cursor;
};

static void color_widget_iface_init (LypadColorWidgetInterface *iface);

G_DEFINE_TYPE_WITH_CODE (LypadColorWheel, lypad_color_wheel, GTK_TYPE_WIDGET,
                         G_IMPLEMENT_INTERFACE (LYPAD_TYPE_COLOR_WIDGET, color_widget_iface_init))

#define INNER_RADIUS_FRACTION (0.8)
#define MINIMUM_RADIUS (100)
#define DRAW_PADDING (3)

static inline double square (double x)
{
    return x * x;
}

static void lypad_color_wheel_set_color (LypadColorWidget *widget, const LypadColor *color)
{
    LypadColorWheel *wheel = LYPAD_COLOR_WHEEL (widget);
    lypad_color_to_hsv (color, &wheel->hue, &wheel->saturation, &wheel->value, &wheel->alpha);
    gtk_widget_queue_draw (GTK_WIDGET (wheel));
}

static void lypad_color_wheel_get_color (LypadColorWidget *widget, LypadColor *color)
{
    LypadColorWheel *wheel = LYPAD_COLOR_WHEEL (widget);
    lypad_color_from_hsv (color, wheel->hue, wheel->saturation, wheel->value, wheel->alpha);
}

static void color_widget_iface_init (LypadColorWidgetInterface *iface)
{
    iface->set_color = lypad_color_wheel_set_color;
    iface->get_color = lypad_color_wheel_get_color;
}

static void get_triangle_vertices (LypadColorWheel *wheel, double *x1, double *y1, double *x2, double *y2, double *x3, double *y3)
{
    double center_x = 0.5 * gtk_widget_get_allocated_width (GTK_WIDGET (wheel));
    double center_y = 0.5 * gtk_widget_get_allocated_height (GTK_WIDGET (wheel));
    double radius = wheel->radius * INNER_RADIUS_FRACTION;
    double angle = wheel->hue * 2.0 * G_PI;

    *x1 = center_x + radius * cos (angle);
    *y1 = center_y - radius * sin (angle);
    *x2 = center_x + radius * cos (angle + 2.0 * G_PI / 3.0);
    *y2 = center_y - radius * sin (angle + 2.0 * G_PI / 3.0);
    *x3 = center_x + radius * cos (angle + 4.0 * G_PI / 3.0);
    *y3 = center_y - radius * sin (angle + 4.0 * G_PI / 3.0);
}

static gboolean is_point_in_circle (LypadColorWheel *wheel, int x, int y)
{
    double center_x = 0.5 * gtk_widget_get_allocated_width (GTK_WIDGET (wheel));
    double center_y = 0.5 * gtk_widget_get_allocated_height (GTK_WIDGET (wheel));
    double outter_radius2 = square (wheel->radius);
    double inner_radius2 = outter_radius2 * INNER_RADIUS_FRACTION * INNER_RADIUS_FRACTION;
    double r2 = square (x - center_x) + square (y - center_y);
    return r2 >= inner_radius2 && r2 <= outter_radius2;
}

static gboolean is_point_in_triangle (LypadColorWheel *wheel, int x, int y)
{
    double x1, y1, x2, y2, x3, y3;
    get_triangle_vertices (wheel, &x1, &y1, &x2, &y2, &x3, &y3);

    double s1 = (x - x1) * (y2 - y1) - (y - y1) * (x2 - x1);
    double s2 = (x - x2) * (y3 - y2) - (y - y2) * (x3 - x2);
    double s3 = (x - x3) * (y1 - y3) - (y - y3) * (x1 - x3);
    return s1 >= 0 && s2 >= 0 && s3 >= 0;
}

static void lypad_color_wheel_set_hue_from_point (LypadColorWheel *wheel, int x, int y)
{
    double center_x = 0.5 * gtk_widget_get_allocated_width (GTK_WIDGET (wheel));
    double center_y = 0.5 * gtk_widget_get_allocated_height (GTK_WIDGET (wheel));
    double angle = atan2 (center_y - y, x - center_x);
    if (angle < 0.0)
        angle += 2.0 * G_PI;
    wheel->hue = angle * (1. / (2.0 * G_PI));
    gtk_widget_queue_draw (GTK_WIDGET (wheel));
    _lypad_color_widget_changed (LYPAD_COLOR_WIDGET (wheel));
}

static void lypad_color_wheel_set_sv_from_point (LypadColorWheel *wheel, int x, int y)
{
    double center_x = 0.5 * gtk_widget_get_allocated_width (GTK_WIDGET (wheel));
    double center_y = 0.5 * gtk_widget_get_allocated_height (GTK_WIDGET (wheel));
    double x1, y1, x2, y2, x3, y3;
    double s, v;
    get_triangle_vertices (wheel, &x1, &y1, &x2, &y2, &x3, &y3);

    if ((x - x1) * (y2 - y1) - (y - y1) * (x2 - x1) < 0)
    {
        double Vx = x1 - x2;
        double Vy = y1 - y2;
        double d = Vx * Vx + Vy * Vy;
        wheel->saturation = 1;
        v = (Vx * (x - x2) + Vy * (y - y2)) / d;
        wheel->value = CLAMP (v, 0, 1);
    }
    else if ((x - x2) * (y3 - y2) - (y - y2) * (x3 - x2) < 0)
    {
        double Vx = x3 - x2;
        double Vy = y3 - y2;
        double d = Vx * Vx + Vy * Vy;
        wheel->saturation = 0;
        v = (Vx * (x - x2) + Vy * (y - y2)) / d;
        wheel->value = CLAMP (v, 0, 1);
    }
    else if ((x - x3) * (y1 - y3) - (y - y3) * (x1 - x3) < 0)
    {
        double Vx = x1 - x3;
        double Vy = y1 - y3;
        double d = Vx * Vx + Vy * Vy;
        wheel->value = 1;
        s = (Vx * (x - x3) + Vy * (y - y3)) / d;
        wheel->saturation = CLAMP (s, 0, 1);
    }
    else
    {
        double Vx = center_x - x2;
        double Vy = center_y - y2;
        double d = (3.0 / 2.0) * (Vx * Vx + Vy * Vy);
        Vx /= d;
        Vy /= d;

        v = (x - x2) * Vx + (y - y2) * Vy;
        double Ax = x2 + v * (x3 - x2);
        double Ay = y2 + v * (y3 - y2);
        s = (x - Ax) * (x1 - x3) + (y - Ay) * (y1 - y3);
        d = square (x1 - x3) + square (y1 - y3);
        s /= d * v;

        s = CLAMP (s, 0, 1);
        v = CLAMP (v, 0, 1);
        wheel->saturation = s;
        wheel->value = v;
    }
    gtk_widget_queue_draw (GTK_WIDGET (wheel));
    _lypad_color_widget_changed (LYPAD_COLOR_WIDGET (wheel));
}

static void on_gesture_pressed (GtkGestureMultiPress *gesture, gint n_press, double x, double y, gpointer user_data)
{
    LypadColorWheel *wheel = LYPAD_COLOR_WHEEL (user_data);
    GdkEventSequence *sequence = gtk_gesture_single_get_current_sequence (GTK_GESTURE_SINGLE (gesture));
    if (is_point_in_circle (wheel, x, y))
    {
        lypad_color_wheel_set_hue_from_point (wheel, x, y);
        wheel->focus_on_triangle = FALSE;
        gtk_gesture_set_sequence_state (GTK_GESTURE (gesture), sequence, GTK_EVENT_SEQUENCE_CLAIMED);
        gdk_window_set_cursor (gtk_widget_get_window (GTK_WIDGET (wheel)), wheel->cursor);
    }
    else if (is_point_in_triangle (wheel, x, y))
    {
        lypad_color_wheel_set_sv_from_point (wheel, x, y);
        wheel->focus_on_triangle = TRUE;
        gtk_gesture_set_sequence_state (GTK_GESTURE (gesture), sequence, GTK_EVENT_SEQUENCE_CLAIMED);
        gdk_window_set_cursor (gtk_widget_get_window (GTK_WIDGET (wheel)), wheel->cursor);
    }
    else
        gtk_gesture_set_sequence_state (GTK_GESTURE (gesture), sequence, GTK_EVENT_SEQUENCE_DENIED);
}

static void on_gesture_update (GtkGesture *gesture, GdkEventSequence *sequence, gpointer user_data)
{
    LypadColorWheel *wheel = LYPAD_COLOR_WHEEL (user_data);
    double x, y;
    gtk_gesture_get_point (gesture, sequence, &x, &y);
    if (wheel->focus_on_triangle)
        lypad_color_wheel_set_sv_from_point (wheel, x, y);
    else
        lypad_color_wheel_set_hue_from_point (wheel, x, y);
}

static void on_gesture_end (GtkGesture *gesture, GdkEventSequence *sequence, gpointer user_data)
{
    gdk_window_set_cursor (gtk_widget_get_window (GTK_WIDGET (user_data)), NULL);
}

static void lypad_color_wheel_init (LypadColorWheel *wheel)
{
    gtk_widget_set_has_window (GTK_WIDGET (wheel), TRUE);
    wheel->radius = MINIMUM_RADIUS;
    wheel->focus_on_triangle = TRUE;
    wheel->cursor = NULL;

    wheel->hue = 0;
    wheel->saturation = 1;
    wheel->value = 1;
    wheel->alpha = 1;

    wheel->gesture = gtk_gesture_multi_press_new (GTK_WIDGET (wheel));
    g_signal_connect (wheel->gesture, "pressed", G_CALLBACK (on_gesture_pressed), wheel);
    g_signal_connect (wheel->gesture, "update", G_CALLBACK (on_gesture_update), wheel);
    g_signal_connect (wheel->gesture, "end", G_CALLBACK (on_gesture_end), wheel);

    GtkStyleContext *context = gtk_widget_get_style_context (GTK_WIDGET (wheel));
    gtk_style_context_add_class (context, GTK_STYLE_CLASS_FRAME);
}

static gboolean lypad_color_wheel_draw (GtkWidget *widget, cairo_t *cr)
{
    LypadColorWheel *wheel = LYPAD_COLOR_WHEEL (widget);

    double dx, dy, dx2, dy2, radius2, angle;
    double hue, r, g, b;
    int width = gtk_widget_get_allocated_width (widget);
    int height = gtk_widget_get_allocated_height (widget);
    double center_x = 0.5 * width;
    double center_y = 0.5 * height;

    int stride = cairo_format_stride_for_width (CAIRO_FORMAT_RGB24, width);
    guchar *data = g_new (guchar, stride * height);

    /* Draw ring */
    double outter_radius2 = square (wheel->radius + DRAW_PADDING);
    double inner_radius2 = square (wheel->radius * INNER_RADIUS_FRACTION - DRAW_PADDING);

    for (int y = 0; y < height; ++y)
    {
        dy = center_y - y;
        dy2 = dy * dy;
        guchar *pixel = data + y * stride;
        for (int x = 0; x < width; ++x, pixel += 4)
        {
            dx = x - center_x;
            dx2 = dx * dx;
            radius2 = dx2 + dy2;
            if (radius2 >= inner_radius2 && radius2 < outter_radius2)
            {
                angle = atan2 (dy, dx);
                if (angle < 0.0)
                    angle += 2.0 * G_PI;
                hue = angle * (1 / (2.0 * G_PI));
                gtk_hsv_to_rgb (hue, 1, 1, &r, &g, &b);
                pixel[2] = (r * 255 + 0.5);
                pixel[1] = (g * 255 + 0.5);
                pixel[0] = (b * 255 + 0.5);
            }
        }
    }

    cairo_surface_t *source = cairo_image_surface_create_for_data (data, CAIRO_FORMAT_RGB24, width, height, stride);
    cairo_set_source_surface (cr, source, 0, 0);
    cairo_new_path (cr);
    cairo_set_line_width (cr, wheel->radius * (1 - INNER_RADIUS_FRACTION));
    cairo_arc (cr, center_x, center_y, wheel->radius * 0.5 * (1 + INNER_RADIUS_FRACTION), 0, 2 * G_PI);
    cairo_stroke (cr);
    cairo_surface_destroy (source);

    /* Draw triangle */
    double x1, y1, x2, y2, x3, y3;
    get_triangle_vertices (wheel, &x1, &y1, &x2, &y2, &x3, &y3);

    int y_min = MIN (MIN (y1, y2), y3);
    int y_max = MAX (MAX (y1, y2), y3);
    int x_min = MIN (MIN (x1, x2), x3);
    int x_max = MAX (MAX (x1, x2), x3);

    double v, s;
    double Ax, Ay;
    double Vx = center_x - x2;
    double Vy = center_y - y2;
    double d = (3.0 / 2.0) * (Vx * Vx + Vy * Vy);
    Vx /= d;
    Vy /= d;

    d = square (x1 - x2) + square (y1 - y2);

    for (int y = y_min; y < y_max; ++y)
    {
        guchar *pixel = data + y * stride + 4 * x_min;
        for (int x = x_min; x < x_max; ++x, pixel += 4)
        {
            v = (x - x2) * Vx + (y - y2) * Vy;

            Ax = x2 + v * (x3 - x2);
            Ay = y2 + v * (y3 - y2);
            s = (x - Ax) * (x1 - x3) + (y - Ay) * (y1 - y3);
            s /= d * v;

            s = CLAMP (s, 0, 1);
            v = CLAMP (v, 0, 1);
            gtk_hsv_to_rgb (wheel->hue, s, v, &r, &g, &b);

            pixel[2] = (r * 255 + 0.5);
            pixel[1] = (g * 255 + 0.5);
            pixel[0] = (b * 255 + 0.5);
        }
    }

    source = cairo_image_surface_create_for_data (data, CAIRO_FORMAT_RGB24, width, height, stride);
    cairo_set_source_surface (cr, source, 0, 0);
    cairo_move_to (cr, x1, y1);
    cairo_line_to (cr, x2, y2);
    cairo_line_to (cr, x3, y3);
    cairo_close_path (cr);
    cairo_fill (cr);
    cairo_surface_destroy (source);

    /* Draw indicators */

    cairo_set_line_width (cr, 2.2);

    angle = wheel->hue * (2.0 * G_PI);
    r = wheel->radius * INNER_RADIUS_FRACTION;
    cairo_set_source_rgb (cr, 0.0, 0.0, 0.0);
    cairo_move_to (cr, center_x + r * cos (angle), center_y - r * sin (angle));
    cairo_line_to (cr, center_x + wheel->radius * cos (angle), center_y - wheel->radius * sin (angle));
    cairo_stroke (cr);

    double marker_x = x2 + (x3 - x2) * wheel->value + (x1 - x3) * wheel->saturation * wheel->value;
    double marker_y = y2 + (y3 - y2) * wheel->value + (y1 - y3) * wheel->saturation * wheel->value;

    cairo_set_source_rgb (cr, 1.0, 1.0, 1.0);
    cairo_arc (cr, marker_x, marker_y, 3.5, 0, 2 * G_PI);
    cairo_stroke (cr);
    cairo_set_source_rgb (cr, 0.0, 0.0, 0.0);
    cairo_arc (cr, marker_x, marker_y, 5.5, 0, 2 * G_PI);
    cairo_stroke (cr);

    g_free (data);
    return TRUE;
}

static void lypad_color_wheel_realize (GtkWidget *widget)
{
    GtkAllocation allocation;
    GdkWindow *window;
    GdkWindowAttr attributes;
    gint attributes_mask;

    gtk_widget_get_allocation (widget, &allocation);

    attributes.x = allocation.x;
    attributes.y = allocation.y;
    attributes.width = allocation.width;
    attributes.height = allocation.height;
    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.event_mask = gtk_widget_get_events (widget);
    attributes.visual = gtk_widget_get_visual (widget);
    attributes.wclass = GDK_INPUT_OUTPUT;

    attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL;

    window = gdk_window_new (gtk_widget_get_parent_window (widget), &attributes, attributes_mask);

    gtk_widget_set_window (widget, window);
    gtk_widget_register_window (widget, window);
    gtk_widget_set_realized (widget, TRUE);

    LYPAD_COLOR_WHEEL (widget)->cursor = gdk_cursor_new_from_name (gdk_window_get_display (window), "crosshair");
}


static void lypad_color_wheel_finalize (GObject *object)
{
    LypadColorWheel *wheel = LYPAD_COLOR_WHEEL (object);
    g_clear_object (&wheel->gesture);
    g_clear_object (&wheel->cursor);
    G_OBJECT_CLASS (lypad_color_wheel_parent_class)->finalize (object);
}

static void lypad_color_wheel_get_preferred_size (GtkWidget *widget, gint *minimum, gint *natural)
{
    *minimum = *natural = 2 * MINIMUM_RADIUS;
}

static void lypad_color_wheel_size_allocate (GtkWidget *widget, GtkAllocation *allocation)
{
    LypadColorWheel *wheel = LYPAD_COLOR_WHEEL (widget);
    wheel->radius = MIN (allocation->width, allocation->height) / 2;
    GTK_WIDGET_CLASS (lypad_color_wheel_parent_class)->size_allocate (widget, allocation);
}

static void lypad_color_wheel_class_init (LypadColorWheelClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

    object_class->finalize             = lypad_color_wheel_finalize;
    widget_class->get_preferred_width  = lypad_color_wheel_get_preferred_size;
    widget_class->get_preferred_height = lypad_color_wheel_get_preferred_size;
    widget_class->size_allocate        = lypad_color_wheel_size_allocate;
    widget_class->draw                 = lypad_color_wheel_draw;
    widget_class->realize              = lypad_color_wheel_realize;
}

GtkWidget *lypad_color_wheel_new ()
{
    return g_object_new (LYPAD_TYPE_COLOR_WHEEL, NULL);
}
