/*
 *  lypad-color-picker-button.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-color-picker-button.h"
#include "lypad-color-widget.h"

struct _LypadColorPickerButton
{
    GtkButton   parent;
    LypadColor  color;
    GtkWidget  *grab_widget;
    GdkCursor  *grab_cursor;
};

static void color_widget_iface_init (LypadColorWidgetInterface *iface);

G_DEFINE_TYPE_WITH_CODE (LypadColorPickerButton, lypad_color_picker_button, GTK_TYPE_BUTTON,
                         G_IMPLEMENT_INTERFACE (LYPAD_TYPE_COLOR_WIDGET, color_widget_iface_init))

static void lypad_color_picker_button_set_color (LypadColorWidget *widget, const LypadColor *color)
{
    LYPAD_COLOR_PICKER_BUTTON (widget)->color = *color;
}

static void lypad_color_picker_button_get_color (LypadColorWidget *widget, LypadColor *color)
{
    *color = LYPAD_COLOR_PICKER_BUTTON (widget)->color;
}

static void color_widget_iface_init (LypadColorWidgetInterface *iface)
{
    iface->set_color = lypad_color_picker_button_set_color;
    iface->get_color = lypad_color_picker_button_get_color;
}

static void lypad_color_picker_button_init (LypadColorPickerButton *button)
{
    lypad_color_init (&button->color);

    GtkWidget *image = gtk_image_new_from_icon_name ("color-select-symbolic", GTK_ICON_SIZE_BUTTON);
    gtk_button_set_image (GTK_BUTTON (button), image);
    gtk_widget_set_tooltip_text (GTK_WIDGET (button), "Pick color from screen");

    gtk_style_context_add_class (gtk_widget_get_style_context (GTK_WIDGET (button)), "circular");

    button->grab_widget = NULL;
    button->grab_cursor = NULL;
}

// TODO: Handle failure
static void pick_screen_color (LypadColorPickerButton *button, GdkEvent *event)
{
    double x_root, y_root;
    int x_win, y_win;

    GdkScreen *screen = gdk_event_get_screen (event);
    GdkWindow *window = gdk_device_get_window_at_position (gdk_event_get_device (event), &x_win, &y_win);
    if (window)
    {
        x_root = x_win;
        y_root = y_win;
    }
    else
    {
        window = gdk_screen_get_root_window (screen);
        gdk_event_get_root_coords (event, &x_root, &y_root);
    }

    cairo_surface_t *surface = cairo_image_surface_create (CAIRO_FORMAT_RGB24, 1, 1);
    cairo_t *cr = cairo_create (surface);
    gdk_cairo_set_source_window (cr, window, -x_root, -y_root);
    cairo_paint (cr);
    cairo_destroy (cr);

    GdkRGBA rgb;
    guchar *data = cairo_image_surface_get_data (surface);
    rgb.red   = (double) data[2] / 255.;
    rgb.green = (double) data[1] / 255.;
    rgb.blue  = (double) data[0] / 255.;
    rgb.alpha = 1.;
    rgb.red   = CLAMP (rgb.red  , 0, 1);
    rgb.green = CLAMP (rgb.green, 0, 1);
    rgb.blue  = CLAMP (rgb.blue , 0, 1);
    lypad_color_from_rgb (&button->color, &rgb);

    cairo_surface_destroy (surface);
    _lypad_color_widget_changed (LYPAD_COLOR_WIDGET (button));
}

static void drag_widget_finalize_drag (GtkWidget *widget)
{
    GdkDisplay *display = gtk_widget_get_display (widget);
    gtk_grab_remove (widget);
    gdk_seat_ungrab (gdk_display_get_default_seat (display));
}

static gboolean grab_widget_button_release (GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    if (event->button == GDK_BUTTON_PRIMARY)
    {
        pick_screen_color (LYPAD_COLOR_PICKER_BUTTON (user_data), (GdkEvent *) event);
        drag_widget_finalize_drag (widget);
        return TRUE;
    }
    return FALSE;
}

static gboolean grab_widget_key_press (GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
    if (event->keyval == GDK_KEY_Escape)
    {
        drag_widget_finalize_drag (widget);
        return TRUE;
    }
    return FALSE;
}

static inline void lypad_color_picker_button_initialize_grab (LypadColorPickerButton *button)
{
    if (!button->grab_widget)
    {
        button->grab_widget = gtk_invisible_new ();
        gtk_widget_add_events (button->grab_widget, GDK_BUTTON_RELEASE_MASK | GDK_KEY_PRESS_MASK);

        g_signal_connect (button->grab_widget, "button-release-event", G_CALLBACK (grab_widget_button_release), button);
        g_signal_connect (button->grab_widget, "key-press-event", G_CALLBACK (grab_widget_key_press), button);

        gtk_widget_show (button->grab_widget);
    }
    if (!button->grab_cursor)
        button->grab_cursor = gdk_cursor_new_from_name (gtk_widget_get_display (GTK_WIDGET (button)), "cell");
}

static void lypad_color_picker_button_clicked (GtkButton *widget)
{
    LypadColorPickerButton *button = LYPAD_COLOR_PICKER_BUTTON (widget);
    lypad_color_picker_button_initialize_grab (button);

    GdkDisplay *display = gtk_widget_get_display (button->grab_widget);
    if (gdk_seat_grab (gdk_display_get_default_seat (display), gtk_widget_get_window (button->grab_widget),
                       GDK_SEAT_CAPABILITY_ALL, FALSE, button->grab_cursor, NULL, NULL, NULL) != GDK_GRAB_SUCCESS)
    {
        g_warning ("LypadColorPickerButton::clicked error: Failed to grab seat.");
        return;
    }

    gtk_grab_add (button->grab_widget);
}

static void lypad_color_picker_button_destroy (GtkWidget *widget)
{
    LypadColorPickerButton *button = LYPAD_COLOR_PICKER_BUTTON (widget);
    if (button->grab_widget)
    {
        gtk_widget_destroy (button->grab_widget);
        button->grab_widget = NULL;
    }
    g_clear_object (&button->grab_cursor);
    GTK_WIDGET_CLASS (lypad_color_picker_button_parent_class)->destroy (widget);
}

static void lypad_color_picker_button_class_init (LypadColorPickerButtonClass *klass)
{
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
    GtkButtonClass *button_class = GTK_BUTTON_CLASS (klass);

    button_class->clicked = lypad_color_picker_button_clicked;
    widget_class->destroy = lypad_color_picker_button_destroy;
}

GtkWidget *lypad_color_picker_button_new ()
{
    return g_object_new (LYPAD_TYPE_COLOR_PICKER_BUTTON, NULL);
}
