/*
 *  lypad-color.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-color.h"

void lypad_color_init (LypadColor *color)
{
    color->_h = 0.0;
    color->_s = 1.0;
    color->_l = 0.5;
    color->_a = 1.0;
}

/* Util functions */

static void get_hsl_from_rgb (double r, double g, double b, double *h, double *s, double *l)
{
    double min, max;
    if (r == g && g == b)
    {
        if (h)
            *h = 0;
        min = r;
        max = r;
    }
    else if (r > b && r > g)
    {
        min = MIN (b, g);
        max = r;
        if (h)
        {
            *h = (g - b) / (max - min) / 6.;
            if (*h < 0)
                *h += 1.;
        }
    }
    else if (g > b)
    {
        min = MIN (r, b);
        max = g;
        if (h)
            *h = (2. + (b - r) / (max - min)) / 6.;
    }
    else
    {
        min = MIN (r, g);
        max = b;
        if (h)
            *h = (4. + (r - g) / (max - min)) / 6.;
    }

    double sum = min + max;

    if (l)
        *l = 0.5 * sum;

    if (s)
    {
        if (sum == 0. || sum == 2.)
            *s = 0;
        else if (sum < 1)
            *s = (max - min) / sum;
        else
            *s =  (max - min) / (2. - sum);
    }
}


static void get_rgb_from_hsl (double h, double s, double l, double *r, double *g, double *b)
{
    double c = 2 * s * (l < 0.5 ? l : 1. - l);
    double h1 = h * 6;
    double red, green, blue;
    if (h1 < 1.)
    {
        red   = c;
        green = c * h1;
        blue  = 0;
    }
    else if (h1 < 2.)
    {
        red   = c * (2. - h1);
        green = c;
        blue  = 0;
    }
    else if (h1 < 3.)
    {
        red   = 0;
        green = c;
        blue  = c * (h1 - 2.);
    }
    else if (h1 < 4.)
    {
        red   = 0;
        green = c * (4. - h1);
        blue  = c;
    }
    else if (h1 < 5.)
    {
        red   = c * (h1 - 4.);
        green = 0;
        blue  = c;
    }
    else
    {
        red   = c;
        green = 0;
        blue  = c * (6 - h1);
    }
    double m = l - 0.5 * c;
    red   += m;
    green += m;
    blue  += m;
    if (r)
        *r = red;
    if (g)
        *g = green;
    if (b)
        *b = blue;
}

static void get_sl_from_sv (double sv, double v, double *sl, double *l)
{
    double lumi = v * (1. - 0.5 * sv);
    if (l)
        *l = lumi;
    if (sl)
    {
        if (lumi == 0.)
            *sl = sv / (2. - sv);
        else if (lumi == 1.)
            *sl = 0.;
        else
            *sl = (v - lumi) / (lumi < 0.5 ? lumi : 1. - lumi);
    }
}

static void get_sv_from_sl (double sl, double l, double *sv, double *v)
{
    double val = l + sl * (l < 0.5 ? l : 1. - l);
    if (v)
        *v = val;
    if (sv)
    {
        if (val == 0.)
            *sv = 2. * (sl / (sl + 1));
        else
            *sv = 2. * (1. - l / val);
    }
}

/* Get components */

double lypad_color_get_red (const LypadColor *color)
{
    double c = color->_s * (color->_l < 0.5 ? color->_l : 1. - color->_l);
    double red = color->_l - c;
    double h1 = color->_h * 6;
    if (h1 < 1. || h1 >= 5.)
        red += 2. * c;
    else if (h1 < 2.)
        red += 2. * c * (2. - h1);
    else if (h1 > 4.)
        red += 2. * c * (h1 - 4.);
    return red;
}

double lypad_color_get_green (const LypadColor *color)
{
    double c = color->_s * (color->_l < 0.5 ? color->_l : 1. - color->_l);
    double green = color->_l - c;
    double h1 = color->_h * 6;
    if (h1 < 1.)
        green += 2. * c * h1;
    else if (h1 < 3.)
        green += 2. * c;
    else if (h1 < 4.)
        green += 2. * c * (4. - h1);
    return green;
}

double lypad_color_get_blue (const LypadColor *color)
{
    double c = color->_s * (color->_l < 0.5 ? color->_l : 1. - color->_l);
    double blue = color->_l - c;
    double h1 = color->_h * 6;
    if (h1 > 5)
        blue += 2. * c * (6 - h1);
    else if (h1 > 3.)
        blue += 2. * c;
    else if (h1 > 2.)
        blue += 2. * c * (h1 - 2.);
    return blue;
}

double lypad_color_get_hue (const LypadColor *color)
{
    return color->_h;
}

double lypad_color_get_saturation_l (const LypadColor *color)
{
    return color->_s;
}

double lypad_color_get_saturation_v (const LypadColor *color)
{
    double value = color->_l + color->_s * (color->_l < 0.5 ? color->_l : 1. - color->_l);
    if (value == 0.)
        return 2. * (color->_s / (color->_s + 1));
    else
        return 2. * (1. - color->_l / value);
}

double lypad_color_get_luminosity (const LypadColor *color)
{
    return color->_l;
}

double lypad_color_get_value (const LypadColor *color)
{
    return color->_l + color->_s * (color->_l < 0.5 ? color->_l : 1. - color->_l);
}

double lypad_color_get_alpha (const LypadColor *color)
{
    return color->_a;
}

/* Update components */

void lypad_color_set_red (LypadColor *color, double r)
{
    double g, b;
    double h, s;
    get_rgb_from_hsl (color->_h, color->_s, color->_l, NULL, &g, &b);
    get_hsl_from_rgb (r, g, b, &h, &s, &color->_l);
    if (r != g || g != b)
        color->_h = h;
    if (color->_l != 0. && color->_l != 1.)
        color->_s = s;
}

void lypad_color_set_green (LypadColor *color, double g)
{
    double r, b;
    double h, s;
    get_rgb_from_hsl (color->_h, color->_s, color->_l, &r, NULL, &b);
    get_hsl_from_rgb (r, g, b, &h, &s, &color->_l);
    if (r != g || g != b)
        color->_h = h;
    if (color->_l != 0. && color->_l != 1.)
        color->_s = s;
}

void lypad_color_set_blue (LypadColor *color, double b)
{
    double r, g;
    double h, s;
    get_rgb_from_hsl (color->_h, color->_s, color->_l, &r, &g, NULL);
    get_hsl_from_rgb (r, g, b, &h, &s, &color->_l);
    if (r != g || g != b)
        color->_h = h;
    if (color->_l != 0. && color->_l != 1.)
        color->_s = s;
}

void lypad_color_set_hue (LypadColor *color, double h)
{
    color->_h = h;
}

void lypad_color_set_saturation_l (LypadColor *color, double sl)
{
    color->_s = sl;
}

void lypad_color_set_saturation_v (LypadColor *color, double sv)
{
    double v, sl;
    get_sv_from_sl (color->_s, color->_l, NULL, &v);
    get_sl_from_sv (sv, v, &sl, &color->_l);
    if (color->_l != 0. || color->_l != 1.)
        color->_s = sl;
}

void lypad_color_set_luminosity (LypadColor *color, double l)
{
    color->_l = l;
}

void lypad_color_set_value (LypadColor *color, double v)
{
    double sv, sl;
    get_sv_from_sl (color->_s, color->_l, &sv, NULL);
    get_sl_from_sv (sv, v, &sl, &color->_l);
    if (color->_l != 0. || color->_l != 1.)
        color->_s = sl;
}

void lypad_color_set_alpha (LypadColor *color, double a)
{
    color->_a = a;
}

/* Transform space */

void lypad_color_to_rgb (const LypadColor *color, GdkRGBA *rgb)
{
    get_rgb_from_hsl (color->_h, color->_s, color->_l, &rgb->red, &rgb->green, &rgb->blue);
    rgb->alpha = color->_a;
}

void lypad_color_from_rgb (LypadColor *color, const GdkRGBA *rgb)
{
    get_hsl_from_rgb (rgb->red, rgb->green, rgb->blue, &color->_h, &color->_s, &color->_l);
    color->_a = rgb->alpha;
}

void lypad_color_from_hsv (LypadColor *color, double h, double s, double v, double a)
{
    color->_h = h;
    get_sl_from_sv (s, v, &color->_s, &color->_l);
    color->_a = a;
}

void lypad_color_to_hsv (const LypadColor *color, double *h, double *s, double *v, double *a)
{
    *h = color->_h;
    get_sv_from_sl (color->_s, color->_l, s, v);
    *a = color->_a;
}

void lypad_color_from_hsl (LypadColor *color, double h, double s, double l, double a)
{
    color->_h = h;
    color->_s = s;
    color->_l = l;
    color->_a = a;
}
