/*
 *  lypad-color-swatch.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define LYPAD_TYPE_COLOR_SWATCH (lypad_color_swatch_get_type ())
G_DECLARE_FINAL_TYPE (LypadColorSwatch, lypad_color_swatch, LYPAD, COLOR_SWATCH, GtkWidget)

#define LYPAD_BORDERS_NONE    0
#define LYPAD_BORDERS_LEFT    1
#define LYPAD_BORDERS_RIGHT   2
#define LYPAD_BORDERS_TOP     4
#define LYPAD_BORDERS_BOTTOM  8
#define LYPAD_BORDERS_ALL    15

GtkWidget *lypad_color_swatch_new                (void);
void       lypad_color_swatch_set_borders        (LypadColorSwatch *swatch, gint8 borders);
void       lypad_color_swatch_set_show_hover_box (LypadColorSwatch *swatch, gboolean show);
void       lypad_color_swatch_set_selected       (LypadColorSwatch *swatch, gboolean selected);

G_END_DECLS
