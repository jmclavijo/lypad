/*
 *  lypad-color-slider.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-color-slider.h"

struct _LypadColorSlider
{
    GtkWidget   parent;
    GtkGesture *gesture;
    LypadColor  color;
    int         channel;
    double      value;
};

static void color_widget_iface_init (LypadColorWidgetInterface *iface);

G_DEFINE_TYPE_WITH_CODE (LypadColorSlider, lypad_color_slider, GTK_TYPE_WIDGET,
                         G_IMPLEMENT_INTERFACE (LYPAD_TYPE_COLOR_WIDGET, color_widget_iface_init))

#define SLIDER_WIDTH  (224)
#define SLIDER_HEIGHT (32)
#define ARROW_SIZE    (4)

static void lypad_color_slider_set_color (LypadColorWidget *widget, const LypadColor *color)
{
    LypadColorSlider *slider = LYPAD_COLOR_SLIDER (widget);
    slider->color = *color;
    switch (slider->channel)
    {
        case LYPAD_CHANNEL_RED:
            slider->value = lypad_color_get_red (color);
            break;

        case LYPAD_CHANNEL_GREEN:
            slider->value = lypad_color_get_green (color);
            break;

        case LYPAD_CHANNEL_BLUE:
            slider->value = lypad_color_get_blue (color);
            break;

        case LYPAD_CHANNEL_ALPHA:
            slider->value = lypad_color_get_alpha (color);
            break;

        case LYPAD_CHANNEL_HUE:
            slider->value = lypad_color_get_hue (color);
            break;

        case LYPAD_CHANNEL_SATURATION_L:
            slider->value = lypad_color_get_saturation_l (color);
            break;

        case LYPAD_CHANNEL_LUMINOSITY:
            slider->value = lypad_color_get_luminosity (color);
            break;

        case LYPAD_CHANNEL_SATURATION_V:
            slider->value = lypad_color_get_saturation_v (color);
            break;

        case LYPAD_CHANNEL_VALUE:
            slider->value = lypad_color_get_value (color);
            break;

        default:
            g_warning ("LypadColorSlider::set-color error: Invalid color channel");
    }
    gtk_widget_queue_draw (GTK_WIDGET (slider));
}

static void lypad_color_slider_get_color (LypadColorWidget *widget, LypadColor *color)
{
    *color = LYPAD_COLOR_SLIDER (widget)->color;
}

static void color_widget_iface_init (LypadColorWidgetInterface *iface)
{
    iface->set_color = lypad_color_slider_set_color;
    iface->get_color = lypad_color_slider_get_color;
}

static void lypad_color_slider_set_value (LypadColorSlider *slider, double value)
{
    switch (slider->channel)
    {
        case LYPAD_CHANNEL_RED:
            lypad_color_set_red (&slider->color, value);
            break;

        case LYPAD_CHANNEL_GREEN:
            lypad_color_set_green (&slider->color, value);
            break;

        case LYPAD_CHANNEL_BLUE:
            lypad_color_set_blue (&slider->color, value);
            break;

        case LYPAD_CHANNEL_ALPHA:
            lypad_color_set_alpha (&slider->color, value);
            break;

        case LYPAD_CHANNEL_HUE:
            lypad_color_set_hue (&slider->color, value);
            break;

        case LYPAD_CHANNEL_SATURATION_L:
            lypad_color_set_saturation_l (&slider->color, value);
            break;

        case LYPAD_CHANNEL_LUMINOSITY:
            lypad_color_set_luminosity (&slider->color, value);
            break;

        case LYPAD_CHANNEL_SATURATION_V:
            lypad_color_set_saturation_v (&slider->color, value);
            break;

        case LYPAD_CHANNEL_VALUE:
            lypad_color_set_value (&slider->color, value);
            break;

        default:
            g_warning ("LypadColorSlider::set-value error: Invalid color channel");
    }
    slider->value = value;
    _lypad_color_widget_changed (LYPAD_COLOR_WIDGET (slider));
    gtk_widget_queue_draw (GTK_WIDGET (slider));
}

void lypad_color_slider_set_channel (LypadColorSlider *slider, LypadColorChannel channel)
{
    slider->channel = channel;
    switch (channel)
    {
        case LYPAD_CHANNEL_RED:
            slider->value = lypad_color_get_red (&slider->color);
            break;

        case LYPAD_CHANNEL_GREEN:
            slider->value = lypad_color_get_green (&slider->color);
            break;

        case LYPAD_CHANNEL_BLUE:
            slider->value = lypad_color_get_blue (&slider->color);
            break;

        case LYPAD_CHANNEL_ALPHA:
            slider->value = lypad_color_get_alpha (&slider->color);
            break;

        case LYPAD_CHANNEL_HUE:
            slider->value = lypad_color_get_hue (&slider->color);
            break;

        case LYPAD_CHANNEL_SATURATION_L:
            slider->value = lypad_color_get_saturation_l (&slider->color);
            break;

        case LYPAD_CHANNEL_LUMINOSITY:
            slider->value = lypad_color_get_luminosity (&slider->color);
            break;

        case LYPAD_CHANNEL_SATURATION_V:
            slider->value = lypad_color_get_saturation_v (&slider->color);
            break;

        case LYPAD_CHANNEL_VALUE:
            slider->value = lypad_color_get_value (&slider->color);
            break;

        default:
            g_warning ("LypadColorSlider::set-channel error: Invalid color channel");
    }
    gtk_widget_queue_draw (GTK_WIDGET (slider));
}

static void lypad_color_slider_set_value_from_point (LypadColorSlider *slider, double x, double y)
{
    int width = gtk_widget_get_allocated_width (GTK_WIDGET (slider));
    double value = x / width;
    value = CLAMP (value, 0., 1.);
    lypad_color_slider_set_value (slider, value);
}

static void on_gesture_pressed (GtkGestureMultiPress *gesture, gint n_press, double x, double y, gpointer user_data)
{
    LypadColorSlider *slider = LYPAD_COLOR_SLIDER (user_data);
    GdkEventSequence *sequence = gtk_gesture_single_get_current_sequence (GTK_GESTURE_SINGLE (gesture));
    gtk_gesture_set_sequence_state (GTK_GESTURE (gesture), sequence, GTK_EVENT_SEQUENCE_CLAIMED);
    lypad_color_slider_set_value_from_point (slider, x, y);
}

static void on_gesture_update (GtkGesture *gesture, GdkEventSequence *sequence, gpointer user_data)
{
    LypadColorSlider *slider = LYPAD_COLOR_SLIDER (user_data);
    double x, y;
    gtk_gesture_get_point (gesture, sequence, &x, &y);
    lypad_color_slider_set_value_from_point (slider, x, y);
}

static void lypad_color_slider_init (LypadColorSlider *slider)
{
    gtk_widget_set_has_window (GTK_WIDGET (slider), TRUE);

    slider->channel = LYPAD_CHANNEL_RED;
    lypad_color_init (&slider->color);
    slider->value = 1;

    slider->gesture = gtk_gesture_multi_press_new (GTK_WIDGET (slider));
    g_signal_connect (slider->gesture, "pressed", G_CALLBACK (on_gesture_pressed), slider);
    g_signal_connect (slider->gesture, "update", G_CALLBACK (on_gesture_update), slider);
}

static gboolean lypad_color_slider_draw (GtkWidget *widget, cairo_t *cr)
{
    LypadColorSlider *slider = LYPAD_COLOR_SLIDER (widget);
    GtkAllocation allocation;
    gtk_widget_get_allocation (widget, &allocation);

    if (slider->channel == LYPAD_CHANNEL_ALPHA)
    {
        cairo_pattern_t *checkered_pattern = _lypad_color_widget_get_checkered_pattern ();
        cairo_set_source_rgb (cr, 0.66, 0.66, 0.66);
        cairo_mask (cr, checkered_pattern);
        cairo_pattern_destroy (checkered_pattern);
    }

    GdkRGBA rgb;
    LypadColor tmp_color;
    cairo_pattern_t *pattern = cairo_pattern_create_linear (0, 0, allocation.width, 0);
    switch (slider->channel)
    {
        case LYPAD_CHANNEL_RED:
            lypad_color_to_rgb (&slider->color, &rgb);
            cairo_pattern_add_color_stop_rgb (pattern, 0.0, 0, rgb.green, rgb.blue);
            cairo_pattern_add_color_stop_rgb (pattern, 1.0, 1, rgb.green, rgb.blue);
            break;

        case LYPAD_CHANNEL_GREEN:
            lypad_color_to_rgb (&slider->color, &rgb);
            cairo_pattern_add_color_stop_rgb (pattern, 0.0, rgb.red, 0, rgb.blue);
            cairo_pattern_add_color_stop_rgb (pattern, 1.0, rgb.red, 1, rgb.blue);
            break;

        case LYPAD_CHANNEL_BLUE:
            lypad_color_to_rgb (&slider->color, &rgb);
            cairo_pattern_add_color_stop_rgb (pattern, 0.0, rgb.red, rgb.green, 0);
            cairo_pattern_add_color_stop_rgb (pattern, 1.0, rgb.red, rgb.green, 1);
            break;

        case LYPAD_CHANNEL_ALPHA:
            lypad_color_to_rgb (&slider->color, &rgb);
            cairo_pattern_add_color_stop_rgba (pattern, 0.0, rgb.red, rgb.green, rgb.blue, 0);
            cairo_pattern_add_color_stop_rgba (pattern, 1.0, rgb.red, rgb.green, rgb.blue, 1);
            break;

        case LYPAD_CHANNEL_HUE:
            cairo_pattern_add_color_stop_rgb (pattern, 0.     , 1., 0., 0.);
            cairo_pattern_add_color_stop_rgb (pattern, 1. / 6., 1., 1., 0.);
            cairo_pattern_add_color_stop_rgb (pattern, 2. / 6., 0., 1., 0.);
            cairo_pattern_add_color_stop_rgb (pattern, 3. / 6., 0., 1., 1.);
            cairo_pattern_add_color_stop_rgb (pattern, 4. / 6., 0., 0., 1.);
            cairo_pattern_add_color_stop_rgb (pattern, 5. / 6., 1., 0., 1.);
            cairo_pattern_add_color_stop_rgb (pattern, 1.     , 1., 0., 0.);
            break;

        case LYPAD_CHANNEL_SATURATION_L:
            tmp_color = slider->color;
            lypad_color_set_saturation_l (&tmp_color, 0.);
            lypad_color_to_rgb (&tmp_color, &rgb);
            cairo_pattern_add_color_stop_rgb (pattern, 0., rgb.red, rgb.green, rgb.blue);

            lypad_color_set_saturation_l (&tmp_color, 1.);
            lypad_color_to_rgb (&tmp_color, &rgb);
            cairo_pattern_add_color_stop_rgb (pattern, 1., rgb.red, rgb.green, rgb.blue);
            break;

        case LYPAD_CHANNEL_LUMINOSITY:
            cairo_pattern_add_color_stop_rgb (pattern, 0., 0., 0., 0.);

            tmp_color = slider->color;
            lypad_color_set_luminosity (&tmp_color, 0.5);
            lypad_color_to_rgb (&tmp_color, &rgb);
            cairo_pattern_add_color_stop_rgb (pattern, 0.5, rgb.red, rgb.green, rgb.blue);

            cairo_pattern_add_color_stop_rgb (pattern, 1., 1., 1., 1.);
            break;

        case LYPAD_CHANNEL_SATURATION_V:
            tmp_color = slider->color;
            lypad_color_set_saturation_v (&tmp_color, 0.);
            lypad_color_to_rgb (&tmp_color, &rgb);
            cairo_pattern_add_color_stop_rgb (pattern, 0., rgb.red, rgb.green, rgb.blue);

            lypad_color_set_saturation_v (&tmp_color, 1.);
            lypad_color_to_rgb (&tmp_color, &rgb);
            cairo_pattern_add_color_stop_rgb (pattern, 1., rgb.red, rgb.green, rgb.blue);
            break;

        case LYPAD_CHANNEL_VALUE:
            cairo_pattern_add_color_stop_rgb (pattern, 0., 0., 0., 0.);

            tmp_color = slider->color;
            lypad_color_set_saturation_l (&tmp_color, 1.);
            lypad_color_to_rgb (&tmp_color, &rgb);
            cairo_pattern_add_color_stop_rgb (pattern, 1., rgb.red, rgb.green, rgb.blue);
            break;

        default:
            g_warning ("LypadColorSlider::draw error: Invalid color channel");
    }

    cairo_set_source (cr, pattern);
    cairo_paint (cr);
    cairo_pattern_destroy (pattern);

    /* Draw indicator */
    double arrow_x = slider->value * allocation.width;
    cairo_move_to (cr, arrow_x - ARROW_SIZE, 0);
    cairo_line_to (cr, arrow_x, ARROW_SIZE);
    cairo_line_to (cr, arrow_x + ARROW_SIZE, 0);
    cairo_close_path (cr);

    cairo_move_to (cr, arrow_x - ARROW_SIZE, allocation.height);
    cairo_line_to (cr, arrow_x, allocation.height - ARROW_SIZE);
    cairo_line_to (cr, arrow_x + ARROW_SIZE, allocation.height);
    cairo_close_path (cr);

    cairo_set_line_width (cr, 2);
    cairo_set_source_rgb (cr, 1, 1, 1);
    cairo_stroke_preserve (cr);
    cairo_set_source_rgb (cr, 0, 0, 0);
    cairo_fill (cr);

    return TRUE;
}

static void lypad_color_slider_realize (GtkWidget *widget)
{
    GtkAllocation allocation;
    GdkWindow *window;
    GdkWindowAttr attributes;
    gint attributes_mask;

    gtk_widget_get_allocation (widget, &allocation);

    attributes.x = allocation.x;
    attributes.y = allocation.y;
    attributes.width = allocation.width;
    attributes.height = allocation.height;
    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.event_mask = gtk_widget_get_events (widget);
    attributes.visual = gtk_widget_get_visual (widget);
    attributes.wclass = GDK_INPUT_OUTPUT;

    attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL;

    window = gdk_window_new (gtk_widget_get_parent_window (widget), &attributes, attributes_mask);

    gtk_widget_set_window (widget, window);
    gtk_widget_register_window (widget, window);
    gtk_widget_set_realized (widget, TRUE);
}

static void lypad_color_slider_get_preferred_width (GtkWidget *widget, gint *minimum, gint *natural)
{
    *minimum = *natural = SLIDER_WIDTH;
}

static void lypad_color_slider_get_preferred_height (GtkWidget *widget, gint *minimum, gint *natural)
{
    *minimum = *natural = SLIDER_HEIGHT;
}

static void lypad_color_slider_finalize (GObject *object)
{
    LypadColorSlider *slider = LYPAD_COLOR_SLIDER (object);
    g_clear_object (&slider->gesture);
    G_OBJECT_CLASS (lypad_color_slider_parent_class)->finalize (object);
}

static void lypad_color_slider_class_init (LypadColorSliderClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

    object_class->finalize             = lypad_color_slider_finalize;
    widget_class->get_preferred_width  = lypad_color_slider_get_preferred_width;
    widget_class->get_preferred_height = lypad_color_slider_get_preferred_height;
    widget_class->draw                 = lypad_color_slider_draw;
    widget_class->realize              = lypad_color_slider_realize;
}

GtkWidget *lypad_color_slider_new ()
{
    return g_object_new (LYPAD_TYPE_COLOR_SLIDER, NULL);
}
