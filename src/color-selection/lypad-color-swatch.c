/*
 *  lypad-color-swatch.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-color-swatch.h"
#include "lypad-color-widget.h"

struct _LypadColorSwatch
{
    GtkWidget   parent;
    GdkRGBA     color;
    guint8      borders;
    gboolean    show_hover_box : 1;
    gboolean    hover          : 1;
    gboolean    selected       : 1;
    GtkGesture *gesture;
};

static void color_widget_iface_init (LypadColorWidgetInterface *iface);

enum
{
    SIGNAL_CLICKED,
    SIGNAL_LAST
};

static guint signals[SIGNAL_LAST] = {0};

G_DEFINE_TYPE_WITH_CODE (LypadColorSwatch, lypad_color_swatch, GTK_TYPE_WIDGET,
                         G_IMPLEMENT_INTERFACE (LYPAD_TYPE_COLOR_WIDGET, color_widget_iface_init))

#define BORDER_WIDTH (1)
#define MINIMUM_SIZE (16)

static void lypad_color_swatch_set_color (LypadColorWidget *widget, const GdkRGBA *color)
{
    LYPAD_COLOR_SWATCH (widget)->color = *color;
    gtk_widget_queue_draw (GTK_WIDGET (widget));
}

static void lypad_color_swatch_get_color (LypadColorWidget *widget, GdkRGBA *color)
{
    *color = LYPAD_COLOR_SWATCH (widget)->color;
}

static void color_widget_iface_init (LypadColorWidgetInterface *iface)
{
    iface->set_rgba = lypad_color_swatch_set_color;
    iface->get_rgba = lypad_color_swatch_get_color;
}

static void on_gesture_pressed (GtkGestureMultiPress *gesture, guint n_press, gdouble x, gdouble y, gpointer user_data)
{
    gtk_gesture_set_state (GTK_GESTURE (gesture), GTK_EVENT_SEQUENCE_CLAIMED);
}

static void on_gesture_released (GtkGestureMultiPress *gesture, guint n_press, gdouble x, gdouble y, gpointer user_data)
{
    LypadColorSwatch *swatch = LYPAD_COLOR_SWATCH (user_data);
    if (swatch->hover)
        g_signal_emit (user_data, signals[SIGNAL_CLICKED], 0);
}

static void lypad_color_swatch_init (LypadColorSwatch *swatch)
{
    gtk_widget_set_has_window (GTK_WIDGET (swatch), TRUE);
    gtk_widget_add_events (GTK_WIDGET (swatch), GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK | GDK_BUTTON_PRESS_MASK);

    swatch->color.red   = 1.0;
    swatch->color.green = 0.0;
    swatch->color.blue  = 0.0;
    swatch->color.alpha = 1.0;

    swatch->borders = LYPAD_BORDERS_ALL;
    swatch->show_hover_box = FALSE;
    swatch->hover = FALSE;

    swatch->gesture = gtk_gesture_multi_press_new (GTK_WIDGET (swatch));
    g_signal_connect (swatch->gesture, "pressed", G_CALLBACK (on_gesture_pressed), swatch);
    g_signal_connect (swatch->gesture, "released", G_CALLBACK (on_gesture_released), swatch);
}

void lypad_color_swatch_set_borders (LypadColorSwatch *swatch, gint8 borders)
{
    g_return_if_fail (LYPAD_IS_COLOR_SWATCH (swatch));
    swatch->borders = borders;
    gtk_widget_queue_draw (GTK_WIDGET (swatch));
}

void lypad_color_swatch_set_show_hover_box (LypadColorSwatch *swatch, gboolean show)
{
    g_return_if_fail (LYPAD_IS_COLOR_SWATCH (swatch));
    swatch->show_hover_box = show;
    gtk_widget_queue_draw (GTK_WIDGET (swatch));
}

void lypad_color_swatch_set_selected (LypadColorSwatch *swatch, gboolean selected)
{
    g_return_if_fail (LYPAD_IS_COLOR_SWATCH (swatch));
    swatch->selected = selected;
    gtk_widget_queue_draw (GTK_WIDGET (swatch));
}

static gboolean lypad_color_swatch_draw (GtkWidget *widget, cairo_t *cr)
{
    LypadColorSwatch *swatch = LYPAD_COLOR_SWATCH (widget);
    GtkAllocation allocation;
    gtk_widget_get_allocation (widget, &allocation);
    allocation.x = 0;
    allocation.y = 0;

    if (swatch->show_hover_box)
    {
        cairo_save (cr);
        allocation.x += 2;
        allocation.y += 2;
        allocation.width -= 2;
        allocation.height -= 2;

        cairo_rectangle (cr, allocation.x, allocation.y, allocation.width - allocation.x, allocation.height - allocation.y);
        cairo_clip (cr);
    }

    // Draw check pattern
    if (swatch->color.alpha < 1.)
    {
        cairo_pattern_t *pattern = _lypad_color_widget_get_checkered_pattern ();
        cairo_set_source_rgb (cr, 0.66, 0.66, 0.66);
        cairo_mask (cr, pattern);
        cairo_pattern_destroy (pattern);
    }

    // Fill with color
    gdk_cairo_set_source_rgba (cr, &swatch->color);
    cairo_paint (cr);

    // Draw border
    cairo_set_source_rgb (cr, 0, 0, 0);
    cairo_set_line_width (cr, BORDER_WIDTH);

    if (swatch->borders & LYPAD_BORDERS_TOP)
    {
        cairo_move_to (cr, allocation.x     + BORDER_WIDTH - 0.5, allocation.y      + BORDER_WIDTH - 0.5);
        cairo_line_to (cr, allocation.width - BORDER_WIDTH + 0.5, allocation.y      + BORDER_WIDTH - 0.5);
    }
    if (swatch->borders & LYPAD_BORDERS_RIGHT)
    {
        cairo_move_to (cr, allocation.width - BORDER_WIDTH + 0.5, allocation.y      + BORDER_WIDTH - 0.5);
        cairo_line_to (cr, allocation.width - BORDER_WIDTH + 0.5, allocation.height - BORDER_WIDTH + 0.5);
    }
    if (swatch->borders & LYPAD_BORDERS_BOTTOM)
    {
        cairo_move_to (cr, allocation.width - BORDER_WIDTH + 0.5, allocation.height - BORDER_WIDTH + 0.5);
        cairo_line_to (cr, allocation.x     + BORDER_WIDTH - 0.5, allocation.height - BORDER_WIDTH + 0.5);
    }
    if (swatch->borders & LYPAD_BORDERS_LEFT)
    {
        cairo_move_to (cr, allocation.x     + BORDER_WIDTH - 0.5, allocation.height - BORDER_WIDTH + 0.5);
        cairo_line_to (cr, allocation.x     + BORDER_WIDTH - 0.5, allocation.y      + BORDER_WIDTH - 0.5);
    }
    cairo_stroke (cr);

    if (swatch->show_hover_box)
    {
        cairo_restore (cr);

        // Draw hover/selection box
        if (swatch->hover || swatch->selected)
        {
            GtkStyleContext *context = gtk_widget_get_style_context (widget);
            GValue value = G_VALUE_INIT;
            gtk_style_context_get_property (context, GTK_STYLE_PROPERTY_BORDER_COLOR, gtk_style_context_get_state (context), &value);
            GdkRGBA *color = g_value_get_boxed (&value);
            gdk_cairo_set_source_rgba (cr, color);
            cairo_rectangle (cr, 0, 0, allocation.width + 2, allocation.height + 2);
            cairo_stroke (cr);
            g_value_unset (&value);
        }
    }

    return TRUE;
}

static void lypad_color_swatch_realize (GtkWidget *widget)
{
    GtkAllocation allocation;
    GdkWindow *window;
    GdkWindowAttr attributes;
    gint attributes_mask;

    gtk_widget_get_allocation (widget, &allocation);

    attributes.x = allocation.x;
    attributes.y = allocation.y;
    attributes.width = allocation.width;
    attributes.height = allocation.height;
    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.event_mask = gtk_widget_get_events (widget);
    attributes.visual = gtk_widget_get_visual (widget);
    attributes.wclass = GDK_INPUT_OUTPUT;

    attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL;

    window = gdk_window_new (gtk_widget_get_parent_window (widget), &attributes, attributes_mask);

    gtk_widget_set_window (widget, window);
    gtk_widget_register_window (widget, window);
    gtk_widget_set_realized (widget, TRUE);
}

static void lypad_color_swatch_get_preferred_size (GtkWidget *widget, gint *minimum, gint *natural)
{
    *minimum = *natural = MINIMUM_SIZE;
}

static gboolean lypad_color_swatch_enter_notify (GtkWidget *widget, GdkEventCrossing *event)
{
    LYPAD_COLOR_SWATCH (widget)->hover = TRUE;
    gtk_widget_queue_draw (widget);
    return FALSE;
}

static gboolean lypad_color_swatch_leave_notify (GtkWidget *widget, GdkEventCrossing *event)
{
    LYPAD_COLOR_SWATCH (widget)->hover = FALSE;
    gtk_widget_queue_draw (widget);
    return FALSE;
}

static void lypad_color_swatch_finalize (GObject *object)
{
    LypadColorSwatch *swatch = LYPAD_COLOR_SWATCH (object);
    g_clear_object (&swatch->gesture);
    G_OBJECT_CLASS (lypad_color_swatch_parent_class)->finalize (object);
}

static void lypad_color_swatch_class_init (LypadColorSwatchClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

    object_class->finalize             = lypad_color_swatch_finalize;
    widget_class->get_preferred_width  = lypad_color_swatch_get_preferred_size;
    widget_class->get_preferred_height = lypad_color_swatch_get_preferred_size;
    widget_class->draw                 = lypad_color_swatch_draw;
    widget_class->realize              = lypad_color_swatch_realize;
    widget_class->enter_notify_event   = lypad_color_swatch_enter_notify;
    widget_class->leave_notify_event   = lypad_color_swatch_leave_notify;

    signals[SIGNAL_CLICKED] = g_signal_new ("clicked", G_TYPE_FROM_CLASS (klass),
                                            G_SIGNAL_RUN_FIRST,
                                            0, NULL, NULL,
                                            g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

}

GtkWidget *lypad_color_swatch_new ()
{
    return g_object_new (LYPAD_TYPE_COLOR_SWATCH, NULL);
}
