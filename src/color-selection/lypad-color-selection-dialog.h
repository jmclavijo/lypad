/*
 *  lypad-color-selection-dialog.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include "lypad-color-store.h"

G_BEGIN_DECLS

#define LYPAD_TYPE_COLOR_SELECTION_DIALOG (lypad_color_selection_dialog_get_type ())
G_DECLARE_FINAL_TYPE (LypadColorSelectionDialog, lypad_color_selection_dialog, LYPAD, COLOR_SELECTION_DIALOG, GtkDialog)

GtkWidget *lypad_color_selection_dialog_new           (GtkWindow *parent);
void       lypad_color_selection_dialog_set_use_alpha (LypadColorSelectionDialog *dialog, gboolean use_alpha);
void       lypad_color_selection_dialog_update_recent (LypadColorSelectionDialog *dialog, LypadColorSet color_set);

G_END_DECLS
