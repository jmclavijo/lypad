/*
 *  lypad-text-color-chooser.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-text-color-chooser.h"
#include "lypad-color-widget.h"
#include "lypad-color-swatch.h"
#include "lypad-color-selection-dialog.h"
#include "lypad-color-store.h"
#include "lypad-color-utils.h"

struct _LypadTextColorChooser
{
    GtkToggleButton  parent;
    GtkWidget       *popover;
    GdkRGBA          color;
    GdkRGBA          auto_color;
    GtkWidget       *auto_swatch;

    GtkWidget       *color_indicator;
    GtkWidget       *dialog;

    GtkWidget       *color_grid;
    GtkWidget       *recent_grid;
    GtkWidget       *last_recent_swatch;
    GtkWidget       *selected_swatch;
    GList           *recent_swatches;
};

static void color_widget_iface_init (LypadColorWidgetInterface *iface);

G_DEFINE_TYPE_WITH_CODE (LypadTextColorChooser, lypad_text_color_chooser, GTK_TYPE_TOGGLE_BUTTON,
                         G_IMPLEMENT_INTERFACE (LYPAD_TYPE_COLOR_WIDGET, color_widget_iface_init))

static void lypad_text_color_chooser_set_rgba (LypadColorWidget *widget, const GdkRGBA *color)
{
    LYPAD_TEXT_COLOR_CHOOSER (widget)->color = *color;
    gtk_widget_queue_draw (GTK_WIDGET (widget));
}

static void lypad_text_color_chooser_get_rgba (LypadColorWidget *widget, GdkRGBA *color)
{
    *color = LYPAD_TEXT_COLOR_CHOOSER (widget)->color;
}

static void color_widget_iface_init (LypadColorWidgetInterface *iface)
{
    iface->set_rgba = lypad_text_color_chooser_set_rgba;
    iface->get_rgba = lypad_text_color_chooser_get_rgba;
}

static void lypad_text_color_chooser_select_current_color (LypadTextColorChooser *chooser)
{
    gboolean color_found = FALSE;
    GdkRGBA color;

    if (gdk_rgba_equal (&chooser->color, &LYPAD_AUTOMATIC_TEXT_COLOR))
        color_found = TRUE;

    if (!color_found)
    {
        GList *children = gtk_container_get_children (GTK_CONTAINER (chooser->color_grid));
        for (GList *l = children; l != NULL; l = l->next)
        {
            lypad_color_widget_get_color (l->data, &color);
            if (gdk_rgba_equal (&color, &chooser->color))
            {
                lypad_color_swatch_set_selected (l->data, TRUE);
                chooser->selected_swatch = l->data;
                color_found = TRUE;
                break;
            }
        }
        g_list_free (children);
    }

    if (!color_found)
    {
        GList *children = gtk_container_get_children (GTK_CONTAINER (chooser->recent_grid));
        for (GList *l = children; l != NULL; l = l->next)
        {
            lypad_color_widget_get_color (l->data, &color);
            if (gdk_rgba_equal (&color, &chooser->color))
            {
                lypad_color_swatch_set_selected (l->data, TRUE);
                chooser->selected_swatch = l->data;
                color_found = TRUE;
                break;
            }
        }
        g_list_free (children);
    }

    if (!color_found)
    {
        lypad_color_widget_set_color (LYPAD_COLOR_WIDGET (chooser->last_recent_swatch), &chooser->color);
        lypad_color_swatch_set_selected (LYPAD_COLOR_SWATCH (chooser->last_recent_swatch), TRUE);
        chooser->selected_swatch = chooser->last_recent_swatch;
    }
}

static void lypad_text_color_chooser_update_recent_colors (LypadTextColorChooser *chooser)
{
    GdkRGBA fill_color = {1, 1, 1, 1};
    int n_recent;
    GdkRGBA *recent_colors = lypad_color_store_get_recent (LYPAD_TEXT_COLOR_SET, &n_recent);
    int i = 0;
    for (GList *l = chooser->recent_swatches; l != NULL; l = l->next, ++i)
    {
        if (i < n_recent)
            lypad_color_widget_set_color (LYPAD_COLOR_WIDGET (l->data), &recent_colors[i]);
        else
            lypad_color_widget_set_color (LYPAD_COLOR_WIDGET (l->data), &fill_color);
    }
    g_free (recent_colors);
}

static void lypad_text_color_chooser_clicked (GtkButton *button)
{
    if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button)))
    {
        lypad_text_color_chooser_update_recent_colors (LYPAD_TEXT_COLOR_CHOOSER (button));
        lypad_text_color_chooser_select_current_color (LYPAD_TEXT_COLOR_CHOOSER (button));
        gtk_popover_popup (GTK_POPOVER (LYPAD_TEXT_COLOR_CHOOSER (button)->popover));
    }
    GTK_BUTTON_CLASS (lypad_text_color_chooser_parent_class)->clicked (button);
}

static void on_popover_closed (GtkPopover *popover, gpointer user_data)
{
    LypadTextColorChooser *chooser = LYPAD_TEXT_COLOR_CHOOSER (user_data);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (chooser), FALSE);
    if (chooser->selected_swatch)
    {
        lypad_color_swatch_set_selected (LYPAD_COLOR_SWATCH (chooser->selected_swatch), FALSE);
        chooser->selected_swatch = NULL;
    }
}

static gboolean draw_color_indicator (GtkWidget *widget, cairo_t *cr, gpointer user_data)
{
    LypadTextColorChooser *chooser = LYPAD_TEXT_COLOR_CHOOSER (user_data);
    if (gdk_rgba_equal (&chooser->color, &LYPAD_AUTOMATIC_TEXT_COLOR))
        gdk_cairo_set_source_rgba (cr, &chooser->auto_color);
    else
        gdk_cairo_set_source_rgba (cr, &chooser->color);
    cairo_paint (cr);
    return TRUE;
}

static void on_color_clicked (LypadColorSwatch *swatch, gpointer user_data)
{
    LypadTextColorChooser *chooser = LYPAD_TEXT_COLOR_CHOOSER (user_data);
    GdkRGBA color;
    lypad_color_widget_get_color (LYPAD_COLOR_WIDGET (swatch), &color);
    chooser->color = color;
    gtk_widget_queue_draw (GTK_WIDGET (chooser));
    gtk_popover_popdown (GTK_POPOVER (chooser->popover));
    _lypad_color_widget_changed (LYPAD_COLOR_WIDGET (chooser));
    if (gtk_widget_get_parent (GTK_WIDGET (swatch)) == chooser->recent_grid)
        lypad_color_store_add_recent (&color, LYPAD_TEXT_COLOR_SET);
}

static void on_dialog_response (GtkDialog *dialog, int response_id, gpointer user_data)
{
    LypadTextColorChooser *chooser = LYPAD_TEXT_COLOR_CHOOSER (user_data);
    if (response_id == GTK_RESPONSE_ACCEPT)
    {
        GdkRGBA color;
        lypad_color_widget_get_color (LYPAD_COLOR_WIDGET (dialog), &color);
        chooser->color = color;
        gtk_widget_queue_draw (GTK_WIDGET (chooser));
        _lypad_color_widget_changed (LYPAD_COLOR_WIDGET (chooser));
        lypad_color_store_add_recent (&color, LYPAD_TEXT_COLOR_SET);
    }
    gtk_widget_hide (GTK_WIDGET (dialog));
}

static void on_dialog_destroy (GtkWidget *dialog, gpointer user_data)
{
    LypadTextColorChooser *chooser = LYPAD_TEXT_COLOR_CHOOSER (user_data);
    chooser->dialog = NULL;
}


static gboolean on_dialog_delete_event (GtkWidget *dialog, GdkEvent *event, gpointer user_data)
{
    g_signal_emit_by_name (dialog, "response", GTK_RESPONSE_CANCEL);
    return TRUE;
}

static void lypad_text_color_chooser_init_dialog (LypadTextColorChooser *chooser)
{
    if (chooser->dialog)
        return;

    chooser->dialog = lypad_color_selection_dialog_new (GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (chooser))));
    lypad_color_selection_dialog_set_use_alpha (LYPAD_COLOR_SELECTION_DIALOG (chooser->dialog), FALSE);
    gtk_window_set_modal (GTK_WINDOW (chooser->dialog), TRUE);
    g_signal_connect (chooser->dialog, "response", G_CALLBACK (on_dialog_response), chooser);
    g_signal_connect (chooser->dialog, "destroy", G_CALLBACK (on_dialog_destroy), chooser);
    g_signal_connect (chooser->dialog, "delete-event", G_CALLBACK (on_dialog_delete_event), NULL);
}

static void on_custom_color_button_clicked (GtkButton *button, gpointer user_data)
{
    LypadTextColorChooser *chooser = LYPAD_TEXT_COLOR_CHOOSER (user_data);
    lypad_text_color_chooser_init_dialog (chooser);
    lypad_color_widget_set_color (LYPAD_COLOR_WIDGET (chooser->dialog), &chooser->color);
    gtk_popover_popdown (GTK_POPOVER (chooser->popover));
    lypad_color_selection_dialog_update_recent (LYPAD_COLOR_SELECTION_DIALOG (chooser->dialog), LYPAD_TEXT_COLOR_SET);
    gtk_widget_show (chooser->dialog);
}

static void on_auto_color_button_clicked (GtkButton *button, gpointer user_data)
{
    LypadTextColorChooser *chooser = LYPAD_TEXT_COLOR_CHOOSER (user_data);
    chooser->color = LYPAD_AUTOMATIC_TEXT_COLOR;
    gtk_popover_popdown (GTK_POPOVER (chooser->popover));
    _lypad_color_widget_changed (LYPAD_COLOR_WIDGET (chooser));
    gtk_widget_queue_draw (GTK_WIDGET (chooser));
}

static void lypad_text_color_chooser_init_popup (LypadTextColorChooser *chooser)
{
    chooser->popover = gtk_popover_new (GTK_WIDGET (chooser));
    g_object_ref (chooser->popover);

    GtkWidget *box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 4);
    GtkWidget *swatch;

    GtkWidget *auto_button = gtk_button_new ();
    GtkWidget *auto_box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 8);

    swatch = lypad_color_swatch_new ();
    lypad_color_widget_set_color (LYPAD_COLOR_WIDGET (swatch), &chooser->auto_color);
    gtk_widget_set_size_request (swatch, 16, 16);
    gtk_widget_set_halign (swatch, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (swatch, GTK_ALIGN_CENTER);
    chooser->auto_swatch = swatch;
    gtk_box_pack_start (GTK_BOX (auto_box), swatch, FALSE, FALSE, 0);

    GtkWidget *auto_label = gtk_label_new ("Automatic");
    gtk_box_pack_start (GTK_BOX (auto_box), auto_label, FALSE, TRUE, 0);
    gtk_widget_set_halign (auto_box, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (auto_box, GTK_ALIGN_CENTER);

    gtk_container_add (GTK_CONTAINER (auto_button), auto_box);
    gtk_box_pack_start (GTK_BOX (box), auto_button, FALSE, TRUE, 4);
    g_signal_connect (auto_button, "clicked", G_CALLBACK (on_auto_color_button_clicked), chooser);

    GtkWidget *frame = gtk_frame_new (NULL);
    chooser->color_grid = gtk_grid_new ();
    gtk_container_set_border_width (GTK_CONTAINER (chooser->color_grid), 2);

    LypadColorPalette *palette = lypad_color_store_get_palette (LYPAD_TEXT_COLOR_SET);
    for (int i = 0; i < palette->n_colors; ++i)
    {
        swatch = lypad_color_swatch_new ();
        lypad_color_widget_set_color (LYPAD_COLOR_WIDGET (swatch), &palette->colors[i]);
        lypad_color_swatch_set_borders (LYPAD_COLOR_SWATCH (swatch), LYPAD_BORDERS_NONE);
        lypad_color_swatch_set_show_hover_box (LYPAD_COLOR_SWATCH (swatch), TRUE);
        gtk_widget_set_size_request (swatch, 20, 20);
        gtk_grid_attach (GTK_GRID (chooser->color_grid), swatch, i % palette->n_columns, i / palette->n_columns, 1, 1);
        g_signal_connect (swatch, "clicked", G_CALLBACK (on_color_clicked), chooser);
    }
    gtk_container_add (GTK_CONTAINER (frame), chooser->color_grid);
    gtk_box_pack_start (GTK_BOX (box), frame, FALSE, TRUE, 0);

    frame = gtk_frame_new (" Recent ");
    chooser->recent_grid = gtk_grid_new ();
    gtk_container_set_border_width (GTK_CONTAINER (chooser->recent_grid), 2);
    GdkRGBA fill_color = {1, 1, 1, 1};
    int n_recent;
    chooser->recent_swatches = NULL;
    GdkRGBA *recent_colors = lypad_color_store_get_recent (LYPAD_TEXT_COLOR_SET, &n_recent);
    for (int i = 0; i < palette->n_columns; ++i)
    {
        swatch = lypad_color_swatch_new ();
        if (i < n_recent)
            lypad_color_widget_set_color (LYPAD_COLOR_WIDGET (swatch), &recent_colors[i]);
        else
            lypad_color_widget_set_color (LYPAD_COLOR_WIDGET (swatch), &fill_color);
        lypad_color_swatch_set_borders (LYPAD_COLOR_SWATCH (swatch), LYPAD_BORDERS_NONE);
        lypad_color_swatch_set_show_hover_box (LYPAD_COLOR_SWATCH (swatch), TRUE);
        gtk_widget_set_size_request (swatch, 20, 20);
        gtk_grid_attach (GTK_GRID (chooser->recent_grid), swatch, i, 0, 1, 1);
        g_signal_connect (swatch, "clicked", G_CALLBACK (on_color_clicked), chooser);
        chooser->last_recent_swatch = swatch;
        chooser->recent_swatches = g_list_append (chooser->recent_swatches, swatch);
    }
    g_free (recent_colors);
    gtk_container_add (GTK_CONTAINER (frame), chooser->recent_grid);
    gtk_box_pack_start (GTK_BOX (box), frame, FALSE, TRUE, 0);

    GtkWidget *custom_color = gtk_button_new_with_label ("Custom color");
    gtk_box_pack_start (GTK_BOX (box), custom_color, FALSE, TRUE, 4);
    g_signal_connect (custom_color, "clicked", G_CALLBACK (on_custom_color_button_clicked), chooser);

    gtk_container_set_border_width (GTK_CONTAINER (box), 8);
    gtk_container_add (GTK_CONTAINER (chooser->popover), box);
    gtk_widget_show_all (box);

    g_signal_connect (chooser->popover, "closed", G_CALLBACK (on_popover_closed), chooser);
}

static void lypad_text_color_chooser_init (LypadTextColorChooser *chooser)
{
    gtk_widget_set_name (GTK_WIDGET (chooser), "LypadTextColorChooser");
    gtk_widget_set_can_focus (GTK_WIDGET (chooser), FALSE);

    chooser->color.red   = 0;
    chooser->color.green = 0;
    chooser->color.blue  = 0;
    chooser->color.alpha = 1;

    chooser->auto_color.red   = 0;
    chooser->auto_color.green = 0;
    chooser->auto_color.blue  = 0;
    chooser->auto_color.alpha = 1;

    lypad_text_color_chooser_init_popup (chooser);

    chooser->selected_swatch = NULL;
    chooser->dialog = NULL;

    GtkWidget *grid = gtk_grid_new ();
    GtkWidget *label = gtk_label_new ("A");
    GtkWidget *arrow = gtk_image_new_from_icon_name ("pan-down-symbolic", GTK_ICON_SIZE_BUTTON);

    chooser->color_indicator = gtk_event_box_new ();
    gtk_widget_set_size_request (chooser->color_indicator, 18, 6);
    g_signal_connect (chooser->color_indicator, "draw", G_CALLBACK (draw_color_indicator), chooser);

    gtk_grid_attach (GTK_GRID (grid), label, 0, 0, 1, 1);
    gtk_grid_attach (GTK_GRID (grid), chooser->color_indicator, 0, 1, 1, 1);
    gtk_grid_attach (GTK_GRID (grid), arrow, 1, 0, 1, 2);
    gtk_container_add (GTK_CONTAINER (chooser), grid);
    gtk_widget_show_all (grid);
}

void lypad_text_color_chooser_set_bkg_color (LypadTextColorChooser *chooser, const GdkRGBA *bkg_color)
{
    g_return_if_fail (LYPAD_IS_TEXT_COLOR_CHOOSER (chooser));
    lypad_color_utils_get_text_color_for_background (bkg_color, &chooser->auto_color);
    lypad_color_widget_set_color (LYPAD_COLOR_WIDGET (chooser->auto_swatch), &chooser->auto_color);
    gtk_widget_queue_draw (GTK_WIDGET (chooser));
}

static void lypad_text_color_chooser_dispose (GObject *object)
{
    LypadTextColorChooser *chooser = LYPAD_TEXT_COLOR_CHOOSER (object);
    if (chooser->dialog)
        gtk_widget_destroy (chooser->dialog);
    g_clear_list (&chooser->recent_swatches, NULL);
    if (chooser->popover)
    {
        gtk_widget_destroy (chooser->popover);
        g_object_unref (chooser->popover);
        chooser->popover = NULL;
    }
    G_OBJECT_CLASS (lypad_text_color_chooser_parent_class)->dispose (object);
}

static void lypad_text_color_chooser_class_init (LypadTextColorChooserClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GtkButtonClass *button_class = GTK_BUTTON_CLASS (klass);
    object_class->dispose = lypad_text_color_chooser_dispose;
    button_class->clicked = lypad_text_color_chooser_clicked;
}

LypadTextColorChooser *lypad_text_color_chooser_new ()
{
    return g_object_new (LYPAD_TYPE_TEXT_COLOR_CHOOSER, NULL);
}
