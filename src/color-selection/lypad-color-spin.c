/*
 *  lypad-color-spin.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-color-spin.h"

struct _LypadColorSpin
{
    GtkSpinButton parent;
    LypadColor    color;
    int           channel;
    gboolean      in_modification : 1;
};

static void color_widget_iface_init (LypadColorWidgetInterface *iface);

G_DEFINE_TYPE_WITH_CODE (LypadColorSpin, lypad_color_spin, GTK_TYPE_SPIN_BUTTON,
                         G_IMPLEMENT_INTERFACE (LYPAD_TYPE_COLOR_WIDGET, color_widget_iface_init))

static void lypad_color_spin_set_color (LypadColorWidget *widget, const LypadColor *color)
{
    LypadColorSpin *spin = LYPAD_COLOR_SPIN (widget);
    spin->in_modification = TRUE;
    spin->color = *color;
    switch (spin->channel)
    {
        case LYPAD_CHANNEL_RED:
            gtk_spin_button_set_value (GTK_SPIN_BUTTON (widget), lypad_color_get_red (color) * 255);
            break;

        case LYPAD_CHANNEL_GREEN:
            gtk_spin_button_set_value (GTK_SPIN_BUTTON (widget), lypad_color_get_green (color) * 255);
            break;

        case LYPAD_CHANNEL_BLUE:
            gtk_spin_button_set_value (GTK_SPIN_BUTTON (widget), lypad_color_get_blue (color) * 255);
            break;

        case LYPAD_CHANNEL_ALPHA:
            gtk_spin_button_set_value (GTK_SPIN_BUTTON (widget), lypad_color_get_alpha (color) * 100);
            break;

        case LYPAD_CHANNEL_HUE:
            gtk_spin_button_set_value (GTK_SPIN_BUTTON (widget), lypad_color_get_hue (color) * 360);
            break;

        case LYPAD_CHANNEL_SATURATION_L:
            gtk_spin_button_set_value (GTK_SPIN_BUTTON (widget), lypad_color_get_saturation_l (color) * 100);
            break;

        case LYPAD_CHANNEL_LUMINOSITY:
            gtk_spin_button_set_value (GTK_SPIN_BUTTON (widget), lypad_color_get_luminosity (color) * 100);
            break;

        case LYPAD_CHANNEL_SATURATION_V:
            gtk_spin_button_set_value (GTK_SPIN_BUTTON (widget), lypad_color_get_saturation_v (color) * 100);
            break;

        case LYPAD_CHANNEL_VALUE:
            gtk_spin_button_set_value (GTK_SPIN_BUTTON (widget), lypad_color_get_value (color) * 100);
            break;

        default:
            g_warning ("Invalid color channel in LypadColorSpin");
    }
    spin->in_modification = FALSE;
}

static void lypad_color_spin_get_color (LypadColorWidget *widget, LypadColor *color)
{
    *color = LYPAD_COLOR_SPIN (widget)->color;
}

static void color_widget_iface_init (LypadColorWidgetInterface *iface)
{
    iface->set_color = lypad_color_spin_set_color;
    iface->get_color = lypad_color_spin_get_color;
}

void lypad_color_spin_set_channel (LypadColorSpin *spin, LypadColorChannel channel)
{
    g_return_if_fail (LYPAD_IS_COLOR_SPIN (spin));
    spin->in_modification = TRUE;
    spin->channel = channel;
    switch (channel)
    {
        case LYPAD_CHANNEL_RED:
            gtk_spin_button_set_range (GTK_SPIN_BUTTON (spin), 0, 255);
            gtk_spin_button_set_value (GTK_SPIN_BUTTON (spin), lypad_color_get_red (&spin->color) * 255);
            break;

        case LYPAD_CHANNEL_GREEN:
            gtk_spin_button_set_range (GTK_SPIN_BUTTON (spin), 0, 255);
            gtk_spin_button_set_value (GTK_SPIN_BUTTON (spin), lypad_color_get_green (&spin->color) * 255);
            break;

        case LYPAD_CHANNEL_BLUE:
            gtk_spin_button_set_range (GTK_SPIN_BUTTON (spin), 0, 255);
            gtk_spin_button_set_value (GTK_SPIN_BUTTON (spin), lypad_color_get_blue (&spin->color) * 255);
            break;

        case LYPAD_CHANNEL_ALPHA:
            gtk_spin_button_set_range (GTK_SPIN_BUTTON (spin), 0, 100);
            gtk_spin_button_set_value (GTK_SPIN_BUTTON (spin), lypad_color_get_alpha (&spin->color) * 100);
            break;

        case LYPAD_CHANNEL_HUE:
            gtk_spin_button_set_range (GTK_SPIN_BUTTON (spin), 0, 360);
            gtk_spin_button_set_value (GTK_SPIN_BUTTON (spin), lypad_color_get_hue (&spin->color) * 360);
            break;

        case LYPAD_CHANNEL_SATURATION_L:
            gtk_spin_button_set_range (GTK_SPIN_BUTTON (spin), 0, 100);
            gtk_spin_button_set_value (GTK_SPIN_BUTTON (spin), lypad_color_get_saturation_l (&spin->color) * 100);
            break;

        case LYPAD_CHANNEL_LUMINOSITY:
            gtk_spin_button_set_range (GTK_SPIN_BUTTON (spin), 0, 100);
            gtk_spin_button_set_value (GTK_SPIN_BUTTON (spin), lypad_color_get_luminosity (&spin->color) * 100);
            break;

        case LYPAD_CHANNEL_SATURATION_V:
            gtk_spin_button_set_range (GTK_SPIN_BUTTON (spin), 0, 100);
            gtk_spin_button_set_value (GTK_SPIN_BUTTON (spin), lypad_color_get_saturation_v (&spin->color) * 100);
            break;

        case LYPAD_CHANNEL_VALUE:
            gtk_spin_button_set_range (GTK_SPIN_BUTTON (spin), 0, 100);
            gtk_spin_button_set_value (GTK_SPIN_BUTTON (spin), lypad_color_get_value (&spin->color) * 100);
            break;

        default:
            g_warning ("Invalid color channel in LypadColorSpin");
    }
    gtk_widget_queue_draw (GTK_WIDGET (spin));
    spin->in_modification = FALSE;
}

static void lypad_color_spin_init (LypadColorSpin *spin)
{
    gtk_spin_button_set_increments (GTK_SPIN_BUTTON (spin), 1, 10);

    spin->channel = LYPAD_CHANNEL_RED;
    lypad_color_init (&spin->color);
    spin->in_modification = FALSE;
}

static void lypad_color_spin_value_changed (GtkSpinButton *widget)
{
    LypadColorSpin *spin = LYPAD_COLOR_SPIN (widget);
    if (spin->in_modification)
        return;

    double value = gtk_spin_button_get_value (widget);
    switch (spin->channel)
    {
        case LYPAD_CHANNEL_RED:
            lypad_color_set_red (&spin->color, value / 255.);
            break;

        case LYPAD_CHANNEL_GREEN:
            lypad_color_set_green (&spin->color, value / 255.);
            break;

        case LYPAD_CHANNEL_BLUE:
            lypad_color_set_blue (&spin->color, value / 255.);
            break;

        case LYPAD_CHANNEL_ALPHA:
            lypad_color_set_alpha (&spin->color, value / 100.);
            break;

        case LYPAD_CHANNEL_HUE:
            lypad_color_set_hue (&spin->color, value / 360.);
            break;

        case LYPAD_CHANNEL_SATURATION_L:
            lypad_color_set_saturation_l (&spin->color, value / 100.);
            break;

        case LYPAD_CHANNEL_LUMINOSITY:
            lypad_color_set_luminosity (&spin->color, value / 100.);
            break;

        case LYPAD_CHANNEL_SATURATION_V:
            lypad_color_set_saturation_v (&spin->color, value / 100.);
            break;

        case LYPAD_CHANNEL_VALUE:
            lypad_color_set_value (&spin->color, value / 100.);
            break;
        default:
            g_warning ("Invalid color channel in LypadColorSpin");
    }
    _lypad_color_widget_changed (LYPAD_COLOR_WIDGET (widget));
}

static void lypad_color_spin_class_init (LypadColorSpinClass *klass)
{
    GtkSpinButtonClass *spin_button_class = GTK_SPIN_BUTTON_CLASS (klass);

    spin_button_class->value_changed = lypad_color_spin_value_changed;
}

GtkWidget *lypad_color_spin_new ()
{
    return g_object_new (LYPAD_TYPE_COLOR_SPIN, "digits", 0, "climb-rate", 1.0, NULL);
}
