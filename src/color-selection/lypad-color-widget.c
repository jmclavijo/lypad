/*
 *  lypad-color-widget.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-color-widget.h"
#include "lypad-color-utils.h"

G_DEFINE_INTERFACE (LypadColorWidget, lypad_color_widget, G_TYPE_OBJECT)

enum
{
    SIGNAL_CHANGED,
    SIGNAL_LAST
};
static guint signals[SIGNAL_LAST] = {0};

static void lypad_color_widget_default_init (LypadColorWidgetInterface *iface)
{
    iface->set_rgba = NULL;
    iface->get_rgba = NULL;
    iface->set_color = NULL;
    iface->get_color = NULL;

    signals[SIGNAL_CHANGED] = g_signal_new ("color-changed", G_TYPE_FROM_INTERFACE (iface),
                                            G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                            g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
}

void _lypad_color_widget_changed (LypadColorWidget *widget)
{
    g_signal_emit (widget, signals[SIGNAL_CHANGED], 0);
}

void lypad_color_widget_set_color (LypadColorWidget *widget, const GdkRGBA *color)
{
    g_return_if_fail (LYPAD_IS_COLOR_WIDGET (widget));
    LypadColorWidgetInterface *iface = LYPAD_COLOR_WIDGET_GET_IFACE (widget);
    if (iface->set_rgba)
        iface->set_rgba (widget, color);
    else if (iface->set_color)
    {
        LypadColor full_color;
        lypad_color_from_rgb (&full_color, color);
        iface->set_color (widget, &full_color);
    }
    else
        g_warning ("LypadColorWidget::set_rgba not implemented for '%s'", g_type_name (G_TYPE_FROM_INSTANCE (widget)));
}

void lypad_color_widget_get_color (LypadColorWidget *widget, GdkRGBA *color)
{
    g_return_if_fail (LYPAD_IS_COLOR_WIDGET (widget));
    LypadColorWidgetInterface *iface = LYPAD_COLOR_WIDGET_GET_IFACE (widget);
    if (iface->get_rgba)
        iface->get_rgba (widget, color);
    else if (iface->get_color)
    {
        LypadColor full_color;
        iface->get_color (widget, &full_color);
        lypad_color_to_rgb (&full_color, color);
    }
    else
    {
        g_warning ("LypadColorWidget::get_rgba not implemented for '%s'", g_type_name (G_TYPE_FROM_INSTANCE (widget)));
        return;
    }

    if (!gdk_rgba_equal (color, &LYPAD_AUTOMATIC_TEXT_COLOR))
    {
        color->red   = (double)((int)(CLAMP (color->red, 0., 1.)   * 255. + 0.5)) / 255.;
        color->green = (double)((int)(CLAMP (color->green, 0., 1.) * 255. + 0.5)) / 255.;
        color->blue  = (double)((int)(CLAMP (color->blue, 0., 1.)  * 255. + 0.5)) / 255.;
        color->alpha = (double)((int)(CLAMP (color->alpha, 0., 1.) * 255. + 0.5)) / 255.;
    }
}

void lypad_color_widget_set_color_full (LypadColorWidget *widget, const LypadColor *color)
{
    g_return_if_fail (LYPAD_IS_COLOR_WIDGET (widget));
    LypadColorWidgetInterface *iface = LYPAD_COLOR_WIDGET_GET_IFACE (widget);
    if (iface->set_color)
        iface->set_color (widget, color);
    else if (iface->set_rgba)
    {
        GdkRGBA rgba;
        lypad_color_to_rgb (color, &rgba);
        iface->set_rgba (widget, &rgba);
    }
    else
        g_warning ("LypadColorWidget::set_color not implemented for '%s'", g_type_name (G_TYPE_FROM_INSTANCE (widget)));
}

void lypad_color_widget_get_color_full (LypadColorWidget *widget, LypadColor *color)
{
    g_return_if_fail (LYPAD_IS_COLOR_WIDGET (widget));
    LypadColorWidgetInterface *iface = LYPAD_COLOR_WIDGET_GET_IFACE (widget);
    if (iface->get_color)
        iface->get_color (widget, color);
    else if (iface->get_rgba)
    {
        GdkRGBA rgba;
        iface->get_rgba (widget, &rgba);
        lypad_color_from_rgb (color, &rgba);
    }
    else
        g_warning ("LypadColorWidget::get_color not implemented for '%s'", g_type_name (G_TYPE_FROM_INSTANCE (widget)));
}

cairo_pattern_t *_lypad_color_widget_get_checkered_pattern ()
{
    static unsigned char data[8] = { 0xFF, 0x00, 0x00, 0x00,
                                    0x00, 0xFF, 0x00, 0x00 };
    static cairo_surface_t *checkered_surface = NULL;

    if (checkered_surface == NULL)
        checkered_surface = cairo_image_surface_create_for_data (data, CAIRO_FORMAT_A8, 2, 2, 4);

    cairo_pattern_t *pattern = cairo_pattern_create_for_surface (checkered_surface);
    cairo_pattern_set_extend (pattern, CAIRO_EXTEND_REPEAT);
    cairo_pattern_set_filter (pattern, CAIRO_FILTER_NEAREST);

    cairo_matrix_t matrix;
    cairo_matrix_init_scale (&matrix, 0.125, 0.125);
    cairo_pattern_set_matrix (pattern, &matrix);
    return pattern;
}
