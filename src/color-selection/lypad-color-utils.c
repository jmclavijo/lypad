/*
 *  lypad-color-utils.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-color-utils.h"

const GdkRGBA LYPAD_AUTOMATIC_TEXT_COLOR = {-1, -1, -1, -1};

void lypad_color_utils_get_text_color_for_background (const GdkRGBA *bkg_color, GdkRGBA *text_color)
{
    if (LYPAD_RGB_LUMINANCE (bkg_color->red, bkg_color->green, bkg_color->blue) > 0.5)
    {
        text_color->red   = 0.;
        text_color->green = 0.;
        text_color->blue  = 0.;
        text_color->alpha = 1.;
    }
    else
    {
        text_color->red   = 1.;
        text_color->green = 1.;
        text_color->blue  = 1.;
        text_color->alpha = 1.;
    }
}
