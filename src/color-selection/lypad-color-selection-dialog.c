/*
 *  lypad-color-selection-dialog.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-color-selection-dialog.h"
#include "lypad-color-slider.h"
#include "lypad-color-swatch.h"
#include "lypad-color-wheel.h"
#include "lypad-color-widget.h"
#include "lypad-color-spin.h"
#include "lypad-color-hex-entry.h"
#include "lypad-color-picker-button.h"
#include "lypad-color.h"
#include "src/lypad-preferences.h"
#include "src/lypad-application.h"

enum {
    CURRENT_SWATCH,
    SLIDER_CHANNEL1,
    SLIDER_CHANNEL2,
    SLIDER_CHANNEL3,
    SLIDER_ALPHA,
    SPIN_CHANNEL1,
    SPIN_CHANNEL2,
    SPIN_CHANNEL3,
    SPIN_ALPHA,
    COLOR_WHEEL,
    HEX_ENTRY,
    PICKER_BUTTON,
    N_COLOR_WIDGETS
};

enum {
    COLOR_SPACE_0,
    COLOR_SPACE_RGB,
    COLOR_SPACE_HSL,
    COLOR_SPACE_HSV
};

struct _LypadColorSelectionDialog
{
    GtkDialog         parent;
    LypadColorWidget *color_widget[N_COLOR_WIDGETS];
    LypadColorWidget *prev_swatch;
    gboolean          use_alpha;
    GtkLabel         *channel_label[4];
    int               color_space;
    GtkWidget        *recent_swatches[LYPAD_COLOR_STORE_MAX_RECENT_COLORS];
};

#define PREFERENCES_GROUP_NAME "Color selection dialog"

static void color_widget_iface_init (LypadColorWidgetInterface *iface);

G_DEFINE_TYPE_WITH_CODE (LypadColorSelectionDialog, lypad_color_selection_dialog, GTK_TYPE_DIALOG,
                         G_IMPLEMENT_INTERFACE (LYPAD_TYPE_COLOR_WIDGET, color_widget_iface_init))

static void on_color_widget_changed (LypadColorWidget *widget, gpointer user_data)
{
    LypadColorSelectionDialog *dialog = LYPAD_COLOR_SELECTION_DIALOG (user_data);
    LypadColor color;
    lypad_color_widget_get_color_full (widget, &color);
    if (!dialog->use_alpha)
        lypad_color_set_alpha (&color, 1.);

    for (int i = 0; i < N_COLOR_WIDGETS; ++i)
        if (dialog->color_widget[i] != widget)
            lypad_color_widget_set_color_full (dialog->color_widget[i], &color);
}

static void lypad_color_selection_dialog_set_rgba (LypadColorWidget *widget, const GdkRGBA *color)
{
    LypadColorSelectionDialog *dialog = LYPAD_COLOR_SELECTION_DIALOG (widget);
    GdkRGBA real_color = *color;
    if (gdk_rgba_equal (color, &LYPAD_AUTOMATIC_TEXT_COLOR))
    {
        real_color.red = 0;
        real_color.green = 0;
        real_color.blue = 0;
        real_color.alpha = 1;
    }
    else if (!dialog->use_alpha)
        real_color.alpha = 1.;

    for (int i = 0; i < N_COLOR_WIDGETS; ++i)
        lypad_color_widget_set_color (dialog->color_widget[i], &real_color);
    lypad_color_widget_set_color (dialog->prev_swatch, &real_color);
}

static void lypad_color_selection_dialog_get_rgba (LypadColorWidget *widget, GdkRGBA *color)
{
    LypadColorSelectionDialog *dialog = LYPAD_COLOR_SELECTION_DIALOG (widget);
    lypad_color_widget_get_color (dialog->color_widget[CURRENT_SWATCH], color);
}

static void color_widget_iface_init (LypadColorWidgetInterface *iface)
{
    iface->set_rgba = lypad_color_selection_dialog_set_rgba;
    iface->get_rgba = lypad_color_selection_dialog_get_rgba;
}

static void lypad_color_selection_dialog_set_color_space (LypadColorSelectionDialog *dialog, int color_space, gboolean save_preferences)
{
    if (dialog->color_space == color_space)
        return;

    dialog->color_space = color_space;
    switch (color_space)
    {
        case COLOR_SPACE_RGB:
            gtk_label_set_label (dialog->channel_label[1], "R:");
            gtk_label_set_label (dialog->channel_label[2], "G:");
            gtk_label_set_label (dialog->channel_label[3], "B:");
            lypad_color_slider_set_channel (LYPAD_COLOR_SLIDER (dialog->color_widget[SLIDER_CHANNEL1]), LYPAD_CHANNEL_RED);
            lypad_color_slider_set_channel (LYPAD_COLOR_SLIDER (dialog->color_widget[SLIDER_CHANNEL2]), LYPAD_CHANNEL_GREEN);
            lypad_color_slider_set_channel (LYPAD_COLOR_SLIDER (dialog->color_widget[SLIDER_CHANNEL3]), LYPAD_CHANNEL_BLUE);
            lypad_color_spin_set_channel (LYPAD_COLOR_SPIN (dialog->color_widget[SPIN_CHANNEL1]), LYPAD_CHANNEL_RED);
            lypad_color_spin_set_channel (LYPAD_COLOR_SPIN (dialog->color_widget[SPIN_CHANNEL2]), LYPAD_CHANNEL_BLUE);
            lypad_color_spin_set_channel (LYPAD_COLOR_SPIN (dialog->color_widget[SPIN_CHANNEL3]), LYPAD_CHANNEL_GREEN);
            break;

        case COLOR_SPACE_HSL:
            gtk_label_set_label (dialog->channel_label[1], "H:");
            gtk_label_set_label (dialog->channel_label[2], "S:");
            gtk_label_set_label (dialog->channel_label[3], "L:");
            lypad_color_slider_set_channel (LYPAD_COLOR_SLIDER (dialog->color_widget[SLIDER_CHANNEL1]), LYPAD_CHANNEL_HUE);
            lypad_color_slider_set_channel (LYPAD_COLOR_SLIDER (dialog->color_widget[SLIDER_CHANNEL2]), LYPAD_CHANNEL_SATURATION_L);
            lypad_color_slider_set_channel (LYPAD_COLOR_SLIDER (dialog->color_widget[SLIDER_CHANNEL3]), LYPAD_CHANNEL_LUMINOSITY);
            lypad_color_spin_set_channel (LYPAD_COLOR_SPIN (dialog->color_widget[SPIN_CHANNEL1]), LYPAD_CHANNEL_HUE);
            lypad_color_spin_set_channel (LYPAD_COLOR_SPIN (dialog->color_widget[SPIN_CHANNEL2]), LYPAD_CHANNEL_SATURATION_L);
            lypad_color_spin_set_channel (LYPAD_COLOR_SPIN (dialog->color_widget[SPIN_CHANNEL3]), LYPAD_CHANNEL_LUMINOSITY);
            break;

        case COLOR_SPACE_HSV:
            gtk_label_set_label (dialog->channel_label[1], "H:");
            gtk_label_set_label (dialog->channel_label[2], "S:");
            gtk_label_set_label (dialog->channel_label[3], "V:");
            lypad_color_slider_set_channel (LYPAD_COLOR_SLIDER (dialog->color_widget[SLIDER_CHANNEL1]), LYPAD_CHANNEL_HUE);
            lypad_color_slider_set_channel (LYPAD_COLOR_SLIDER (dialog->color_widget[SLIDER_CHANNEL2]), LYPAD_CHANNEL_SATURATION_V);
            lypad_color_slider_set_channel (LYPAD_COLOR_SLIDER (dialog->color_widget[SLIDER_CHANNEL3]), LYPAD_CHANNEL_VALUE);
            lypad_color_spin_set_channel (LYPAD_COLOR_SPIN (dialog->color_widget[SPIN_CHANNEL1]), LYPAD_CHANNEL_HUE);
            lypad_color_spin_set_channel (LYPAD_COLOR_SPIN (dialog->color_widget[SPIN_CHANNEL2]), LYPAD_CHANNEL_SATURATION_V);
            lypad_color_spin_set_channel (LYPAD_COLOR_SPIN (dialog->color_widget[SPIN_CHANNEL3]), LYPAD_CHANNEL_VALUE);
            break;

        default:
            g_warning ("LypadColorSelectionDialog::set-color-space error: Invalid value.");
    }
    if (save_preferences)
    {
        LypadPreferences *preferences = lypad_application_get_preferences (LYPAD_APPLICATION_DEFAULT);
        GKeyFile *key_file = lypad_preferences_get_key_file (preferences);
        g_key_file_set_integer (key_file, PREFERENCES_GROUP_NAME, "color-space", color_space);
        lypad_preferences_set_modified (preferences);
    }
}

static void on_color_space_button_rgb_toggled (GtkToggleButton *button, gpointer user_data)
{
    if (gtk_toggle_button_get_active (button))
        lypad_color_selection_dialog_set_color_space (LYPAD_COLOR_SELECTION_DIALOG (user_data), COLOR_SPACE_RGB, TRUE);
}

static void on_color_space_button_hsl_toggled (GtkToggleButton *button, gpointer user_data)
{
    if (gtk_toggle_button_get_active (button))
        lypad_color_selection_dialog_set_color_space (LYPAD_COLOR_SELECTION_DIALOG (user_data), COLOR_SPACE_HSL, TRUE);
}

static void on_color_space_button_hsv_toggled (GtkToggleButton *button, gpointer user_data)
{
    if (gtk_toggle_button_get_active (button))
        lypad_color_selection_dialog_set_color_space (LYPAD_COLOR_SELECTION_DIALOG (user_data), COLOR_SPACE_HSV, TRUE);
}

static void on_recent_color_clicked (LypadColorSwatch *swatch, gpointer user_data)
{
    LypadColorSelectionDialog *dialog = LYPAD_COLOR_SELECTION_DIALOG (user_data);
    LypadColor color;
    lypad_color_widget_get_color_full (LYPAD_COLOR_WIDGET (swatch), &color);

    if (!dialog->use_alpha)
        lypad_color_set_alpha (&color, 1.);

    for (int i = 0; i < N_COLOR_WIDGETS; ++i)
        lypad_color_widget_set_color_full (dialog->color_widget[i], &color);
}

static void lypad_color_selection_dialog_init (LypadColorSelectionDialog *dialog)
{
    dialog->use_alpha = TRUE;
    dialog->color_space = COLOR_SPACE_RGB;

    GtkWidget *content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));
    GtkWidget *label;

    GtkWidget *main_grid = gtk_grid_new ();
    gtk_grid_set_column_spacing (GTK_GRID (main_grid), 32);
    gtk_grid_set_row_spacing (GTK_GRID (main_grid), 12);
    gtk_container_set_border_width (GTK_CONTAINER (main_grid), 12);

    gtk_container_add (GTK_CONTAINER (content_area), main_grid);

    // Color wheel
    GtkWidget *color_wheel = lypad_color_wheel_new ();
    gtk_widget_set_hexpand (color_wheel, TRUE);
    gtk_widget_set_vexpand (color_wheel, TRUE);
    gtk_grid_attach (GTK_GRID (main_grid), color_wheel, 0, 0, 1, 2);
    dialog->color_widget[COLOR_WHEEL] = LYPAD_COLOR_WIDGET (color_wheel);

    // Preview grid
    GtkWidget *preview_grid = gtk_grid_new ();
    gtk_grid_set_column_spacing (GTK_GRID (preview_grid), 8);
    gtk_widget_set_valign (preview_grid, GTK_ALIGN_END);
    label = gtk_label_new ("Current:");
    gtk_label_set_xalign (GTK_LABEL (label), 1);
    gtk_grid_attach (GTK_GRID (preview_grid), label, 0, 0, 1, 1);
    GtkWidget *swatch = lypad_color_swatch_new ();
    gtk_widget_set_hexpand (swatch, TRUE);
    lypad_color_swatch_set_borders (LYPAD_COLOR_SWATCH (swatch), LYPAD_BORDERS_ALL & ~LYPAD_BORDERS_BOTTOM);
    gtk_grid_attach (GTK_GRID (preview_grid), GTK_WIDGET (swatch), 1, 0, 1, 1);
    dialog->color_widget[CURRENT_SWATCH] = LYPAD_COLOR_WIDGET (swatch);

    label = gtk_label_new ("Previous:");
    gtk_label_set_xalign (GTK_LABEL (label), 1);
    gtk_grid_attach (GTK_GRID (preview_grid), label, 0, 1, 1, 1);
    dialog->prev_swatch = LYPAD_COLOR_WIDGET (lypad_color_swatch_new ());
    gtk_widget_set_hexpand (GTK_WIDGET (dialog->prev_swatch), TRUE);
    gtk_grid_attach (GTK_GRID (preview_grid), GTK_WIDGET (dialog->prev_swatch), 1, 1, 1, 1);
    gtk_grid_attach (GTK_GRID (main_grid), preview_grid, 0, 2, 1, 1);

    // Slider grid
    GtkWidget *slider_grid = gtk_grid_new ();
    gtk_grid_set_row_spacing (GTK_GRID (slider_grid), 4);
    gtk_grid_set_column_spacing (GTK_GRID (slider_grid), 8);

    // Color space selector
    GtkWidget *color_space_box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_style_context_add_class (gtk_widget_get_style_context (color_space_box), "linked");
    GtkWidget *rgb_button = gtk_radio_button_new_with_label (NULL, "RGB");
    GtkWidget *hsl_button = gtk_radio_button_new_with_label (gtk_radio_button_get_group (GTK_RADIO_BUTTON (rgb_button)), "HSL");
    GtkWidget *hsv_button = gtk_radio_button_new_with_label (gtk_radio_button_get_group (GTK_RADIO_BUTTON (rgb_button)), "HSV");
    gtk_toggle_button_set_mode (GTK_TOGGLE_BUTTON (rgb_button), FALSE);
    gtk_toggle_button_set_mode (GTK_TOGGLE_BUTTON (hsl_button), FALSE);
    gtk_toggle_button_set_mode (GTK_TOGGLE_BUTTON (hsv_button), FALSE);
    gtk_box_pack_start (GTK_BOX (color_space_box), rgb_button, FALSE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX (color_space_box), hsl_button, FALSE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX (color_space_box), hsv_button, FALSE, TRUE, 0);
    gtk_widget_set_hexpand (rgb_button, TRUE);
    gtk_widget_set_hexpand (hsl_button, TRUE);
    gtk_widget_set_hexpand (hsv_button, TRUE);
    gtk_style_context_add_class (gtk_widget_get_style_context (rgb_button), "LypadColorSpaceRadio");
    gtk_style_context_add_class (gtk_widget_get_style_context (hsl_button), "LypadColorSpaceRadio");
    gtk_style_context_add_class (gtk_widget_get_style_context (hsv_button), "LypadColorSpaceRadio");
    gtk_widget_set_margin_bottom (color_space_box, 2);
    gtk_grid_attach (GTK_GRID (slider_grid), color_space_box, 1, -1, 2, 1);

    GtkSizeGroup *label_size_group = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);
    GtkWidget *slider;
    GtkWidget *spin;
    label = gtk_label_new ("R:");
    slider = lypad_color_slider_new ();
    spin = lypad_color_spin_new ();
    gtk_grid_attach (GTK_GRID (slider_grid), label,  0, 0, 1, 1);
    gtk_grid_attach (GTK_GRID (slider_grid), slider, 1, 0, 1, 1);
    gtk_grid_attach (GTK_GRID (slider_grid), spin,   2, 0, 1, 1);
    gtk_label_set_xalign (GTK_LABEL (label), 1);
    gtk_widget_set_hexpand (slider, TRUE);
    lypad_color_slider_set_channel (LYPAD_COLOR_SLIDER (slider), LYPAD_CHANNEL_RED);
    lypad_color_spin_set_channel   (LYPAD_COLOR_SPIN   (spin),   LYPAD_CHANNEL_RED);
    dialog->channel_label[1] = GTK_LABEL (label);
    dialog->color_widget[SLIDER_CHANNEL1] = LYPAD_COLOR_WIDGET (slider);
    dialog->color_widget[SPIN_CHANNEL1] = LYPAD_COLOR_WIDGET (spin);
    gtk_size_group_add_widget (label_size_group, label);

    label = gtk_label_new ("G:");
    slider = lypad_color_slider_new ();
    spin = lypad_color_spin_new ();
    gtk_grid_attach (GTK_GRID (slider_grid), label,  0, 1, 1, 1);
    gtk_grid_attach (GTK_GRID (slider_grid), slider, 1, 1, 1, 1);
    gtk_grid_attach (GTK_GRID (slider_grid), spin,   2, 1, 1, 1);
    gtk_label_set_xalign (GTK_LABEL (label), 1);
    gtk_widget_set_hexpand (slider, TRUE);
    lypad_color_slider_set_channel (LYPAD_COLOR_SLIDER (slider), LYPAD_CHANNEL_GREEN);
    lypad_color_spin_set_channel   (LYPAD_COLOR_SPIN   (spin),   LYPAD_CHANNEL_GREEN);
    dialog->channel_label[2] = GTK_LABEL (label);
    dialog->color_widget[SLIDER_CHANNEL2] = LYPAD_COLOR_WIDGET (slider);
    dialog->color_widget[SPIN_CHANNEL2] = LYPAD_COLOR_WIDGET (spin);
    gtk_size_group_add_widget (label_size_group, label);

    label = gtk_label_new ("B:");
    slider = lypad_color_slider_new ();
    spin = lypad_color_spin_new ();
    gtk_grid_attach (GTK_GRID (slider_grid), label,  0, 2, 1, 1);
    gtk_grid_attach (GTK_GRID (slider_grid), slider, 1, 2, 1, 1);
    gtk_grid_attach (GTK_GRID (slider_grid), spin,   2, 2, 1, 1);
    gtk_label_set_xalign (GTK_LABEL (label), 1);
    gtk_widget_set_hexpand (slider, TRUE);
    lypad_color_slider_set_channel (LYPAD_COLOR_SLIDER (slider), LYPAD_CHANNEL_BLUE);
    lypad_color_spin_set_channel   (LYPAD_COLOR_SPIN   (spin),   LYPAD_CHANNEL_BLUE);
    dialog->channel_label[3] = GTK_LABEL (label);
    dialog->color_widget[SLIDER_CHANNEL3] = LYPAD_COLOR_WIDGET (slider);
    dialog->color_widget[SPIN_CHANNEL3] = LYPAD_COLOR_WIDGET (spin);
    gtk_size_group_add_widget (label_size_group, label);

    label = gtk_label_new ("A:");
    slider = lypad_color_slider_new ();
    spin = lypad_color_spin_new ();
    gtk_grid_attach (GTK_GRID (slider_grid), label,  0, 3, 1, 1);
    gtk_grid_attach (GTK_GRID (slider_grid), slider, 1, 3, 1, 1);
    gtk_grid_attach (GTK_GRID (slider_grid), spin,   2, 3, 1, 1);
    gtk_label_set_xalign (GTK_LABEL (label), 1);
    gtk_widget_set_hexpand (slider, TRUE);
    lypad_color_slider_set_channel (LYPAD_COLOR_SLIDER (slider), LYPAD_CHANNEL_ALPHA);
    lypad_color_spin_set_channel   (LYPAD_COLOR_SPIN   (spin),   LYPAD_CHANNEL_ALPHA);
    dialog->channel_label[0] = GTK_LABEL (label);
    dialog->color_widget[SLIDER_ALPHA] = LYPAD_COLOR_WIDGET (slider);
    dialog->color_widget[SPIN_ALPHA] = LYPAD_COLOR_WIDGET (spin);
    gtk_size_group_add_widget (label_size_group, label);

    gtk_grid_attach (GTK_GRID (main_grid), slider_grid, 1, 0, 1, 1);

    // Hex box
    GtkWidget *hex_box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 8);
    label = gtk_label_new ("Hex:");
    spin = lypad_color_hex_entry_new ();
    gtk_box_pack_start (GTK_BOX (hex_box), label, FALSE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX (hex_box), spin, FALSE, TRUE, 0);
    gtk_label_set_xalign (GTK_LABEL (label), 1);
    gtk_widget_set_halign (spin, GTK_ALIGN_START);
    dialog->color_widget[HEX_ENTRY] = LYPAD_COLOR_WIDGET (spin);
    gtk_widget_set_valign (hex_box, GTK_ALIGN_CENTER);
    gtk_grid_attach (GTK_GRID (main_grid), hex_box, 1, 1, 1, 1);
    gtk_size_group_add_widget (label_size_group, label);

    spin = lypad_color_picker_button_new ();
    gtk_box_pack_end (GTK_BOX (hex_box), spin, FALSE, TRUE, 0);
    dialog->color_widget[PICKER_BUTTON] = LYPAD_COLOR_WIDGET (spin);

    g_object_unref (label_size_group);

    // Recent box
    GtkWidget *recent_container_box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 4);
    label = gtk_label_new ("Recent colors:");
    gtk_label_set_xalign (GTK_LABEL (label), 0);
    gtk_box_pack_start (GTK_BOX (recent_container_box), label, FALSE, TRUE, 0);
    GtkWidget *recent_box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_pack_start (GTK_BOX (recent_container_box), recent_box, FALSE, TRUE, 0);
    for (int i = 0; i < LYPAD_COLOR_STORE_MAX_RECENT_COLORS; ++i)
    {
        swatch = lypad_color_swatch_new ();
        lypad_color_swatch_set_borders (LYPAD_COLOR_SWATCH (swatch), LYPAD_BORDERS_NONE);
        lypad_color_swatch_set_show_hover_box (LYPAD_COLOR_SWATCH (swatch), TRUE);
        gtk_widget_set_size_request (swatch, 20, 20);
        gtk_widget_set_no_show_all (swatch, TRUE);
        gtk_widget_hide (swatch);
        gtk_box_pack_start (GTK_BOX (recent_box), swatch, FALSE, TRUE, 0);
        g_signal_connect (swatch, "clicked", G_CALLBACK (on_recent_color_clicked), dialog);
        dialog->recent_swatches[i] = swatch;
    }

    gtk_grid_attach (GTK_GRID (main_grid), recent_container_box, 1, 2, 1, 1);

    LypadPreferences *preferences = lypad_application_get_preferences (LYPAD_APPLICATION_DEFAULT);
    GKeyFile *key_file = lypad_preferences_get_key_file (preferences);
    int color_space = g_key_file_get_integer (key_file, PREFERENCES_GROUP_NAME, "color-space", NULL);
    if (color_space)
    {
        switch (color_space)
        {
            case COLOR_SPACE_RGB:
                gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (rgb_button), TRUE);
                break;

            case COLOR_SPACE_HSL:
                gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (hsl_button), TRUE);
                break;

            case COLOR_SPACE_HSV:
                gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (hsv_button), TRUE);
                break;
        }
        lypad_color_selection_dialog_set_color_space (dialog, color_space, FALSE);
    }

    gtk_widget_show_all (content_area);

    g_signal_connect (rgb_button, "toggled", G_CALLBACK (on_color_space_button_rgb_toggled), dialog);
    g_signal_connect (hsl_button, "toggled", G_CALLBACK (on_color_space_button_hsl_toggled), dialog);
    g_signal_connect (hsv_button, "toggled", G_CALLBACK (on_color_space_button_hsv_toggled), dialog);

    for (int i = 0; i < N_COLOR_WIDGETS; ++i)
        g_signal_connect (dialog->color_widget[i], "color-changed", G_CALLBACK (on_color_widget_changed), dialog);

    gtk_dialog_add_button (GTK_DIALOG (dialog), "Cancel", GTK_RESPONSE_CANCEL);
    GtkWidget *accept_button = gtk_dialog_add_button (GTK_DIALOG (dialog), "Select", GTK_RESPONSE_ACCEPT);
    gtk_style_context_add_class (gtk_widget_get_style_context (accept_button), GTK_STYLE_CLASS_SUGGESTED_ACTION);
    gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_ACCEPT);
}

void lypad_color_selection_dialog_set_use_alpha (LypadColorSelectionDialog *dialog, gboolean use_alpha)
{
    if (use_alpha != dialog->use_alpha)
    {
        dialog->use_alpha = use_alpha;
        gtk_widget_set_visible (GTK_WIDGET (dialog->color_widget[SLIDER_ALPHA]), use_alpha);
        gtk_widget_set_visible (GTK_WIDGET (dialog->color_widget[SPIN_ALPHA]), use_alpha);
        gtk_widget_set_visible (GTK_WIDGET (dialog->channel_label[0]), use_alpha);

        if (!use_alpha)
        {
            LypadColor color;
            lypad_color_widget_get_color_full (dialog->color_widget[CURRENT_SWATCH], &color);
            if (lypad_color_get_alpha (&color) < 1.)
            {
                lypad_color_set_alpha (&color, 1);
                for (int i = 0; i < N_COLOR_WIDGETS; ++i)
                    if (i != CURRENT_SWATCH)
                        lypad_color_widget_set_color_full (dialog->color_widget[i], &color);
            }
        }
    }
}

void lypad_color_selection_dialog_update_recent (LypadColorSelectionDialog *dialog, LypadColorSet color_set)
{
    int n_recent;
    GdkRGBA *recent_colors = lypad_color_store_get_recent (color_set, &n_recent);
    for (int i = 0; i < LYPAD_COLOR_STORE_MAX_RECENT_COLORS; ++i)
    {
        if (i < n_recent)
        {
            lypad_color_widget_set_color (LYPAD_COLOR_WIDGET (dialog->recent_swatches[i]), &recent_colors[i]);
            gtk_widget_show (dialog->recent_swatches[i]);
        }
        else
            gtk_widget_hide (dialog->recent_swatches[i]);
    }
    g_free (recent_colors);
}

static void lypad_color_selection_dialog_class_init (LypadColorSelectionDialogClass *klass)
{
}

GtkWidget *lypad_color_selection_dialog_new (GtkWindow *parent)
{
    return g_object_new (LYPAD_TYPE_COLOR_SELECTION_DIALOG,
                         "transient-for", parent,
                         "destroy-with-parent", TRUE,
                         "title", "Select color",
                         "use-header-bar", TRUE,
                         NULL);
}
