/*
 *  lypad-color-widget.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include "lypad-color.h"

G_BEGIN_DECLS

#define LYPAD_TYPE_COLOR_WIDGET (lypad_color_widget_get_type ())
G_DECLARE_INTERFACE (LypadColorWidget, lypad_color_widget, LYPAD, COLOR_WIDGET, GObject)

typedef enum
{
    LYPAD_CHANNEL_RED,
    LYPAD_CHANNEL_GREEN,
    LYPAD_CHANNEL_BLUE,
    LYPAD_CHANNEL_ALPHA,
    LYPAD_CHANNEL_HUE,
    LYPAD_CHANNEL_SATURATION_L,
    LYPAD_CHANNEL_LUMINOSITY,
    LYPAD_CHANNEL_SATURATION_V,
    LYPAD_CHANNEL_VALUE
} LypadColorChannel;

extern const GdkRGBA LYPAD_AUTOMATIC_TEXT_COLOR;

void  lypad_color_widget_set_color      (LypadColorWidget *widget, const GdkRGBA *color);
void  lypad_color_widget_get_color      (LypadColorWidget *widget, GdkRGBA *color);

void  lypad_color_widget_set_color_full (LypadColorWidget *widget, const LypadColor *color);
void  lypad_color_widget_get_color_full (LypadColorWidget *widget, LypadColor *color);

struct _LypadColorWidgetInterface
{
    GTypeInterface parent_iface;
    void (*set_rgba)  (LypadColorWidget *widget, const GdkRGBA *color);
    void (*get_rgba)  (LypadColorWidget *widget, GdkRGBA *color);
    void (*set_color) (LypadColorWidget *widget, const LypadColor *color);
    void (*get_color) (LypadColorWidget *widget, LypadColor *color);
};

void             _lypad_color_widget_changed               (LypadColorWidget *widget);
cairo_pattern_t *_lypad_color_widget_get_checkered_pattern (void);                             // [transfer full]

G_END_DECLS
