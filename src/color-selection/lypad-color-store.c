/*
 *  lypad-color-store.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-color-store.h"
#include "src/lypad-preferences.h"
#include "src/lypad-application.h"

struct _LypadColorStore
{
    GQueue            recent[LYPAD_N_COLOR_SETS];
    LypadColorPalette palette[LYPAD_N_COLOR_SETS];
    gboolean          recent_changed[LYPAD_N_COLOR_SETS];
};

#define PREFERENCES_GROUP_NAME "Color store"

static char *preferences_recent_set_name[] =
{
    "recent-text-color",
    "recent-note-color",
    "recent-tag-color"
};

static const int text_colors[] =
{
    0x000000, 0x202020, 0x404040, 0x606060, 0x7f7f7f, 0x9f9f9f, 0xbfbfbf, 0xdfdfdf, 0xffffff,
    0xfac2c2, 0xfbdebf, 0xfbeabc, 0xe4f5bc, 0xcfe7cd, 0xbedee1, 0xbeedf9, 0xdef3ff, 0xc7bfff,
    0xf66a6a, 0xf7bd7f, 0xf9d67a, 0xc8eb79, 0x9ecf99, 0x7dbdc3, 0x7cdbf3, 0x7dc3e8, 0xa18fff,
    0xcc0000, 0xf39d3c, 0xf5c12e, 0xace12e, 0x6ec664, 0x3a9ca6, 0x35caed, 0x39a5dc, 0x8461f7,
    0xa30000, 0xec7a08, 0xf0ab00, 0x92d400, 0x3f9c35, 0x007a87, 0x00b9e4, 0x0088ce, 0x703fec,
    0x8b0000, 0xb35c00, 0xb58100, 0x6ca100, 0x2d7623, 0x005c66, 0x008bad, 0x00659c, 0x582fc0,
    0x470000, 0x773d00, 0x795600, 0x486b00, 0x1e4f18, 0x003d44, 0x005c73, 0x004368, 0x40199a,
};

static const int note_colors[] =
{
    0xfac2c2, 0xfbdebf, 0xfbeabc, 0xe4f5bc, 0xcfe7cd, 0xbedee1, 0xbeedf9, 0xdef3ff, 0xc7bfff,
    0xf66a6a, 0xf7bd7f, 0xf9d67a, 0xc8eb79, 0x9ecf99, 0x7dbdc3, 0x7cdbf3, 0x7dc3e8, 0xa18fff,
    0x8b0000, 0xb35c00, 0xb58100, 0x6ca100, 0x2d7623, 0x005c66, 0x008bad, 0x00659c, 0x582fc0,
};

static const char* const note_color_rows[] = {"Pale", "Vivid", "Dark", NULL};

static inline void lypad_color_store_load_config (LypadColorStore *store)
{
    LypadPreferences *preferences = lypad_application_get_preferences (LYPAD_APPLICATION_DEFAULT);
    GKeyFile *key_file = lypad_preferences_get_key_file (preferences);
    for (int set = 0; set < LYPAD_N_COLOR_SETS; ++set)
    {
        char **colors = g_key_file_get_string_list (key_file, PREFERENCES_GROUP_NAME, preferences_recent_set_name[set], NULL, NULL);
        if (!colors)
            continue;

        for (int i = 0; colors[i] != NULL; ++i)
        {
            GdkRGBA color;
            if (gdk_rgba_parse (&color, colors[i]))
                g_queue_push_tail (&store->recent[set], gdk_rgba_copy (&color));
            else
                g_warning ("LypadColorStore::load-config error: Invalid color '%s\n'", colors[i]);
        }
        g_strfreev (colors);

        while (g_queue_get_length (&store->recent[set]) > LYPAD_COLOR_STORE_MAX_RECENT_COLORS)
        {
            GdkRGBA *item = g_queue_pop_tail (&store->recent[set]);
            gdk_rgba_free (item);
        }
    }
}

static void lypad_color_store_init (LypadColorStore *store)
{
    for (int i = 0; i < LYPAD_N_COLOR_SETS; ++i)
    {
        g_queue_init (&store->recent[i]);
        store->recent_changed[i] = FALSE;
    }

    LypadColorPalette *palette = &store->palette[LYPAD_TEXT_COLOR_SET];
    palette->colors = g_new (GdkRGBA, G_N_ELEMENTS (text_colors));
    palette->n_colors = G_N_ELEMENTS (text_colors);
    palette->n_columns = 9;
    palette->row_names = NULL;
    for (int i = 0; i < (int) G_N_ELEMENTS (text_colors); ++i)
    {
        palette->colors[i].red   = (double)((text_colors[i] >> 16) & 0xFF) / 255.;
        palette->colors[i].green = (double)((text_colors[i] >>  8) & 0xFF) / 255.;
        palette->colors[i].blue  = (double)( text_colors[i]        & 0xFF) / 255.;
        palette->colors[i].alpha = 1.;
    }

    palette = &store->palette[LYPAD_NOTE_COLOR_SET];
    palette->colors = g_new (GdkRGBA, G_N_ELEMENTS (note_colors));
    palette->n_colors = G_N_ELEMENTS (note_colors);
    palette->n_columns = 9;
    palette->row_names = note_color_rows;
    for (int i = 0; i < (int) G_N_ELEMENTS (note_colors); ++i)
    {
        palette->colors[i].red   = (double)((note_colors[i] >> 16) & 0xFF) / 255.;
        palette->colors[i].green = (double)((note_colors[i] >>  8) & 0xFF) / 255.;
        palette->colors[i].blue  = (double)( note_colors[i]        & 0xFF) / 255.;
        palette->colors[i].alpha = 1.;
    }

    lypad_color_store_load_config (store);
}

static LypadColorStore *lypad_get_color_store ()
{
    static LypadColorStore *store = NULL;
    if (G_UNLIKELY (!store))
    {
        store = g_new (LypadColorStore, 1);
        lypad_color_store_init (store);
    }
    return store;
}

LypadColorPalette* lypad_color_store_get_palette (LypadColorSet set)
{
    LypadColorStore *store = lypad_get_color_store ();
    return &store->palette[set];
}

static void lypad_color_store_save_config (LypadColorStore *store)
{
    gboolean changed = FALSE;
    LypadPreferences *preferences = lypad_application_get_preferences (LYPAD_APPLICATION_DEFAULT);
    GKeyFile *key_file = lypad_preferences_get_key_file (preferences);
    for (int set = 0; set < LYPAD_N_COLOR_SETS; ++set)
    {
        if (store->recent_changed[set])
            changed = TRUE;
        else
            continue;
        int len = store->recent[set].length;
        char **colors = g_new (char*, len + 1);
        colors[len] = NULL;
        int i = 0;
        for (GList *l = store->recent[set].head; l != NULL; l = l->next, ++i)
            colors[i] = gdk_rgba_to_string ((GdkRGBA *) l->data);
        g_key_file_set_string_list (key_file, PREFERENCES_GROUP_NAME, preferences_recent_set_name[set], (const char * const *) colors, len);
        g_strfreev (colors);
    }

    if (changed)
        lypad_preferences_set_modified (preferences);
}

void lypad_color_store_add_recent (const GdkRGBA *color, LypadColorSet set)
{
    LypadColorStore *store = lypad_get_color_store ();
    LypadColorPalette *palette = &store->palette[set];
    for (int i = 0; i < palette->n_colors; ++i)
    {
        if (gdk_rgba_equal (&palette->colors[i], color))
            return;
    }
    store->recent_changed[set] = TRUE;
    for (GList *l = store->recent[set].head; l != NULL; l = l->next)
    {
        if (gdk_rgba_equal (l->data, color))
        {
            g_queue_unlink (&store->recent[set], l);
            g_queue_push_head_link (&store->recent[set], l);
            goto save_config;
        }
    }
    g_queue_push_head (&store->recent[set], gdk_rgba_copy (color));
    while (g_queue_get_length (&store->recent[set]) > LYPAD_COLOR_STORE_MAX_RECENT_COLORS)
    {
        GdkRGBA *item = g_queue_pop_tail (&store->recent[set]);
        gdk_rgba_free (item);
    }

save_config:
    lypad_color_store_save_config (store);
}

GdkRGBA *lypad_color_store_get_recent (LypadColorSet set, int *n_colors)
{
    LypadColorStore *store = lypad_get_color_store ();
    *n_colors = store->recent[set].length;
    GdkRGBA *colors = g_new (GdkRGBA, store->recent[set].length);
    int i = 0;
    for (GList *l = store->recent[set].head; l != NULL; l = l->next, ++i)
        colors[i] = *((GdkRGBA *) l->data);
    return colors;
}
