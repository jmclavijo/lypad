/*
 *  lypad-color-hex-entry.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-color-hex-entry.h"
#include "lypad-color-widget.h"

struct _LypadColorHexEntry
{
    GtkEntry   parent;
    LypadColor color;
    gboolean   use_alpha : 1;
};

static void color_widget_iface_init (LypadColorWidgetInterface *iface);

G_DEFINE_TYPE_WITH_CODE (LypadColorHexEntry, lypad_color_hex_entry, GTK_TYPE_ENTRY,
                         G_IMPLEMENT_INTERFACE (LYPAD_TYPE_COLOR_WIDGET, color_widget_iface_init))

static char *hex_from_rgba (const GdkRGBA *rgba, gboolean with_alpha)
{
    unsigned int r = CLAMP (rgba->red, 0, 1) * 255. + 0.5;
    unsigned int g = CLAMP (rgba->green, 0, 1) * 255. + 0.5;
    unsigned int b = CLAMP (rgba->blue, 0, 1) * 255. + 0.5;
    unsigned int a = CLAMP (rgba->alpha, 0, 1) * 255. + 0.5;
    if (with_alpha)
        return g_strdup_printf ("%02x%02x%02x%02x", r, g, b, a);
    else
        return g_strdup_printf ("%02x%02x%02x", r, g, b);
}

static void lypad_color_hex_entry_set_color (LypadColorWidget *widget, const LypadColor *color)
{
    LypadColorHexEntry *entry = LYPAD_COLOR_HEX_ENTRY (widget);
    entry->color = *color;
    GdkRGBA rgb;
    lypad_color_to_rgb (color, &rgb);
    char *text = hex_from_rgba (&rgb, entry->use_alpha);
    gtk_entry_set_text (GTK_ENTRY (entry), text);
    g_free (text);
    gtk_editable_set_position (GTK_EDITABLE (entry), -1);
}

static void lypad_color_hex_entry_get_color (LypadColorWidget *widget, LypadColor *color)
{
    *color = LYPAD_COLOR_HEX_ENTRY (widget)->color;
}

static void color_widget_iface_init (LypadColorWidgetInterface *iface)
{
    iface->set_color = lypad_color_hex_entry_set_color;
    iface->get_color = lypad_color_hex_entry_get_color;
}

static void lypad_color_hex_entry_init (LypadColorHexEntry *entry)
{
    entry->use_alpha = TRUE;
    lypad_color_init (&entry->color);
}

void lypad_color_hex_entry_set_use_alpha (LypadColorHexEntry *entry, gboolean use_alpha)
{
    g_return_if_fail (LYPAD_IS_COLOR_HEX_ENTRY (entry));
    if (use_alpha != entry->use_alpha)
    {
        GdkRGBA rgb;
        entry->use_alpha = use_alpha;
        lypad_color_to_rgb (&entry->color, &rgb);
        char *text = hex_from_rgba (&rgb, entry->use_alpha);
        gtk_entry_set_text (GTK_ENTRY (entry), text);
        g_free (text);
        gtk_editable_set_position (GTK_EDITABLE (entry), -1);
    }
}

static int parse_hex_value (const char *hex)
{
    int x = g_ascii_xdigit_value (hex[0]);
    int y = g_ascii_xdigit_value (hex[1]);
    if (x == -1 || y == -1)
        return -1;
    return (x << 4) + y;
}

static gboolean lypad_color_hex_entry_parse_text (LypadColorHexEntry *entry)
{
    const char *text = gtk_entry_get_text (GTK_ENTRY (entry));
    if (text[0] == '#')
        text += 1;

    int len = strlen (text);
    if (len != 6 && len != 8)
        return FALSE;

    int r, g, b, a;
    r = parse_hex_value (text);
    g = parse_hex_value (text + 2);
    b = parse_hex_value (text + 4);
    if (len == 8)
        a = parse_hex_value (text + 6);
    else
        a = 255;

    if (r == -1 || g == -1 || b == -1 || a == -1)
        return FALSE;

    GdkRGBA rgb;
    rgb.red   = (double) r / 255.;
    rgb.green = (double) g / 255.;
    rgb.blue  = (double) b / 255.;
    rgb.alpha = (double) a / 255.;
    lypad_color_from_rgb (&entry->color, &rgb);
    return TRUE;
}


static void lypad_color_hex_entry_activate (GtkEntry *widget)
{
    LypadColorHexEntry *entry = LYPAD_COLOR_HEX_ENTRY (widget);
    if (lypad_color_hex_entry_parse_text (entry))
        _lypad_color_widget_changed (LYPAD_COLOR_WIDGET (entry));

    GdkRGBA rgb;
    lypad_color_to_rgb (&entry->color, &rgb);
    char *text = hex_from_rgba (&rgb, entry->use_alpha);
    gtk_entry_set_text (widget, text);
    g_free (text);
    gtk_editable_set_position (GTK_EDITABLE (entry), -1);
}

static void lypad_color_hex_entry_class_init (LypadColorHexEntryClass *klass)
{
    GtkEntryClass *entry_class = GTK_ENTRY_CLASS (klass);
    entry_class->activate = lypad_color_hex_entry_activate;
}

GtkWidget *lypad_color_hex_entry_new ()
{
    return g_object_new (LYPAD_TYPE_COLOR_HEX_ENTRY, "text", "FF0000FF", NULL);
}
