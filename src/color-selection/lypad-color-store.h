/*
 *  lypad-color-store.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gdk/gdk.h>

G_BEGIN_DECLS

typedef struct _LypadColorStore LypadColorStore;

typedef enum {
    LYPAD_TEXT_COLOR_SET,
    LYPAD_NOTE_COLOR_SET,
    LYPAD_TAG_COLOR_SET,
    LYPAD_N_COLOR_SETS
} LypadColorSet;

typedef struct
{
    GdkRGBA            *colors;
    int                 n_colors;
    int                 n_columns;
    const char * const *row_names;
} LypadColorPalette;

#define LYPAD_COLOR_STORE_MAX_RECENT_COLORS 12

LypadColorPalette* lypad_color_store_get_palette (LypadColorSet set);                           // [transfer none]

void               lypad_color_store_add_recent  (const GdkRGBA *color, LypadColorSet set);
GdkRGBA           *lypad_color_store_get_recent  (LypadColorSet set, int *n_colors);            // [array, free with g_free]

G_END_DECLS
