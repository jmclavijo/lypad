/*
 *  lypad-color.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

typedef struct _LypadColor LypadColor;

struct _LypadColor
{
    double _h;
    double _s;
    double _l;
    double _a;
};

void   lypad_color_init             (LypadColor *color);

double lypad_color_get_red          (const LypadColor *color);
double lypad_color_get_green        (const LypadColor *color);
double lypad_color_get_blue         (const LypadColor *color);
double lypad_color_get_hue          (const LypadColor *color);
double lypad_color_get_saturation_l (const LypadColor *color);
double lypad_color_get_saturation_v (const LypadColor *color);
double lypad_color_get_luminosity   (const LypadColor *color);
double lypad_color_get_value        (const LypadColor *color);
double lypad_color_get_alpha        (const LypadColor *color);

void   lypad_color_set_red          (LypadColor *color, double r);
void   lypad_color_set_green        (LypadColor *color, double g);
void   lypad_color_set_blue         (LypadColor *color, double b);
void   lypad_color_set_hue          (LypadColor *color, double h);
void   lypad_color_set_saturation_l (LypadColor *color, double sl);
void   lypad_color_set_saturation_v (LypadColor *color, double sv);
void   lypad_color_set_luminosity   (LypadColor *color, double l);
void   lypad_color_set_value        (LypadColor *color, double v);
void   lypad_color_set_alpha        (LypadColor *color, double a);

void   lypad_color_to_rgb           (const LypadColor *color, GdkRGBA *rgb);
void   lypad_color_from_rgb         (LypadColor *color, const GdkRGBA *rgb);
void   lypad_color_from_hsl         (LypadColor *color, double h, double s, double l, double a);
void   lypad_color_from_hsv         (LypadColor *color, double h, double s, double v, double a);
void   lypad_color_to_hsv           (const LypadColor *color, double *h, double *s, double *v, double *a);

G_END_DECLS
