/*
 *  lypad-color-utils.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gdk/gdk.h>

G_BEGIN_DECLS

extern const GdkRGBA LYPAD_AUTOMATIC_TEXT_COLOR;

#define LYPAD_RGB_LUMINANCE(r,g,b) ((r) * 0.22248840 + \
                                    (g) * 0.71690369 + \
                                    (b) * 0.06060791)

void lypad_color_utils_get_text_color_for_background (const GdkRGBA *bkg_color, GdkRGBA *text_color);

G_END_DECLS
