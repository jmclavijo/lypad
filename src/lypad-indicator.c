/*
 *  lypad-indicator.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-indicator.h"

#ifdef USE_LEGACY_INDICATORS
#    include <libappindicator/app-indicator.h>
#    if !defined(APP_IS_INDICATOR) && defined(IS_APP_INDICATOR)
#        define APP_IS_INDICATOR(obj) IS_APP_INDICATOR(obj)
#    endif
#else
#    include <libayatana-appindicator/app-indicator.h>
#    if !defined(APP_IS_INDICATOR) && defined(IS_APP_INDICATOR)
#        define APP_IS_INDICATOR(obj) IS_APP_INDICATOR(obj)
#    endif
#endif

static AppIndicator *indicator = NULL;
static gboolean app_hold = FALSE;

static void on_show_tray_icon_changed (GSettings *settings, gchar *key, gpointer app)
{
    g_return_if_fail (APP_IS_INDICATOR (indicator));
    gboolean enabled = g_settings_get_boolean (settings, "show-tray-icon");

    if (enabled)
    {
        app_indicator_set_status(indicator, APP_INDICATOR_STATUS_ACTIVE);
        if (!app_hold)
        {
            g_application_hold (G_APPLICATION (app));
            app_hold = TRUE;
        }
    }
    else
    {
        app_indicator_set_status(indicator, APP_INDICATOR_STATUS_PASSIVE);
        if (app_hold)
        {
            g_application_release (G_APPLICATION (app));
            app_hold = FALSE;
        }
    }
}

void lypad_init_indicator (LypadApplication *app)
{
    g_return_if_fail (indicator == NULL);

    indicator = app_indicator_new ("lypad-indicator", "org.gnome.lypad-indicator-symbolic", APP_INDICATOR_CATEGORY_APPLICATION_STATUS);
    GtkWidget *menu = gtk_menu_new_from_model (lypad_application_get_menu (LYPAD_APPLICATION (app), LYPAD_INDICATOR_MENU));
    gtk_widget_insert_action_group (menu, "app", G_ACTION_GROUP (LYPAD_APPLICATION_DEFAULT));
    app_indicator_set_menu (indicator, GTK_MENU (menu));
    gtk_widget_show_all(menu);

    GSettings *settings = g_settings_new ("org.gnome.lypad");
    g_signal_connect (settings, "changed::show-tray-icon", G_CALLBACK (on_show_tray_icon_changed), app);

    on_show_tray_icon_changed (settings, NULL, app);
}
