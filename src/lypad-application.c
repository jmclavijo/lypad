/*
 *  lypad-application.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-application.h"
#include "lypad-main-window.h"
#include "lypad-preferences.h"
#include "lypad-notes-manager.h"
#include "lypad-indicator.h"
#include "lypad.h"
#include "themes/lypad-theme-flat.h"
#include "export-formats/lypad-export-format-pdf.h"
#include "export-formats/lypad-export-format-plain.h"

#include <gdk/gdkx.h>
#include <gdk/gdkwayland.h>
#include <fontconfig/fontconfig.h>

enum {
    GDK_BACKEND_X11,
    GDK_BACKEND_WAYLAND,
    GDK_BACKEND_OTHER
};

struct _LypadApplication
{
    GtkApplication     parent;
    LypadNotesManager *notes_manager;
    LypadPreferences  *preferences;
    GMenu             *menu[LYPAD_N_MENUS];

    gboolean           supports_desktop_notes;
    int                gdk_backend;
};

G_DEFINE_TYPE (LypadApplication, lypad_application, GTK_TYPE_APPLICATION)

static void lypad_application_activate (GApplication *app);

static void on_quit_activated (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
    g_application_quit (G_APPLICATION (user_data));
}

static void on_about_activated (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
    lypad_application_show_about_dialog (LYPAD_APPLICATION (user_data), NULL);
}

static void on_show_main_window_activated (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
    lypad_application_activate (G_APPLICATION (user_data));
}

static void on_new_note_activated (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
    LypadNote *note = lypad_note_new ();
    lypad_notes_manager_import (LYPAD_APPLICATION (user_data)->notes_manager, note);
    lypad_note_set_show_on_desktop (note, TRUE, NULL);
}

static GActionEntry action_entries[] = {
	{ "new-note", on_new_note_activated, NULL, NULL, NULL },
	{ "show-main-window", on_show_main_window_activated, NULL, NULL, NULL },
	{ "about", on_about_activated, NULL, NULL, NULL },
	{ "quit", on_quit_activated, NULL, NULL, NULL }
};

static void lypad_application_init (LypadApplication *app)
{
    static const GOptionEntry option_entries[] = {
        {"background",      'b', 0, G_OPTION_ARG_NONE,           NULL, "Start in background",      NULL},
        {"version",         'v', 0, G_OPTION_ARG_NONE,           NULL, "Show version information", NULL},
        {"quit",            'q', 0, G_OPTION_ARG_NONE,           NULL, "Quit Lypad",               NULL},
        {G_OPTION_REMAINING, 0 , 0, G_OPTION_ARG_FILENAME_ARRAY, NULL,  NULL,                  "[FILE…]"},
        {NULL}
    };
    g_application_add_main_option_entries (G_APPLICATION (app), option_entries);

    app->notes_manager = NULL;
    app->preferences = NULL;
    app->supports_desktop_notes = FALSE;
    app->gdk_backend = GDK_BACKEND_OTHER;

    for (int i = 0; i < LYPAD_N_MENUS; ++i)
        app->menu[i] = NULL;

    gdk_set_allowed_backends ("x11,*");
}

static void add_accelerator (GtkApplication *app, const char *action_name, const char *accel)
{
	const gchar *vaccels[] = {accel, NULL};
	gtk_application_set_accels_for_action (app, action_name, vaccels);
}

static inline void lypad_application_setup_fonts (LypadApplication *app)
{
    const char *data_path = lypad_preferences_get_data_path (app->preferences);
    GFile *in_file = g_file_new_for_uri ("resource:///org/gnome/lypad/fonts/lypad-symbols.ttf");
    GFile *font_folder = g_file_new_build_filename (data_path, "fonts", NULL);
    char *font_path = g_build_filename (data_path, "fonts", "lypad-symbols.ttf", NULL);
    GFile *out_file = g_file_new_for_path (font_path);
    GError *error = NULL;
    gboolean font_created = FALSE;

    if (!g_file_make_directory_with_parents (font_folder, NULL, &error))
    {
        if (error->code != G_IO_ERROR_EXISTS)
        {
            g_warning ("%s\n", error->message);
            goto file_cleanup;
        }
        g_clear_error (&error);
    }

    //FIXME: Avoid making the copy if the file exist and it is the same
    if (!g_file_copy (in_file, out_file, G_FILE_COPY_OVERWRITE, NULL, NULL, NULL, &error))
    {
        g_warning ("%s\n", error->message);
        goto file_cleanup;
    }
    font_created = TRUE;

file_cleanup:
    g_clear_error (&error);
    g_object_unref (font_folder);
    g_object_unref (in_file);
    g_object_unref (out_file);

    if (!font_created)
    {
        g_free (font_path);
        return;
    }

    FcConfig *fc_config = FcInitLoadConfig ();
    FcConfigSetCurrent (fc_config);
    FcConfigAppFontAddFile (fc_config, (const unsigned char *) font_path);
    g_free (font_path);
}

static void lypad_application_startup (GApplication *app)
{
    G_APPLICATION_CLASS (lypad_application_parent_class)->startup(app);

    // Flatpak hack for TMPDIR
    if (g_file_test ("/app", G_FILE_TEST_EXISTS))
    {
        // Set /tmp for GLib, /tmp not accessible in flatpak
        char *tmpdir = g_strdup_printf ("%s/app/org.gnome.lypad", g_getenv ("XDG_RUNTIME_DIR"));
        g_setenv("TMPDIR", tmpdir, TRUE);
        g_free (tmpdir);
    }

    GdkScreen *screen = gdk_screen_get_default ();
    GtkCssProvider *provider = gtk_css_provider_new ();
    gtk_css_provider_load_from_resource (provider, "/org/gnome/lypad/css/lypad.css");
    gtk_style_context_add_provider_for_screen (screen,
                                               GTK_STYLE_PROVIDER (provider),
                                               GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    g_object_unref (provider);

    GtkIconTheme *icon_theme = gtk_icon_theme_get_default ();
    gtk_icon_theme_add_resource_path (icon_theme, "/org/gnome/lypad/icons");

    LYPAD_APPLICATION (app)->menu[LYPAD_NOTE_WINDOW_MENU] = gtk_application_get_menu_by_id (GTK_APPLICATION (app), "note-window-menu");
    LYPAD_APPLICATION (app)->menu[LYPAD_INDICATOR_MENU]   = gtk_application_get_menu_by_id (GTK_APPLICATION (app), "lypad-indicator");

    add_accelerator (GTK_APPLICATION (app), "win.new-note", "<Primary>N");

	g_action_map_add_action_entries (G_ACTION_MAP (app), action_entries, G_N_ELEMENTS (action_entries), app);

    GdkDisplay *display = gdk_display_get_default ();
    #ifdef GDK_WINDOWING_X11
        if (GDK_IS_X11_DISPLAY (display))
        {
            LYPAD_APPLICATION (app)->supports_desktop_notes = TRUE;
            LYPAD_APPLICATION (app)->gdk_backend = GDK_BACKEND_X11;
        }
        else
    #endif
    #ifdef GDK_WINDOWING_WAYLAND
        if (GDK_IS_WAYLAND_DISPLAY (display))
        {
            LYPAD_APPLICATION (app)->supports_desktop_notes = FALSE;
            LYPAD_APPLICATION (app)->gdk_backend = GDK_BACKEND_WAYLAND;
        }
        else
    #endif
        {
            LYPAD_APPLICATION (app)->supports_desktop_notes = FALSE;
            LYPAD_APPLICATION (app)->gdk_backend = GDK_BACKEND_OTHER;
        }

    if (!lypad_application_get_supports_desktop_notes (LYPAD_APPLICATION (app)))
    {
        GAction *action = g_action_map_lookup_action (G_ACTION_MAP (app), "new-note");
        g_simple_action_set_enabled (G_SIMPLE_ACTION (action), FALSE);
    }

    lypad_theme_register (LYPAD_TYPE_THEME_FLAT);
    lypad_export_format_register (LYPAD_TYPE_EXPORT_FORMAT_PDF);
    lypad_export_format_register (LYPAD_TYPE_EXPORT_FORMAT_PLAIN);

    LYPAD_APPLICATION (app)->preferences = lypad_preferences_new ();
    lypad_application_setup_fonts (LYPAD_APPLICATION (app));

    LYPAD_APPLICATION (app)->notes_manager = lypad_notes_manager_new ();
    lypad_init_indicator (LYPAD_APPLICATION (app));
}

static GtkWindow *lypad_application_get_main_window (GApplication *app)
{
    GtkWindow *window = NULL;
    for (GList *l = gtk_application_get_windows (GTK_APPLICATION (app)); l != NULL; l = l->next)
        if (LYPAD_IS_MAIN_WINDOW (l->data))
        {
            window = l->data;
            break;
        }
    if (!window)
        window = GTK_WINDOW (lypad_main_window_new (LYPAD_APPLICATION(app)));
    return window;
}

GMenuModel *lypad_application_get_menu (LypadApplication *app, LypadAppMenu id)
{
    return G_MENU_MODEL (app->menu[id]);
}

static void lypad_application_activate (GApplication *app)
{
    GtkWindow *window = lypad_application_get_main_window (app);
    gtk_window_present_with_time (window, g_get_monotonic_time () / 1000);
}

void lypad_application_edit (LypadApplication *app, LypadNote *note)
{
    GtkWindow *window = lypad_application_get_main_window (G_APPLICATION (app));
    GSList *notes = g_slist_prepend (NULL, note);
    lypad_main_window_edit_notes (LYPAD_MAIN_WINDOW (window), notes);
    g_slist_free (notes);
    gtk_window_present (window);
}

LypadNotesManager *lypad_application_get_notes_manager (LypadApplication *app)
{
    return app->notes_manager;
}

LypadPreferences *lypad_application_get_preferences (LypadApplication *app)
{
    return app->preferences;
}

static GtkWidget *about_dialog = NULL;

static void on_about_dialog_response (GtkDialog *dialog, int response_id, gpointer user_data)
{
    about_dialog = NULL;
    gtk_widget_destroy (GTK_WIDGET (dialog));
}

void lypad_application_show_about_dialog (LypadApplication *app, GtkWindow *parent)
{
    if (!about_dialog)
    {
        about_dialog = gtk_about_dialog_new ();
        g_signal_connect (about_dialog, "response", G_CALLBACK (on_about_dialog_response), NULL);

        const gchar *authors[] =
        {
            "José M. Clavijo <jmclavijo94@gmail.com>",
            NULL
        };

        g_object_set (about_dialog,
                      "application", app,
                      "program-name", "Lypad",
                      "version", G_STRINGIFY (LYPAD_VERSION),
                      "comments", "Sticky notes application",
                      //"website", "",
                      "copyright", "© 2021 José M. Clavijo",
                      "license-type", GTK_LICENSE_GPL_3_0,
                      "authors", authors,
                      "logo-icon-name", "org.gnome.lypad",
                      NULL);

        gtk_window_set_transient_for (GTK_WINDOW (about_dialog), parent);
        gtk_window_set_modal (GTK_WINDOW (about_dialog), parent != NULL);
    }
    gtk_window_present_with_time (GTK_WINDOW (about_dialog), g_get_monotonic_time () / 1000);
}

gboolean lypad_application_get_supports_desktop_notes (LypadApplication *app)
{
    return app->gdk_backend == GDK_BACKEND_X11;
}


static void on_invalid_backend_dialog_response (GtkDialog *dialog, int response_id, gpointer user_data)
{
    gtk_widget_destroy (GTK_WIDGET (dialog));
}

void lypad_application_show_invalid_backend_dialog (LypadApplication *app, GtkWindow *parent)
{
    g_return_if_fail (app->gdk_backend != GDK_BACKEND_X11);
    GtkWidget *dialog = gtk_message_dialog_new (parent,
                                                GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                                GTK_MESSAGE_WARNING,
                                                GTK_BUTTONS_OK,
                                                app->gdk_backend == GDK_BACKEND_WAYLAND ?
                                                    "This feature is not supported on Wayland" :
                                                    "This feature is not supported in the current windowing system");

    gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog), "Change to x11 if you want to use it.");
    g_signal_connect (dialog, "response", G_CALLBACK (on_invalid_backend_dialog_response), NULL);
    gtk_widget_show (dialog);
}

static void lypad_application_open (GApplication *app, GFile **files, gint n_files, const gchar *hint)
{
    GSList *notes_list = NULL;
    GSList *loaded_notes = lypad_notes_manager_get_notes (LYPAD_APPLICATION (app)->notes_manager);

    for (int i = 0; i < n_files; ++i)
    {
        LypadNote *note = NULL;
        for (GSList *l = loaded_notes; l != NULL; l = l->next)
        {
            if (lypad_note_file_equal (l->data, files[i]))
            {
                note = g_object_ref (l->data);
                break;
            }
        }
        if (!note)
            note = lypad_note_load_from_file (files[i]);
        if (note)
            notes_list = g_slist_prepend (notes_list, note);
    }
    if (notes_list)
    {
        notes_list = g_slist_reverse (notes_list);
        GtkWindow *window = lypad_application_get_main_window (app);
        lypad_main_window_edit_notes (LYPAD_MAIN_WINDOW (window), notes_list);
        gtk_window_present (window);
        g_slist_free_full (notes_list, g_object_unref);
    }
}

static gint lypad_application_handle_local_options (GApplication *app, GVariantDict *options)
{
    if (g_variant_dict_contains (options, "version"))
    {
        g_print ("Lypad - v" G_STRINGIFY (LYPAD_VERSION) "\n");
        return EXIT_SUCCESS;
    }
    return -1;
}

static int lypad_application_command_line (GApplication *app, GApplicationCommandLine *command_line)
{
	GVariantDict *options = g_application_command_line_get_options_dict (command_line);
	const gchar **remaining_args;

    if (g_variant_dict_contains (options, "quit"))
        g_application_quit (app);

    if (g_variant_dict_lookup (options, G_OPTION_REMAINING, "^a&ay", &remaining_args))
    {
        GPtrArray *file_array = g_ptr_array_new_full (0, g_object_unref);
        for (const char **filename = remaining_args; *filename != NULL; ++filename)
        {
            GFile *file = g_application_command_line_create_file_for_arg (command_line, *filename);
            g_ptr_array_add (file_array, file);
        }
        lypad_application_open (app, (GFile **) file_array->pdata, file_array->len, "");
        g_ptr_array_unref (file_array);
        g_free (remaining_args);
    }

    if (!g_variant_dict_contains (options, "background"))
        lypad_application_activate (app);
    return 0;
}

static void lypad_application_shutdown (GApplication *application)
{
    LypadApplication *app = LYPAD_APPLICATION (application);
    lypad_notes_manager_save_all (app->notes_manager);
    lypad_preferences_save (app->preferences);
    g_object_unref (app->notes_manager);
    g_object_unref (app->preferences);
    G_APPLICATION_CLASS (lypad_application_parent_class)->shutdown (application);
}

static void lypad_application_class_init (LypadApplicationClass *klass)
{
    GApplicationClass *g_app_class = G_APPLICATION_CLASS (klass);

    g_app_class->activate             = lypad_application_activate;
    g_app_class->command_line         = lypad_application_command_line;
    g_app_class->handle_local_options = lypad_application_handle_local_options;
    g_app_class->open                 = lypad_application_open;
    g_app_class->startup              = lypad_application_startup;

    g_app_class->shutdown             = lypad_application_shutdown;
}

LypadApplication *lypad_application_new ()
{
    return g_object_new (LYPAD_TYPE_APPLICATION,
                        "application-id", "org.gnome.lypad",
                        "flags", G_APPLICATION_HANDLES_OPEN | G_APPLICATION_HANDLES_COMMAND_LINE,
                        NULL);
}
