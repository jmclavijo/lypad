/*
 *  lypad-export.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>
#include "lypad-note.h"

G_BEGIN_DECLS

#define LYPAD_TYPE_EXPORT_FORMAT (lypad_export_format_get_type ())
G_DECLARE_INTERFACE (LypadExportFormat, lypad_export_format, LYPAD, EXPORT_FORMAT, GObject)

void lypad_export_format_register (GType type);
void lypad_export_dialog_launch   (GtkWindow *parent, GSList *notes);

struct _LypadExportFormatInterface
{
    GTypeInterface parent_iface;

    const char * (*get_name)      (LypadExportFormat *format);
    const char * (*get_extension) (LypadExportFormat *format);
    const char * (*get_mime_type) (LypadExportFormat *format);
    gboolean     (*export_note)   (LypadExportFormat *format, LypadNote *note, const char *filename);
    gboolean     (*export_notes)  (LypadExportFormat *format, GSList *notes, const char *filename);
};

G_END_DECLS
