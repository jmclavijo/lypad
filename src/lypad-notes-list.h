/*
 *  lypad-notes-list.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include "lypad-note.h"

G_BEGIN_DECLS

#define LYPAD_TYPE_NOTES_LIST (lypad_notes_list_get_type ())
G_DECLARE_FINAL_TYPE (LypadNotesList, lypad_notes_list, LYPAD, NOTES_LIST, GtkListBox)

typedef enum
{
    LYPAD_SORT_MODIFIED = 0,
    LYPAD_SORT_CREATED  = 1,
    LYPAD_SORT_TITLE    = 2
} LypadSortMode;

GtkWidget *lypad_notes_list_new            (void);
void       lypad_notes_list_add            (LypadNotesList *list, GtkWidget *widget);
void       lypad_notes_list_remove_by_note (LypadNotesList *list, LypadNote *note);

void       lypad_notes_list_set_toggle     (LypadNotesList *list, GtkWidget *toggle_button);

G_END_DECLS
