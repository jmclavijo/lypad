/*
 *  lypad-tags-list.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-tags-list.h"
#include "lypad-tag-widget.h"
#include "lypad-button.h"

typedef struct _ListRowData ListRowData;

struct _LypadTagsList
{
    GtkBox      parent;
    gulong      tag_created_handler;
};

G_DEFINE_TYPE (LypadTagsList, lypad_tags_list, GTK_TYPE_LIST_BOX)

enum
{
    SIGNAL_TAG_CLICKED,
    SIGNAL_LAST
};

static guint signals[SIGNAL_LAST] = {0};

struct _ListRowData
{
    LypadTagsList  *parent;
    LypadTag       *tag;
    LypadTagWidget *tag_widget;
    GtkWidget      *color_button;
    gulong          tag_destroy_handler;
    gulong          tag_color_handler;
};

static GQuark data_quark = 0;

static void on_tag_destroy (LypadTag *tag, gpointer user_data)
{
    GtkListBoxRow *row = user_data;
    gtk_widget_destroy (GTK_WIDGET (row));
}

static void list_row_data_free (gpointer user_data)
{
    ListRowData *data = user_data;
    g_object_unref (data->color_button);
    g_signal_handler_disconnect (data->tag, data->tag_color_handler);
    g_signal_handler_disconnect (data->tag, data->tag_destroy_handler);
    g_object_unref (data->tag);
    g_free (data);
}

static int lypad_tags_list_sort_function (GtkListBoxRow *row1, GtkListBoxRow *row2, gpointer user_data)
{
    ListRowData *data1 = g_object_get_qdata (G_OBJECT (row1), data_quark);
    ListRowData *data2 = g_object_get_qdata (G_OBJECT (row2), data_quark);
    return g_ascii_strcasecmp (lypad_tag_get_name (data1->tag), lypad_tag_get_name (data2->tag));
}

static void lypad_tags_list_row_activated (GtkListBox *list_box, GtkListBoxRow *row)
{
    ListRowData *data = g_object_get_qdata (G_OBJECT (row), data_quark);
    g_signal_emit (list_box, signals[SIGNAL_TAG_CLICKED], 0, data->tag);
}

static void on_remove_tag_dialog_response (GtkDialog *dialog, gint response_id, gpointer user_data)
{
    if (response_id == GTK_RESPONSE_OK)
    {
        LypadTag *tag = LYPAD_TAG (user_data);
        lypad_tag_destroy (tag);
    }
    gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void on_row_remove_clicked (GtkButton *button, gpointer user_data)
{
    LypadTag *tag = LYPAD_TAG (user_data);
    GtkWidget *dialog = gtk_message_dialog_new (GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (button))),
                                                GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                                GTK_MESSAGE_WARNING,
                                                GTK_BUTTONS_NONE,
                                                "Are you sure you want to remove the \"%s\" tag?",
                                                lypad_tag_get_name (tag));
    gtk_dialog_add_button (GTK_DIALOG (dialog), "Cancel", GTK_RESPONSE_CANCEL);
    GtkWidget *remove_button = gtk_dialog_add_button (GTK_DIALOG (dialog), "Remove", GTK_RESPONSE_OK);
    GtkStyleContext *context = gtk_widget_get_style_context (remove_button);
    gtk_style_context_add_class (context, GTK_STYLE_CLASS_DESTRUCTIVE_ACTION);
    gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog), "It will be removed from all the notes that use it.");
    g_signal_connect (dialog, "response", G_CALLBACK (on_remove_tag_dialog_response), tag);
    gtk_window_present (GTK_WINDOW (dialog));
}

static void on_row_color_set (GtkColorButton *button, gpointer user_data)
{
    LypadTag *tag = LYPAD_TAG (user_data);
    GdkRGBA color;
    gtk_color_chooser_get_rgba (GTK_COLOR_CHOOSER (button), &color);
    lypad_tag_set_color (tag, &color);
}

static void on_tag_color_changed (LypadTag *tag, GParamSpec *pspec, gpointer user_data)
{
    ListRowData *data = user_data;
    GdkRGBA color;
    lypad_tag_get_color (tag, &color);
    gtk_color_chooser_set_rgba (GTK_COLOR_CHOOSER (data->color_button), &color);
}

static void lypad_tags_list_add (LypadTagsList *list, LypadTag *tag)
{
    ListRowData *data = g_new (ListRowData, 1);
    data->parent = list;
    data->tag = g_object_ref (tag);

    GtkWidget *row = gtk_list_box_row_new ();
    GtkStyleContext *context = gtk_widget_get_style_context (row);
    gtk_style_context_add_class (context, "LypadTagListRow");

    GtkWidget *box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 8);
    GtkWidget *tag_widget = g_object_ref_sink (lypad_tag_widget_new (tag));
    lypad_tag_widget_set_mode (LYPAD_TAG_WIDGET (tag_widget), LYPAD_TAG_FULL_SIZE);
    gtk_box_pack_start (GTK_BOX (box), tag_widget, FALSE, TRUE, 0);

    GtkWidget *remove_button = gtk_button_new_from_icon_name ("edit-delete-symbolic", GTK_ICON_SIZE_BUTTON);
    gtk_box_pack_end (GTK_BOX (box), remove_button, FALSE, TRUE, 0);
    g_signal_connect (remove_button, "clicked", G_CALLBACK (on_row_remove_clicked), tag);

    GdkRGBA color;
    lypad_tag_get_color (tag, &color);
    data->color_button = gtk_color_button_new_with_rgba (&color);
    g_object_ref_sink (data->color_button);
    gtk_box_pack_end (GTK_BOX (box), data->color_button, FALSE, TRUE, 0);
    g_signal_connect (data->color_button, "color-set", G_CALLBACK (on_row_color_set), tag);

    gtk_container_add (GTK_CONTAINER (row), box);

    data->tag_destroy_handler = g_signal_connect (tag, "destroy", G_CALLBACK (on_tag_destroy), row);
    data->tag_color_handler = g_signal_connect (tag, "notify::color", G_CALLBACK (on_tag_color_changed), data);

    g_object_set_qdata_full (G_OBJECT (row), data_quark, data, list_row_data_free);
    gtk_container_add (GTK_CONTAINER (list), row);
    gtk_widget_show_all (row);
}

static void on_tag_created (GObject *signaler, LypadTag *tag, gpointer user_data)
{
    LypadTagsList *list = LYPAD_TAGS_LIST (user_data);
    lypad_tags_list_add (list, tag);
}

static void lypad_tags_list_init (LypadTagsList *list)
{
    gtk_list_box_set_sort_func (GTK_LIST_BOX (list), lypad_tags_list_sort_function, NULL, NULL);

    GSList *tags = lypad_tag_get_all ();
    for (GSList *l = tags; l != NULL; l = l->next)
        lypad_tags_list_add (list, l->data);
    g_slist_free (tags);

    list->tag_created_handler = g_signal_connect (lypad_tag_get_signaler (), "tag-created", G_CALLBACK (on_tag_created), list);
}

static void lypad_tags_list_destroy (GtkWidget *widget)
{
    LypadTagsList *list = LYPAD_TAGS_LIST (widget);
    g_clear_signal_handler (&list->tag_created_handler, lypad_tag_get_signaler ());
    GTK_WIDGET_CLASS (lypad_tags_list_parent_class)->destroy (widget);
}

static void lypad_tags_list_class_init (LypadTagsListClass *klass)
{
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
    GtkListBoxClass *list_class = GTK_LIST_BOX_CLASS (klass);

    widget_class->destroy = lypad_tags_list_destroy;
    list_class->row_activated = lypad_tags_list_row_activated;

    signals[SIGNAL_TAG_CLICKED] = g_signal_new ("tag-clicked", G_TYPE_FROM_CLASS (klass),
                                                 G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                                 g_cclosure_marshal_VOID__OBJECT,
                                                 G_TYPE_NONE, 1, LYPAD_TYPE_TAG);

    data_quark = g_quark_from_string ("lypad-row-data");
}

GtkWidget *lypad_tags_list_new ()
{
    return g_object_new (LYPAD_TYPE_TAGS_LIST, NULL);
}
