/*
 *  lypad-toolbar.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include "lypad-text-buffer.h"

G_BEGIN_DECLS

#define LYPAD_TYPE_TOOLBAR (lypad_toolbar_get_type ())
G_DECLARE_FINAL_TYPE (LypadToolbar, lypad_toolbar, LYPAD, TOOLBAR, GtkRevealer)

LypadToolbar *lypad_toolbar_new              (void);
void          lypad_toolbar_set_buffer       (LypadToolbar *toolbar, LypadTextBuffer *text_buffer);
void          lypad_toolbar_release_buffer   (LypadToolbar *toolbar);
void          lypad_toolbar_set_text_view    (LypadToolbar *toolbar, GtkTextView *text_view);
void          lypad_toolbar_notify_bkg_color (LypadToolbar *toolbar, const GdkRGBA *color);

void          lypad_toolbar_show             (LypadToolbar *toolbar);
void          lypad_toolbar_hide             (LypadToolbar *toolbar);
void          lypad_toolbar_auto_hide        (LypadToolbar *toolbar, double delay); // Set delay to 0 to disable auto hide

G_END_DECLS
