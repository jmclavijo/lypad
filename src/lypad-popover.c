/*
 *  lypad-popover.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-popover.h"

struct _LypadPopover
{
    GtkWindow  parent;
    GtkWidget *relative_to;
    gboolean   expand_width;
};

G_DEFINE_TYPE (LypadPopover, lypad_popover, GTK_TYPE_WINDOW)

enum
{
	SIGNAL_CLOSED,
	SIGNAL_LAST
};

static guint signals[SIGNAL_LAST] = {0};
static GQuark quark_popover = 0;

static void lypad_popover_destroy_unref (LypadPopover *popover)
{
    gtk_widget_destroy (GTK_WIDGET (popover));
    g_object_unref (popover);
}

static void add_popover_to_widget (GtkWidget *widget, LypadPopover *popover)
{
    // Only one popover can be added to a widget
    g_assert (g_object_get_qdata (G_OBJECT (widget), quark_popover) == NULL);
    g_object_set_qdata_full (G_OBJECT (widget), quark_popover, g_object_ref_sink (popover), (GDestroyNotify) lypad_popover_destroy_unref);
}

static void remove_popover_from_widget (GtkWidget *widget, LypadPopover *popover)
{
    g_assert (g_object_get_qdata (G_OBJECT (widget), quark_popover) == popover);
    g_object_set_qdata (G_OBJECT (widget), quark_popover, NULL);
}

void lypad_popover_set_relative_to (LypadPopover *popover, GtkWidget *relative_to)
{
    g_assert (relative_to != NULL);

    if (popover->relative_to)
    {
        remove_popover_from_widget (popover->relative_to, popover);
        gtk_window_group_remove_window (gtk_window_get_group (GTK_WINDOW (popover)), GTK_WINDOW (popover));
    }
    popover->relative_to = relative_to;
    add_popover_to_widget (relative_to, popover);
    gtk_window_set_attached_to (GTK_WINDOW (popover), relative_to);

    GtkWidget *toplevel = gtk_widget_get_toplevel (relative_to);
    if (GTK_IS_WINDOW (toplevel))
    {
        gtk_window_set_transient_for (GTK_WINDOW (popover), GTK_WINDOW (toplevel));
        gtk_window_group_add_window (gtk_window_get_group (GTK_WINDOW (toplevel)), GTK_WINDOW (popover));
    }

    gtk_window_set_screen (GTK_WINDOW (popover), gtk_widget_get_screen (popover->relative_to));
}

void lypad_popover_popup (LypadPopover *popover)
{
    int x, y;
    GtkAllocation allocation;
    GdkWindow *window = gtk_widget_get_window (popover->relative_to);
    if (!window)
        return;

    gtk_widget_get_allocation (popover->relative_to, &allocation);
    gdk_window_get_origin (window, &x, &y);

    x += allocation.x;
    y += allocation.y + allocation.height;

    gtk_window_move (GTK_WINDOW (popover), x, y);
    if (popover->expand_width)
        gtk_window_resize (GTK_WINDOW (popover), allocation.width, 1);

    GTK_WIDGET_CLASS (lypad_popover_parent_class)->show(GTK_WIDGET (popover));

    gtk_grab_add (GTK_WIDGET (popover));
    GdkDevice *device = gtk_get_current_event_device ();
    if (device)
    {
        if (gdk_device_get_source (device) == GDK_SOURCE_KEYBOARD)
            device = gdk_device_get_associated_device (device);

        gdk_seat_grab (gdk_device_get_seat (device),
                       gtk_widget_get_window (GTK_WIDGET (popover)),
                       GDK_SEAT_CAPABILITY_ALL,
                       TRUE, NULL, NULL,
                       NULL, NULL);
    }
    else
        g_warning ("Event emitted without any device");
}

void lypad_popover_set_expand_width (LypadPopover *popover, gboolean expand)
{
    popover->expand_width = expand;
}

void lypad_popover_popdown (LypadPopover *popover)
{
    gtk_widget_hide (GTK_WIDGET (popover));
}

static void lypad_popover_init (LypadPopover *popover)
{
    popover->expand_width = TRUE;
}

static gboolean lypad_popover_key_press (GtkWidget *widget, GdkEventKey *event)
{
    if (event->keyval == GDK_KEY_Escape)
    {
        lypad_popover_popdown (LYPAD_POPOVER (widget));
        return TRUE;
    }
    return GTK_WIDGET_CLASS (lypad_popover_parent_class)->key_press_event (widget, event);
}

static void lypad_popover_show (GtkWidget *widget)
{
}

static void lypad_popover_show_all (GtkWidget *widget)
{
    gtk_widget_show_all (gtk_bin_get_child (GTK_BIN (widget)));
}

static gboolean lypad_popover_button_press (GtkWidget *widget, GdkEventButton *event)
{
    GtkAllocation allocation;
    gint window_x, window_y;
    gdk_window_get_position (gtk_widget_get_window (widget), &window_x, &window_y);
    gtk_widget_get_allocation (widget, &allocation);
    if (event->x_root >= window_x && event->x_root < window_x + allocation.width &&
        event->y_root >= window_y && event->y_root < window_y + allocation.height)
        return GTK_WIDGET_CLASS (lypad_popover_parent_class)->button_press_event (widget, event);

    lypad_popover_popdown (LYPAD_POPOVER (widget));
    return TRUE;
}

static void lypad_popover_hide (GtkWidget *widget)
{
    gtk_grab_remove (widget);
    g_signal_emit (widget, signals[SIGNAL_CLOSED], 0);
    GTK_WIDGET_CLASS (lypad_popover_parent_class)->hide(widget);
}

static void lypad_popover_class_init (LypadPopoverClass *klass)
{
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

    widget_class->key_press_event = lypad_popover_key_press;
    widget_class->button_press_event = lypad_popover_button_press;
    widget_class->show = lypad_popover_show;
    widget_class->show_all = lypad_popover_show_all;
    widget_class->hide = lypad_popover_hide;

	signals[SIGNAL_CLOSED] = g_signal_new ("closed", G_TYPE_FROM_CLASS (klass),
										   G_SIGNAL_RUN_LAST, 0, NULL, NULL,
										   g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

    quark_popover = g_quark_from_string ("lypad-attached-popover");
}

LypadPopover *lypad_popover_new (GtkWidget *relative_to)
{
    LypadPopover *popover = g_object_new (LYPAD_TYPE_POPOVER, "type", GTK_WINDOW_POPUP, "type-hint", GDK_WINDOW_TYPE_HINT_COMBO, NULL);
    lypad_popover_set_relative_to (popover, relative_to);
    return popover;
}
