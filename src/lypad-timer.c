/*
 *  lypad-timer.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-timer.h"

struct _LypadTimer
{
    GObject  parent;
    int      delay;
    int      tolerance;
    GTimer  *g_timer;
    // Signal handlers
    gulong   timeout_handler;
};

G_DEFINE_TYPE (LypadTimer, lypad_timer, G_TYPE_OBJECT)

enum
{
	SIGNAL_TIMEOUT,
	SIGNAL_LAST
};
static guint signals[SIGNAL_LAST] = {0};

static void lypad_timer_init (LypadTimer *timer)
{
    timer->delay = 0;
    timer->tolerance = 0;
    timer->timeout_handler = 0;
    timer->g_timer = g_timer_new ();
}

static gboolean lypad_timer_callback (gpointer user_data)
{
    LypadTimer *timer = user_data;
    g_signal_emit (timer, signals[SIGNAL_TIMEOUT], 0);
    timer->timeout_handler = 0;
    return G_SOURCE_REMOVE;
}

void lypad_timer_set_delay (LypadTimer *timer, int delay, int tolerance)
{
    g_return_if_fail (LYPAD_IS_TIMER (timer));
    timer->delay = delay;
    timer->tolerance = tolerance;
}

void lypad_timer_start (LypadTimer *timer)
{
    g_return_if_fail (LYPAD_IS_TIMER (timer));
    if (timer->timeout_handler)
    {
        double elapsed = g_timer_elapsed (timer->g_timer, NULL);
        if (elapsed < timer->tolerance)
            return;
        g_source_remove (timer->timeout_handler);
    }
    timer->timeout_handler = g_timeout_add_seconds (timer->delay, lypad_timer_callback, timer);
    g_timer_start (timer->g_timer);
}

void lypad_timer_stop (LypadTimer *timer)
{
    g_return_if_fail (LYPAD_IS_TIMER (timer));
    if (timer->timeout_handler)
    {
        g_source_remove (timer->timeout_handler);
        timer->timeout_handler = 0;
    }
}

static void lypad_timer_finalize (GObject *object)
{
    LypadTimer *timer = LYPAD_TIMER (object);
    if (timer->timeout_handler)
        g_source_remove (timer->timeout_handler);
    g_timer_destroy (timer->g_timer);
    G_OBJECT_CLASS (lypad_timer_parent_class)->finalize (object);
}

static void lypad_timer_class_init (LypadTimerClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = lypad_timer_finalize;

	signals[SIGNAL_TIMEOUT] = g_signal_new ("timeout", G_TYPE_FROM_CLASS (klass),
										    G_SIGNAL_RUN_LAST, 0, NULL, NULL,
											g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
}

LypadTimer *lypad_timer_new ()
{
    return g_object_new (LYPAD_TYPE_TIMER, NULL);
}