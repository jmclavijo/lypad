/*
 *  lypad-text-history.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-text-history.h"

typedef struct _TextAction TextAction;
typedef enum _ActionType ActionType;


struct _LypadTextHistory
{
    GObject                parent;
    GList                 *actions;
    GList                 *redo_actions;
    int                    user_action_count;
    int                    lock_count;
    LypadTextHistoryFuncs  funcs;
    GtkTextBuffer         *buffer;
};

enum _ActionType
{
    ACTION_TYPE_SEPARATOR,
    ACTION_TYPE_INSERT_TEXT,
    ACTION_TYPE_DELETE_TEXT,
    ACTION_TYPE_APPLY_TAG,
    ACTION_TYPE_REMOVE_TAG
};

struct _TextAction
{
    ActionType type;
    int start;
    int end;
    union
    {
        char *text;
        GtkTextTag *tag;
    } u;
};

G_DEFINE_TYPE (LypadTextHistory, lypad_text_history, G_TYPE_OBJECT)

static TextAction *action_separator = NULL;

static void text_action_free (gpointer data)
{
    TextAction *action = data;
    if (action == action_separator)
        return;

    switch (action->type)
    {
        case ACTION_TYPE_SEPARATOR:
            break;

        case ACTION_TYPE_INSERT_TEXT:
        case ACTION_TYPE_DELETE_TEXT:
            g_free (action->u.text);
            break;

        case ACTION_TYPE_APPLY_TAG:
        case ACTION_TYPE_REMOVE_TAG:
            g_clear_object (&action->u.tag);
            break;

        default:
            g_warn_if_reached ();
    }
    g_free (action);
}

static void lypad_text_history_clear_redo_actions (LypadTextHistory *history)
{
    if (history->redo_actions)
    {
        g_list_free_full (history->redo_actions, text_action_free);
        history->redo_actions = NULL;
    }
}

void lypad_text_history_clear (LypadTextHistory *history)
{
    lypad_text_history_clear_redo_actions (history);
    if (history->actions)
    {
        g_list_free_full (history->actions, text_action_free);
        history->actions = NULL;
    }
}

gboolean lypad_text_history_can_undo (LypadTextHistory *history)
{
    return history->actions != NULL;
}

gboolean lypad_text_history_can_redo (LypadTextHistory *history)
{
    return history->redo_actions != NULL;
}

static void lypad_history_push (LypadTextHistory *history, TextAction *action)
{
    lypad_text_history_clear_redo_actions (history);
    history->actions = g_list_prepend (history->actions, action);
}

static void lypad_text_history_execute_action (LypadTextHistory *history, TextAction *action)
{
    switch (action->type)
    {
        case ACTION_TYPE_INSERT_TEXT:
        {
            GtkTextIter start;
            gtk_text_buffer_get_iter_at_offset (history->buffer, &start, action->start);
            history->funcs.insert_text (history->buffer, &start, action->u.text, -1);
        } break;

        case ACTION_TYPE_DELETE_TEXT:
        {
            GtkTextIter start, end;
            gtk_text_buffer_get_iter_at_offset (history->buffer, &start, action->start);
            gtk_text_buffer_get_iter_at_offset (history->buffer, &end, action->end);
            history->funcs.delete_text (history->buffer, &start, &end);
        } break;

        case ACTION_TYPE_APPLY_TAG:
        {
            GtkTextIter start, end;
            gtk_text_buffer_get_iter_at_offset (history->buffer, &start, action->start);
            gtk_text_buffer_get_iter_at_offset (history->buffer, &end, action->end);
            history->funcs.apply_tag (history->buffer, action->u.tag, &start, &end);
        } break;

        case ACTION_TYPE_REMOVE_TAG:
        {
            GtkTextIter start, end;
            gtk_text_buffer_get_iter_at_offset (history->buffer, &start, action->start);
            gtk_text_buffer_get_iter_at_offset (history->buffer, &end, action->end);
            history->funcs.remove_tag (history->buffer, action->u.tag, &start, &end);
        } break;

        default:
            g_return_if_reached ();
    }
}

static void lypad_text_history_reverse_action (LypadTextHistory *history, TextAction *action)
{
    TextAction reversed_action = *action;
    switch (action->type)
    {
        case ACTION_TYPE_INSERT_TEXT:
            reversed_action.type = ACTION_TYPE_DELETE_TEXT;
            break;
        case ACTION_TYPE_DELETE_TEXT:
            reversed_action.type = ACTION_TYPE_INSERT_TEXT;
            break;
        case ACTION_TYPE_APPLY_TAG:
            reversed_action.type = ACTION_TYPE_REMOVE_TAG;
            break;
        case ACTION_TYPE_REMOVE_TAG:
            reversed_action.type = ACTION_TYPE_APPLY_TAG;
            break;
        default:
            g_warn_if_reached ();
    }
    lypad_text_history_execute_action (history, &reversed_action);
}

static gpointer g_list_pop (GList **list)
{
    if ((*list))
    {
        gpointer data = (*list)->data;
        (*list) = g_list_delete_link (*list, *list);
        return data;
    }
    else
        return NULL;
}

static void g_list_push (GList **list, gpointer data)
{
    *list = g_list_prepend (*list, data);
}

void lypad_text_history_undo (LypadTextHistory *history)
{
    if (!lypad_text_history_can_undo (history))
        return;

    TextAction *separator = g_list_pop (&history->actions);
    g_warn_if_fail (separator->type == ACTION_TYPE_SEPARATOR);
    text_action_free (separator);

    TextAction *action = g_list_pop (&history->actions);
    do
    {
        lypad_text_history_reverse_action (history, action);
        g_list_push (&history->redo_actions, action);

        action = g_list_pop (&history->actions);
    } while (action && action->type != ACTION_TYPE_SEPARATOR);
    if (action)
        g_list_push (&history->actions, action);
    g_list_push (&history->redo_actions, action_separator);
}

void lypad_text_history_redo (LypadTextHistory *history)
{
    if (!lypad_text_history_can_redo (history))
        return;

    TextAction *separator = g_list_pop (&history->redo_actions);
    g_warn_if_fail (separator->type == ACTION_TYPE_SEPARATOR);
    text_action_free (separator);

    TextAction *action = g_list_pop (&history->redo_actions);
    do
    {
        lypad_text_history_execute_action (history, action);
        g_list_push (&history->actions, action);

        action = g_list_pop (&history->redo_actions);
    } while (action && action->type != ACTION_TYPE_SEPARATOR);
    if (action)
        g_list_push (&history->redo_actions, action);
    g_list_push (&history->actions, action_separator);
}

void lypad_text_history_lock (LypadTextHistory *history)
{
    history->lock_count += 1;
}

void lypad_text_history_unlock (LypadTextHistory *history)
{
    g_return_if_fail (history->lock_count > 0);
    history->lock_count -= 1;
}

void lypad_text_history_begin_user_action (LypadTextHistory *history)
{
    history->user_action_count += 1;
}

void lypad_text_history_end_user_action (LypadTextHistory *history)
{
    g_return_if_fail (history->user_action_count > 0);
    history->user_action_count -= 1;
    if (history->user_action_count == 0 && history->actions && ((TextAction*)(history->actions->data))->type != ACTION_TYPE_SEPARATOR)
        lypad_history_push (history, action_separator);
}

void lypad_text_history_insert_text (LypadTextHistory *history, GtkTextIter *start, GtkTextIter *end, const char *text, int len)
{
    if (history->lock_count > 0)
        return;

    TextAction *action = g_new (TextAction, 1);
    action->type = ACTION_TYPE_INSERT_TEXT;
    action->u.text = g_strndup (text, len);
    action->start = gtk_text_iter_get_offset (start);
    action->end = gtk_text_iter_get_offset (end);
    lypad_history_push (history, action);
    if (history->user_action_count == 0)
        lypad_history_push (history, action_separator);
}

struct TagIterPair
{
    GtkTextTag *tag;
    GtkTextIter iter;
};

void lypad_text_history_delete_text (LypadTextHistory *history, GtkTextIter *start, GtkTextIter *end)
{
    if (history->lock_count > 0)
        return;

    lypad_text_history_begin_user_action (history);

    GtkTextIter iter = *start;
    GSList *tag_list = gtk_text_iter_get_tags (&iter);
    GList *open_tags = NULL;
    for (GSList *l = tag_list; l != NULL; l = l->next)
    {
        struct TagIterPair *item = g_new (struct TagIterPair, 1);
        item->tag = l->data;
        item->iter = iter;
        open_tags = g_list_prepend (open_tags, item);
    }
    g_slist_free (tag_list);

    gtk_text_iter_forward_to_tag_toggle (&iter, NULL);
    while (gtk_text_iter_compare (&iter, end) < 0)
    {
        // Close tags
        GList *l = open_tags;
        while (l != NULL)
        {
            GList *next = l->next;
            struct TagIterPair *item = l->data;
            if (gtk_text_iter_ends_tag (&iter, item->tag))
            {
                lypad_text_history_remove_tag (history, item->tag, &item->iter, &iter);
                open_tags = g_list_delete_link (open_tags, l);
                g_free (item);
            }
            l = next;
        }

        // Append new open tags
        tag_list = gtk_text_iter_get_toggled_tags (&iter, TRUE);
        for (GSList *sl = tag_list; sl != NULL; sl = sl->next)
        {
            struct TagIterPair *item = g_new (struct TagIterPair, 1);
            item->tag = sl->data;
            item->iter = iter;
            open_tags = g_list_prepend (open_tags, item);
        }
        g_slist_free (tag_list);

        gtk_text_iter_forward_to_tag_toggle (&iter, NULL);
    }

    for (GList *l = open_tags; l != NULL; l = l->next)
    {
        struct TagIterPair *item = l->data;
        lypad_text_history_remove_tag (history, item->tag, &item->iter, end);
    }
    g_list_free_full (open_tags, g_free);

    TextAction *action = g_new (TextAction, 1);
    action->type = ACTION_TYPE_DELETE_TEXT;

    action->u.text = gtk_text_buffer_get_text (history->buffer, start, end, TRUE);
    action->start = gtk_text_iter_get_offset (start);
    action->end = gtk_text_iter_get_offset (end);
    lypad_history_push (history, action);

    lypad_text_history_end_user_action (history);
}

void lypad_text_history_apply_tag (LypadTextHistory *history, GtkTextTag *tag, const GtkTextIter *start, const GtkTextIter *end)
{
    if (history->lock_count > 0)
        return;

    TextAction *action = g_new (TextAction, 1);
    action->type = ACTION_TYPE_APPLY_TAG;

    action->u.tag = g_object_ref (tag);
    action->start = gtk_text_iter_get_offset (start);
    action->end = gtk_text_iter_get_offset (end);
    lypad_history_push (history, action);
    if (history->user_action_count == 0)
        lypad_history_push (history, action_separator);
}

void lypad_text_history_remove_tag (LypadTextHistory *history, GtkTextTag *tag, const GtkTextIter *start, const GtkTextIter *end)
{
    if (history->lock_count > 0)
        return;

    TextAction *action = g_new (TextAction, 1);
    action->type = ACTION_TYPE_REMOVE_TAG;

    action->u.tag = g_object_ref (tag);
    action->start = gtk_text_iter_get_offset (start);
    action->end = gtk_text_iter_get_offset (end);
    lypad_history_push (history, action);
    if (history->user_action_count == 0)
        lypad_history_push (history, action_separator);
}

static void lypad_text_history_init (LypadTextHistory *history)
{
    history->actions = NULL;
    history->redo_actions = NULL;
    history->user_action_count = 0;
}

static void lypad_text_history_finalize (GObject *object)
{
    LypadTextHistory *history = LYPAD_TEXT_HISTORY (object);
    lypad_text_history_clear (history);
    G_OBJECT_CLASS (lypad_text_history_parent_class)->finalize (object);
}

static void lypad_text_history_class_init (LypadTextHistoryClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = lypad_text_history_finalize;

    action_separator = g_new (TextAction, 1);
    action_separator->type = ACTION_TYPE_SEPARATOR;
}

LypadTextHistory *lypad_text_history_new (GtkTextBuffer *buffer, LypadTextHistoryFuncs *funcs)
{
    LypadTextHistory *history = g_object_new (LYPAD_TYPE_TEXT_HISTORY, NULL);
    history->buffer = buffer;
    history->funcs = *funcs;
    return history;
}
