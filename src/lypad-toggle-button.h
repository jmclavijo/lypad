/*
 *  lypad-toggle-button.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include "lypad-button.h"

G_BEGIN_DECLS

#define LYPAD_TYPE_TOGGLE_BUTTON (lypad_toggle_button_get_type ())
G_DECLARE_DERIVABLE_TYPE (LypadToggleButton, lypad_toggle_button, LYPAD, TOGGLE_BUTTON, LypadButton)

struct _LypadToggleButtonClass
{
    GtkEventBoxClass parent_class;

    void (* toggled)  (LypadToggleButton *button);
};

GtkWidget *lypad_toggle_button_new        (void);
gboolean   lypad_toggle_button_get_active (LypadToggleButton *button);
void       lypad_toggle_button_set_active (LypadToggleButton *button, gboolean active);
void       lypad_toggle_button_set_images (LypadToggleButton *button, GtkWidget *base_image, GtkWidget *active_image);

G_END_DECLS
