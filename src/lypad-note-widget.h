/*
 *  lypad-note-widget.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include "lypad-button.h"
#include "lypad-note.h"

G_BEGIN_DECLS

#define LYPAD_TYPE_NOTE_WIDGET (lypad_note_widget_get_type ())
G_DECLARE_FINAL_TYPE (LypadNoteWidget, lypad_note_widget, LYPAD, NOTE_WIDGET, LypadButton)

GtkWidget *lypad_note_widget_new      (LypadNote *note);
LypadNote *lypad_note_widget_get_note (LypadNoteWidget *widget);    // [transfer none]

G_END_DECLS
