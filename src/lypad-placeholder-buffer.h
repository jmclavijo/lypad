/*
 *  lypad-placeholder-buffer.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define LYPAD_TYPE_PLACEHOLDER_BUFFER (lypad_placeholder_buffer_get_type ())
G_DECLARE_FINAL_TYPE (LypadPlaceholderBuffer, lypad_placeholder_buffer, LYPAD, PLACEHOLDER_BUFFER, GtkTextBuffer)

GtkTextBuffer *lypad_placeholder_buffer_new              (void);
void           lypad_placeholder_buffer_set_text         (LypadPlaceholderBuffer *buffer, const char *text);
void           lypad_placeholder_buffer_notify_bkg_color (LypadPlaceholderBuffer *buffer, const GdkRGBA *color);

G_END_DECLS
