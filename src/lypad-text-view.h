/*
 *  lypad-text-view.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include "lypad-text-buffer.h"

G_BEGIN_DECLS

#define LYPAD_TYPE_TEXT_VIEW (lypad_text_view_get_type ())
G_DECLARE_DERIVABLE_TYPE (LypadTextView, lypad_text_view, LYPAD, TEXT_VIEW, GtkTextView)

struct _LypadTextViewClass
{
    GtkTextViewClass parent_class;

    void (* undo) (LypadTextView *view);
    void (* redo) (LypadTextView *view);

    void            (* set_buffer)     (LypadTextView *view, GtkTextBuffer *buffer);
    void            (* release_buffer) (LypadTextView *view);
    GtkTextBuffer * (* get_buffer)     (LypadTextView *view);
    void            (* insert_text)    (LypadTextView *view, const char *text);
};

GtkWidget     *lypad_text_view_new                 (void);
void           lypad_text_view_set_buffer          (LypadTextView *text_view, GtkTextBuffer *buffer);
void           lypad_text_view_release_buffer      (LypadTextView *text_view);
GtkTextBuffer *lypad_text_view_get_buffer          (LypadTextView *text_view);
void           lypad_text_view_set_placeholder     (LypadTextView *text_view, const char *placeholder);
void           lypad_text_view_set_context_menu    (LypadTextView *text_view, GtkMenu *menu);
void           lypad_text_view_notify_bkg_color    (LypadTextView *text_view, const GdkRGBA *color);

void           lypad_text_view_set_default_font    (LypadTextView *text_view, const char *family, int size, GdkRGBA *color);

GtkWidget     *_lypad_text_view_build_context_menu (LypadTextView *text_view);

G_END_DECLS
