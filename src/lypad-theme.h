/*
 *  lypad-theme.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include "lypad-note.h"
#include "lypad-toggle-button.h"
#include "lypad-text-view.h"

G_BEGIN_DECLS

#define LYPAD_TYPE_THEME (lypad_theme_get_type ())

G_DECLARE_DERIVABLE_TYPE (LypadTheme, lypad_theme, LYPAD, THEME, GObject)

typedef struct _LayoutData LayoutData;
typedef struct _LayoutDataPrivate LayoutDataPrivate;

struct _LayoutData
{
    GtkWidget         *layout_base;
    LypadTextView     *content_view;
    LypadTextView     *title_view;               // [nullable]
    LypadToggleButton *lock_button;              // [nullable]
    LypadToggleButton *menu_button;              // [nullable]
    GtkWidget         *drag_widget;              // [nullable]
    LayoutDataPrivate *priv;
};

GSList     *lypad_list_themes            (void);                                                   // [transfer container]
LypadTheme *lypad_theme_get_default      (void);                                                   // [transfer none]
void        lypad_theme_register         (GType type);
const char *lypad_theme_get_name         (LypadTheme *theme);
LypadTheme *lypad_theme_get_for_name     (const char *name);
const char *lypad_theme_get_display_name (LypadTheme *theme);
LayoutData *lypad_theme_build_layout     (LypadTheme *theme, LypadNote *note);
void        lypad_theme_destroy_layout   (LypadTheme *theme, LayoutData *layout);
gboolean    lypad_theme_equal            (LypadTheme *theme1, LypadTheme *theme2);

struct _LypadThemeClass
{
    GObjectClass parent_class;

    const char * (*get_name)         (LypadTheme *theme);
    const char * (*get_display_name) (LypadTheme *theme);
    void         (*init_layout)      (LypadTheme *theme, LayoutData *layout, LypadNote *note);
    void         (*destroy_layout)   (LypadTheme *theme, LayoutData *layout);
};

G_END_DECLS
