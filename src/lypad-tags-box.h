/*
 *  lypad-tags-box.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include "lypad-tag-widget.h"

G_BEGIN_DECLS

#define LYPAD_TYPE_TAGS_BOX (lypad_tags_box_get_type ())
G_DECLARE_FINAL_TYPE (LypadTagsBox, lypad_tags_box, LYPAD, TAGS_BOX, GtkContainer)

GtkWidget *lypad_tags_box_new             ();
void       lypad_tags_box_add_tag         (LypadTagsBox *box, LypadTag *tag);
void       lypad_tags_box_remove_tag      (LypadTagsBox *box, LypadTag *tag);
void       lypad_tags_box_clear           (LypadTagsBox *box);
void       lypad_tags_box_set_mode        (LypadTagsBox *box, LypadTagMode mode);
void       lypad_tags_box_set_interactive (LypadTagsBox *box, gboolean interactive);
void       lypad_tags_box_set_spacing     (LypadTagsBox *box, int spacing);

G_END_DECLS
