/*
 *  lypad-theme-miniature.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

typedef struct _LypadTheme LypadTheme;
typedef struct _LypadNote LypadNote;

#define LYPAD_TYPE_THEME_MINIATURE (lypad_theme_miniature_get_type ())
G_DECLARE_FINAL_TYPE (LypadThemeMiniature, lypad_theme_miniature, LYPAD, THEME_MINIATURE, GtkContainer)

GtkWidget *lypad_theme_miniature_new (LypadTheme *theme, LypadNote *note);

G_END_DECLS
