/*
 *  lypad-theme-flat.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


#include "lypad-theme-flat.h"

struct _LypadThemeFlat
{
    LypadTheme parent;
};

G_DEFINE_TYPE (LypadThemeFlat, lypad_theme_flat, LYPAD_TYPE_THEME)

enum
{
    MENU_ITEM_LOCK,
    N_MENU_ITEMS
};

struct _LayoutDataPrivate
{
    LypadNote *note;
    GtkWidget *menu_item[N_MENU_ITEMS];
};

static inline void lypad_theme_flat_release_note (LayoutData *layout);

static void lypad_theme_flat_init (LypadThemeFlat *theme)
{
}

static const char *lypad_theme_flat_get_name (LypadTheme *theme)
{
    static char *name = "flat";
    return name;
}

static const char *lypad_theme_flat_get_display_name (LypadTheme *theme)
{
    static char *display_name = "Flat";
    return display_name;
}

static void on_note_lock_changed (LypadNote *note, gpointer user_data)
{
    LayoutData *layout = user_data;
    if (layout->priv->note)
    {
        gboolean locked = lypad_note_get_locked (layout->priv->note);
        GtkWidget *label = gtk_bin_get_child (GTK_BIN (layout->priv->menu_item[MENU_ITEM_LOCK]));
        gtk_label_set_text_with_mnemonic (GTK_LABEL (label), locked ? "Un_lock note" : "_Lock note");
    }
}

static void on_lock_activate (GtkMenuItem *item, gpointer user_data)
{
    LayoutData *layout = user_data;
    if (layout->priv->note)
    {
        gboolean locked = lypad_note_get_locked (layout->priv->note);
        lypad_note_set_locked (layout->priv->note, !locked);
    }
}

static void lypad_theme_flat_build_context_menu (LayoutData *layout)
{
    GtkWidget *menu = _lypad_text_view_build_context_menu (layout->content_view);
    GtkWidget *menu_item;

    menu_item = gtk_separator_menu_item_new ();
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

    gboolean locked = lypad_note_get_locked (layout->priv->note);
    menu_item = gtk_menu_item_new_with_mnemonic (locked ? "Un_lock note" : "_Lock note");
    layout->priv->menu_item[MENU_ITEM_LOCK] = menu_item;
    g_signal_connect (menu_item, "activate", G_CALLBACK (on_lock_activate), layout);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);


    menu_item = gtk_menu_item_new_with_mnemonic ("_Actions");

    /* Actions submenu */

    GtkWidget *submenu = gtk_menu_new ();
    GtkWidget *submenu_item;

    submenu_item = gtk_menu_item_new_with_mnemonic ("_New note");
    gtk_actionable_set_action_name (GTK_ACTIONABLE (submenu_item), "app.new-note");
    gtk_menu_shell_append (GTK_MENU_SHELL (submenu), submenu_item);

    submenu_item = gtk_menu_item_new_with_mnemonic ("_Open in editor");
    gtk_actionable_set_action_name (GTK_ACTIONABLE (submenu_item), "win.edit");
    gtk_menu_shell_append (GTK_MENU_SHELL (submenu), submenu_item);

    submenu_item = gtk_menu_item_new_with_mnemonic ("_Hide note");
    gtk_actionable_set_action_name (GTK_ACTIONABLE (submenu_item), "win.hide");
    gtk_menu_shell_append (GTK_MENU_SHELL (submenu), submenu_item);

    submenu_item = gtk_menu_item_new_with_mnemonic ("_Delete note");
    gtk_actionable_set_action_name (GTK_ACTIONABLE (submenu_item), "win.delete");
    gtk_menu_shell_append (GTK_MENU_SHELL (submenu), submenu_item);

    /**/
    gtk_menu_item_set_submenu (GTK_MENU_ITEM (menu_item), submenu);
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

    menu_item = gtk_menu_item_new_with_mnemonic ("Note _settings");
    gtk_actionable_set_action_name (GTK_ACTIONABLE (menu_item), "win.settings");
    gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

    gtk_widget_show_all (menu);
    lypad_text_view_set_context_menu (layout->content_view, GTK_MENU (menu));
}

static void on_note_destroy (LypadNote *note, gpointer user_data)
{
    lypad_theme_flat_release_note (user_data);
}

static inline void lypad_theme_flat_release_note (LayoutData *layout)
{
    if (layout->priv->note)
    {
        g_signal_handlers_disconnect_by_func (layout->priv->note, on_note_destroy, layout);
        g_signal_handlers_disconnect_by_func (layout->priv->note, on_note_lock_changed, layout);
        g_object_unref (layout->priv->note);
        layout->priv->note = NULL;
    }
}

static void lypad_theme_flat_init_layout (LypadTheme *theme, LayoutData *layout, LypadNote *note)
{
    layout->layout_base = g_object_ref_sink (gtk_box_new (GTK_ORIENTATION_VERTICAL, 0));
    GtkWidget *scrolled_window = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window), GTK_POLICY_EXTERNAL, GTK_POLICY_AUTOMATIC);
    gtk_widget_set_size_request (scrolled_window, 64, 64);
    gtk_container_add (GTK_CONTAINER (layout->layout_base), scrolled_window);

    layout->content_view = LYPAD_TEXT_VIEW (lypad_text_view_new ());
    lypad_text_view_set_buffer (LYPAD_TEXT_VIEW (layout->content_view), lypad_note_get_content_buffer (note));
    gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (layout->content_view), GTK_WRAP_WORD_CHAR);
    gtk_widget_set_margin_start  (GTK_WIDGET (layout->content_view), 10);
    gtk_widget_set_margin_end    (GTK_WIDGET (layout->content_view), 10);
    gtk_widget_set_margin_top    (GTK_WIDGET (layout->content_view), 10);
    gtk_widget_set_margin_bottom (GTK_WIDGET (layout->content_view), 10);
    gtk_widget_set_hexpand (GTK_WIDGET (layout->content_view), TRUE);
    gtk_widget_set_vexpand (GTK_WIDGET (layout->content_view), TRUE);
    gtk_container_add (GTK_CONTAINER (scrolled_window), GTK_WIDGET (layout->content_view));

    layout->priv = g_new (LayoutDataPrivate, 1);

    layout->priv->note = g_object_ref (note);
    g_signal_connect (note, "destroy", G_CALLBACK (on_note_destroy), layout);
    g_signal_connect (note, "lock-changed", G_CALLBACK (on_note_lock_changed), layout);

    lypad_theme_flat_build_context_menu (layout);
}

static void lypad_theme_flat_destroy_layout (LypadTheme *theme, LayoutData *layout)
{
    g_object_unref (layout->layout_base);
    lypad_theme_flat_release_note (layout);
    g_free (layout->priv);
    layout->priv = NULL;
}

static void lypad_theme_flat_class_init (LypadThemeFlatClass *klass)
{
    LypadThemeClass *theme_class = LYPAD_THEME_CLASS (klass);
    theme_class->get_name         = lypad_theme_flat_get_name;
    theme_class->get_display_name = lypad_theme_flat_get_display_name;
    theme_class->init_layout      = lypad_theme_flat_init_layout;
    theme_class->destroy_layout   = lypad_theme_flat_destroy_layout;
}

LypadTheme *lypad_theme_flat_new (void)
{
    return g_object_new (LYPAD_TYPE_THEME_FLAT, NULL);
}