/*
 *  lypad-theme-default.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


#include "lypad-theme-default.h"

struct _LypadThemeDefault
{
    LypadTheme parent;
};

G_DEFINE_TYPE (LypadThemeDefault, lypad_theme_default, LYPAD_TYPE_THEME)

static void lypad_theme_default_init (LypadThemeDefault *theme)
{
}

static const char *lypad_theme_default_get_name (LypadTheme *theme)
{
    static char *name = "default";
    return name;
}

static const char *lypad_theme_default_get_display_name (LypadTheme *theme)
{
    static char *display_name = "Default";
    return display_name;
}

static void lypad_theme_default_class_init (LypadThemeDefaultClass *klass)
{
    LypadThemeClass *theme_class = LYPAD_THEME_CLASS (klass);
    theme_class->get_name         = lypad_theme_default_get_name;
    theme_class->get_display_name = lypad_theme_default_get_display_name;
}

LypadTheme *lypad_theme_default_new (void)
{
    return g_object_new (LYPAD_TYPE_THEME_DEFAULT, NULL);
}