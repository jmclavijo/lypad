/*
 *  lypad-theme-miniature.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-theme-miniature.h"
#include "src/lypad-theme.h"
#include "src/lypad-note.h"
#include "src/lypad-title-buffer.h"
#include "src/lypad-utils.h"

//FIXME: Inherit from GtkBin

struct _LypadThemeMiniature
{
    GtkContainer    parent;
    LypadNote      *note;
    LypadTheme     *theme;
    gulong          color_changed_handler;
    gulong          note_destroy_handler;

    GdkWindow      *offscreen_window;
    LayoutData     *layout;
    GtkCssProvider *color_provider;
};

G_DEFINE_TYPE (LypadThemeMiniature, lypad_theme_miniature, GTK_TYPE_CONTAINER)

#define MINIATURE_WIDTH   152
#define MINIATURE_HEIGHT  120
#define MINIATURE_SCALE   0.7

/*
GtkWidget *lypad_banner_new_for_theme (LypadTheme *theme, LypadNote *note)
{
    GtkWidget *window = gtk_offscreen_window_new ();
    gtk_window_set_default_size(GTK_WINDOW(window), 400, 300);
    //LayoutData *data = lypad_theme_build_layout (theme, note);
    //gtk_container_add (GTK_CONTAINER (window), data->layout_base);
    gtk_container_add (GTK_CONTAINER (window), gtk_label_new ("Hello"));

    //GtkStyleContext *style_context = gtk_widget_get_style_context (GTK_WIDGET (win->layout_data->layout_base));
    //gtk_style_context_add_provider (style_context, GTK_STYLE_PROVIDER (win->color_provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

    //gtk_widget_show_all (data->layout_base);
    //gtk_widget_show_all (window);
    gtk_widget_show_all (window);
    gtk_widget_realize (window);
    gtk_widget_map (window);
    GdkPixbuf *pixbuf = gtk_offscreen_window_get_pixbuf (GTK_OFFSCREEN_WINDOW (window));
    GtkWidget *banner = gtk_image_new_from_pixbuf (pixbuf);
    return banner;
}
*/

static void lypad_theme_miniature_init (LypadThemeMiniature *miniature)
{
    gtk_widget_set_has_window (GTK_WIDGET (miniature), FALSE);

    miniature->note = NULL;
    miniature->layout = NULL;
    miniature->offscreen_window = NULL;
    miniature->color_provider = gtk_css_provider_new ();
}

static gboolean lypad_theme_miniature_draw (GtkWidget *widget, cairo_t *cr)
{
    LypadThemeMiniature *miniature = LYPAD_THEME_MINIATURE (widget);

    if (gtk_cairo_should_draw_window (cr, gtk_widget_get_window (widget)))
    {
        if (miniature->layout)
        {
            cairo_matrix_t matrix;
            cairo_surface_t *surface = gdk_offscreen_window_get_surface (miniature->offscreen_window);
            cairo_matrix_init (&matrix, MINIATURE_SCALE, 0.0, 0.0, MINIATURE_SCALE, 0.0, 0.0);
            cairo_transform (cr, &matrix);
            cairo_set_source_surface (cr, surface, 0, 0);
            cairo_paint (cr);
        }
    }
    else if (gtk_cairo_should_draw_window (cr, miniature->offscreen_window))
        if (miniature->layout)
            gtk_container_propagate_draw (GTK_CONTAINER (widget), miniature->layout->layout_base, cr);

    return FALSE;
}

static void lypad_theme_miniature_realize (GtkWidget *widget)
{
    LypadThemeMiniature *miniature = LYPAD_THEME_MINIATURE (widget);

    GTK_WIDGET_CLASS (lypad_theme_miniature_parent_class)->realize (widget);

    GtkAllocation allocation;
    GdkWindowAttr attributes;
    gint attributes_mask;

    gtk_widget_get_allocation (widget, &allocation);
    attributes.x = allocation.x;
    attributes.y = allocation.y;
    attributes.width = allocation.width / MINIATURE_SCALE;
    attributes.height = allocation.height / MINIATURE_SCALE;
    attributes.window_type = GDK_WINDOW_OFFSCREEN;
    attributes.event_mask = gtk_widget_get_events (widget);
    attributes.visual = gtk_widget_get_visual (widget);
    attributes.wclass = GDK_INPUT_OUTPUT;
    attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL;

    miniature->offscreen_window = gdk_window_new (gdk_screen_get_root_window (gtk_widget_get_screen (widget)), &attributes, attributes_mask);
    gdk_window_set_user_data (miniature->offscreen_window, widget);

    if (miniature->layout)
        gtk_widget_set_parent_window (miniature->layout->layout_base, miniature->offscreen_window);
    gdk_offscreen_window_set_embedder (miniature->offscreen_window, gtk_widget_get_window (widget));

    gdk_window_show (miniature->offscreen_window);
}

static void lypad_theme_miniature_unrealize (GtkWidget *widget)
{
    LypadThemeMiniature *miniature = LYPAD_THEME_MINIATURE (widget);

    gdk_window_set_user_data (miniature->offscreen_window, NULL);
    gdk_window_destroy (miniature->offscreen_window);
    miniature->offscreen_window = NULL;

    GTK_WIDGET_CLASS (lypad_theme_miniature_parent_class)->unrealize (widget);
}

static void lypad_theme_miniature_get_preferred_width (GtkWidget *widget, gint *minimum, gint *natural)
{
    *minimum = *natural = MINIATURE_WIDTH;
}

static void lypad_theme_miniature_get_preferred_height (GtkWidget *widget, gint *minimum, gint *natural)
{
    *minimum = *natural = MINIATURE_HEIGHT;
}

static void lypad_theme_miniature_size_allocate (GtkWidget *widget, GtkAllocation *allocation)
{
    LypadThemeMiniature *miniature = LYPAD_THEME_MINIATURE (widget);

    if (gtk_widget_get_realized (widget))
        gdk_window_move_resize (miniature->offscreen_window, allocation->x, allocation->y,
                                allocation->width / MINIATURE_SCALE, allocation->height / MINIATURE_SCALE);

    if (miniature->layout && gtk_widget_get_visible (miniature->layout->layout_base))
    {
        GtkAllocation child_allocation;
        child_allocation.x = 0;
        child_allocation.y = 0;
        child_allocation.width = allocation->width / MINIATURE_SCALE;
        child_allocation.height = allocation->height / MINIATURE_SCALE;
        gtk_widget_size_allocate (miniature->layout->layout_base, &child_allocation);
    }

    GTK_WIDGET_CLASS (lypad_theme_miniature_parent_class)->size_allocate (widget, allocation);
}

static gboolean lypad_theme_miniature_damage_event (GtkWidget *widget, GdkEventExpose *event)
{
    gdk_window_invalidate_rect (gtk_widget_get_window (widget), NULL, FALSE);
    return TRUE;
}

static void lypad_theme_miniature_forall (GtkContainer *container, gboolean include_internals, GtkCallback callback, gpointer callback_data)
{
    LypadThemeMiniature *miniature = LYPAD_THEME_MINIATURE (container);
    if (include_internals && miniature->layout)
        callback (miniature->layout->layout_base, callback_data);
}

static GType lypad_theme_miniature_child_type (GtkContainer *container)
{
    return G_TYPE_NONE;
}

static void lypad_theme_miniature_destroy (GtkWidget *widget)
{
    LypadThemeMiniature *miniature = LYPAD_THEME_MINIATURE (widget);
    if (miniature->note)
    {
        g_signal_handler_disconnect (miniature->note, miniature->color_changed_handler);
        g_signal_handler_disconnect (miniature->note, miniature->note_destroy_handler);
        lypad_theme_destroy_layout (miniature->theme, miniature->layout);
        g_object_unref (miniature->note);
        g_object_unref (miniature->theme);
        miniature->note = NULL;
        miniature->theme = NULL;
        miniature->layout = NULL;
    }
    GTK_WIDGET_CLASS (lypad_theme_miniature_parent_class)->destroy (widget);
}

static void lypad_theme_miniature_finalize (GObject *object)
{
    LypadThemeMiniature *miniature = LYPAD_THEME_MINIATURE (object);
    g_clear_object (&miniature->color_provider);
    G_OBJECT_CLASS (lypad_theme_miniature_parent_class)->finalize (object);
}

static void lypad_theme_miniature_class_init (LypadThemeMiniatureClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
    GtkContainerClass *container_class = GTK_CONTAINER_CLASS (klass);

    object_class->finalize             = lypad_theme_miniature_finalize;
    widget_class->destroy              = lypad_theme_miniature_destroy;
    widget_class->realize              = lypad_theme_miniature_realize;
    widget_class->unrealize            = lypad_theme_miniature_unrealize;
    widget_class->get_preferred_width  = lypad_theme_miniature_get_preferred_width;
    widget_class->get_preferred_height = lypad_theme_miniature_get_preferred_height;
    widget_class->size_allocate        = lypad_theme_miniature_size_allocate;
    widget_class->draw                 = lypad_theme_miniature_draw;
    widget_class->damage_event         = lypad_theme_miniature_damage_event;

    container_class->forall            = lypad_theme_miniature_forall;
    container_class->child_type        = lypad_theme_miniature_child_type;

    gtk_container_class_handle_border_width (container_class);
}

static void lypad_theme_miniature_set_color (LypadThemeMiniature *miniature, const GdkRGBA *color)
{
    int color_r = (int) (0.5 + 255. * color->red);
    int color_g = (int) (0.5 + 255. * color->green);
    int color_b = (int) (0.5 + 255. * color->blue);

    char *style_data = lypad_utils_strdup_printf (
        "* {\n"
        "    background-color: rgba(%d,%d,%d,%.3f);\n"
        "}",
        color_r, color_g, color_b, color->alpha
    );
    gtk_css_provider_load_from_data (miniature->color_provider, style_data, -1, NULL);
}

static void on_note_color_changed (LypadNote *note, gpointer user_data)
{
    LypadThemeMiniature *miniature = LYPAD_THEME_MINIATURE (user_data);
    lypad_theme_miniature_set_color (miniature, lypad_note_get_color (miniature->note));
}

static void on_note_destroy (LypadNote *note, gpointer user_data)
{
    gtk_widget_destroy (GTK_WIDGET (user_data));
}

static inline void lypad_theme_miniature_set_theme_and_note (LypadThemeMiniature *miniature, LypadTheme *theme, LypadNote *note)
{
    g_return_if_fail (miniature->note == NULL);
    g_return_if_fail (miniature->theme == NULL);
    miniature->note = g_object_ref (note);
    miniature->theme = g_object_ref (theme);

    miniature->layout = lypad_theme_build_layout (theme, note);
    gtk_widget_set_parent (miniature->layout->layout_base, GTK_WIDGET (miniature));
    if (miniature->offscreen_window)
        gtk_widget_set_parent_window (miniature->layout->layout_base, miniature->offscreen_window);
    gtk_widget_show_all (miniature->layout->layout_base);

    GtkStyleContext *style_context = gtk_widget_get_style_context (miniature->layout->layout_base);
    gtk_style_context_add_provider (style_context, GTK_STYLE_PROVIDER (miniature->color_provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    lypad_theme_miniature_set_color (miniature, lypad_note_get_color (note));

    miniature->color_changed_handler = g_signal_connect (note, "color-changed", G_CALLBACK (on_note_color_changed), miniature);
    miniature->note_destroy_handler = g_signal_connect (note, "destroy", G_CALLBACK (on_note_destroy), miniature);

    if (miniature->layout->title_view)
    {
        LypadTextBuffer *buffer = lypad_title_buffer_new ();
        gtk_text_buffer_set_text (GTK_TEXT_BUFFER (buffer), "Note title", -1);
        lypad_text_view_set_buffer (miniature->layout->title_view, GTK_TEXT_BUFFER (buffer));
        g_object_unref (buffer);
    }
    if (miniature->layout->content_view)
    {
        LypadTextBuffer *buffer = lypad_text_buffer_new ();
        gtk_text_buffer_set_text (GTK_TEXT_BUFFER (buffer), "Dummy note content:\n - Item 1\n - item 2", -1);
        lypad_text_view_set_buffer (miniature->layout->content_view, GTK_TEXT_BUFFER (buffer));
        g_object_unref (buffer);
    }
}

GtkWidget *lypad_theme_miniature_new (LypadTheme *theme, LypadNote *note)
{
    GtkWidget *widget = g_object_new (LYPAD_TYPE_THEME_MINIATURE, NULL);
    lypad_theme_miniature_set_theme_and_note (LYPAD_THEME_MINIATURE (widget), theme, note);
    return widget;
}
