/*
 *  lypad-note-editor.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "lypad-page.h"
#include "lypad-note.h"

G_BEGIN_DECLS

#define LYPAD_TYPE_NOTE_EDITOR (lypad_note_editor_get_type ())
G_DECLARE_FINAL_TYPE (LypadNoteEditor, lypad_note_editor, LYPAD, NOTE_EDITOR, LypadPage);

GtkWidget  *lypad_note_editor_new        (void);
void        lypad_note_editor_edit_notes (LypadNoteEditor *editor, GSList *notes);
GtkWidget  *lypad_note_editor_get_menu   (LypadNoteEditor *editor);

G_END_DECLS
