/*
 *  lypad-list-chooser.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-list-chooser.h"

struct _LypadListChooser
{
    GtkEventBox        parent;
    LypadTextListType  type;
    gunichar           detail;
    GtkWidget         *popover;
    GtkWidget         *toggle;
    GtkWidget         *dropdown;
    gboolean           pressed;
    gboolean           hover;
    gboolean           dropdown_pressed;
};

G_DEFINE_TYPE (LypadListChooser, lypad_list_chooser, GTK_TYPE_EVENT_BOX)

enum
{
    PROP_0,
    PROP_LIST_TYPE,
    N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES] = {NULL};

enum
{
	SIGNAL_LIST_CHANGED,
	SIGNAL_LAST
};

static guint signals[SIGNAL_LAST] = {0};

static void on_dropdown_clicked (GtkButton *button, gpointer user_data)
{
    LypadListChooser *chooser = LYPAD_LIST_CHOOSER (user_data);
    gtk_popover_popup (GTK_POPOVER (chooser->popover));
    chooser->dropdown_pressed = TRUE;
    gtk_widget_set_state_flags (chooser->dropdown, GTK_STATE_FLAG_CHECKED, FALSE);
    gtk_widget_set_state_flags (chooser->dropdown, GTK_STATE_FLAG_PRELIGHT, FALSE);
    gtk_widget_set_state_flags (chooser->toggle, GTK_STATE_FLAG_PRELIGHT, FALSE);
}

static void on_popover_closed (GtkPopover *popover, gpointer user_data)
{
    LypadListChooser *chooser = LYPAD_LIST_CHOOSER (user_data);
    chooser->dropdown_pressed = FALSE;
    gtk_widget_unset_state_flags (chooser->dropdown, GTK_STATE_FLAG_CHECKED);
}

static void lypad_list_chooser_init_popup (LypadListChooser *chooser)
{
    chooser->popover = gtk_popover_new (GTK_WIDGET (chooser->dropdown));
    g_object_ref (chooser->popover);

    GtkWidget *box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 4);

    GtkWidget *auto_button = gtk_button_new ();
    GtkWidget *auto_box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 8);

    GtkWidget *auto_label = gtk_label_new ("Automatic");
    gtk_box_pack_start (GTK_BOX (auto_box), auto_label, FALSE, TRUE, 0);
    gtk_widget_set_halign (auto_box, GTK_ALIGN_CENTER);
    gtk_widget_set_valign (auto_box, GTK_ALIGN_CENTER);

    gtk_container_add (GTK_CONTAINER (auto_button), auto_box);
    gtk_box_pack_start (GTK_BOX (box), auto_button, FALSE, TRUE, 4);

    gtk_container_set_border_width (GTK_CONTAINER (box), 8);
    gtk_container_add (GTK_CONTAINER (chooser->popover), box);
    gtk_widget_show_all (box);

    g_signal_connect (chooser->popover, "closed", G_CALLBACK (on_popover_closed), chooser);
}

void on_button_state_flags_changed (GtkWidget *widget, GtkStateFlags flags, gpointer user_data)
{
    LypadListChooser *chooser = LYPAD_LIST_CHOOSER (user_data);
    GtkStateFlags old_flags = gtk_widget_get_state_flags (widget);
    GtkStateFlags new_flags = old_flags;
    if (chooser->hover || chooser->dropdown_pressed)
        new_flags |= GTK_STATE_FLAG_PRELIGHT;
    else
        new_flags &= ~GTK_STATE_FLAG_PRELIGHT;

    if (widget == GTK_WIDGET (chooser->toggle) || !chooser->hover)
    {
        if (chooser->pressed)
            new_flags |= GTK_STATE_FLAG_CHECKED;
        else
            new_flags &= ~GTK_STATE_FLAG_CHECKED;
    }
    if (widget == GTK_WIDGET (chooser->dropdown) && chooser->dropdown_pressed)
        new_flags |= GTK_STATE_FLAG_CHECKED;

    if (new_flags != old_flags)
        gtk_widget_set_state_flags (widget, new_flags, TRUE);
}

gboolean on_list_toggled (GtkToggleButton *toggle, gpointer user_data)
{
    LypadListChooser *chooser = LYPAD_LIST_CHOOSER (user_data);
    chooser->pressed = gtk_toggle_button_get_active (toggle);

    if (chooser->pressed)
    {
        gtk_widget_set_state_flags (chooser->toggle, GTK_STATE_FLAG_CHECKED, FALSE);
        if (!chooser->hover)
            gtk_widget_set_state_flags (chooser->dropdown, GTK_STATE_FLAG_CHECKED, FALSE);
    }
    else
    {
        gtk_widget_unset_state_flags (chooser->toggle, GTK_STATE_FLAG_CHECKED);
        gtk_widget_unset_state_flags (chooser->dropdown, GTK_STATE_FLAG_CHECKED);
    }

    if (chooser->pressed)
        g_signal_emit (chooser, signals[SIGNAL_LIST_CHANGED], 0, chooser->type);
    else
        g_signal_emit (chooser, signals[SIGNAL_LIST_CHANGED], 0, LYPAD_TEXT_LIST_TYPE_NONE);
    return FALSE;
}

static void lypad_list_chooser_init (LypadListChooser *chooser)
{
    gtk_widget_add_events (GTK_WIDGET (chooser), GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK);
    chooser->detail = 0;
    chooser->pressed = FALSE;
    chooser->hover = FALSE;
}

gunichar lypad_list_chooser_get_bullet (LypadListChooser *chooser)
{
    g_return_val_if_fail (chooser->type == LYPAD_TEXT_LIST_TYPE_BULLET, 0);
    return chooser->detail;
}

static void lypad_list_chooser_constructed (GObject *object)
{
    LypadListChooser *chooser = LYPAD_LIST_CHOOSER (object);

    GtkWidget *box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_style_context_add_class (gtk_widget_get_style_context (box), "linked");
    gtk_container_add (GTK_CONTAINER (chooser), box);

    chooser->toggle = gtk_toggle_button_new ();
    gtk_widget_set_can_focus (chooser->toggle, FALSE);
    GtkWidget *toggle_image = gtk_image_new_from_icon_name (chooser->type == LYPAD_TEXT_LIST_TYPE_BULLET
                                                                ? "view-list"
                                                                : "view-list-ordered",
                                                            GTK_ICON_SIZE_LARGE_TOOLBAR);
    gtk_container_add (GTK_CONTAINER (chooser->toggle), toggle_image);
    gtk_widget_set_name (chooser->toggle, "LypadTextListToggle");
    gtk_button_set_relief (GTK_BUTTON (chooser->toggle), GTK_RELIEF_NONE);
    gtk_box_pack_start (GTK_BOX (box), chooser->toggle, FALSE, FALSE, 0);

    chooser->dropdown = gtk_button_new ();
    gtk_widget_set_can_focus (chooser->dropdown, FALSE);
    GtkWidget *dropdown_image = gtk_image_new_from_icon_name ("pan-down-symbolic", GTK_ICON_SIZE_BUTTON);
    gtk_container_add (GTK_CONTAINER (chooser->dropdown), dropdown_image);
    gtk_widget_set_name (chooser->dropdown, "LypadTextListDropdown");
    gtk_button_set_relief (GTK_BUTTON (chooser->dropdown), GTK_RELIEF_NONE);
    gtk_box_pack_start (GTK_BOX (box), chooser->dropdown, FALSE, FALSE, 0);

    lypad_list_chooser_init_popup (chooser);

    g_signal_connect (chooser->toggle, "state-flags-changed", G_CALLBACK (on_button_state_flags_changed), chooser);
    g_signal_connect (chooser->dropdown, "state-flags-changed", G_CALLBACK (on_button_state_flags_changed), chooser);

    g_signal_connect (chooser->toggle, "toggled", G_CALLBACK (on_list_toggled), chooser);
    g_signal_connect (chooser->dropdown, "clicked", G_CALLBACK (on_dropdown_clicked), chooser);

    G_OBJECT_CLASS (lypad_list_chooser_parent_class)->constructed (object);
}

gboolean lypad_list_chooser_enter_notify (GtkWidget *widget, GdkEventCrossing *event)
{
    LypadListChooser *chooser = LYPAD_LIST_CHOOSER (widget);
    chooser->hover = TRUE;
    gtk_widget_set_state_flags (chooser->toggle, GTK_STATE_FLAG_PRELIGHT, FALSE);
    gtk_widget_set_state_flags (chooser->dropdown, GTK_STATE_FLAG_PRELIGHT, FALSE);
    gtk_widget_unset_state_flags (chooser->dropdown, GTK_STATE_FLAG_CHECKED);
    return FALSE;
}

gboolean lypad_list_chooser_leave_notify (GtkWidget *widget, GdkEventCrossing *event)
{
    LypadListChooser *chooser = LYPAD_LIST_CHOOSER (widget);
    chooser->hover = FALSE;
    gtk_widget_unset_state_flags (chooser->toggle, GTK_STATE_FLAG_PRELIGHT);
    gtk_widget_unset_state_flags (chooser->dropdown, GTK_STATE_FLAG_PRELIGHT);
    if (chooser->pressed)
        gtk_widget_set_state_flags (chooser->dropdown, GTK_STATE_FLAG_CHECKED, FALSE);
    return FALSE;
}

static void lypad_list_chooser_dispose (GObject *object)
{
    LypadListChooser *chooser = LYPAD_LIST_CHOOSER (object);
    if (chooser->popover)
    {
        gtk_widget_destroy (chooser->popover);
        g_object_unref (chooser->popover);
        chooser->popover = NULL;
    }
    G_OBJECT_CLASS (lypad_list_chooser_parent_class)->dispose (object);
}

void lypad_list_chooser_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
    LypadListChooser *chooser = LYPAD_LIST_CHOOSER (object);
    switch (prop_id)
    {
        case PROP_LIST_TYPE:
            chooser->type = g_value_get_int (value);
            break;
    }
}

static void lypad_list_chooser_class_init (LypadListChooserClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
    object_class->constructed  = lypad_list_chooser_constructed;
    object_class->dispose      = lypad_list_chooser_dispose;
    object_class->set_property = lypad_list_chooser_set_property;
    widget_class->enter_notify_event = lypad_list_chooser_enter_notify;
    widget_class->leave_notify_event = lypad_list_chooser_leave_notify;

    properties[PROP_LIST_TYPE] = g_param_spec_int ("list-type", "List type", "The type of list button",
                                                   LYPAD_TEXT_LIST_TYPE_BULLET, LYPAD_TEXT_LIST_TYPE_ENUM, LYPAD_TEXT_LIST_TYPE_BULLET,
                                                   G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);

    g_object_class_install_properties (object_class, N_PROPERTIES, properties);

	signals[SIGNAL_LIST_CHANGED] = g_signal_new ("list-changed", G_TYPE_FROM_CLASS (klass),
											     G_SIGNAL_RUN_LAST, 0, NULL, NULL,
											     g_cclosure_marshal_VOID__INT, G_TYPE_NONE, 1, G_TYPE_INT /* list type */);
}

GtkWidget *lypad_list_chooser_new (LypadTextListType type)
{
    return g_object_new (LYPAD_TYPE_LIST_CHOOSER, "list-type", type, NULL);
}
