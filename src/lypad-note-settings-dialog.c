/*
 *  lypad-note-settings-dialog.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-note-settings-dialog.h"
#include "lypad-theme.h"
#include "themes/lypad-theme-miniature.h"
#include "color-selection/lypad-color-utils.h"

struct _LypadNoteSettingsDialog
{
    GtkDialog  parent;
    LypadNote *note;
    GtkWidget *theme_container;
    GSList    *theme_widgets;
    GtkWidget *bg_color_chooser;
    GtkWidget *font_chooser;
    GtkWidget *fg_color_chooser;
    GtkWidget *fg_color_automatic;
    GtkWidget *fg_color_custom;
    gulong     bg_color_chooser_changed_handler;
    gulong     note_destroy_handler;
};

G_DEFINE_TYPE (LypadNoteSettingsDialog, lypad_note_settings_dialog, GTK_TYPE_DIALOG)

static void on_bg_color_set (GtkColorButton *button, gpointer user_data)
{
    LypadNoteSettingsDialog *dialog = LYPAD_NOTE_SETTINGS_DIALOG (user_data);
    GdkRGBA color;
    g_return_if_fail (dialog->note != NULL);

    gtk_color_chooser_get_rgba (GTK_COLOR_CHOOSER (button), &color);
    lypad_note_set_color (dialog->note, &color);
}

static void on_font_set (GtkFontButton *button, gpointer user_data)
{
    LypadNoteSettingsDialog *dialog = LYPAD_NOTE_SETTINGS_DIALOG (user_data);
    g_return_if_fail (dialog->note != NULL);

    int size = gtk_font_chooser_get_font_size (GTK_FONT_CHOOSER (button));
    if (size != -1)
        size = (int) ((double) size / (double) PANGO_SCALE + 0.5);

    const char *font;
    PangoFontFamily *family = gtk_font_chooser_get_font_family (GTK_FONT_CHOOSER (button));
    if (family)
        font = pango_font_family_get_name (family);
    else
        font = NULL;

    GtkTextIter start, end;
    GtkTextBuffer *buffer1 = lypad_note_get_content_buffer (dialog->note);
    GtkTextBuffer *buffer2 = lypad_note_get_title_buffer (dialog->note);
    gtk_text_buffer_get_bounds (buffer1, &start, &end);
    if (font)
        lypad_text_buffer_set_font_full (LYPAD_TEXT_BUFFER (buffer1), font, &start, &end);
    if (size != -1)
        lypad_text_buffer_set_size_full (LYPAD_TEXT_BUFFER (buffer1), size, &start, &end);
    gtk_text_buffer_get_bounds (buffer2, &start, &end);
    if (font)
        lypad_text_buffer_set_font_full (LYPAD_TEXT_BUFFER (buffer2), font, &start, &end);
    if (size != -1)
        lypad_text_buffer_set_size_full (LYPAD_TEXT_BUFFER (buffer2), size, &start, &end);
}

static void lypad_note_setting_dialog_set_fg_color (LypadNoteSettingsDialog *dialog, const GdkRGBA *color)
{
    GtkTextIter start, end;
    GtkTextBuffer *buffer1 = lypad_note_get_content_buffer (dialog->note);
    GtkTextBuffer *buffer2 = lypad_note_get_title_buffer (dialog->note);
    gtk_text_buffer_get_bounds (buffer1, &start, &end);
    lypad_text_buffer_set_color_full (LYPAD_TEXT_BUFFER (buffer1), color, &start, &end);
    gtk_text_buffer_get_bounds (buffer2, &start, &end);
    lypad_text_buffer_set_color_full (LYPAD_TEXT_BUFFER (buffer2), color, &start, &end);
}

static void on_fg_color_toggled (GtkToggleButton *button, gpointer user_data)
{
    LypadNoteSettingsDialog *dialog = LYPAD_NOTE_SETTINGS_DIALOG (user_data);
    if (button == GTK_TOGGLE_BUTTON (dialog->fg_color_automatic))
    {
        if (gtk_toggle_button_get_active (button))
        {
            lypad_note_setting_dialog_set_fg_color (dialog, &LYPAD_AUTOMATIC_TEXT_COLOR);
            gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->fg_color_custom), FALSE);
        }
        else if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->fg_color_custom)))
            gtk_toggle_button_set_active (button, TRUE);
    }
    else if (button == GTK_TOGGLE_BUTTON (dialog->fg_color_custom))
    {
        if (gtk_toggle_button_get_active (button))
        {
            GdkRGBA color;
            gtk_color_chooser_get_rgba (GTK_COLOR_CHOOSER (dialog->fg_color_chooser), &color);
            gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->fg_color_automatic), FALSE);
            lypad_note_setting_dialog_set_fg_color (dialog, &color);
        }
        else if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->fg_color_automatic)))
            gtk_toggle_button_set_active (button, TRUE);
    }
}

static void on_fg_color_set (GtkColorButton *button, gpointer user_data)
{
    LypadNoteSettingsDialog *dialog = LYPAD_NOTE_SETTINGS_DIALOG (user_data);
    g_return_if_fail (dialog->note != NULL);

    GdkRGBA color;
    gtk_color_chooser_get_rgba (GTK_COLOR_CHOOSER (button), &color);
    lypad_note_setting_dialog_set_fg_color (dialog, &color);

    g_signal_handlers_block_by_func (button, on_fg_color_toggled, user_data);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->fg_color_custom), TRUE);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->fg_color_automatic), FALSE);
    g_signal_handlers_unblock_by_func (button, on_fg_color_toggled, user_data);
}

static void lypad_note_settings_dialog_init (LypadNoteSettingsDialog *dialog)
{
    gtk_widget_init_template (GTK_WIDGET (dialog));

    dialog->note = NULL;
    dialog->theme_widgets = NULL;
    dialog->bg_color_chooser_changed_handler = 0;
    dialog->note_destroy_handler = 0;

    gtk_container_set_border_width (GTK_CONTAINER (gtk_dialog_get_content_area (GTK_DIALOG (dialog))), 0);

    g_signal_connect (dialog->bg_color_chooser, "color-set", G_CALLBACK (on_bg_color_set), dialog);
    g_signal_connect (dialog->font_chooser, "font-set", G_CALLBACK (on_font_set), dialog);
    g_signal_connect (dialog->fg_color_chooser, "color-set", G_CALLBACK (on_fg_color_set), dialog);
    g_signal_connect (dialog->fg_color_automatic, "toggled", G_CALLBACK (on_fg_color_toggled), dialog);
    g_signal_connect (dialog->fg_color_custom, "toggled", G_CALLBACK (on_fg_color_toggled), dialog);
}

static void lypad_note_settings_dialog_destroy (GtkWidget *widget)
{
    LypadNoteSettingsDialog *dialog = LYPAD_NOTE_SETTINGS_DIALOG (widget);
    if (dialog->note)
    {
        g_signal_handler_disconnect (dialog->note, dialog->bg_color_chooser_changed_handler);
        g_signal_handler_disconnect (dialog->note, dialog->note_destroy_handler);
        g_object_unref (dialog->note);
        dialog->note = NULL;
    }
    if (dialog->theme_widgets)
    {
        g_slist_free_full (dialog->theme_widgets, g_object_unref);
        dialog->theme_widgets = NULL;
    }
    GTK_WIDGET_CLASS (lypad_note_settings_dialog_parent_class)->destroy (widget);
}

static void on_theme_apply_clicked (GtkButton *button, gpointer user_data)
{
    LypadNoteSettingsDialog *dialog = LYPAD_NOTE_SETTINGS_DIALOG (user_data);
    for (GSList *l = dialog->theme_widgets; l != NULL; l = l->next)
    {
        if (gtk_toggle_button_get_active (l->data))
        {
            LypadTheme *theme = g_object_get_data (l->data, "lypad-theme");
            lypad_note_set_theme (dialog->note, theme);
            break;
        }
    }
}

static void lypad_note_settings_dialog_class_init (LypadNoteSettingsDialogClass *klass)
{
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

    widget_class->destroy = lypad_note_settings_dialog_destroy;

    gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/lypad/ui/lypad-note-settings-dialog.glade");
    gtk_widget_class_bind_template_child (widget_class, LypadNoteSettingsDialog, bg_color_chooser);
    gtk_widget_class_bind_template_child (widget_class, LypadNoteSettingsDialog, font_chooser);
    gtk_widget_class_bind_template_child (widget_class, LypadNoteSettingsDialog, fg_color_chooser);
    gtk_widget_class_bind_template_child (widget_class, LypadNoteSettingsDialog, fg_color_automatic);
    gtk_widget_class_bind_template_child (widget_class, LypadNoteSettingsDialog, fg_color_custom);
    gtk_widget_class_bind_template_child (widget_class, LypadNoteSettingsDialog, theme_container);
    gtk_widget_class_bind_template_callback (widget_class, on_theme_apply_clicked);
}

static void on_note_color_changed (LypadNote *note, gpointer user_data)
{
    LypadNoteSettingsDialog *dialog = LYPAD_NOTE_SETTINGS_DIALOG (user_data);
    gtk_color_chooser_set_rgba (GTK_COLOR_CHOOSER (dialog->bg_color_chooser), lypad_note_get_color (note));
}

static void on_note_destroy (LypadNote *note, gpointer user_data)
{
    gtk_widget_destroy (GTK_WIDGET (user_data));
}

static inline void lypad_note_settings_dialog_set_note (LypadNoteSettingsDialog *dialog, LypadNote *note)
{
    g_return_if_fail (dialog->note == NULL);
    dialog->note = g_object_ref (note);

    gtk_color_chooser_set_rgba (GTK_COLOR_CHOOSER (dialog->bg_color_chooser), lypad_note_get_color (note));

    GtkTextBuffer *buffer = lypad_note_get_content_buffer (dialog->note);
    const GdkRGBA *color = lypad_text_buffer_get_insert_color (LYPAD_TEXT_BUFFER (buffer));
    if (color && !gdk_rgba_equal (color, &LYPAD_AUTOMATIC_TEXT_COLOR))
        gtk_color_chooser_set_rgba (GTK_COLOR_CHOOSER (dialog->fg_color_chooser), color);

    PangoFontDescription *font_desc = pango_font_description_new ();
    const char *font = lypad_text_buffer_get_insert_font (LYPAD_TEXT_BUFFER (buffer));
    if (font)
        pango_font_description_set_family (font_desc, font);
    int size = lypad_text_buffer_get_insert_size (LYPAD_TEXT_BUFFER (buffer));
    if (size)
        pango_font_description_set_size (font_desc, size * PANGO_SCALE);
    gtk_font_chooser_set_font_desc (GTK_FONT_CHOOSER (dialog->font_chooser), font_desc);
    pango_font_description_free (font_desc);

    dialog->bg_color_chooser_changed_handler = g_signal_connect (note, "color-changed", G_CALLBACK (on_note_color_changed), dialog);
    dialog->note_destroy_handler = g_signal_connect (note, "destroy", G_CALLBACK (on_note_destroy), dialog);

    // Themes:
    GSList *themes = lypad_list_themes ();
    LypadTheme *note_theme = lypad_note_get_theme (note);

    GtkRadioButton *radio_button = NULL;
    int i = 0;
    for (GSList *l = themes; l != NULL; l = l->next, ++i)
    {
        GtkWidget *miniature = lypad_theme_miniature_new (l->data, dialog->note);
        gtk_widget_set_margin_top (GTK_WIDGET (miniature), 4);
        gtk_widget_set_margin_start (GTK_WIDGET (miniature), 4);
        gtk_widget_set_margin_end (GTK_WIDGET (miniature), 4);
        GtkWidget *button = gtk_radio_button_new_from_widget (radio_button);
        gtk_toggle_button_set_mode (GTK_TOGGLE_BUTTON (button), FALSE);
        GtkWidget *box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 8);
        GtkWidget *label = gtk_label_new (lypad_theme_get_display_name (l->data));
        gtk_box_pack_start (GTK_BOX (box), miniature, FALSE, TRUE, 0);
        gtk_box_pack_start (GTK_BOX (box), label, FALSE, TRUE, 0);
        gtk_container_add (GTK_CONTAINER (button), box);
        gtk_widget_set_halign (button, GTK_ALIGN_CENTER);
        gtk_widget_set_valign (button, GTK_ALIGN_CENTER);
        gtk_grid_attach (GTK_GRID (dialog->theme_container), button, i % 2, i / 2, 1, 1);
        radio_button = GTK_RADIO_BUTTON (button);
        dialog->theme_widgets = g_slist_append (dialog->theme_widgets, g_object_ref (button));
        g_object_set_data_full (G_OBJECT (button), "lypad-theme", g_object_ref (l->data), g_object_unref);

        if (lypad_theme_equal (l->data, note_theme))
            gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), TRUE);
    }
    g_slist_free (themes);
    gtk_widget_show_all (dialog->theme_container);
}

GtkWidget *lypad_note_settings_dialog_new (GtkWindow *parent, LypadNote *note)
{
    GtkWidget *dialog = g_object_new (LYPAD_TYPE_NOTE_SETTINGS_DIALOG, "transient-for", parent, NULL);
    lypad_note_settings_dialog_set_note (LYPAD_NOTE_SETTINGS_DIALOG (dialog), note);
    return dialog;
}
