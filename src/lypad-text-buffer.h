/*
 *  lypad-text-buffer.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define LYPAD_TYPE_TEXT_BUFFER (lypad_text_buffer_get_type ())
G_DECLARE_DERIVABLE_TYPE (LypadTextBuffer, lypad_text_buffer, LYPAD, TEXT_BUFFER, GtkTextBuffer)

enum
{
    TAG_TYPE_0,
    TAG_TYPE_FONT,
    TAG_TYPE_SIZE,
    TAG_TYPE_COLOR,
    TAG_TYPE_TOGGLE,
    TAG_TYPE_LIST
};

typedef enum
{
    BOLD_TAG,
    ITALIC_TAG,
    UNDERLINE_TAG,
    STRIKETHROUGH_TAG,
    LYPAD_N_TOGGLE_TAGS
    //TODO: Add subscript and superscript
} LypadToggleTag;

typedef enum
{
    LYPAD_TEXT_LIST_TYPE_NONE,
    LYPAD_TEXT_LIST_TYPE_BULLET,
    LYPAD_TEXT_LIST_TYPE_ENUM,
    LYPAD_TEXT_LIST_TYPE_CHECK
} LypadTextListType;

typedef struct _LypadTextListInfo LypadTextListInfo;

LypadTextListType lypad_text_list_info_get_type      (LypadTextListInfo *info);

struct _LypadTextBufferClass
{
    GtkTextBufferClass parent_class;
};

LypadTextBuffer  *lypad_text_buffer_new              (void);
void              lypad_text_buffer_set_font         (LypadTextBuffer *buffer, const char *font);
void              lypad_text_buffer_set_size         (LypadTextBuffer *buffer, int size);
void              lypad_text_buffer_set_color        (LypadTextBuffer *buffer, const GdkRGBA *color);
void              lypad_text_buffer_set_toggle_tag   (LypadTextBuffer *buffer, LypadToggleTag tag, gboolean enable);

void              lypad_text_buffer_unset_list       (LypadTextBuffer *buffer);
void              lypad_text_buffer_set_bullet       (LypadTextBuffer *buffer, gunichar bullet);
void              lypad_text_buffer_set_enum         (LypadTextBuffer *buffer, char enum_type);

gboolean          lypad_text_buffer_can_undo         (LypadTextBuffer *buffer);
gboolean          lypad_text_buffer_can_redo         (LypadTextBuffer *buffer);
void              lypad_text_buffer_undo             (LypadTextBuffer *buffer);
void              lypad_text_buffer_redo             (LypadTextBuffer *buffer);

char             *lypad_text_buffer_serialize        (LypadTextBuffer *buffer, gsize *length);
gboolean          lypad_text_buffer_deserialize      (LypadTextBuffer *buffer, const char *data, gsize length);

void              lypad_text_buffer_clear_selection  (LypadTextBuffer *buffer);
void              lypad_text_buffer_set_edit_mode    (LypadTextBuffer *buffer, gboolean value);
void              lypad_text_buffer_notify_bkg_color (LypadTextBuffer *buffer, const GdkRGBA *color);

// To be called i n the "style-changed" signal handler
const char       *lypad_text_buffer_get_font         (LypadTextBuffer *buffer);
int               lypad_text_buffer_get_size         (LypadTextBuffer *buffer);
const GdkRGBA    *lypad_text_buffer_get_color        (LypadTextBuffer *buffer);
gboolean          lypad_text_buffer_get_toggle_tag   (LypadTextBuffer *buffer, LypadToggleTag tag);

const char       *lypad_text_buffer_get_insert_font  (LypadTextBuffer *buffer);
int               lypad_text_buffer_get_insert_size  (LypadTextBuffer *buffer);
const GdkRGBA    *lypad_text_buffer_get_insert_color (LypadTextBuffer *buffer);

void              lypad_text_buffer_set_font_full    (LypadTextBuffer *buffer, const char *font, GtkTextIter *start, GtkTextIter *end);
void              lypad_text_buffer_set_size_full    (LypadTextBuffer *buffer, int size, GtkTextIter *start, GtkTextIter *end);
void              lypad_text_buffer_set_color_full   (LypadTextBuffer *buffer, const GdkRGBA *color, GtkTextIter *start, GtkTextIter *end);

G_END_DECLS
