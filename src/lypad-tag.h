/*
 *  lypad-tag.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

typedef struct _LypadNote LypadNote;

#define LYPAD_TYPE_TAG (lypad_tag_get_type ())
G_DECLARE_FINAL_TYPE (LypadTag, lypad_tag, LYPAD, TAG, GObject)

LypadTag   *lypad_tag_new           (const char *name);
LypadTag   *lypad_tag_find          (const char *name);                        // [transfer none]
void        lypad_tag_destroy       (LypadTag *tag);

LypadTag   *lypad_tag_ref_by_note   (LypadTag *tag, LypadNote *note);
void        lypad_tag_unref_by_note (LypadTag *tag, LypadNote *note);

void        lypad_tag_set_color     (LypadTag *tag, GdkRGBA *color);
void        lypad_tag_get_color     (LypadTag *tag, GdkRGBA *color);
const char *lypad_tag_get_name      (LypadTag *tag);

char       *lypad_tag_serialize     (LypadTag *tag);                           // [transfer full]
LypadTag   *lypad_tag_parse         (const char *str);                         // [transfer full]

GSList     *lypad_tag_get_all       ();                                        // [transfer container]

GObject    *lypad_tag_get_signaler  ();

G_END_DECLS
