/*
 *  lypad-note-widget.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-note-widget.h"
#include "lypad-toggle-button.h"
#include "lypad-text-view.h"
#include "lypad-title-view.h"
#include "lypad-utils.h"
#include "lypad-tags-box.h"
#include "lypad-tag-widget.h"
#include "color-selection/lypad-color-utils.h"

struct _LypadNoteWidget
{
    LypadButton     parent;
    GtkWidget      *main_box;
    GtkWidget      *pin_toggle;
    GtkWidget      *title_view;
    GtkWidget      *content_view;
    LypadNote      *note;
    GtkCssProvider *color_provider;
    GtkLabel       *date;
    GtkWidget      *header_box;
    GtkWidget      *tags_box;

    gulong          tags_changed_handler;
    gulong          color_changed_handler;
    gulong          date_changed_handler;
    gulong          note_destroy_handler;
    gulong          note_pin_toggled_handler;
};

G_DEFINE_TYPE (LypadNoteWidget, lypad_note_widget, LYPAD_TYPE_BUTTON)

static void lypad_note_widget_set_color (LypadNoteWidget *widget, const GdkRGBA *color)
{
    GdkRGBA fg_color;
    lypad_color_utils_get_text_color_for_background (color, &fg_color);

    int color_r = (int) (0.5 + 255. * color->red);
    int color_g = (int) (0.5 + 255. * color->green);
    int color_b = (int) (0.5 + 255. * color->blue);

    int fg_r = (int) (0.5 + 255. * fg_color.red);
    int fg_g = (int) (0.5 + 255. * fg_color.green);
    int fg_b = (int) (0.5 + 255. * fg_color.blue);

    char *style_data = lypad_utils_strdup_printf (
        "box {\n"
        "    background-color: rgba(%d,%d,%d,%.3f);\n"
        "    border-color: rgb(%d,%d,%d);\n"
        "    color: rgb(%d,%d,%d);\n"
        "}\n"
        ".LypadButton:active > box, .LypadButton:checked > box {\n"
        "    background-color: rgba(%d,%d,%d,%.3f);\n"
        "}\n"
        ".LypadToggleButton {\n"
        "    border-style: solid;\n"
        "    border-width: 1px;\n"
        "    border-radius: 6px;\n"
        "    background-color: rgba(0,0,0,0.1);\n"
        "    border-color: rgb(150,150,150);\n"
        "}\n"
        ".LypadToggleButton:checked {\n"
        "    background-color: rgba(250,250,250,0.7);\n"
        "    border-color: rgb(55,158,256);\n"
        "}\n"
        ".LypadDateLabel {\n"
        "    color: rgba(%d, %d, %d, 0.7);\n"
        "}\n",
        color_r, color_g, color_b, color->alpha,
        (int) (0.8 * color_r),
        (int) (0.8 * color_g),
        (int) (0.8 * color_b),

        fg_r, fg_g, fg_b,

        (int) (0.9 * color_r),
        (int) (0.9 * color_g),
        (int) (0.9 * color_b),
        color->alpha,

        fg_r, fg_g, fg_b
    );
    gtk_css_provider_load_from_data (widget->color_provider, style_data, -1, NULL);
}

void on_pin_toggled (LypadToggleButton *button, gboolean active, gpointer user_data)
{
    LypadNoteWidget *widget = LYPAD_NOTE_WIDGET (user_data);
    g_return_if_fail (widget->note);
    GtkWidget *toplevel = gtk_widget_get_toplevel (GTK_WIDGET (widget));
    if (!GTK_IS_WINDOW (toplevel))
        toplevel = NULL;
    lypad_note_set_show_on_desktop (widget->note, active, GTK_WINDOW (toplevel));
}

static void lypad_note_widget_init (LypadNoteWidget *widget)
{
    gtk_widget_init_template (GTK_WIDGET (widget));
    widget->note = NULL;

    widget->color_provider = gtk_css_provider_new ();
    GtkStyleContext *style_context = gtk_widget_get_style_context (GTK_WIDGET (widget->main_box));
    gtk_style_context_add_provider (style_context, GTK_STYLE_PROVIDER (widget->color_provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

    style_context = gtk_widget_get_style_context (GTK_WIDGET (widget->date));
    gtk_style_context_add_provider (style_context, GTK_STYLE_PROVIDER (widget->color_provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

    GtkWidget *image_on = gtk_image_new_from_icon_name ("lypad-pin-on", GTK_ICON_SIZE_BUTTON);
    GtkWidget *image_off = gtk_image_new_from_icon_name ("lypad-pin-off-symbolic", GTK_ICON_SIZE_BUTTON);
    lypad_toggle_button_set_images (LYPAD_TOGGLE_BUTTON (widget->pin_toggle), image_off, image_on);

    style_context = gtk_widget_get_style_context (widget->pin_toggle);
    gtk_style_context_add_provider (style_context, GTK_STYLE_PROVIDER (widget->color_provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

    lypad_text_view_set_placeholder (LYPAD_TEXT_VIEW (widget->title_view), "(Untitled)");
    lypad_text_view_set_placeholder (LYPAD_TEXT_VIEW (widget->content_view), "(Empty)");

    widget->tags_box = lypad_tags_box_new ();
    lypad_tags_box_set_mode (LYPAD_TAGS_BOX (widget->tags_box), LYPAD_TAG_MINIMAL_SIZE);
    gtk_box_pack_end (GTK_BOX (widget->header_box), widget->tags_box, FALSE, FALSE, 0);
    gtk_widget_show (widget->tags_box);

    g_signal_connect (widget->pin_toggle, "toggled", G_CALLBACK (on_pin_toggled), widget);

    widget->tags_changed_handler = 0;
    widget->color_changed_handler = 0;
    widget->date_changed_handler = 0;
    widget->note_destroy_handler = 0;
    widget->note_pin_toggled_handler = 0;
}

static void on_note_color_changed (LypadNote *note, gpointer user_data)
{
    LypadNoteWidget *widget = LYPAD_NOTE_WIDGET (user_data);
    const GdkRGBA *color = lypad_note_get_color (note);
    lypad_note_widget_set_color (widget, color);
    lypad_text_view_notify_bkg_color (LYPAD_TEXT_VIEW (widget->title_view), color);
    lypad_text_view_notify_bkg_color (LYPAD_TEXT_VIEW (widget->content_view), color);
}

static void lypad_note_widget_set_date (LypadNoteWidget *widget, time_t date)
{
    char *date_str = lypad_utils_format_time (date);
    gtk_label_set_label (widget->date, date_str);
    g_free (date_str);
}

static void on_note_date_changed (LypadNote *note, gpointer user_data)
{
    LypadNoteWidget *widget = LYPAD_NOTE_WIDGET (user_data);
    lypad_note_widget_set_date (widget, lypad_note_get_modification_date (note));
}

static void on_note_tags_changed (LypadNote *note, gpointer user_data)
{
    LypadNoteWidget *widget = user_data;
    lypad_tags_box_clear (LYPAD_TAGS_BOX (widget->tags_box));

    GList *tags = lypad_note_get_tags (note);
    for (GList *l = tags; l != NULL; l = l->next)
        lypad_tags_box_add_tag (LYPAD_TAGS_BOX (widget->tags_box), l->data);
}

static void lypad_note_widget_release_note (LypadNoteWidget *widget)
{
    if (widget->note)
    {
        g_signal_handler_disconnect (widget->note, widget->tags_changed_handler);
        g_signal_handler_disconnect (widget->note, widget->color_changed_handler);
        g_signal_handler_disconnect (widget->note, widget->date_changed_handler);
        g_signal_handler_disconnect (widget->note, widget->note_destroy_handler);
        g_signal_handler_disconnect (widget->note, widget->note_pin_toggled_handler);
        g_object_unref (widget->note);
        widget->note = NULL;
    }
}

static void on_note_destroy (LypadNote *note, gpointer user_data)
{
    LypadNoteWidget *widget = LYPAD_NOTE_WIDGET (user_data);
    lypad_note_widget_release_note (widget);
    gtk_widget_destroy (GTK_WIDGET (widget));
}

static void on_note_pin_toggled (LypadNote *note, gboolean active, gpointer user_data)
{
    LypadNoteWidget *widget = LYPAD_NOTE_WIDGET (user_data);
    g_signal_handlers_block_by_func (widget->pin_toggle, on_pin_toggled, widget);
    lypad_toggle_button_set_active (LYPAD_TOGGLE_BUTTON (widget->pin_toggle), active);
    g_signal_handlers_unblock_by_func (widget->pin_toggle, on_pin_toggled, widget);
}

static void lypad_note_widget_dispose (GObject *object)
{
    LypadNoteWidget *note_widget = LYPAD_NOTE_WIDGET (object);
    lypad_note_widget_release_note (note_widget);
    g_clear_object (&note_widget->color_provider);
    G_OBJECT_CLASS (lypad_note_widget_parent_class)->dispose (object);
}

static void lypad_note_widget_class_init (LypadNoteWidgetClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

    object_class->dispose = lypad_note_widget_dispose;

    g_type_ensure (LYPAD_TYPE_TOGGLE_BUTTON);
    g_type_ensure (LYPAD_TYPE_TEXT_VIEW);
    g_type_ensure (LYPAD_TYPE_TITLE_VIEW);
    gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/lypad/ui/lypad-note-widget.glade");
    gtk_widget_class_bind_template_child (widget_class, LypadNoteWidget, main_box);
    gtk_widget_class_bind_template_child (widget_class, LypadNoteWidget, pin_toggle);
    gtk_widget_class_bind_template_child (widget_class, LypadNoteWidget, title_view);
    gtk_widget_class_bind_template_child (widget_class, LypadNoteWidget, content_view);
    gtk_widget_class_bind_template_child (widget_class, LypadNoteWidget, date);
    gtk_widget_class_bind_template_child (widget_class, LypadNoteWidget, header_box);
}

static void lypad_note_widget_set_note (LypadNoteWidget *widget, LypadNote *note)
{
    lypad_note_widget_release_note (widget);
    widget->note = g_object_ref (note);
    lypad_text_view_set_buffer (LYPAD_TEXT_VIEW (widget->title_view), lypad_note_get_title_buffer (note));
    lypad_text_view_set_buffer (LYPAD_TEXT_VIEW (widget->content_view), lypad_note_get_content_buffer (note));

    GList *tags = lypad_note_get_tags (note);
    for (GList *l = tags; l != NULL; l = l->next)
        lypad_tags_box_add_tag (LYPAD_TAGS_BOX (widget->tags_box), l->data);
    widget->tags_changed_handler = g_signal_connect (note, "tags-changed", G_CALLBACK (on_note_tags_changed), widget);

    const GdkRGBA *color = lypad_note_get_color (note);
    lypad_note_widget_set_color (widget, color);
    lypad_text_view_notify_bkg_color (LYPAD_TEXT_VIEW (widget->title_view), color);
    lypad_text_view_notify_bkg_color (LYPAD_TEXT_VIEW (widget->content_view), color);
    widget->color_changed_handler = g_signal_connect (note, "color-changed", G_CALLBACK (on_note_color_changed), widget);

    lypad_note_widget_set_date (widget, lypad_note_get_modification_date (note));
    widget->date_changed_handler = g_signal_connect (note, "date-changed", G_CALLBACK (on_note_date_changed), widget);

    lypad_toggle_button_set_active (LYPAD_TOGGLE_BUTTON (widget->pin_toggle), lypad_note_get_show_on_desktop (note));
    widget->note_destroy_handler = g_signal_connect (note, "destroy", G_CALLBACK (on_note_destroy), widget);

    widget->note_pin_toggled_handler = g_signal_connect (note, "pin-toggled", G_CALLBACK (on_note_pin_toggled), widget);
}

LypadNote *lypad_note_widget_get_note (LypadNoteWidget *widget)
{
    g_return_val_if_fail (LYPAD_IS_NOTE_WIDGET (widget), NULL);
    return widget->note;
}

GtkWidget *lypad_note_widget_new (LypadNote *note)
{
    LypadNoteWidget *widget = g_object_new (LYPAD_TYPE_NOTE_WIDGET, NULL);
    lypad_note_widget_set_note (widget, note);
    return GTK_WIDGET (widget);
}
