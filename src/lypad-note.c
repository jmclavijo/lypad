/*
 *  lypad-note.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-note.h"
#include <stdio.h>
#include <string.h>
#include "lypad-title-buffer.h"
#include "lypad-preferences.h"
#include "lypad-note-window.h"
#include "lypad-timer.h"
#include "lypad-tag.h"
#include "lypad-theme.h"
#include "lypad.h"
#include "color-selection/lypad-color-store.h"
#include "color-selection/lypad-color-utils.h"
#include <uuid/uuid.h>

struct _LypadNote
{
    GObject          parent;
    GFile           *file;
    LypadTextBuffer *title;
    LypadTextBuffer *content;
    GdkRGBA          color;
    GList           *tags;
    time_t           time_created;
    time_t           time_modified;
    LypadNoteWindow *desktop_window;
    gboolean         show_on_desktop;
    gboolean         is_modified;
    gboolean         locked;

    GdkRectangle     window_position;
    LypadTimer      *autosave_timer;
    gboolean         deleted;

    char            *id;
    LypadTheme      *theme;
};

G_DEFINE_TYPE (LypadNote, lypad_note, G_TYPE_OBJECT)

enum
{
    SIGNAL_COLOR_CHANGED,
    SIGNAL_DATE_CHANGED,
    SIGNAL_PIN_TOGGLED,
    SIGNAL_TAGS_CHANGED,
    SIGNAL_THEME_CHANGED,
    SIGNAL_LOCK_CHANGED,
    SIGNAL_DESTROY,
    SIGNAL_LAST
};

static guint signals[SIGNAL_LAST] = {0};

static char *lypad_note_id_create ()
{
    uuid_t id_bin;
    uuid_generate_time (id_bin);
    char id_str[38];
    uuid_unparse (id_bin, id_str);
    return g_strdup (id_str);
}

static inline gboolean lypad_note_get_modified (LypadNote *note)
{
    return note->is_modified;
}

static inline void lypad_note_unset_modified (LypadNote *note)
{
    gtk_text_buffer_set_modified (GTK_TEXT_BUFFER (note->title), FALSE);
    gtk_text_buffer_set_modified (GTK_TEXT_BUFFER (note->content), FALSE);
    note->is_modified = FALSE;
}

gboolean lypad_note_file_equal (LypadNote *note, GFile *file)
{
    g_return_val_if_fail (LYPAD_IS_NOTE (note), FALSE);
    return g_file_equal (note->file, file);
}

gboolean lypad_note_equal (LypadNote *note1, LypadNote *note2)
{
    if (note1 == NULL || note2 == NULL)
        return FALSE;
    else if (note1 == note2)
        return TRUE;
    else if (!note1->file || !note2->file)
        return FALSE;
    else if (g_file_equal (note1->file, note2->file))
        return TRUE;
    else
        return FALSE;
}

time_t lypad_note_get_creation_date (LypadNote *note)
{
    g_return_val_if_fail (LYPAD_IS_NOTE (note), 0);
    return note->time_created;
}

time_t lypad_note_get_modification_date (LypadNote *note)
{
    return note->time_modified;
}

static void lypad_note_set_modified (LypadNote *note)
{
    note->is_modified = TRUE;
    note->time_modified = time (NULL);
    g_signal_emit (note, signals[SIGNAL_DATE_CHANGED], 0);
    lypad_timer_start (note->autosave_timer);
}

static void lypad_note_set_geometry_modified (LypadNote *note)
{
    note->is_modified = TRUE;
    lypad_timer_start (note->autosave_timer);
}

static void on_buffer_end_user_action (GtkTextBuffer *text_buffer, gpointer user_data)
{
    if (gtk_text_buffer_get_modified (text_buffer))
        lypad_note_set_modified (LYPAD_NOTE (user_data));
}

static void on_autosave_timeout (LypadTimer *time, gpointer user_data)
{
    LypadNote *note = user_data;
    lypad_note_save (note);
}

static void lypad_note_init (LypadNote *note)
{
    GSettings *default_settings = g_settings_new ("org.gnome.lypad.defaults");

    gboolean color_set = FALSE;
    if (g_settings_get_boolean (default_settings, "bg-color-fixed"))
    {
        char *color_str = g_settings_get_string (default_settings, "bg-color");
        if (gdk_rgba_parse (&note->color, color_str))
            color_set = TRUE;
        g_free (color_str);
    }
    else
    {
        char *color_pool = g_settings_get_string (default_settings, "bg-color-pool");
        LypadColorPalette *palette = lypad_color_store_get_palette (LYPAD_NOTE_COLOR_SET);
        for (int i = 0; palette->row_names[i] != NULL; ++i)
            if (g_ascii_strcasecmp (palette->row_names[i], color_pool) == 0)
            {
                static int last_color_idx = -1;
                int color_idx;
                do {
                    color_idx = i * palette->n_columns + g_random_int_range (0, palette->n_columns);
                } while (color_idx == last_color_idx && palette->n_columns > 1);
                last_color_idx = color_idx;
                note->color = palette->colors[color_idx];
                color_set = TRUE;
            }
        g_free (color_pool);
    }
    if (!color_set)
    {
        note->color.red   = 0.95;
        note->color.green = 0.95;
        note->color.blue  = 0.58;
        note->color.alpha = 1.00;
    }

    note->tags    = NULL;
    note->file    = NULL;
    note->id      = NULL;
    note->deleted = FALSE;

    note->title = lypad_title_buffer_new ();
    note->content = lypad_text_buffer_new ();
    lypad_text_buffer_notify_bkg_color (note->title, &note->color);
    lypad_text_buffer_notify_bkg_color (note->content, &note->color);

    color_set = FALSE;
    if (!g_settings_get_boolean (default_settings, "fg-automatic"))
    {
        GdkRGBA fg_color;
        char *color_str = g_settings_get_string (default_settings, "fg-color");
        if (gdk_rgba_parse (&fg_color, color_str))
        {
            lypad_text_buffer_set_color (note->content, &fg_color);
            color_set = TRUE;
        }
        g_free (color_str);
    }
    if (!color_set)
        lypad_text_buffer_set_color (note->content, &LYPAD_AUTOMATIC_TEXT_COLOR);

    char *font = g_settings_get_string (default_settings, "font");
    PangoFontDescription *font_desc = pango_font_description_from_string (font);
    g_free (font);
    char *family = g_strdup (pango_font_description_get_family (font_desc));
    int size = pango_font_description_get_size (font_desc);
    if (size && !pango_font_description_get_size_is_absolute (font_desc))
        size = (int) ((double) size / (double) (PANGO_SCALE) + 0.5);
    if (!family || !size)
    {
        GtkSettings *gtk_settings = gtk_settings_get_default ();
        if (gtk_settings)
        {
            char *font_default;
            g_object_get (gtk_settings, "gtk-font-name", &font_default, NULL);
            PangoFontDescription *font_desc_default = pango_font_description_from_string (font_default);
            g_free (font_default);

            if (!family)
                family = g_strdup (pango_font_description_get_family (font_desc_default));
            if (!size)
            {
                size = pango_font_description_get_size (font_desc_default);
                if (!pango_font_description_get_size_is_absolute (font_desc_default))
                    size = (int) ((double) size / (double) (PANGO_SCALE) + 0.5);
            }
            pango_font_description_free (font_desc_default);
        }
    }
    if (!family)
        family = g_strdup ("Sans");
    if (!size)
        size = 11;

    lypad_text_buffer_set_font (note->title, family);
    lypad_text_buffer_set_font (note->content, family);
    g_free (family);

    lypad_text_buffer_set_size (note->title, size);
    lypad_text_buffer_set_size (note->content, size);
    if (pango_font_description_get_weight (font_desc) == PANGO_WEIGHT_BOLD)
    {
        lypad_text_buffer_set_toggle_tag (note->title, BOLD_TAG, TRUE);
        lypad_text_buffer_set_toggle_tag (note->content, BOLD_TAG, TRUE);
    }
    if (pango_font_description_get_style (font_desc) == PANGO_STYLE_ITALIC || pango_font_description_get_style (font_desc) == PANGO_STYLE_OBLIQUE)
    {
        lypad_text_buffer_set_toggle_tag (note->title, ITALIC_TAG, TRUE);
        lypad_text_buffer_set_toggle_tag (note->content, ITALIC_TAG, TRUE);
    }
    pango_font_description_free (font_desc);

    char *theme_name = g_settings_get_string (default_settings, "theme");
    note->theme = lypad_theme_get_for_name (theme_name);
    if (note->theme)
        g_object_ref (note->theme);

    g_object_unref (default_settings);

    note->is_modified = FALSE;

    note->show_on_desktop = FALSE;
    note->desktop_window = NULL;

    note->window_position.x = 500;
    note->window_position.y = 200;
    note->window_position.width = 300;
    note->window_position.height = 200;

    time_t now = time (NULL);
    note->time_created = now;
    note->time_modified = now;

    note->autosave_timer = lypad_timer_new ();
    lypad_timer_set_delay (note->autosave_timer, 10, 5);

    g_signal_connect (note->title, "end-user-action", G_CALLBACK (on_buffer_end_user_action), note);
    g_signal_connect (note->content, "end-user-action", G_CALLBACK (on_buffer_end_user_action), note);
    g_signal_connect (note->autosave_timer, "timeout", G_CALLBACK (on_autosave_timeout), note);
}

const char *lypad_note_get_id (LypadNote *note)
{
    g_return_val_if_fail (LYPAD_IS_NOTE (note), NULL);
    return note->id;
}

GtkTextBuffer *lypad_note_get_title_buffer (LypadNote *note)
{
    g_return_val_if_fail (LYPAD_IS_NOTE (note), NULL);
    return GTK_TEXT_BUFFER (note->title);
}

GtkTextBuffer *lypad_note_get_content_buffer (LypadNote *note)
{
    g_return_val_if_fail (LYPAD_IS_NOTE (note), NULL);
    return GTK_TEXT_BUFFER (note->content);
}

void lypad_note_set_color (LypadNote *note, const GdkRGBA *color)
{
    g_return_if_fail (LYPAD_IS_NOTE (note));
    note->color = *color;
    lypad_note_set_geometry_modified (note);
    g_signal_emit (note, signals[SIGNAL_COLOR_CHANGED], 0);
    lypad_text_buffer_notify_bkg_color (note->title, color);
    lypad_text_buffer_notify_bkg_color (note->content, color);
}

const GdkRGBA *lypad_note_get_color (LypadNote *note)
{
    g_return_val_if_fail (LYPAD_IS_NOTE (note), NULL);
    return &note->color;
}

static void on_tag_destroy (LypadTag *tag, gpointer user_data)
{
    LypadNote *note = LYPAD_NOTE (user_data);
    lypad_note_remove_tag (note, tag);
}

static void on_tag_color_changed (LypadTag *tag, GParamSpec *pspec, gpointer user_data)
{
    LypadNote *note = LYPAD_NOTE (user_data);
    lypad_note_set_geometry_modified (note);
}

void lypad_note_add_tag (LypadNote *note, LypadTag *tag)
{
    g_return_if_fail (LYPAD_IS_NOTE (note));
    gboolean found = FALSE;
    for (GList *l = note->tags; l != NULL; l = l->next)
        if (tag == l->data)
        {
            found = TRUE;
            break;
        }
    if (!found)
    {
        note->tags = g_list_append (note->tags, lypad_tag_ref_by_note (tag, note));
        g_signal_connect (tag, "destroy", G_CALLBACK (on_tag_destroy), note);
        g_signal_connect (tag, "notify::color", G_CALLBACK (on_tag_color_changed), note);
        g_signal_emit (note, signals[SIGNAL_TAGS_CHANGED], 0);
        lypad_note_set_geometry_modified (note);
    }
}

void lypad_note_remove_tag (LypadNote *note, LypadTag *tag)
{
    GList *item = g_list_find (note->tags, tag);
    if (item)
    {
        g_signal_handlers_disconnect_by_func (tag, on_tag_destroy, note);
        g_signal_handlers_disconnect_by_func (tag, on_tag_color_changed, note);
        lypad_tag_unref_by_note (tag, note);
        note->tags = g_list_delete_link (note->tags, item);
        g_signal_emit (note, signals[SIGNAL_TAGS_CHANGED], 0);
        lypad_note_set_geometry_modified (note);
    }
}

gboolean lypad_note_has_tag (LypadNote *note, LypadTag *tag)
{
    g_return_val_if_fail (LYPAD_IS_NOTE (note), FALSE);
    return g_list_find (note->tags, tag) != NULL;
}

GList *lypad_note_get_tags (LypadNote *note)
{
    g_return_val_if_fail (LYPAD_IS_NOTE (note), NULL);
    return note->tags;
}

gboolean lypad_note_get_show_on_desktop (LypadNote *note)
{
    g_return_val_if_fail (LYPAD_IS_NOTE (note), FALSE);
    return note->show_on_desktop;
}

void lypad_note_set_show_on_desktop (LypadNote *note, gboolean show, GtkWindow *window)
{
    g_return_if_fail (LYPAD_IS_NOTE (note));
    if (note->show_on_desktop != show)
    {
        note->show_on_desktop = show;
        g_signal_emit (note, signals[SIGNAL_PIN_TOGGLED], 0, show);

        if (show)
        {
            g_return_if_fail (note->desktop_window == NULL);
            lypad_note_show (note, window);
        }
        else
        {
            if (!note->desktop_window)
                return;
            gtk_widget_destroy (GTK_WIDGET (note->desktop_window));
            g_clear_object (&note->desktop_window);
        }
        lypad_note_set_geometry_modified (note);
    }
}

void lypad_note_show (LypadNote *note, GtkWindow *window)
{
    g_return_if_fail (LYPAD_IS_NOTE (note));
    if (note->show_on_desktop && !note->desktop_window)
    {
        if (lypad_application_get_supports_desktop_notes (LYPAD_APPLICATION_DEFAULT))
        {
            note->desktop_window = g_object_ref_sink (lypad_note_window_new (LYPAD_APPLICATION_DEFAULT, note));
            gtk_widget_show (GTK_WIDGET (note->desktop_window));
        }
        else if (window)
            lypad_application_show_invalid_backend_dialog (LYPAD_APPLICATION_DEFAULT, window);
    }
}

void lypad_note_set_locked (LypadNote *note, gboolean lock)
{
    g_return_if_fail (LYPAD_IS_NOTE (note));
    if (note->locked != lock)
    {
        note->locked = lock;
        lypad_note_set_geometry_modified (note);
        g_signal_emit (note, signals[SIGNAL_LOCK_CHANGED], 0);
    }
}

gboolean lypad_note_get_locked (LypadNote *note)
{
    g_return_val_if_fail (LYPAD_IS_NOTE (note), FALSE);
    return note->locked;
}

void lypad_note_set_theme (LypadNote *note, LypadTheme *theme)
{
    g_return_if_fail (LYPAD_IS_NOTE (note));
    if (theme != note->theme)
    {
        if (note->theme)
        {
            g_object_unref (note->theme);
            note->theme = NULL;
        }
        if (theme)
            note->theme = g_object_ref (theme);
        g_signal_emit (note, signals[SIGNAL_THEME_CHANGED], 0);
    }
}

LypadTheme *lypad_note_get_theme (LypadNote *note)
{
    g_return_val_if_fail (LYPAD_IS_NOTE (note), NULL);
    if (note->theme)
        return note->theme;
    else
        return lypad_theme_get_default ();
}

void lypad_note_set_position (LypadNote *note, int x, int y, int width, int height)
{
    g_return_if_fail (LYPAD_IS_NOTE (note));
    note->window_position.x = x;
    note->window_position.y = y;
    if (width != -1)
        note->window_position.width = width;
    if (height != -1)
        note->window_position.height = height;
    lypad_note_set_geometry_modified (note);
}

void lypad_note_get_position (LypadNote *note, int *x, int *y)
{
    g_return_if_fail (LYPAD_IS_NOTE (note));
    if (x)
        *x = note->window_position.x;
    if (y)
        *y = note->window_position.y;
}

void lypad_note_get_size (LypadNote *note, int *width, int *height)
{
    g_return_if_fail (LYPAD_IS_NOTE (note));
    if (width)
        *width = note->window_position.width;
    if (height)
        *height = note->window_position.height;
}

static void lypad_note_dispose (GObject *object)
{
    LypadNote *note = LYPAD_NOTE (object);
    if (note->title)
    {
        g_signal_handlers_disconnect_by_func (note->title, on_buffer_end_user_action, note);
        g_object_unref (note->title);
        note->title = FALSE;
    }
    if (note->content)
    {
        g_signal_handlers_disconnect_by_func (note->content, on_buffer_end_user_action, note);
        g_object_unref (note->content);
        note->content = FALSE;
    }
    if (note->tags)
    {
        for (GList *l = note->tags; l != NULL; l = l->next)
        {
            g_signal_handlers_disconnect_by_func (l->data, on_tag_destroy, note);
            g_signal_handlers_disconnect_by_func (l->data, on_tag_color_changed, note);
            lypad_tag_unref_by_note (l->data, note);
        }
        g_list_free (note->tags);
        note->tags = NULL;
    }
    if (note->desktop_window)
    {
        gtk_widget_destroy (GTK_WIDGET (note->desktop_window));
        g_object_unref (note->desktop_window);
        note->desktop_window = NULL;
    }
    g_clear_object (&note->theme);

    G_OBJECT_CLASS (lypad_note_parent_class)->dispose (object);
}

static void lypad_note_finalize (GObject *object)
{
    LypadNote *note = LYPAD_NOTE (object);
    lypad_note_save (note);
    if (note->file)
        g_object_unref (note->file);
    g_clear_pointer (&note->id, g_free);
    g_object_unref (note->autosave_timer);
    G_OBJECT_CLASS (lypad_note_parent_class)->finalize (object);
}

static void lypad_note_class_init (LypadNoteClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->dispose = lypad_note_dispose;
    object_class->finalize = lypad_note_finalize;

    signals[SIGNAL_COLOR_CHANGED] = g_signal_new ("color-changed", G_TYPE_FROM_CLASS (klass),
                                                  G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                                  g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

    signals[SIGNAL_DATE_CHANGED] = g_signal_new ("date-changed", G_TYPE_FROM_CLASS (klass),
                                                 G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                                 g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

    signals[SIGNAL_PIN_TOGGLED] = g_signal_new ("pin-toggled", G_TYPE_FROM_CLASS (klass),
                                                G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                                g_cclosure_marshal_VOID__BOOLEAN, G_TYPE_NONE, 1, G_TYPE_BOOLEAN);

    signals[SIGNAL_TAGS_CHANGED] = g_signal_new ("tags-changed", G_TYPE_FROM_CLASS (klass),
                                                 G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                                 g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

    signals[SIGNAL_THEME_CHANGED] = g_signal_new ("theme-changed", G_TYPE_FROM_CLASS (klass),
                                                  G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                                  g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

    signals[SIGNAL_LOCK_CHANGED] = g_signal_new ("lock-changed", G_TYPE_FROM_CLASS (klass),
                                                  G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                                  g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

    signals[SIGNAL_DESTROY] = g_signal_new ("destroy", G_TYPE_FROM_CLASS (klass),
                                            G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                            g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
}

LypadNote *lypad_note_new ()
{
    return g_object_new (LYPAD_TYPE_NOTE, NULL);
}

#define BUFFER_SIZE 1024

#define LINE_EQUAL_LITERAL(text, line, line_length) \
    (line_length == sizeof text - 1 && memcmp (text, line, sizeof text - 1) == 0)

#define LINE_STARTS_WITH_LITERAL(text, line, line_length) \
    (line_length >= sizeof text - 1 && memcmp (text, line, sizeof text - 1) == 0)

#define STR_LITERAL_LEN(text) \
    (sizeof text - 1)

LypadNote *lypad_note_load_from_file (GFile *file)
{
    LypadNote *note = lypad_note_new ();
    time_t note_modified_date = 0;
    note->file = g_object_ref (file);

    GString *buffer = g_string_sized_new (BUFFER_SIZE);
    char tmp_buffer[BUFFER_SIZE];
    GError *error = NULL;
    GInputStream *stream = (GInputStream *) g_file_read (file, NULL, &error);
    if (!stream)
    {
        g_warning ("%s", error->message);
        g_error_free (error);
        goto deserialize_cleanup;
    }
    int readed = 0;
    do {
        error = NULL;
        readed = g_input_stream_read (stream, tmp_buffer, BUFFER_SIZE, NULL, &error);
        if (readed > 0)
            g_string_append_len (buffer, tmp_buffer, readed);
    } while (readed == BUFFER_SIZE);
    if (readed == -1)
    {
        g_warning ("%s", error->message);
        g_error_free (error);
        g_object_unref (stream);
        goto deserialize_cleanup;
    }
    g_string_append_c (buffer, '\n');
    g_input_stream_close (stream, NULL, NULL);
    g_object_unref (stream);

    char *line = buffer->str;
    gsize remaining_len = buffer->len;
    char *next_line;
    gsize line_len;
    int section = 0;    // 0 -> None, 1 -> header, 2 -> title, 3 -> content

    // Read header
    int major_version;
    int minor_version;

    next_line = g_utf8_strchr (line, remaining_len, '\n');
    line_len = next_line - line;
    if (line_len > remaining_len)
        goto file_corrupted;
    if (sscanf (line, LYPAD_MAGIC_HEADER "%d.%d\n", &major_version, &minor_version) != 2)
    {
        char *filename =  g_file_get_parse_name (file);
        g_warning ("'%s' doesn't contain a note", filename);
        g_free (filename);
        goto deserialize_cleanup;
    }
    line = next_line + 1;
    remaining_len -= line_len + 1;


    while (remaining_len > 0)
    {
        next_line = g_utf8_strchr (line, remaining_len, '\n');
        if (!next_line)
            break;
        line_len = next_line - line;
        if (line_len > remaining_len)
            goto file_corrupted;

        if (LINE_EQUAL_LITERAL ("<header>", line, line_len))
        {
            if (section == 0)
                section = 1;
            else
                goto file_corrupted;
        }
        else if (LINE_EQUAL_LITERAL ("</header>", line, line_len))
        {
            if (section == 1)
                section = 0;
            else
                goto file_corrupted;
        }
        else if (LINE_STARTS_WITH_LITERAL ("<title>", line, line_len))
        {
            if (section == 0)
            {
                section = 2;
                gsize title_len;
                if (sscanf (line, "<title> %lu\n", &title_len) == 0)
                    goto file_corrupted;

                line = next_line + 1;
                remaining_len -= line_len + 1;

                if (title_len > remaining_len)
                    goto file_corrupted;
                if (!lypad_text_buffer_deserialize (note->title, line, title_len))
                    goto file_corrupted;

                line += title_len;
                remaining_len -= title_len;
                continue;
            }
            else
                goto file_corrupted;
        }
        else if (LINE_EQUAL_LITERAL ("</title>", line, line_len))
        {
            if (section == 2)
                section = 0;
            else
                goto file_corrupted;
        }
        else if (LINE_STARTS_WITH_LITERAL ("<content>", line, line_len))
        {
            if (section == 0)
            {
                section = 3;
                gsize content_len;
                if (sscanf (line, "<content> %lu\n", &content_len) == 0)
                    goto file_corrupted;

                line = next_line + 1;
                remaining_len -= line_len + 1;

                if (content_len > remaining_len)
                    goto file_corrupted;
                if (!lypad_text_buffer_deserialize (note->content, line, content_len))
                    goto file_corrupted;

                line += content_len;
                remaining_len -= content_len;
                continue;
            }
            else
                goto file_corrupted;
        }
        else if (LINE_EQUAL_LITERAL ("</content>", line, line_len))
        {
            if (section == 3)
                section = 0;
            else
                goto file_corrupted;
        }
        else
        {
            switch (section)
            {
                case 0:
                    if (line_len > 0)
                        goto file_corrupted;
                    break;
                case 1:
                    if (LINE_STARTS_WITH_LITERAL ("id: ", line, line_len))
                    {
                        line[line_len] = '\0';
                        g_clear_pointer (&note->id, g_free);
                        note->id = g_strdup (line + STR_LITERAL_LEN ("id: "));
                    }
                    else if (LINE_STARTS_WITH_LITERAL ("color: ", line, line_len))
                    {
                        line[line_len] = '\0';
                        if (!gdk_rgba_parse (&note->color, line + STR_LITERAL_LEN ("color: ")))
                            goto file_corrupted;
                    }
                    else if (LINE_STARTS_WITH_LITERAL ("created: ", line, line_len))
                    {
                        line[line_len] = '\0';
                        if (sscanf (line, "created: %ld", &note->time_created) == 0)
                            goto file_corrupted;
                    }
                    else if (LINE_STARTS_WITH_LITERAL ("modified: ", line, line_len))
                    {
                        line[line_len] = '\0';
                        if (sscanf (line, "modified: %ld", &note_modified_date) == 0)
                            goto file_corrupted;
                    }
                    else if (LINE_STARTS_WITH_LITERAL ("pinned: ", line, line_len))
                    {
                        if (line[STR_LITERAL_LEN ("pinned: ")] == '1')
                            note->show_on_desktop = TRUE;
                        else if (line[STR_LITERAL_LEN ("pinned: ")] == '0')
                            note->show_on_desktop = FALSE;
                        else
                            goto file_corrupted;
                    }
                    else if (LINE_STARTS_WITH_LITERAL ("locked: ", line, line_len))
                    {
                        if (line[STR_LITERAL_LEN ("locked: ")] == '1')
                            note->locked = TRUE;
                        else if (line[STR_LITERAL_LEN ("locked: ")] == '0')
                            note->locked = FALSE;
                        else
                            goto file_corrupted;
                    }
                    else if (LINE_STARTS_WITH_LITERAL ("theme: ", line, line_len))
                    {
                        line[line_len] = '\0';
                        const char *theme_name = line + STR_LITERAL_LEN ("theme: ");
                        g_clear_object (&note->theme);
                        note->theme = g_object_ref (lypad_theme_get_for_name (theme_name));
                        if (note->theme)
                            g_object_ref (note->theme);
                        else
                            g_warning ("Theme '%s' doesn't exist, using default theme", theme_name);
                    }
                    else if (LINE_STARTS_WITH_LITERAL ("window-rect: ", line, line_len))
                    {
                        if (sscanf (line, "window-rect: %d %d %d %d",
                                    &note->window_position.x, &note->window_position.y,
                                    &note->window_position.width, &note->window_position.height) != 4)
                            goto file_corrupted;
                    }
                    else if (LINE_STARTS_WITH_LITERAL ("tag: ", line, line_len))
                    {
                        line[line_len] = '\0';
                        LypadTag *tag = lypad_tag_parse (line + STR_LITERAL_LEN ("tag: "));
                        if (!tag)
                            goto file_corrupted;
                        lypad_note_add_tag (note, tag);
                        g_object_unref (tag);
                    }
                    else if (line_len > 0)
                        goto file_corrupted;
                    break;
                case 2:
                case 3:
                    // deserialize
                    break;
            }
        }

        line = next_line + 1;
        remaining_len -= line_len + 1;
    }
    if (section != 0)
        goto file_corrupted;

//deserialize_end:
    g_string_free (buffer, TRUE);
    lypad_note_unset_modified (note);
    if (note_modified_date != 0)
        note->time_modified = note_modified_date;
    lypad_text_buffer_notify_bkg_color (note->title, &note->color);
    lypad_text_buffer_notify_bkg_color (note->content, &note->color);
    return note;

    char *filename;
file_corrupted:
    filename =  g_file_get_parse_name (file);  // Used for logs only
    g_warning ("File '%s' is corrupt and therefore can not be loaded.", filename);
    g_free (filename);

deserialize_cleanup:
    g_string_free (buffer, TRUE);
    g_object_unref (note);
    return NULL;
}

//BUG: g_string_insert_len: assertion 'len == 0 || val != NULL' failed

void lypad_note_save (LypadNote *note)
{
    g_return_if_fail (LYPAD_IS_NOTE (note));
    if (!lypad_note_get_modified (note))
        return;
    if (note->deleted)
        return;

    if (!note->id)
        note->id = lypad_note_id_create ();

    GString *buffer = g_string_new (NULL);
    g_string_printf (buffer, "LYPAD NOTE v%d.%d\n\n", LYPAD_VERSION_MAJOR, LYPAD_VERSION_MINOR);

    g_string_append (buffer, "<header>\n");
    char *color = gdk_rgba_to_string (&note->color);
    g_string_append_printf (buffer, "id: %s\n", note->id);
    g_string_append_printf (buffer, "created: %ld\n", note->time_created);
    g_string_append_printf (buffer, "modified: %ld\n", note->time_modified);
    g_string_append_printf (buffer, "color: %s\n", color);
    g_string_append_printf (buffer, "pinned: %d\n", (note->show_on_desktop ? 1 : 0));
    g_string_append_printf (buffer, "locked: %d\n", (note->locked ? 1 : 0));
    if (note->theme)
        g_string_append_printf (buffer, "theme: %s\n", lypad_theme_get_name (note->theme));
    g_string_append_printf (buffer, "window-rect: %d %d %d %d\n",
                            note->window_position.x, note->window_position.y,
                            note->window_position.width, note->window_position.height);
    g_free (color);
    for (GList *l = note->tags; l != NULL; l = l->next)
    {
        char *tag_str = lypad_tag_serialize (l->data);
        g_string_append (buffer, "tag: ");
        g_string_append (buffer, tag_str);
        g_string_append_c (buffer, '\n');
        g_free (tag_str);
    }
    g_string_append (buffer, "</header>\n\n");

    gsize text_len;
    char *text = lypad_text_buffer_serialize (note->title, &text_len);
    g_string_append_printf (buffer, "<title> %ld\n", text_len);
    g_string_append_len (buffer, text, text_len);
    g_string_append (buffer, "\n</title>\n\n");
    g_free (text);

    text = lypad_text_buffer_serialize (note->content, &text_len);
    g_string_append_printf (buffer, "<content> %ld\n", text_len);
    g_string_append_len (buffer, text, text_len);
    g_string_append (buffer, "\n</content>\n");
    g_free (text);

    GFileIOStream *stream;
    GError *error = NULL;
    GFile *tmp_file = g_file_new_tmp ("lypad_XXXXXX", &stream, &error);
    if (!tmp_file)
    {
        g_warning ("Error saving note: %s", error->message);
        g_error_free (error);
    }
    GOutputStream *ostream = g_io_stream_get_output_stream (G_IO_STREAM (stream));
    gssize bytes_written;
    error = NULL;
    bytes_written = g_output_stream_write (ostream, buffer->str, buffer->len, NULL, &error);
    if (bytes_written == -1)
    {
        g_warning ("Error saving note: %s", error->message);
        g_error_free (error);
    }
    g_io_stream_close (G_IO_STREAM (stream), NULL, &error);
    g_object_unref (stream);
    if (note->file)
    {
        error = NULL;
        if (!g_file_move (tmp_file, note->file, G_FILE_COPY_OVERWRITE, NULL, NULL, NULL, &error))
        {
            g_warning ("Error saving note: %s", error->message);
            g_error_free (error);
        }
    }
    else
    {
        gboolean done = FALSE;
        for (int i = 0; TRUE; ++i)
        {
            char **paths = lypad_preferences_get_note_paths (lypad_application_get_preferences (LYPAD_APPLICATION_DEFAULT));
            char *filename;
            if (i == 0)
                filename = g_strdup_printf ("%s/%s.lyp", paths[0], note->id);
            else
                filename = g_strdup_printf ("%s/%s-%d.lyp", paths[0], note->id, i);
            GFile *note_file = g_file_new_for_path (filename);
            error = NULL;
            if (g_file_move (tmp_file, note_file, G_FILE_COPY_NONE, NULL, NULL, NULL, &error))
            {
                note->file = note_file;
                done = TRUE;
            }
            else
            {
                g_object_unref (note_file);
                if (error->code != G_IO_ERROR_EXISTS && error->code != G_IO_ERROR_IS_DIRECTORY)
                {
                    done = TRUE;
                    g_warning ("Error saving note: %s", error->message);
                }
                g_error_free (error);
            }
            g_free (filename);
            g_strfreev (paths);
            if (done)
                break;
        }
        g_clear_error (&error);
    }

    g_object_unref (tmp_file);
    g_string_free (buffer, TRUE);
    lypad_note_unset_modified (note);
}

void lypad_note_delete (LypadNote *note)
{
    g_return_if_fail (LYPAD_IS_NOTE (note));
    note->deleted = TRUE;
    if (note->file)
    {
        GError *error = NULL;
        if (!g_file_delete (note->file, NULL, &error))
        {
            g_warning ("LypadNote::delete %s", error->message);
            g_error_free (error);
        }
        g_object_unref (note->file);
        note->file = NULL;
    }
    g_signal_emit (note, signals[SIGNAL_DESTROY], 0);
}
