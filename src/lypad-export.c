/*
 *  lypad-export.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-export.h"

G_DEFINE_INTERFACE (LypadExportFormat, lypad_export_format, G_TYPE_OBJECT)

static GList *registered_formats = NULL;

static void lypad_export_format_default_init (LypadExportFormatInterface *iface)
{
    iface->get_name = NULL;
    iface->get_extension = NULL;
    iface->export_note = NULL;
    iface->export_notes = NULL;
}

void lypad_export_format_register (GType type)
{
    for (GList *l = registered_formats; l != NULL; l = l->next)
        if (G_TYPE_CHECK_INSTANCE_TYPE (l->data, type))
        {
            g_warning ("LypadExportFormat::register error: Type '%s' is already registered.", g_type_name (type));
            return;
        }

    LypadExportFormat *format = g_object_new (type, NULL);
    LypadExportFormatInterface *iface = LYPAD_EXPORT_FORMAT_GET_IFACE (format);
    if (!iface->get_name)
        g_warning ("LypadExportFormat::get_name not implemented for \"%s\".", G_OBJECT_TYPE_NAME (format));
    else if (!iface->get_extension)
        g_warning ("LypadExportFormat::get_extension not implemented for \"%s\".", G_OBJECT_TYPE_NAME (format));
    else
        registered_formats = g_list_append (registered_formats, g_object_ref (format));
    g_object_unref (format);
}

//TODO: Handle export errors
static void on_export_dialog_response (GtkDialog *dialog, int response_id, gpointer user_data)
{
    GSList *notes = user_data;
    if (response_id == GTK_RESPONSE_ACCEPT)
    {
        GtkFileFilter *filter = gtk_file_chooser_get_filter (GTK_FILE_CHOOSER (dialog));
        if (!filter)
        {
            g_slist_free_full (notes, g_object_unref);
            gtk_widget_destroy (GTK_WIDGET (dialog));
            g_return_if_reached ();
        }

        LypadExportFormat *format = g_object_get_data (G_OBJECT (filter), "lypad-export-format");
        LypadExportFormatInterface *iface = LYPAD_EXPORT_FORMAT_GET_IFACE (format);

        char *filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
        char *ext = g_utf8_strrchr (filename, -1, '.');
        if (ext == NULL || g_strcmp0 (ext + 1, iface->get_extension (format)) != 0)
        {
            char *tmp = g_strdup_printf ("%s.%s", filename, iface->get_extension (format));
            g_free (filename);
            filename = tmp;
        }

        int n_notes = g_slist_length (notes);
        if (n_notes == 1 && iface->export_note)
            iface->export_note (format, notes->data, filename);
        else if (n_notes > 1 && iface->export_notes)
            iface->export_notes (format, notes, filename);

        g_free (filename);
    }
    g_slist_free_full (notes, g_object_unref);
    gtk_widget_destroy (GTK_WIDGET (dialog));
}

void lypad_export_dialog_launch (GtkWindow *parent, GSList *in_notes)
{
    GSList *notes = NULL;
    int n_notes = 0;
    for (GSList *l = in_notes; l != NULL; l = l->next)
    {
        notes = g_slist_prepend (notes, g_object_ref (l->data));
        n_notes += 1;
    }
    notes = g_slist_reverse (notes);
    g_return_if_fail (n_notes > 0);

    GtkWidget *dialog = gtk_file_chooser_dialog_new ("Export note",
                                                     parent,
                                                     GTK_FILE_CHOOSER_ACTION_SAVE,
                                                     "_Cancel",
                                                     GTK_RESPONSE_CANCEL,
                                                     "_Save",
                                                     GTK_RESPONSE_ACCEPT,
                                                     NULL);
    gtk_file_chooser_set_do_overwrite_confirmation (GTK_FILE_CHOOSER (dialog), TRUE);

    for (GList *l = registered_formats; l != NULL; l = l->next)
    {
        LypadExportFormat *format = LYPAD_EXPORT_FORMAT (l->data);
        LypadExportFormatInterface *iface = LYPAD_EXPORT_FORMAT_GET_IFACE (format);
        if (n_notes == 1 && iface->export_note == NULL)
            continue;
        else if (n_notes > 1 && iface->export_notes == NULL)
            continue;

        GtkFileFilter *filter = gtk_file_filter_new ();
        char *name = g_strdup_printf ("%s (*.%s)", iface->get_name (format), iface->get_extension (format));
        gtk_file_filter_set_name (filter, name);
        g_free (name);
        g_object_set_data_full (G_OBJECT (filter), "lypad-export-format", g_object_ref (format), g_object_unref);

        if (iface->get_mime_type)
            gtk_file_filter_add_mime_type (filter, iface->get_mime_type (format));
        else
        {
            char *pattern = g_strdup_printf ("*.%s", iface->get_extension (format));
            gtk_file_filter_add_pattern (filter, pattern);
            g_free (pattern);
        }
        gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dialog), filter);
    }

    g_signal_connect (dialog, "response", G_CALLBACK (on_export_dialog_response), notes);
    gtk_widget_show (dialog);
}
