/*
 *  lypad-title-view.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-title-view.h"

struct _LypadTitleView
{
    LypadTextView parent;
};

G_DEFINE_TYPE (LypadTitleView, lypad_title_view, LYPAD_TYPE_TEXT_VIEW)

static void lypad_title_view_init (LypadTitleView *view)
{
}

static void lypad_title_view_class_init (LypadTitleViewClass *klass)
{
}

GtkWidget *lypad_title_view_new ()
{
    return g_object_new (LYPAD_TYPE_TITLE_VIEW, NULL);
}
