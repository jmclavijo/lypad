/*
 *  lypad-text-buffer.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-text-buffer.h"
#include "lypad-text-history.h"
#include "color-selection/lypad-color-utils.h"

typedef struct _LypadTextBufferPrivate LypadTextBufferPrivate;

struct _LypadTextBufferPrivate
{
    GtkTextTag       *toggle_tags[LYPAD_N_TOGGLE_TAGS];
    GList            *font_tags;
    GList            *size_tags;
    GList            *color_tags;
    GtkTextTag       *automatic_color_tag;

    gboolean          insert_toggle_status[LYPAD_N_TOGGLE_TAGS];
    GtkTextTag       *insert_font_tag;
    GtkTextTag       *insert_size_tag;
    GtkTextTag       *insert_color_tag;

    gboolean          selection_toggle_status[LYPAD_N_TOGGLE_TAGS];
    GtkTextTag       *selection_font;
    GtkTextTag       *selection_size;
    GtkTextTag       *selection_color;

    LypadTextHistory *history;
    gboolean          edit_mode : 1;

    GtkTextMark      *prev_insert_mark;
    GtkTextMark      *prev_selection_mark;
    gboolean          ignore_mark_set;
};

G_DEFINE_TYPE_WITH_PRIVATE (LypadTextBuffer, lypad_text_buffer, GTK_TYPE_TEXT_BUFFER)

enum
{
	SIGNAL_STYLE_CHANGED,
	SIGNAL_LAST
};

static guint signals[SIGNAL_LAST] = {0};

static GQuark tag_type_quark = 0;
static GQuark tag_value_quark = 0;

struct _LypadTextListInfo
{
    LypadTextListType type;
    union
    {
        gunichar bullet;
    } u;
    int      list_margin;
    int      text_padding;
};

#define LYPAD_BULLET_DEFAULT 0xE001

LypadTextListType lypad_text_list_info_get_type (LypadTextListInfo *info)
{
    return info->type;
}

static void lypad_text_list_info_free (LypadTextListInfo *info)
{
    // switch (info->type)
    // {
    //     case LYPAD_TEXT_LIST_TYPE_BULLET:
    //         break;

    //     default:
    //         g_warn_if_reached ();
    // }
    g_free (info);
}

static void lypad_text_buffer_history_insert_text (GtkTextBuffer *buffer, GtkTextIter *position, const char  *text, int len);
static void lypad_text_buffer_history_delete_text (GtkTextBuffer *buffer, GtkTextIter *start, GtkTextIter *end);
static void lypad_text_buffer_history_apply_tag   (GtkTextBuffer *buffer, GtkTextTag *tag, GtkTextIter *start, GtkTextIter *end);
static void lypad_text_buffer_history_remove_tag  (GtkTextBuffer *buffer, GtkTextTag *tag, GtkTextIter *start, GtkTextIter *end);

static LypadTextHistoryFuncs history_funcs = {
    lypad_text_buffer_history_insert_text,
    lypad_text_buffer_history_delete_text,
    lypad_text_buffer_history_apply_tag,
    lypad_text_buffer_history_remove_tag
};

static void lypad_text_buffer_init (LypadTextBuffer *buffer)
{
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    priv->font_tags = NULL;
    priv->size_tags = NULL;
    priv->color_tags = NULL;
    priv->automatic_color_tag = NULL;

    for (int i = 0; i < LYPAD_N_TOGGLE_TAGS; ++i)
        priv->insert_toggle_status[i] = FALSE;
    priv->insert_font_tag = NULL;
    priv->insert_size_tag = NULL;
    priv->insert_color_tag = NULL;

    for (int i = 0; i < LYPAD_N_TOGGLE_TAGS; ++i)
        priv->selection_toggle_status[i] = FALSE;
    priv->selection_font = NULL;
    priv->selection_size = NULL;
    priv->selection_color = NULL;

    priv->history = lypad_text_history_new (GTK_TEXT_BUFFER (buffer), &history_funcs);
    priv->edit_mode = FALSE;

    priv->ignore_mark_set = FALSE;
}

static void lypad_text_buffer_history_insert_text (GtkTextBuffer *buffer, GtkTextIter *location, const char *text, int len)
{
    GTK_TEXT_BUFFER_CLASS (lypad_text_buffer_parent_class)->insert_text (buffer, location, text, len);
}

static void lypad_text_buffer_history_delete_text (GtkTextBuffer *buffer, GtkTextIter *start, GtkTextIter *end)
{
    GTK_TEXT_BUFFER_CLASS (lypad_text_buffer_parent_class)->delete_range (buffer, start, end);
}

static void lypad_text_buffer_history_apply_tag (GtkTextBuffer *buffer, GtkTextTag *tag, GtkTextIter *start, GtkTextIter *end)
{
    gtk_text_buffer_set_modified (buffer, TRUE);
    GTK_TEXT_BUFFER_CLASS (lypad_text_buffer_parent_class)->apply_tag (buffer, tag, start, end);
}

static void lypad_text_buffer_history_remove_tag (GtkTextBuffer *buffer, GtkTextTag *tag, GtkTextIter *start, GtkTextIter *end)
{
    gtk_text_buffer_set_modified (buffer, TRUE);
    GTK_TEXT_BUFFER_CLASS (lypad_text_buffer_parent_class)->remove_tag (buffer, tag, start, end);
}

void lypad_text_buffer_clear_selection (LypadTextBuffer *buffer)
{
    GtkTextBuffer *text_buffer = GTK_TEXT_BUFFER (buffer);
    if (gtk_text_buffer_get_selection_bounds (text_buffer, NULL, NULL))
    {
        GtkTextIter iter;
        gtk_text_buffer_get_iter_at_mark (text_buffer, &iter, gtk_text_buffer_get_insert (text_buffer));
        gtk_text_buffer_place_cursor (text_buffer, &iter);
    }
}

void lypad_text_buffer_set_edit_mode (LypadTextBuffer *buffer, gboolean value)
{
    g_return_if_fail (LYPAD_IS_TEXT_BUFFER (buffer));
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    if (value != priv->edit_mode)
        priv->edit_mode = value;
}

void lypad_text_buffer_notify_bkg_color (LypadTextBuffer *buffer, const GdkRGBA *color)
{
    g_return_if_fail (LYPAD_IS_TEXT_BUFFER (buffer));
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    GdkRGBA auto_color;
    lypad_color_utils_get_text_color_for_background (color, &auto_color);
    g_object_set (G_OBJECT (priv->automatic_color_tag), "foreground-rgba", &auto_color, NULL);
}

gboolean lypad_text_buffer_can_undo (LypadTextBuffer *buffer)
{
    g_return_val_if_fail (LYPAD_IS_TEXT_BUFFER (buffer), FALSE);
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    return lypad_text_history_can_undo (priv->history);
}

gboolean lypad_text_buffer_can_redo (LypadTextBuffer *buffer)
{
    g_return_val_if_fail (LYPAD_IS_TEXT_BUFFER (buffer), FALSE);
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    return lypad_text_history_can_redo (priv->history);
}

void lypad_text_buffer_undo (LypadTextBuffer *buffer)
{
    g_return_if_fail (LYPAD_IS_TEXT_BUFFER (buffer));
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    lypad_text_history_undo (priv->history);
}

void lypad_text_buffer_redo (LypadTextBuffer *buffer)
{
    g_return_if_fail (LYPAD_IS_TEXT_BUFFER (buffer));
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    lypad_text_history_redo (priv->history);
}

void lypad_text_buffer_set_toggle_tag (LypadTextBuffer *buffer, LypadToggleTag tag, gboolean enable)
{
    g_return_if_fail (LYPAD_IS_TEXT_BUFFER (buffer));
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    GtkTextIter start, end;
    gtk_text_buffer_begin_user_action (GTK_TEXT_BUFFER (buffer));

    priv->insert_toggle_status[tag] = enable;

    if (gtk_text_buffer_get_selection_bounds (GTK_TEXT_BUFFER (buffer), &start, &end))
    {
        if (enable)
            gtk_text_buffer_apply_tag (GTK_TEXT_BUFFER (buffer), priv->toggle_tags[tag], &start, &end);
        else
            gtk_text_buffer_remove_tag (GTK_TEXT_BUFFER (buffer), priv->toggle_tags[tag], &start, &end);
    }

    gtk_text_buffer_end_user_action (GTK_TEXT_BUFFER (buffer));
}

static GtkTextTag *lypad_text_buffer_add_font_tag (LypadTextBuffer *buffer, const char *font)
{
    //FIXME: Merge inside lypad_text_buffer_get_font_tag
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    GtkTextTag *tag = gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (buffer), NULL, "family", font, NULL);
    // To avoid precedence over the font used for bullets
    gtk_text_tag_set_priority (tag, 0);
    g_object_set_qdata (G_OBJECT (tag), tag_type_quark, GINT_TO_POINTER (TAG_TYPE_FONT));
    g_object_set_qdata_full (G_OBJECT (tag), tag_value_quark, g_strdup (font), g_free);
    priv->font_tags = g_list_append (priv->font_tags, tag);
    return tag;
}

static GtkTextTag *lypad_text_buffer_add_size_tag (LypadTextBuffer *buffer, int size)
{
    //FIXME: Merge inside lypad_text_buffer_get_size_tag
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    GtkTextTag *tag = gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (buffer), NULL, "size-points", (double) size, NULL);
    g_object_set_qdata (G_OBJECT (tag), tag_type_quark, GINT_TO_POINTER (TAG_TYPE_SIZE));
    g_object_set_qdata (G_OBJECT (tag), tag_value_quark, GINT_TO_POINTER (size));
    priv->size_tags = g_list_append (priv->size_tags, tag);
    return tag;
}

static GtkTextTag *lypad_text_buffer_add_color_tag (LypadTextBuffer *buffer, const GdkRGBA *color)
{
    //FIXME: Merge inside lypad_text_buffer_get_color_tag
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    GtkTextTag *tag  = gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (buffer), NULL, "foreground-rgba", color, NULL);
    g_object_set_qdata (G_OBJECT (tag), tag_type_quark, GINT_TO_POINTER (TAG_TYPE_COLOR));
    g_object_set_qdata_full (G_OBJECT (tag), tag_value_quark, gdk_rgba_copy (color), (GDestroyNotify) gdk_rgba_free);
    priv->color_tags = g_list_append (priv->color_tags, tag);
    return tag;
}

void lypad_text_buffer_set_font (LypadTextBuffer *buffer, const char *font)
{
    g_return_if_fail (LYPAD_IS_TEXT_BUFFER (buffer));
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    GtkTextIter start, end;
    gboolean has_selection = gtk_text_buffer_get_selection_bounds (GTK_TEXT_BUFFER (buffer), &start, &end);
    GtkTextTag *tag = NULL;
    gtk_text_buffer_begin_user_action (GTK_TEXT_BUFFER (buffer));

    //FIXME: Duplicate of lypad_text_buffer_get_font_tag
    for (GList *l = priv->font_tags; l != NULL; l = l->next)
        if (g_strcmp0 (font, g_object_get_qdata (l->data, tag_value_quark)) == 0)
        {
            tag = l->data;
            break;
        }
    if (!tag)
        tag = lypad_text_buffer_add_font_tag (buffer, font);

    priv->insert_font_tag = tag;
    if (has_selection)
        gtk_text_buffer_apply_tag (GTK_TEXT_BUFFER (buffer), tag, &start, &end);

    gtk_text_buffer_end_user_action (GTK_TEXT_BUFFER (buffer));
}

void lypad_text_buffer_set_size (LypadTextBuffer *buffer, int size)
{
    g_return_if_fail (LYPAD_IS_TEXT_BUFFER (buffer));
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    GtkTextIter start, end;
    gboolean has_selection = gtk_text_buffer_get_selection_bounds (GTK_TEXT_BUFFER (buffer), &start, &end);
    GtkTextTag *tag = NULL;
    gtk_text_buffer_begin_user_action (GTK_TEXT_BUFFER (buffer));

    // FIXME: dulpicate of lypad_text_buffer_get_size_tag
    for (GList *l = priv->size_tags; l != NULL; l = l->next)
        if (GPOINTER_TO_INT (g_object_get_qdata (l->data, tag_value_quark)) == size)
        {
            tag = l->data;
            break;
        }
    if (!tag)
        tag = lypad_text_buffer_add_size_tag (buffer, size);

    priv->insert_size_tag = tag;
    if (has_selection)
        gtk_text_buffer_apply_tag (GTK_TEXT_BUFFER (buffer), tag, &start, &end);

    gtk_text_buffer_end_user_action (GTK_TEXT_BUFFER (buffer));
}

void lypad_text_buffer_set_color (LypadTextBuffer *buffer, const GdkRGBA *color)
{
    g_return_if_fail (LYPAD_IS_TEXT_BUFFER (buffer));
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    GtkTextIter start, end;
    gboolean has_selection = gtk_text_buffer_get_selection_bounds (GTK_TEXT_BUFFER (buffer), &start, &end);
    GtkTextTag *tag = NULL;
    gtk_text_buffer_begin_user_action (GTK_TEXT_BUFFER (buffer));

    //FIMXE: Duplicate code lypad_text_buffer_get_color_tag
    for (GList *l = priv->color_tags; l != NULL; l = l->next)
        if (gdk_rgba_equal (g_object_get_qdata (l->data, tag_value_quark), color))
        {
            tag = l->data;
            break;
        }
    if (!tag)
        tag = lypad_text_buffer_add_color_tag (buffer, color);

    priv->insert_color_tag = tag;
    if (has_selection)
        gtk_text_buffer_apply_tag (GTK_TEXT_BUFFER (buffer), tag, &start, &end);

    gtk_text_buffer_end_user_action (GTK_TEXT_BUFFER (buffer));
}

void lypad_text_buffer_set_font_full (LypadTextBuffer *buffer, const char *font, GtkTextIter *start, GtkTextIter *end)
{
    g_return_if_fail (LYPAD_IS_TEXT_BUFFER (buffer));
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    GtkTextTag *tag = NULL;
    gtk_text_buffer_begin_user_action (GTK_TEXT_BUFFER (buffer));

    //FIMXE: Duplicate code lypad_text_buffer_get_font_tag
    for (GList *l = priv->font_tags; l != NULL; l = l->next)
        if (!tag && g_strcmp0 (font, g_object_get_qdata (l->data, tag_value_quark)) == 0)
            tag = l->data;
    if (!tag)
        tag = lypad_text_buffer_add_font_tag (buffer, font);

    gtk_text_buffer_apply_tag (GTK_TEXT_BUFFER (buffer), tag, start, end);

    gtk_text_buffer_end_user_action (GTK_TEXT_BUFFER (buffer));
}

void lypad_text_buffer_set_size_full (LypadTextBuffer *buffer, int size, GtkTextIter *start, GtkTextIter *end)
{
    g_return_if_fail (LYPAD_IS_TEXT_BUFFER (buffer));
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    GtkTextTag *tag = NULL;
    gtk_text_buffer_begin_user_action (GTK_TEXT_BUFFER (buffer));

    //FIMXE: Duplicate code lypad_text_buffer_get_size_tag
    for (GList *l = priv->size_tags; l != NULL; l = l->next)
        if (!tag && GPOINTER_TO_INT (g_object_get_qdata (l->data, tag_value_quark)) == size)
            tag = l->data;
    if (!tag)
        tag = lypad_text_buffer_add_size_tag (buffer, size);

    gtk_text_buffer_apply_tag (GTK_TEXT_BUFFER (buffer), tag, start, end);

    gtk_text_buffer_end_user_action (GTK_TEXT_BUFFER (buffer));
}

void lypad_text_buffer_set_color_full (LypadTextBuffer *buffer, const GdkRGBA *color, GtkTextIter *start, GtkTextIter *end)
{
    g_return_if_fail (LYPAD_IS_TEXT_BUFFER (buffer));
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    GtkTextTag *tag = NULL;
    gtk_text_buffer_begin_user_action (GTK_TEXT_BUFFER (buffer));

    //FIMXE: Duplicate code lypad_text_buffer_get_color_tag
    for (GList *l = priv->color_tags; l != NULL; l = l->next)
        if (!tag && gdk_rgba_equal (g_object_get_qdata (l->data, tag_value_quark), color))
            tag = l->data;
    if (!tag)
        tag = lypad_text_buffer_add_color_tag (buffer, color);

    gtk_text_buffer_apply_tag (GTK_TEXT_BUFFER (buffer), tag, start, end);

    gtk_text_buffer_end_user_action (GTK_TEXT_BUFFER (buffer));
}

const char *lypad_text_buffer_get_font (LypadTextBuffer *buffer)
{
    g_return_val_if_fail (LYPAD_IS_TEXT_BUFFER (buffer), NULL);
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    if (priv->selection_font)
        return g_object_get_qdata (G_OBJECT (priv->selection_font), tag_value_quark);
    return NULL;
}

int lypad_text_buffer_get_size (LypadTextBuffer *buffer)
{
    g_return_val_if_fail (LYPAD_IS_TEXT_BUFFER (buffer), 0);
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    if (priv->selection_size)
        return GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (priv->selection_size), tag_value_quark));
    return 0;
}

const GdkRGBA *lypad_text_buffer_get_color (LypadTextBuffer *buffer)
{
    g_return_val_if_fail (LYPAD_IS_TEXT_BUFFER (buffer), NULL);
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    if (priv->selection_color)
        return g_object_get_qdata (G_OBJECT (priv->selection_color), tag_value_quark);
    return NULL;
}

gboolean lypad_text_buffer_get_toggle_tag (LypadTextBuffer *buffer, LypadToggleTag tag)
{
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    return priv->selection_toggle_status[tag];
}

const char *lypad_text_buffer_get_insert_font (LypadTextBuffer *buffer)
{
    g_return_val_if_fail (LYPAD_IS_TEXT_BUFFER (buffer), NULL);
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    if (priv->insert_font_tag)
        return g_object_get_qdata (G_OBJECT (priv->insert_font_tag), tag_value_quark);
    return NULL;
}

int lypad_text_buffer_get_insert_size (LypadTextBuffer *buffer)
{
    g_return_val_if_fail (LYPAD_IS_TEXT_BUFFER (buffer), 0);
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    if (priv->insert_size_tag)
        return GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (priv->insert_size_tag), tag_value_quark));
    return 0;
}

const GdkRGBA *lypad_text_buffer_get_insert_color (LypadTextBuffer *buffer)
{
    g_return_val_if_fail (LYPAD_IS_TEXT_BUFFER (buffer), NULL);
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    if (priv->insert_color_tag)
        return g_object_get_qdata (G_OBJECT (priv->insert_color_tag), tag_value_quark);
    return NULL;
}

static void lypad_text_buffer_line_set_bullet (LypadTextBuffer *buffer, GtkTextIter *line, GtkTextTag *tag)
{
    GtkTextIter iter = *line;
    gtk_text_iter_set_line_offset (&iter, 0);
    gboolean done = FALSE;
    GSList *iter_tags = gtk_text_iter_get_tags (&iter);
    for (GSList *l = iter_tags; l != NULL; l = l->next)
    {
        int tag_type = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (l->data), tag_type_quark));
        if (tag_type == TAG_TYPE_LIST)
        {
            if ((GtkTextTag *) l->data == tag)
                done = TRUE;
            else
            {
                GtkTextIter end = iter;
                gtk_text_iter_forward_to_tag_toggle (&end, l->data);
                gtk_text_buffer_delete (GTK_TEXT_BUFFER (buffer), &iter, &end);
            }
            break;
        }
    }
    g_slist_free (iter_tags);
    if (done)
        return;

    LypadTextListInfo *info = g_object_get_qdata (G_OBJECT (tag), tag_value_quark);
    char bullet_str[6];
    int bullet_strlen = g_unichar_to_utf8 (info->u.bullet, bullet_str);
    gtk_text_buffer_insert (GTK_TEXT_BUFFER (buffer), &iter, bullet_str, bullet_strlen);
    gtk_text_buffer_insert (GTK_TEXT_BUFFER (buffer), &iter, "\t", 1);

    GtkTextIter end = iter;
    gtk_text_iter_set_line_index (&iter, 0);

    gtk_text_buffer_apply_tag (GTK_TEXT_BUFFER (buffer), tag, &iter, &end);
    *line = end;
}

static void lypad_text_buffer_real_insert_text (GtkTextBuffer *buffer, GtkTextIter *location, const char *text, gint len)
{
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (LYPAD_TEXT_BUFFER (buffer));
    int start_offset = gtk_text_iter_get_offset (location);
    GTK_TEXT_BUFFER_CLASS (lypad_text_buffer_parent_class)->insert_text (buffer, location, text, len);

    GtkTextIter start;
    gtk_text_buffer_get_iter_at_offset (buffer, &start, start_offset);
    lypad_text_history_insert_text (priv->history, &start, location, text, len);

    gtk_text_buffer_remove_all_tags (buffer, &start, location);

    //TODO: Skip this if location is not at cursor

    gboolean style_changed = FALSE;

    for (int i = 0; i < LYPAD_N_TOGGLE_TAGS; ++i)
    {
        if (priv->insert_toggle_status[i])
            gtk_text_buffer_apply_tag (buffer, priv->toggle_tags[i], &start, location);
        if (priv->insert_toggle_status[i] != priv->selection_toggle_status[i])
        {
            priv->selection_toggle_status[i] = priv->insert_toggle_status[i];
            style_changed = TRUE;
        }
    }

    if (priv->insert_font_tag)
        gtk_text_buffer_apply_tag (buffer, priv->insert_font_tag, &start, location);
    if (priv->insert_size_tag)
        gtk_text_buffer_apply_tag (buffer, priv->insert_size_tag, &start, location);
    if (priv->insert_color_tag)
        gtk_text_buffer_apply_tag (buffer, priv->insert_color_tag, &start, location);

    if (priv->insert_font_tag != priv->selection_font)
    {
        priv->selection_font = priv->insert_font_tag;
        style_changed = TRUE;
    }
    if (priv->insert_size_tag != priv->selection_size)
    {
        priv->selection_size = priv->insert_size_tag;
        style_changed = TRUE;
    }
    if (priv->insert_color_tag != priv->selection_color)
    {
        priv->selection_color = priv->insert_color_tag;
        style_changed = TRUE;
    }

    if (style_changed)
        g_signal_emit (buffer, signals[SIGNAL_STYLE_CHANGED], 0);
}

static void lypad_text_buffer_insert_text (GtkTextBuffer *buffer, GtkTextIter *location, const char *text, gint len)
{
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (LYPAD_TEXT_BUFFER (buffer));
    lypad_text_history_begin_user_action (priv->history);

    GSList *tag_list = NULL;
    GtkTextTag *bullet_tag = NULL;

    // Check if inserting new line on a bullet list
    if (strcmp (text, "\n") == 0)
    {
        GdkEvent *event = gtk_get_current_event ();
        if (event && event->type == GDK_KEY_PRESS && event->key.keyval == GDK_KEY_Return)
        {
            GtkTextIter start = *location;
            gtk_text_iter_set_line_offset (&start, 0);

            tag_list = gtk_text_iter_get_tags (&start);
            bullet_tag = NULL;
            for (GSList *l = tag_list; l != NULL; l = l->next)
            {
                int tag_type = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (l->data), tag_type_quark));
                if (tag_type == TAG_TYPE_LIST)
                {
                    bullet_tag = l->data;
                    break;
                }
            }
            if (bullet_tag)
            {
                GtkTextIter end = start;
                gtk_text_iter_forward_to_tag_toggle (&end, bullet_tag);
                if (gtk_text_iter_ends_line (&end)
                    && (gtk_text_iter_in_range (location, &start, &end) || gtk_text_iter_equal (location, &end)))
                {
                    lypad_text_history_delete_text (priv->history, &start, location);
                    GTK_TEXT_BUFFER_CLASS (lypad_text_buffer_parent_class)->delete_range (buffer, &start, location);
                    goto end_insert;
                }
                else
                {
                    lypad_text_buffer_real_insert_text (buffer, location, text, len);
                    lypad_text_buffer_line_set_bullet (LYPAD_TEXT_BUFFER (buffer), location, bullet_tag);
                    goto end_insert;
                }
            }
            g_slist_free (tag_list);
        }
    }

    // Check if overwriting a bullet
    tag_list = gtk_text_iter_get_tags (location);
    bullet_tag = NULL;
    for (GSList *l = tag_list; l != NULL; l = l->next)
    {
        int tag_type = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (l->data), tag_type_quark));
        if (tag_type == TAG_TYPE_LIST)
        {
            bullet_tag = l->data;
            break;
        }
    }
    if (bullet_tag)
    {
        GtkTextIter range_end = *location;
        gtk_text_iter_set_line_offset (location, 0);
        gtk_text_iter_forward_to_tag_toggle (&range_end, bullet_tag);
        lypad_text_history_delete_text (priv->history, location, &range_end);
        GTK_TEXT_BUFFER_CLASS (lypad_text_buffer_parent_class)->delete_range (buffer, location, &range_end);
    }
    g_slist_free (tag_list);

    lypad_text_buffer_real_insert_text (buffer, location, text, len);

end_insert:
    lypad_text_history_end_user_action (priv->history);
}

static void lypad_text_buffer_update_style (LypadTextBuffer *buffer)
{
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    GtkTextIter iter1, iter2;
    gtk_text_buffer_get_bounds (GTK_TEXT_BUFFER (buffer), &iter1, &iter2);

    priv->selection_font = priv->insert_font_tag;
    priv->selection_size = priv->insert_size_tag;
    priv->selection_color = priv->insert_color_tag;

    for (int i = 0; i < LYPAD_N_TOGGLE_TAGS; ++i)
        priv->selection_toggle_status[i] = priv->insert_toggle_status[i];
    if (G_LIKELY (!gtk_text_iter_equal (&iter1, &iter2)))
    {
        gboolean has_selection = gtk_text_buffer_get_selection_bounds (GTK_TEXT_BUFFER (buffer), &iter1, &iter2);
        GtkTextIter it = iter1;
        if (!has_selection)
            gtk_text_iter_backward_char (&it);

        for (int i = 0; i < LYPAD_N_TOGGLE_TAGS; ++i)
        {
            priv->insert_toggle_status[i] = FALSE;
            priv->selection_toggle_status[i] = FALSE;
        }

        GSList *iter_tags = gtk_text_iter_get_tags (&it);
        for (GSList *l = iter_tags; l != NULL; l = l->next)
        {
            int tag_type = GPOINTER_TO_INT (g_object_get_qdata (l->data, tag_type_quark));
            switch (tag_type)
            {
                case TAG_TYPE_FONT:
                    priv->insert_font_tag = l->data;
                    priv->selection_font = l->data;
                    break;

                case TAG_TYPE_SIZE:
                    priv->insert_size_tag = l->data;
                    priv->selection_size = l->data;
                    break;

                case TAG_TYPE_COLOR:
                    priv->insert_color_tag = l->data;
                    priv->selection_color = l->data;
                    break;

                case TAG_TYPE_TOGGLE:
                {
                    int toggle_type = GPOINTER_TO_INT (g_object_get_qdata (l->data, tag_value_quark));
                    priv->insert_toggle_status[toggle_type] = TRUE;
                    priv->selection_toggle_status[toggle_type] = TRUE;
                    break;
                }

                case TAG_TYPE_LIST:
                    break;

                default:
                    g_warn_if_reached ();
            }
        }
        g_slist_free (iter_tags);

        if (has_selection)
        {
            while (gtk_text_iter_forward_to_tag_toggle (&it, NULL) && gtk_text_iter_compare (&it, &iter2) < 0)
            {
                if (priv->selection_font && !gtk_text_iter_has_tag (&it, priv->selection_font))
                    priv->selection_font = NULL;

                if (priv->selection_size && !gtk_text_iter_has_tag (&it, priv->selection_size))
                    priv->selection_size = NULL;

                if (priv->selection_color && !gtk_text_iter_has_tag (&it, priv->selection_color))
                    priv->selection_color = NULL;

                for (int i = 0; i < LYPAD_N_TOGGLE_TAGS; ++i)
                    if (priv->selection_toggle_status[i] && !gtk_text_iter_has_tag (&it, priv->toggle_tags[i]))
                        priv->selection_toggle_status[i] = FALSE;
            }
        }
    }
    g_signal_emit (buffer, signals[SIGNAL_STYLE_CHANGED], 0);
}

static void lypad_text_buffer_mark_set (GtkTextBuffer *text_buffer, const GtkTextIter *location, GtkTextMark *mark)
{
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (LYPAD_TEXT_BUFFER (text_buffer));
    if (!priv->edit_mode)
        GTK_TEXT_BUFFER_CLASS (lypad_text_buffer_parent_class)->mark_set (text_buffer, location, mark);

    if (!priv->ignore_mark_set)
    {
        if (mark == gtk_text_buffer_get_insert (text_buffer))
        {
            GtkTextIter curr_iter, prev_iter;
            gtk_text_buffer_get_iter_at_mark (text_buffer, &curr_iter, mark);
            gtk_text_buffer_get_iter_at_mark (text_buffer, &prev_iter, priv->prev_insert_mark);
            GSList *tag_list = gtk_text_iter_get_tags (&curr_iter);
            GtkTextTag *bullet_tag = NULL;
            for (GSList *l = tag_list; l != NULL; l = l->next)
            {
                int tag_type = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (l->data), tag_type_quark));
                if (tag_type == TAG_TYPE_LIST)
                {
                    bullet_tag = l->data;
                    break;
                }
            }

            if (bullet_tag)
            {
                GdkEvent *event = gtk_get_current_event ();
                if (event
                    && (event->type == GDK_KEY_PRESS || event->type == GDK_KEY_RELEASE)
                    && event->key.keyval == GDK_KEY_Right
                    && gtk_text_iter_get_line_offset (&prev_iter) == 0
                    && gtk_text_iter_get_line (&prev_iter) == gtk_text_iter_get_line (&curr_iter))
                {
                    gtk_text_iter_forward_to_tag_toggle (&curr_iter, bullet_tag);
                }
                else
                    gtk_text_iter_set_line_offset (&curr_iter, 0);

                priv->ignore_mark_set = TRUE;
                gtk_text_buffer_move_mark (text_buffer, gtk_text_buffer_get_insert (text_buffer), &curr_iter);
                priv->ignore_mark_set = FALSE;
            }
            g_slist_free (tag_list);

            priv->ignore_mark_set = TRUE;
            gtk_text_buffer_move_mark (text_buffer, priv->prev_insert_mark, &curr_iter);
            priv->ignore_mark_set = FALSE;

            lypad_text_buffer_update_style (LYPAD_TEXT_BUFFER (text_buffer));
        }
        else if (mark ==  gtk_text_buffer_get_selection_bound (text_buffer))
        {
            GtkTextIter curr_iter, prev_iter;
            gtk_text_buffer_get_iter_at_mark (text_buffer, &curr_iter, mark);
            gtk_text_buffer_get_iter_at_mark (text_buffer, &prev_iter, priv->prev_selection_mark);
            GSList *tag_list = gtk_text_iter_get_tags (&curr_iter);
            GtkTextTag *bullet_tag = NULL;
            for (GSList *l = tag_list; l != NULL; l = l->next)
            {
                int tag_type = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (l->data), tag_type_quark));
                if (tag_type == TAG_TYPE_LIST)
                {
                    bullet_tag = l->data;
                    break;
                }
            }

            if (bullet_tag)
            {
                GdkEvent *event = gtk_get_current_event ();
                if (event
                    && (event->type == GDK_KEY_PRESS || event->type == GDK_KEY_RELEASE)
                    && event->key.keyval == GDK_KEY_Right
                    && gtk_text_iter_get_line_offset (&prev_iter) == 0
                    && gtk_text_iter_get_line (&prev_iter) == gtk_text_iter_get_line (&curr_iter))
                {
                    gtk_text_iter_forward_to_tag_toggle (&curr_iter, bullet_tag);
                }
                else
                    gtk_text_iter_set_line_offset (&curr_iter, 0);

                priv->ignore_mark_set = TRUE;
                gtk_text_buffer_move_mark (text_buffer, gtk_text_buffer_get_selection_bound (text_buffer), &curr_iter);
                priv->ignore_mark_set = FALSE;
            }
            g_slist_free (tag_list);

            priv->ignore_mark_set = TRUE;
            gtk_text_buffer_move_mark (text_buffer, priv->prev_selection_mark, &curr_iter);
            priv->ignore_mark_set = FALSE;

            lypad_text_buffer_update_style (LYPAD_TEXT_BUFFER (text_buffer));
        }
    }
}

static void lypad_text_buffer_delete_range (GtkTextBuffer *buffer, GtkTextIter *start, GtkTextIter *end)
{
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (LYPAD_TEXT_BUFFER (buffer));

    // Check if deleting a bullet
    GSList *tag_list = gtk_text_iter_get_tags (start);
    for (GSList *l = tag_list; l != NULL; l = l->next)
    {
        int tag_type = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (l->data), tag_type_quark));
        if (tag_type == TAG_TYPE_LIST)
        {
            gtk_text_iter_set_line_offset (start, 0);
            break;
        }
    }
    g_slist_free (tag_list);
    tag_list = gtk_text_iter_get_tags (end);
    for (GSList *l = tag_list; l != NULL; l = l->next)
    {
        int tag_type = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (l->data), tag_type_quark));
        if (tag_type == TAG_TYPE_LIST)
        {
            gtk_text_iter_forward_to_tag_toggle (end, l->data);
            break;
        }
    }
    g_slist_free (tag_list);

    lypad_text_history_delete_text (priv->history, start, end);
    GTK_TEXT_BUFFER_CLASS (lypad_text_buffer_parent_class)->delete_range (buffer, start, end);
    lypad_text_buffer_update_style (LYPAD_TEXT_BUFFER (buffer));
}

static GList *lypad_text_buffer_import_tag (LypadTextBuffer *buffer, GtkTextTag *tag)
{
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    GList *tag_list = NULL;
    gboolean font_set;
    gboolean size_set;
    gboolean color_set;

    gboolean weight_set;
    gboolean style_set;
    gboolean underline_set;
    gboolean strikethrough_set;

    g_object_get (tag, "family-set", &font_set, "size-set", &size_set, "foreground-set", &color_set,
                  "weight-set", &weight_set, "style-set", &style_set, "underline-set", &underline_set,
                  "strikethrough-set", &strikethrough_set, NULL);
    if (font_set)
    {
        char *font;
        g_object_get (tag, "family", &font, NULL);
        //FIXME: Should use lypad_text_buffer_get_font_tag (to avoid duplicating existing tags)
        tag_list = g_list_append (tag_list, lypad_text_buffer_add_font_tag (buffer, font));
        g_free (font);
    }
    if (size_set)
    {
        double size;
        g_object_get (tag, "size-points", &size, NULL);
        //FIXME: Should use lypad_text_buffer_get_size_tag (to avoid duplicating existing tags)
        tag_list = g_list_append (tag_list, lypad_text_buffer_add_size_tag (buffer, size));
    }
    if (color_set)
    {
        GdkRGBA *color;
        g_object_get (tag, "foreground-rgba", &color, NULL);
        //FIXME: Should use lypad_text_buffer_get_color_tag (to avoid duplicating existing tags)
        tag_list = g_list_append (tag_list, lypad_text_buffer_add_color_tag (buffer, color));
        gdk_rgba_free (color);
    }
    if (weight_set)
    {
        int weight;
        g_object_get (tag, "weight", &weight, NULL);
        if (weight == PANGO_WEIGHT_BOLD)
            tag_list = g_list_append (tag_list, priv->toggle_tags[BOLD_TAG]);
    }
    if (style_set)
    {
        PangoStyle style;
        g_object_get (tag, "style", &style, NULL);
        if (style == PANGO_STYLE_ITALIC)
            tag_list = g_list_append (tag_list, priv->toggle_tags[ITALIC_TAG]);
    }
    if (underline_set)
    {
        PangoUnderline underline;
        g_object_get (tag, "underline", &underline, NULL);
        if (underline == PANGO_UNDERLINE_SINGLE)
            tag_list = g_list_append (tag_list, priv->toggle_tags[UNDERLINE_TAG]);
    }
    if (strikethrough_set)
    {
        gboolean strikethrough;
        g_object_get (tag, "strikethrough", &strikethrough, NULL);
        if (strikethrough)
            tag_list = g_list_append (tag_list, priv->toggle_tags[STRIKETHROUGH_TAG]);
    }
    return tag_list;
}

static void lypad_text_buffer_remove_tags_of_type (GtkTextBuffer *text_buffer, GtkTextTag *tag, const GtkTextIter *start, const GtkTextIter *end)
{
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (LYPAD_TEXT_BUFFER (text_buffer));
    int tag_type = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (tag), tag_type_quark));
    int value = 0;
    if (tag_type == TAG_TYPE_TOGGLE)
        value = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (tag), tag_value_quark));

    GtkTextTag *existing_tag = NULL;
    GtkTextIter tag_start, tag_end;
    tag_start = *start;

    while (gtk_text_iter_compare (&tag_start, end) < 0)
    {
        GSList *iter_tags = gtk_text_iter_get_tags (&tag_start);
        for (GSList *l = iter_tags; l != NULL; l = l->next)
        {
            if (GPOINTER_TO_INT (g_object_get_qdata (l->data, tag_type_quark)) == tag_type)
            {
                if (tag_type != TAG_TYPE_TOGGLE || GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (l->data), tag_value_quark)) == value)
                {
                    existing_tag = l->data;
                    break;
                }
            }
        }
        g_slist_free (iter_tags);

        if (existing_tag)
        {
            tag_end = tag_start;
            gtk_text_iter_forward_to_tag_toggle (&tag_end, existing_tag);
            if (gtk_text_iter_compare (&tag_end, end) > 0)
                tag_end = *end;
            GTK_TEXT_BUFFER_CLASS (lypad_text_buffer_parent_class)->remove_tag (text_buffer, existing_tag, &tag_start, &tag_end);
            lypad_text_history_remove_tag (priv->history, existing_tag, &tag_start, &tag_end);
            tag_start = tag_end;
            existing_tag = NULL;
        }
        else
            gtk_text_iter_forward_to_tag_toggle (&tag_start, NULL);
    }
}

static void lypad_text_buffer_apply_tag (GtkTextBuffer *text_buffer, GtkTextTag *tag, const GtkTextIter *start, const GtkTextIter *end)
{
    gtk_text_buffer_set_modified (text_buffer, TRUE);
    gtk_text_buffer_begin_user_action (text_buffer);
    gpointer tag_type = g_object_get_qdata (G_OBJECT (tag), tag_type_quark);
    if (tag_type)
    {
        LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (LYPAD_TEXT_BUFFER (text_buffer));
        lypad_text_buffer_remove_tags_of_type (text_buffer, tag, start, end);
        GTK_TEXT_BUFFER_CLASS (lypad_text_buffer_parent_class)->apply_tag (text_buffer, tag, start, end);
        lypad_text_history_apply_tag (priv->history, tag, start, end);
    }
    else
    {
        GList *tag_list = lypad_text_buffer_import_tag (LYPAD_TEXT_BUFFER (text_buffer), tag);
        for (GList *l = tag_list; l != NULL; l = l->next)
            lypad_text_buffer_apply_tag (text_buffer, l->data, start, end);
        g_list_free (tag_list);
    }
    gtk_text_buffer_end_user_action (text_buffer);
}

static void lypad_text_buffer_remove_tag (GtkTextBuffer *text_buffer, GtkTextTag *tag, const GtkTextIter *start, const GtkTextIter *end)
{
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (LYPAD_TEXT_BUFFER (text_buffer));
    gtk_text_buffer_set_modified (text_buffer, TRUE);
    GTK_TEXT_BUFFER_CLASS (lypad_text_buffer_parent_class)->remove_tag (text_buffer, tag, start, end);
    lypad_text_history_remove_tag (priv->history, tag, start, end);
}

static void lypad_text_buffer_constructed (GObject *object)
{
    LypadTextBuffer *buffer = LYPAD_TEXT_BUFFER (object);
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    G_OBJECT_CLASS (lypad_text_buffer_parent_class)->constructed (object);

    priv->toggle_tags[BOLD_TAG] = gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (buffer), "bold", "weight", PANGO_WEIGHT_BOLD, NULL);
    priv->toggle_tags[ITALIC_TAG] = gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (buffer), "italic", "style", PANGO_STYLE_ITALIC, NULL);
    priv->toggle_tags[UNDERLINE_TAG] = gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (buffer), "underline", "underline", PANGO_UNDERLINE_SINGLE, NULL);
    priv->toggle_tags[STRIKETHROUGH_TAG] = gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (buffer), "strikethrough", "strikethrough", TRUE, NULL);
    g_object_set_qdata (G_OBJECT (priv->toggle_tags[BOLD_TAG]), tag_type_quark, GINT_TO_POINTER (TAG_TYPE_TOGGLE));
    g_object_set_qdata (G_OBJECT (priv->toggle_tags[ITALIC_TAG]), tag_type_quark, GINT_TO_POINTER (TAG_TYPE_TOGGLE));
    g_object_set_qdata (G_OBJECT (priv->toggle_tags[UNDERLINE_TAG]), tag_type_quark, GINT_TO_POINTER (TAG_TYPE_TOGGLE));
    g_object_set_qdata (G_OBJECT (priv->toggle_tags[STRIKETHROUGH_TAG]), tag_type_quark, GINT_TO_POINTER (TAG_TYPE_TOGGLE));
    g_object_set_qdata (G_OBJECT (priv->toggle_tags[BOLD_TAG]), tag_value_quark, GINT_TO_POINTER (BOLD_TAG));
    g_object_set_qdata (G_OBJECT (priv->toggle_tags[ITALIC_TAG]), tag_value_quark, GINT_TO_POINTER (ITALIC_TAG));
    g_object_set_qdata (G_OBJECT (priv->toggle_tags[UNDERLINE_TAG]), tag_value_quark, GINT_TO_POINTER (UNDERLINE_TAG));
    g_object_set_qdata (G_OBJECT (priv->toggle_tags[STRIKETHROUGH_TAG]), tag_value_quark, GINT_TO_POINTER (STRIKETHROUGH_TAG));

    GtkTextIter iter;
    GtkTextMark *mark = gtk_text_buffer_get_insert (GTK_TEXT_BUFFER (buffer));
    gtk_text_buffer_get_iter_at_mark (GTK_TEXT_BUFFER (buffer), &iter, mark);
    priv->prev_insert_mark = gtk_text_buffer_create_mark (GTK_TEXT_BUFFER (buffer), NULL, &iter, gtk_text_mark_get_left_gravity (mark));
    mark = gtk_text_buffer_get_selection_bound (GTK_TEXT_BUFFER (buffer));
    gtk_text_buffer_get_iter_at_mark (GTK_TEXT_BUFFER (buffer), &iter, mark);
    priv->prev_selection_mark = gtk_text_buffer_create_mark (GTK_TEXT_BUFFER (buffer), NULL, &iter, gtk_text_mark_get_left_gravity (mark));

    GdkRGBA auto_color = {0, 0, 0, 1};
    priv->automatic_color_tag = gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (buffer), NULL, "foreground-rgba", &auto_color, NULL);
    g_object_set_qdata (G_OBJECT (priv->automatic_color_tag), tag_type_quark, GINT_TO_POINTER (TAG_TYPE_COLOR));
    g_object_set_qdata_full (G_OBJECT (priv->automatic_color_tag), tag_value_quark, gdk_rgba_copy (&LYPAD_AUTOMATIC_TEXT_COLOR), (GDestroyNotify) gdk_rgba_free);
    priv->color_tags = g_list_append (priv->color_tags, priv->automatic_color_tag);

    lypad_text_buffer_set_font (buffer, "Sans");
    lypad_text_buffer_set_size (buffer, 10);
    lypad_text_buffer_set_color (buffer, &LYPAD_AUTOMATIC_TEXT_COLOR);
}

static void lypad_text_buffer_dispose (GObject *object)
{
    LypadTextBuffer *buffer = LYPAD_TEXT_BUFFER (object);
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    g_clear_pointer (&priv->font_tags, g_list_free);
    g_clear_pointer (&priv->size_tags, g_list_free);
    g_clear_pointer (&priv->color_tags, g_list_free);
    g_clear_object (&priv->history);
    G_OBJECT_CLASS (lypad_text_buffer_parent_class)->dispose (object);
}

static void lypad_text_buffer_begin_user_action (GtkTextBuffer *buffer)
{
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (LYPAD_TEXT_BUFFER (buffer));
    lypad_text_history_begin_user_action (priv->history);
}

static void lypad_text_buffer_end_user_action (GtkTextBuffer *buffer)
{
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (LYPAD_TEXT_BUFFER (buffer));
    lypad_text_history_end_user_action (priv->history);
}

static void lypad_text_buffer_class_init (LypadTextBufferClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GtkTextBufferClass *buffer_class = GTK_TEXT_BUFFER_CLASS (klass);

    object_class->constructed       = lypad_text_buffer_constructed;
    object_class->dispose           = lypad_text_buffer_dispose;
    buffer_class->insert_text       = lypad_text_buffer_insert_text;
    buffer_class->delete_range      = lypad_text_buffer_delete_range;
    buffer_class->mark_set          = lypad_text_buffer_mark_set;
    buffer_class->apply_tag         = lypad_text_buffer_apply_tag;
    buffer_class->remove_tag        = lypad_text_buffer_remove_tag;
    buffer_class->begin_user_action = lypad_text_buffer_begin_user_action;
    buffer_class->end_user_action   = lypad_text_buffer_end_user_action;

	signals[SIGNAL_STYLE_CHANGED] = g_signal_new ("style-changed", G_TYPE_FROM_CLASS (klass),
								                  G_SIGNAL_RUN_LAST, 0, NULL, NULL,
										          g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

    tag_type_quark = g_quark_from_string ("lypad-text-tag-type");
    tag_value_quark = g_quark_from_string ("lypad-text-tag-value");
}

LypadTextBuffer *lypad_text_buffer_new ()
{
    return g_object_new (LYPAD_TYPE_TEXT_BUFFER, NULL);
}

char *lypad_text_buffer_serialize (LypadTextBuffer *lypad_buffer, gsize *length)
{
    g_return_val_if_fail (LYPAD_IS_TEXT_BUFFER (lypad_buffer), NULL);
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (lypad_buffer);
    GtkTextBuffer *text_buffer = GTK_TEXT_BUFFER (lypad_buffer);
    GtkTextIter iter;
    GString *buffer = g_string_new (NULL);
    gtk_text_buffer_get_start_iter (text_buffer, &iter);

    if (gtk_text_iter_is_end (&iter))
    {
        *length = buffer->len;
        return g_string_free (buffer, FALSE);
    }

    // Serialize text
    g_string_append (buffer, "[text]\n");
    GSList *old_tags = NULL;
    GSList *new_tags = NULL;
    gunichar c;
    GSList *used_tags = NULL;
    int ignore_text_count = 0;
    do {
        new_tags = gtk_text_iter_get_tags (&iter);

        // Find ending_tags
        GSList *ending_tags = NULL;
        for (GSList *l = old_tags; l != NULL; l = l->next)
            if (!g_slist_find (new_tags, l->data))
                ending_tags = g_slist_append (ending_tags, l->data);

        for (GSList *l = ending_tags; l != NULL; l = l->next)
        {
            int tag_type = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (l->data), tag_type_quark));
            if (tag_type == TAG_TYPE_TOGGLE)
            {
                int i = 0;
                for (GSList *tag = used_tags; tag != NULL; tag = tag->next)
                {
                    if (l->data == tag->data)
                        break;
                    i += 1;
                }
                g_string_append_printf (buffer, "</%d>", i);
                break;
            }
            else if (tag_type == TAG_TYPE_LIST)
                ignore_text_count -= 1;
        }
        g_slist_free (ending_tags);

        // Find starting tags
        GSList *starting_tags = NULL;
        for (GSList *l = new_tags; l != NULL; l = l->next)
            if (!g_slist_find (old_tags, l->data))
                starting_tags = g_slist_append (starting_tags, l->data);

        for (GSList *l = starting_tags; l != NULL; l = l->next)
        {
            int i = 0;
            gboolean found = FALSE;
            for (GSList *tag = used_tags; tag != NULL; tag = tag->next)
            {
                if (l->data == tag->data)
                {
                    found = TRUE;
                    break;
                }
                i += 1;
            }
            g_string_append_printf (buffer, "<%d>", i);
            if (!found)
                used_tags = g_slist_append (used_tags, l->data);
            int tag_type = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (l->data), tag_type_quark));
            if (tag_type == TAG_TYPE_LIST)
                ignore_text_count += 1;
        }
        g_slist_free (starting_tags);

        g_slist_free (old_tags);
        old_tags = new_tags;

        // Escape text
        if (!ignore_text_count)
        {
            c = gtk_text_iter_get_char (&iter);
            switch (c)
            {
                case '<':
                    g_string_append (buffer, "&l");
                    c += 1;
                    break;
                case '>':
                    g_string_append (buffer, "&g");
                    c += 1;
                    break;
                case '&':
                    g_string_append (buffer, "&a");
                    c += 1;
                    break;
                default:
                    g_string_append_unichar (buffer, c);
            }
        }
    } while (gtk_text_iter_forward_char (&iter));

    // End unclosed tags
    for (GSList *l = old_tags; l != NULL; l = l->next)
    {
        int tag_type = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (l->data), tag_type_quark));
        if (tag_type == TAG_TYPE_TOGGLE)
        {
            int i = 0;
            for (GSList *tag = used_tags; tag != NULL; tag = tag->next)
            {
                if (l->data == tag->data)
                    break;
                i += 1;
            }
            g_string_append_printf (buffer, "</%d>", i);
            break;
        }
        else if (tag_type == TAG_TYPE_LIST)
            ignore_text_count -= 1;
    }
    g_slist_free (old_tags);

    // Serialize used tags
    GString *tag_buffer = g_string_new (NULL);
    g_string_append (tag_buffer, "[tags]\n");
    for (GSList *l = used_tags; l != NULL; l = l->next)
    {
        int tag_type = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (l->data), tag_type_quark));
        switch (tag_type)
        {
            case TAG_TYPE_TOGGLE:
            {
                switch (GPOINTER_TO_INT (g_object_get_qdata (l->data, tag_value_quark)))
                {
                    case BOLD_TAG:
                        g_string_append (tag_buffer, "bold\n");
                        break;
                    case ITALIC_TAG:
                        g_string_append (tag_buffer, "italic\n");
                        break;
                    case UNDERLINE_TAG:
                        g_string_append (tag_buffer, "underline\n");
                        break;
                    case STRIKETHROUGH_TAG:
                        g_string_append (tag_buffer, "strikethrough\n");
                        break;
                    default:
                        g_warn_if_reached ();
                }
                break;
            }

            case TAG_TYPE_FONT:
                g_string_append_printf (tag_buffer, "font: %s\n", (char*) g_object_get_qdata (l->data, tag_value_quark));
                break;

            case TAG_TYPE_SIZE:
                g_string_append_printf (tag_buffer, "size: %d\n", GPOINTER_TO_INT (g_object_get_qdata (l->data, tag_value_quark)));
                break;

            case TAG_TYPE_COLOR:
                if (l->data == priv->automatic_color_tag)
                    g_string_append (tag_buffer, "color: auto\n");
                else
                {
                    char *color_str = gdk_rgba_to_string (g_object_get_qdata (l->data, tag_value_quark));
                    g_string_append_printf (tag_buffer, "color: %s\n", color_str);
                    g_free (color_str);
                    break;
                }
                break;

            case TAG_TYPE_LIST:
            {
                LypadTextListInfo *info = g_object_get_qdata (l->data, tag_value_quark);
                switch (info->type)
                {
                    case LYPAD_TEXT_LIST_TYPE_BULLET:
                        g_string_append_printf (tag_buffer, "bullet: U+%06X\n", info->u.bullet);
                }
                break;
            }

            default:
                g_warn_if_reached ();
        }
    }
    g_string_append_c (tag_buffer, '\n');
    g_string_prepend_len (buffer, tag_buffer->str, tag_buffer->len);
    g_string_free (tag_buffer, TRUE);

    *length = buffer->len;
    return g_string_free (buffer, FALSE);
}

static char *unescape_text (const char *text, gsize length, gsize *out_len)
{
    GString *buffer = g_string_sized_new (length);
    const char *c = text;
    const char *next_c;
    while (c < text + length)
    {
        if (*c == '&')
        {
            c += 1;
            if (*c == 'l')
                g_string_append_c (buffer, '<');
            else if (*c == 'g')
                g_string_append_c (buffer, '>');
            else if (*c == 'a')
                g_string_append_c (buffer, '&');
            c += 1;
        }
        else
        {
            next_c = g_utf8_next_char (c);
            g_string_append_len (buffer, c, next_c - c);
            c = next_c;
        }
    }
    *out_len = buffer->len;
    return g_string_free (buffer, FALSE);
}

static void lypad_text_buffer_set_tag (LypadTextBuffer *buffer, GtkTextTag *tag)
{
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    int tag_type = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (tag), tag_type_quark));
    switch (tag_type)
    {
        case TAG_TYPE_FONT:
            priv->insert_font_tag = tag;
            break;

        case TAG_TYPE_SIZE:
            priv->insert_size_tag = tag;
            break;

        case TAG_TYPE_COLOR:
            priv->insert_color_tag = tag;
            break;

        case TAG_TYPE_TOGGLE:
        {
            int toggle_tag = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (tag), tag_value_quark));
            priv->insert_toggle_status[toggle_tag] = TRUE;
            break;
        }
    }
}

static void lypad_text_buffer_unset_tag (LypadTextBuffer *buffer, GtkTextTag *tag)
{
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    int tag_type = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (tag), tag_type_quark));
    switch (tag_type)
    {
        case TAG_TYPE_TOGGLE:
        {
            int toggle_tag = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (tag), tag_value_quark));
            priv->insert_toggle_status[toggle_tag] = FALSE;
            break;
        }
    }
}

//FIXME: Store tags inside buffer to avoid creating multiple tags with same values
// If the tag already exists return it directly
static GtkTextTag *lypad_text_buffer_create_bullet_tag (LypadTextBuffer *buffer, gunichar c)
{
    LypadTextListInfo *info = g_new (LypadTextListInfo, 1);
    info->type = LYPAD_TEXT_LIST_TYPE_BULLET;
    info->u.bullet = c;
    info->list_margin = 12;
    info->text_padding = 24;

    PangoTabArray *tab_array = pango_tab_array_new (1, TRUE);
    pango_tab_array_set_tab (tab_array, 0, PANGO_TAB_LEFT, info->text_padding);
    GtkTextTag *tag = gtk_text_buffer_create_tag (GTK_TEXT_BUFFER (buffer), NULL,
                                                  "family", "Lypad Symbols",
                                                  "left_margin", info->list_margin,
                                                  "indent", -info->text_padding,
                                                  "tabs", tab_array, NULL);

    g_object_set_qdata (G_OBJECT (tag), tag_type_quark, GINT_TO_POINTER (TAG_TYPE_LIST));
    g_object_set_qdata_full (G_OBJECT (tag), tag_value_quark, info, (GDestroyNotify) lypad_text_list_info_free);

    return tag;
}

static GtkTextTag *lypad_text_buffer_get_font_tag (LypadTextBuffer *buffer, const char *font)
{
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    for (GList *l = priv->font_tags; l != NULL; l = l->next)
        if (g_strcmp0 (font, g_object_get_qdata (l->data, tag_value_quark)) == 0)
            return l->data;
    return lypad_text_buffer_add_font_tag (buffer, font);
}

static GtkTextTag *lypad_text_buffer_get_size_tag (LypadTextBuffer *buffer, int size)
{
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    for (GList *l = priv->size_tags; l != NULL; l = l->next)
        if (size == GPOINTER_TO_INT (g_object_get_qdata (l->data, tag_value_quark)))
            return l->data;
    return lypad_text_buffer_add_size_tag (buffer, size);
}

static GtkTextTag *lypad_text_buffer_get_color_tag (LypadTextBuffer *buffer, GdkRGBA *color)
{
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (buffer);
    for (GList *l = priv->color_tags; l != NULL; l = l->next)
        if (gdk_rgba_equal (g_object_get_qdata (l->data, tag_value_quark), color))
            return l->data;
    return lypad_text_buffer_add_color_tag (buffer, color);
}

#define LINE_EQUAL_LITERAL(text, line, line_length) \
    (line_length == sizeof text - 1 && memcmp (text, line, sizeof text - 1) == 0)

#define LINE_STARTS_WITH_LITERAL(text, line, line_length) \
    (line_length >= sizeof text - 1 && memcmp (text, line, sizeof text - 1) == 0)

#define STR_LITERAL_LEN(text) \
    (sizeof text - 1)

gboolean lypad_text_buffer_deserialize (LypadTextBuffer *lypad_buffer, const char *data, gsize length)
{
    LypadTextBufferPrivate *priv = lypad_text_buffer_get_instance_private (lypad_buffer);
    gtk_text_buffer_set_text (GTK_TEXT_BUFFER (lypad_buffer), "", 0);
    for (int i = 0; i < LYPAD_N_TOGGLE_TAGS; ++i)
        priv->insert_toggle_status[i] = FALSE;

    const char *line = data;
    gsize remaining_len = length;
    if (length == 0)
        return TRUE;
    if (LINE_STARTS_WITH_LITERAL ("[tags]\n", data, length))
    {
        line += STR_LITERAL_LEN ("[tags]\n");
        remaining_len -= STR_LITERAL_LEN ("[tags]\n");
    }
    else
        return FALSE;

    lypad_text_history_lock (priv->history);
    const char *next_line;
    gsize line_len;

    GArray *tag_array = g_array_new (FALSE, FALSE, sizeof (GtkTextTag*));

    while (remaining_len > 0)
    {
        next_line = g_utf8_strchr (line, remaining_len, '\n');
        if (!next_line)
            break;
        line_len = next_line - line;

        if (LINE_STARTS_WITH_LITERAL ("font: ", line, line_len))
        {
            char *font = g_strndup (line + STR_LITERAL_LEN ("font: "), line_len - STR_LITERAL_LEN ("font: "));
            GtkTextTag *tag = lypad_text_buffer_get_font_tag (lypad_buffer, font);
            g_free (font);
            g_array_append_val (tag_array, tag);
        }
        else if (LINE_STARTS_WITH_LITERAL ("size: ", line, line_len))
        {
            int size;
            if (!sscanf (line, "size: %d\n", &size))
                goto deserialize_error;
            GtkTextTag *tag = lypad_text_buffer_get_size_tag (lypad_buffer, size);
            g_array_append_val (tag_array, tag);
        }
        else if (LINE_STARTS_WITH_LITERAL ("color: ", line, line_len))
        {
            GdkRGBA color;
            char *color_str = g_strndup (line + STR_LITERAL_LEN ("color: "), line_len - STR_LITERAL_LEN ("color: "));
            if (g_strcmp0 (color_str, "auto") == 0)
                color = LYPAD_AUTOMATIC_TEXT_COLOR;
            else if (!gdk_rgba_parse (&color, color_str))
            {
                g_free (color_str);
                goto deserialize_error;
            }
            g_free (color_str);
            GtkTextTag *tag = lypad_text_buffer_get_color_tag (lypad_buffer, &color);
            g_array_append_val (tag_array, tag);
        }
        else if (LINE_EQUAL_LITERAL ("bold", line, line_len))
            g_array_append_val (tag_array, priv->toggle_tags[BOLD_TAG]);
        else if (LINE_EQUAL_LITERAL ("italic", line, line_len))
            g_array_append_val (tag_array, priv->toggle_tags[ITALIC_TAG]);
        else if (LINE_EQUAL_LITERAL ("underline", line, line_len))
            g_array_append_val (tag_array, priv->toggle_tags[UNDERLINE_TAG]);
        else if (LINE_EQUAL_LITERAL ("strikethrough", line, line_len))
            g_array_append_val (tag_array, priv->toggle_tags[STRIKETHROUGH_TAG]);
        else if (LINE_STARTS_WITH_LITERAL ("bullet: ", line, line_len))
        {
            gunichar bullet;
            if (!sscanf (line, "bullet: U+%06X\n", &bullet))
                goto deserialize_error;
            GtkTextTag *tag = lypad_text_buffer_create_bullet_tag (lypad_buffer, bullet);
            g_array_append_val (tag_array, tag);
        }
        else if (LINE_EQUAL_LITERAL ("[text]", line, line_len))
        {
            line = next_line + 1;
            remaining_len -= line_len + 1;
            break;
        }
        else if (line_len > 0)
            goto deserialize_error;

        line = next_line + 1;
        remaining_len -= line_len + 1;
    }

    const char *block = line;
    const char *next_block;
    gsize block_len;
    guint tag_number;

    while (length > 0)
    {
        next_block = g_utf8_strchr (block, remaining_len, '<');
        if (next_block)
            block_len = next_block - block;
        else
            block_len = remaining_len;

        gsize insert_len;
        char *insert_text = unescape_text (block, block_len, &insert_len);
        gtk_text_buffer_insert_at_cursor (GTK_TEXT_BUFFER (lypad_buffer), insert_text, insert_len);
        g_free (insert_text);

        if (!next_block)
            break;
        block = next_block + 1;
        remaining_len -= block_len + 1;

        gboolean disable_tag = FALSE;
        if (*block == '/')
        {
            disable_tag = TRUE;
            block += 1;
            remaining_len -= 1;
        }
        if (!sscanf(block, "%u>", &tag_number))
            goto deserialize_error;
        if (tag_number >= tag_array->len)
            goto deserialize_error;
        GtkTextTag *tag = g_array_index (tag_array, GtkTextTag*, tag_number);
        if (disable_tag)
            lypad_text_buffer_unset_tag (lypad_buffer, tag);
        else
        {
            int tag_type = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (tag), tag_type_quark));
            switch (tag_type)
            {
                case TAG_TYPE_LIST:
                {
                    GtkTextIter iter;
                    gtk_text_buffer_get_iter_at_mark (GTK_TEXT_BUFFER (lypad_buffer), &iter,
                                                      gtk_text_buffer_get_insert (GTK_TEXT_BUFFER (lypad_buffer)));
                    lypad_text_buffer_line_set_bullet (lypad_buffer, &iter, tag);
                    break;
                }

                case TAG_TYPE_FONT:
                case TAG_TYPE_SIZE:
                case TAG_TYPE_COLOR:
                case TAG_TYPE_TOGGLE:
                    lypad_text_buffer_set_tag (lypad_buffer, tag);
                    break;

                default:
                    goto deserialize_error;
            }
        }

        next_block = g_utf8_strchr (block, remaining_len, '>');
        remaining_len -= next_block - block + 1;
        block = next_block + 1;
    }
    g_array_free (tag_array, TRUE);
    lypad_text_history_unlock (priv->history);
    lypad_text_history_clear (priv->history);
    return TRUE;

deserialize_error:
    g_array_free (tag_array, TRUE);
    lypad_text_history_unlock (priv->history);
    lypad_text_history_clear (priv->history);
    return FALSE;
}

static void lypad_text_buffer_line_remove_bullet (LypadTextBuffer *buffer, GtkTextIter *iter, GtkTextTag *tag)
{
    gtk_text_iter_set_line_offset (iter, 0);
    if (!tag)
    {
        GSList *iter_tags = gtk_text_iter_get_tags (iter);
        for (GSList *l = iter_tags; l != NULL; l = l->next)
        {
            int tag_type = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (l->data), tag_type_quark));
            if (tag_type == TAG_TYPE_LIST)
            {
                tag = l->data;
                break;
            }
        }
        g_slist_free (iter_tags);
    }
    g_return_if_fail (tag != NULL);

    GtkTextIter end = *iter;
    gtk_text_iter_forward_to_tag_toggle (&end, tag);
    gtk_text_buffer_delete (GTK_TEXT_BUFFER (buffer), iter, &end);
}

static LypadTextListInfo *lypad_text_buffer_line_get_list_info (const GtkTextIter *iter)
{
    LypadTextListInfo *info = NULL;
    GSList *iter_tags = gtk_text_iter_get_tags (iter);
    for (GSList *l = iter_tags; l != NULL; l = l->next)
    {
        int tag_type = GPOINTER_TO_INT (g_object_get_qdata (G_OBJECT (l->data), tag_type_quark));
        if (tag_type == TAG_TYPE_LIST)
        {
            info = g_object_get_qdata (G_OBJECT (l->data), tag_value_quark);
            break;
        }
    }
    g_slist_free (iter_tags);
    return info;
}

void lypad_text_buffer_unset_list (LypadTextBuffer *buffer)
{
    g_return_if_fail (LYPAD_IS_TEXT_BUFFER (buffer));
    gtk_text_buffer_begin_user_action (GTK_TEXT_BUFFER (buffer));

    GtkTextIter start, end;
    gtk_text_buffer_get_selection_bounds (GTK_TEXT_BUFFER (buffer), &start, &end);
    gtk_text_iter_set_line_index (&start, 0);
    if (!gtk_text_iter_ends_line (&end))
        gtk_text_iter_forward_to_line_end (&end);
    GtkTextMark *range_end_mark = gtk_text_buffer_create_mark (GTK_TEXT_BUFFER (buffer), NULL, &end, FALSE);

    for (GtkTextIter iter = start; gtk_text_iter_compare (&iter, &end) <= 0; )
    {
        if (lypad_text_buffer_line_get_list_info (&iter))
        {
            lypad_text_buffer_line_remove_bullet (buffer, &iter, NULL);
            gtk_text_buffer_get_iter_at_mark (GTK_TEXT_BUFFER (buffer), &end, range_end_mark);
        }
        if (!gtk_text_iter_forward_line (&iter))
            break;
    }
    gtk_text_buffer_delete_mark (GTK_TEXT_BUFFER (buffer), range_end_mark);

    gtk_text_buffer_end_user_action (GTK_TEXT_BUFFER (buffer));
}

void lypad_text_buffer_set_bullet (LypadTextBuffer *buffer, gunichar bullet)
{
    g_return_if_fail (LYPAD_IS_TEXT_BUFFER (buffer));
    GtkTextIter start, end;

    gtk_text_buffer_begin_user_action (GTK_TEXT_BUFFER (buffer));
    gboolean has_selection = gtk_text_buffer_get_selection_bounds (GTK_TEXT_BUFFER (buffer), &start, &end);

    gtk_text_iter_set_line_index (&start, 0);
    if (!gtk_text_iter_ends_line (&end))
        gtk_text_iter_forward_to_line_end (&end);

    if (!bullet)
    {
        GtkTextIter iter = start;
        if (gtk_text_iter_backward_line (&iter))
        {
            LypadTextListInfo *info = lypad_text_buffer_line_get_list_info (&iter);
            if (info && info->type == LYPAD_TEXT_LIST_TYPE_BULLET)
                bullet = info->u.bullet;
        }
    }
    if (!bullet)
        bullet = LYPAD_BULLET_DEFAULT;

    GtkTextTag *tag = lypad_text_buffer_create_bullet_tag (buffer, bullet);
    if (has_selection)
    {
        GtkTextMark *range_end_mark = gtk_text_buffer_create_mark (GTK_TEXT_BUFFER (buffer), NULL, &end, FALSE);
        for (GtkTextIter iter = start; gtk_text_iter_compare (&iter, &end) <= 0; )
        {
            lypad_text_buffer_line_set_bullet (buffer, &iter, tag);
            gtk_text_buffer_get_iter_at_mark (GTK_TEXT_BUFFER (buffer), &end, range_end_mark);
            if (!gtk_text_iter_forward_line (&iter))
                break;
        }
        gtk_text_buffer_delete_mark (GTK_TEXT_BUFFER (buffer), range_end_mark);
    }
    else
    {
        LypadTextListInfo *selected_info = lypad_text_buffer_line_get_list_info (&start);
        if (selected_info && selected_info->type == LYPAD_TEXT_LIST_TYPE_BULLET)
        {
            gunichar selected_bullet = selected_info->u.bullet;
            GtkTextIter iter = start;
            GtkTextMark *start_mark = gtk_text_buffer_create_mark (GTK_TEXT_BUFFER (buffer), NULL, &start, FALSE);

            while (gtk_text_iter_backward_line (&iter))
            {
                LypadTextListInfo *info = lypad_text_buffer_line_get_list_info (&iter);
                if (info && info->type == LYPAD_TEXT_LIST_TYPE_BULLET && info->u.bullet == selected_bullet)
                    lypad_text_buffer_line_set_bullet (buffer, &iter, tag);
                else
                    break;
            }
            gtk_text_buffer_get_iter_at_mark (GTK_TEXT_BUFFER (buffer), &iter, start_mark);
            while (gtk_text_iter_forward_line (&iter))
            {
                LypadTextListInfo *info = lypad_text_buffer_line_get_list_info (&iter);
                if (info && info->type == LYPAD_TEXT_LIST_TYPE_BULLET && info->u.bullet == selected_bullet)
                    lypad_text_buffer_line_set_bullet (buffer, &iter, tag);
                else
                    break;
            }
            gtk_text_buffer_get_iter_at_mark (GTK_TEXT_BUFFER (buffer), &start, start_mark);
            gtk_text_buffer_delete_mark (GTK_TEXT_BUFFER (buffer), start_mark);
        }
        lypad_text_buffer_line_set_bullet (buffer, &start, tag);
    }

    gtk_text_buffer_end_user_action (GTK_TEXT_BUFFER (buffer));
}
