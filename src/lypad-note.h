/*
 *  lypad-note.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include "lypad-text-buffer.h"

G_BEGIN_DECLS

typedef struct _LypadTag   LypadTag;
typedef struct _LypadTheme LypadTheme;

#define LYPAD_TYPE_NOTE (lypad_note_get_type ())
G_DECLARE_FINAL_TYPE (LypadNote, lypad_note, LYPAD, NOTE, GObject)

LypadNote     *lypad_note_new                   (void);
LypadNote     *lypad_note_load_from_file        (GFile *file);
const char    *lypad_note_get_id                (LypadNote *note);
void           lypad_note_save                  (LypadNote *note);
void           lypad_note_delete                (LypadNote *note);

GtkTextBuffer *lypad_note_get_title_buffer      (LypadNote *note);                                        // [transfer none]
GtkTextBuffer *lypad_note_get_content_buffer    (LypadNote *note);                                        // [transfer none]

void           lypad_note_set_color             (LypadNote *note, const GdkRGBA *color);
const GdkRGBA *lypad_note_get_color             (LypadNote *note);

void           lypad_note_add_tag               (LypadNote *note, LypadTag *tag);
void           lypad_note_remove_tag            (LypadNote *note, LypadTag *tag);
gboolean       lypad_note_has_tag               (LypadNote *note, LypadTag *tag);
GList         *lypad_note_get_tags              (LypadNote *note);                                        // [transfer none]

void           lypad_note_set_show_on_desktop   (LypadNote *note, gboolean show, GtkWindow *window);
gboolean       lypad_note_get_show_on_desktop   (LypadNote *note);
void           lypad_note_show                  (LypadNote *note, GtkWindow *window);

void           lypad_note_set_position          (LypadNote *note, int x, int y, int width, int height);
void           lypad_note_get_position          (LypadNote *note, int *x, int *y);
void           lypad_note_get_size              (LypadNote *note, int *width, int *height);

time_t         lypad_note_get_creation_date     (LypadNote *note);
time_t         lypad_note_get_modification_date (LypadNote *note);

void           lypad_note_set_locked            (LypadNote *note, gboolean lock);
gboolean       lypad_note_get_locked            (LypadNote *note);

void           lypad_note_set_theme             (LypadNote *note, LypadTheme *theme);
LypadTheme    *lypad_note_get_theme             (LypadNote *note);                                        // [transfer none]

gboolean       lypad_note_file_equal            (LypadNote *note, GFile *file);
gboolean       lypad_note_equal                 (LypadNote *note1, LypadNote *note2);

G_END_DECLS