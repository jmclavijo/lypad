/*
 *  lypad-page.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

typedef GtkListBoxRow LypadPageRow;
typedef gint (*LypadPageRowSortFunc) (LypadPageRow *row1, LypadPageRow *row2);

#define LYPAD_TYPE_PAGE (lypad_page_get_type ())
G_DECLARE_DERIVABLE_TYPE (LypadPage, lypad_page, LYPAD, PAGE, GtkGrid)

GtkWidget    *lypad_page_get_side_panel         (LypadPage *page);
void          lypad_page_go_back                (LypadPage *page);

/* For use in subclass implementation only */
LypadPageRow *lypad_page_add_panel_row          (LypadPage *page, gpointer user_data);
void          lypad_page_remove_panel_row       (LypadPage *page, LypadPageRow *row);
GList        *lypad_page_get_panel_rows         (LypadPage *page);                                                   // [transfer container]
void          lypad_page_select_panel_row       (LypadPage *page, LypadPageRow *row);
LypadPageRow *lypad_page_get_selected_panel_row (LypadPage *page);

void          lypad_page_row_set_data           (LypadPageRow *row, gpointer data, GDestroyNotify destroy_func);
gpointer      lypad_page_row_get_data           (LypadPageRow *row);

void          lypad_page_set_row_sort_func      (LypadPage *page, LypadPageRowSortFunc sort_func);

struct _LypadPageClass
{
    GtkGridClass parent_class;

    void (* init_row)      (LypadPage *page, LypadPageRow *row, gpointer user_data);
    void (* release_row)   (LypadPage *page, LypadPageRow *row);
    void (* row_activated) (LypadPage *page, LypadPageRow *row);
    void (* go_back)       (LypadPage *page);
};

G_END_DECLS
