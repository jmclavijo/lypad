/*
 *  lypad-selection-widget.h
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define LYPAD_TYPE_SELECTION_WIDGET (lypad_selection_widget_get_type ())
G_DECLARE_FINAL_TYPE (LypadSelectionWidget, lypad_selection_widget, LYPAD, SELECTION_WIDGET, GtkOverlay)

typedef struct _LypadAnimationHandler LypadAnimationHandler;

GtkWidget             *lypad_selection_widget_new            ();
void                   lypad_selection_widget_add_child      (LypadSelectionWidget *widget, GtkWidget *child);
GtkWidget             *lypad_selection_widget_get_child      (LypadSelectionWidget *widget);

LypadAnimationHandler *lypad_animation_handler_new           (GtkWidget *widget);
void                   lypad_animation_handler_free          (LypadAnimationHandler *handler);
void                   lypad_selection_widget_set_selectable (GSList *widgets, gboolean selectable, LypadAnimationHandler *handler);
gboolean               lypad_selection_widget_get_selectable (LypadSelectionWidget *widget);

void                   lypad_selection_widget_set_selected   (LypadSelectionWidget *widget, gboolean selected, LypadAnimationHandler *animation_handler);
gboolean               lypad_selection_widget_get_selected   (LypadSelectionWidget *widget);

G_END_DECLS
