/*
 *  lypad-note-window.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-note-window.h"
#include "lypad-application.h"
#include "lypad-theme.h"
#include "lypad-note-settings-dialog.h"
#include "lypad-utils.h"
#include "color-selection/lypad-color-utils.h"

typedef enum
{
    GRIP_DIR_NONE,
    //GRIP_DIR_N,
    GRIP_DIR_S,
    GRIP_DIR_E,
    //GRIP_DIR_W,
    //GRIP_DIR_NE,
    //GRIP_DIR_NW,
    GRIP_DIR_SE,
    //GRIP_DIR_SW
} ResizeDirection;

struct _LypadNoteWindow
{
    GtkApplicationWindow  parent;
    LypadNote            *note;
    LypadTheme           *theme;
    LayoutData           *layout_data;
    GtkCssProvider       *color_provider;
    GtkWidget            *menu_popover;

    GdkSeat              *grabbed_seat;
    double                grab_x;
    double                grab_y;
    GdkCursor            *grab_cursor;
    GtkWidget            *grabbed_widget;

    ResizeDirection       grip_dir;
    gboolean              is_moving;
    gboolean              is_resizing;

    gulong                note_color_changed_handler;
    gulong                note_theme_changed_handler;
    gulong                note_theme_lock_handler;
    gulong                note_destroy_handler;
};

G_DEFINE_TYPE (LypadNoteWindow, lypad_note_window, GTK_TYPE_APPLICATION_WINDOW)

#define RESIZE_GRIP_WIDTH 10
#define RESIZE_GRIP_CORNER_WIDTH 28

static void on_edit_action_activated (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (user_data);
    g_return_if_fail (win->note != NULL);
    GtkApplication *app = gtk_window_get_application (GTK_WINDOW (win));
    lypad_application_edit (LYPAD_APPLICATION (app), win->note);
}

static void on_hide_action_activated (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (user_data);
    g_return_if_fail (win->note != NULL);
    lypad_note_set_show_on_desktop (win->note, FALSE, NULL);
}

static void on_note_settings_dialog_response (GtkDialog *dialog, gint response_id, gpointer user_data)
{
    gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void on_settings_action_activated (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (user_data);
    GtkWidget *dialog = lypad_note_settings_dialog_new (GTK_WINDOW (win), win->note);
    g_signal_connect (dialog, "response", G_CALLBACK (on_note_settings_dialog_response), win);
    gtk_widget_show (dialog);
}

static void on_delete_action_activated (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (user_data);
    GSList *notes = g_slist_prepend (NULL, win->note);
    lypad_utils_prompt_delete_notes (notes, GTK_WINDOW (win));
    g_slist_free (notes);
}

static GActionEntry action_entries[] = {
	{ "edit", on_edit_action_activated, NULL, NULL},
	{ "hide", on_hide_action_activated, NULL, NULL},
	{ "delete", on_delete_action_activated, NULL, NULL},
	{ "settings", on_settings_action_activated, NULL, NULL},
};

static void lypad_note_window_grab (LypadNoteWindow *win, GtkWidget *grab_widget)
{
    g_return_if_fail (win->grabbed_widget == NULL);
    gtk_grab_add (grab_widget);
    win->grabbed_widget = grab_widget;
    GdkDevice *device = gtk_get_current_event_device ();
    if (device)
    {
        if (gdk_device_get_source (device) == GDK_SOURCE_KEYBOARD)
            device = gdk_device_get_associated_device (device);
        win->grabbed_seat = gdk_device_get_seat (device);
        gdk_seat_grab (win->grabbed_seat,
                       gtk_widget_get_window (grab_widget),
                       GDK_SEAT_CAPABILITY_ALL,
                       FALSE, NULL, NULL,
                       NULL, NULL);
    }
    else
        g_warning ("Event emitted without any device");
}

static inline void lypad_note_window_stop_drag (LypadNoteWindow *win, gboolean cancelled)
{
    gdk_window_set_cursor (gtk_widget_get_window (GTK_WIDGET (win)), NULL);
    win->is_moving = FALSE;
    gdk_seat_ungrab (win->grabbed_seat);
    win->grabbed_seat = NULL;

    gtk_grab_remove (GTK_WIDGET (win->grabbed_widget));
    win->grabbed_widget = NULL;

    gtk_widget_set_sensitive (GTK_WIDGET (win->layout_data->content_view), TRUE);
    if (win->layout_data->title_view)
        gtk_widget_set_sensitive (GTK_WIDGET (win->layout_data->title_view), TRUE);

    int x, y;
    if (cancelled)
    {
        lypad_note_get_position (win->note, &x, &y);
        gtk_window_move (GTK_WINDOW (win), x, y);
    }
    else
    {
        gtk_window_get_position (GTK_WINDOW (win), &x, &y);
        lypad_note_set_position (win->note, x, y, -1, -1);
    }
}

static gboolean on_drag_widget_motion (GtkWidget *widget, GdkEventMotion *event, gpointer user_data)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (user_data);
    if (win->is_moving)
    {
        int new_x = event->x_root - win->grab_x;
        int new_y = event->y_root - win->grab_y;
        gtk_window_move (GTK_WINDOW (win), new_x, new_y);
        return TRUE;
    }
    return FALSE;
}

static gboolean on_drag_widget_button_press (GtkWidget *widget, GdkEventButton *event, gpointer *user_data)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (user_data);
    if (event->type == GDK_BUTTON_PRESS && event->button == GDK_BUTTON_PRIMARY)
    {
        g_return_val_if_fail (!win->is_moving, FALSE);
        win->is_moving = TRUE;

        int grab_x, grab_y;
        gtk_widget_translate_coordinates(widget, GTK_WIDGET (win), event->x, event->y, &grab_x, &grab_y);
        win->grab_x = grab_x;
        win->grab_y = grab_y;
        lypad_note_window_grab (win, widget);
        gdk_window_set_cursor (gtk_widget_get_window (GTK_WIDGET (win)), win->grab_cursor);
        return TRUE;
    }
    else if (event->type == GDK_BUTTON_RELEASE && event->button == GDK_BUTTON_PRIMARY && win->is_moving)
    {
        lypad_note_window_stop_drag (win, FALSE);
        return TRUE;
    }
    return FALSE;
}

static gboolean on_drag_widget_key_press (GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (user_data);
    if (event->keyval == GDK_KEY_Escape && win->is_moving)
    {
        lypad_note_window_stop_drag (win, TRUE);
        return TRUE;
    }
    return FALSE;
}

static gboolean on_button_press (GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (user_data);
    if (event->type == GDK_BUTTON_PRESS && event->button == GDK_BUTTON_PRIMARY
        && (event->state & gtk_accelerator_get_default_mod_mask ()) == GDK_CONTROL_MASK)
    {
        g_return_val_if_fail (!win->is_moving, FALSE);
        win->is_moving = TRUE;

        int grab_x, grab_y;
        gtk_widget_translate_coordinates(widget, GTK_WIDGET (win), event->x, event->y, &grab_x, &grab_y);
        win->grab_x = grab_x;
        win->grab_y = grab_y;
        lypad_note_window_grab (win, GTK_WIDGET (win));
        gtk_widget_set_sensitive (GTK_WIDGET (win->layout_data->content_view), FALSE);
        if (win->layout_data->title_view)
            gtk_widget_set_sensitive (GTK_WIDGET (win->layout_data->title_view), FALSE);
        gdk_window_set_cursor (gtk_widget_get_window (GTK_WIDGET (win)), win->grab_cursor);
        return TRUE;
    }
    else if (event->type == GDK_BUTTON_RELEASE && event->button == GDK_BUTTON_PRIMARY && win->is_moving)
    {
        lypad_note_window_stop_drag (win, FALSE);
        return TRUE;
    }
    return FALSE;
}

void lypad_note_window_enable_transparency (LypadNoteWindow *note)
{
    GdkScreen *screen = gdk_screen_get_default ();
    GdkVisual *visual = gdk_screen_get_rgba_visual (screen);

    if (visual != NULL && gdk_screen_is_composited (screen))
    {
        gtk_widget_set_app_paintable (GTK_WIDGET (note), TRUE);
        gtk_widget_set_visual (GTK_WIDGET (note), visual);
    }
    else
        g_warning ("enabling transparencies failed");
}

static void lypad_note_window_set_color (LypadNoteWindow *win, const GdkRGBA *color)
{
    GdkRGBA fg_color;
    lypad_color_utils_get_text_color_for_background (color, &fg_color);

    int color_r = (int) (0.5 + 255. * color->red);
    int color_g = (int) (0.5 + 255. * color->green);
    int color_b = (int) (0.5 + 255. * color->blue);

    int fg_r = (int) (0.5 + 255. * fg_color.red);
    int fg_g = (int) (0.5 + 255. * fg_color.green);
    int fg_b = (int) (0.5 + 255. * fg_color.blue);

    char *style_data = lypad_utils_strdup_printf (
        ".LypadLayoutBase {\n"
        "    background-color: rgba(%d,%d,%d,%.3f);\n"
        "    color: rgb(%d, %d, %d);\n"
        "}\n"
        ".LypadTextView {\n"
        "    caret-color: rgb(%d, %d, %d);\n"
        "}\n",
        color_r, color_g, color_b, color->alpha,
        fg_r, fg_g, fg_b,
        fg_r, fg_g, fg_b
    );
    gtk_css_provider_load_from_data (win->color_provider, style_data, -1, NULL);
}

static void on_note_color_changed (LypadNote *note, gpointer user_data)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (user_data);
    lypad_note_window_set_color (win, lypad_note_get_color (win->note));
}

static void on_note_destroy (LypadNote *note, gpointer user_data)
{
    gtk_widget_destroy (GTK_WIDGET (user_data));
}

static gboolean lypad_note_window_motion_notify (GtkWidget *widget, GdkEventMotion *event)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (widget);

    if (win->is_resizing)
    {
        switch (win->grip_dir)
        {
            case GRIP_DIR_S:
            {
                int new_height = event->y_root - win->grab_y;
                if (new_height > 0)
                    gtk_window_resize (GTK_WINDOW (win), win->grab_x, new_height);
                break;
            }
            case GRIP_DIR_E:
            {
                int new_width = event->x_root - win->grab_x;
                if (new_width > 0)
                    gtk_window_resize (GTK_WINDOW (win), new_width, win->grab_y);
                break;
            }
            case GRIP_DIR_SE:
            {
                int new_width = event->x_root - win->grab_x;
                int new_height = event->y_root - win->grab_y;
                if (new_width <= 0)
                    new_width = 1;
                if (new_height <= 0)
                    new_height = 1;
                gtk_window_resize (GTK_WINDOW (win), new_width, new_height);
                break;
            }
        }
        return TRUE;
    }
    else if (!win->is_moving)
    {
        GdkWindow *gdk_window = gtk_widget_get_window (widget);
        double width = gdk_window_get_width (gdk_window);
        double height = gdk_window_get_height (gdk_window);
        GdkDisplay *display = gdk_window_get_display (gdk_window);
        GdkCursor *cursor;

        if (event->window != gdk_window)
        {
            cursor = NULL;
            win->grip_dir = GRIP_DIR_NONE;
        }
        else if ((event->x > width - RESIZE_GRIP_WIDTH        && event->y > height - RESIZE_GRIP_CORNER_WIDTH) ||
                 (event->x > width - RESIZE_GRIP_CORNER_WIDTH && event->y > height - RESIZE_GRIP_WIDTH))
        {
            cursor = gdk_cursor_new_from_name (display, "se-resize");
            win->grip_dir = GRIP_DIR_SE;
        }
        else if (event->y > height - RESIZE_GRIP_WIDTH)
        {
            cursor = gdk_cursor_new_from_name (display, "s-resize");
            win->grip_dir = GRIP_DIR_S;
        }
        else if (event->x > width - RESIZE_GRIP_WIDTH)
        {
            cursor = gdk_cursor_new_from_name (display, "e-resize");
            win->grip_dir = GRIP_DIR_E;
        }
        else
        {
            cursor = NULL;
            win->grip_dir = GRIP_DIR_NONE;
        }

        gdk_window_set_cursor (gdk_window, cursor);
        if (cursor)
        {
            g_object_unref (cursor);
            return TRUE;
        }
    }
    return GTK_WIDGET_CLASS (lypad_note_window_parent_class)->motion_notify_event (widget, event);
}

static void lypad_note_window_stop_resize (LypadNoteWindow *win, gboolean cancelled)
{
    win->is_resizing = FALSE;
    gdk_seat_ungrab (win->grabbed_seat);
    win->grabbed_seat = NULL;
    gtk_grab_remove (win->grabbed_widget);

    int x, y, width, height;
    if (cancelled)
    {
        lypad_note_get_position (win->note, &x, &y);
        lypad_note_get_size (win->note, &width, &height);
        gtk_window_move (GTK_WINDOW (win), x, y);
        gtk_window_resize (GTK_WINDOW (win), width, height);
    }
    else
    {
        gtk_window_get_position (GTK_WINDOW (win), &x, &y);
        gtk_window_get_size (GTK_WINDOW (win), &width, &height);
        lypad_note_set_position (win->note, x, y, width, height);
    }
}

static gboolean lypad_note_window_button_press (GtkWidget *widget, GdkEventButton *event)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (widget);
    if (event->button != GDK_BUTTON_PRIMARY || event->window != gtk_widget_get_window (widget))
        return GTK_WIDGET_CLASS (lypad_note_window_parent_class)->button_press_event (widget, event);

    g_return_val_if_fail (!win->is_resizing, FALSE);

    int width, height;
    lypad_note_get_size (win->note, &width, &height);
    switch (win->grip_dir)
    {
        case GRIP_DIR_S:
            win->is_resizing = TRUE;
            win->grab_x = width;
            win->grab_y = event->y_root - height;
            lypad_note_window_grab (win, GTK_WIDGET (win));
            return TRUE;

        case GRIP_DIR_E:
            win->is_resizing = TRUE;
            win->grab_x = event->x_root - width;
            win->grab_y = height;
            lypad_note_window_grab (win, GTK_WIDGET (win));
            return TRUE;

        case GRIP_DIR_SE:
            win->is_resizing = TRUE;
            win->grab_x = event->x_root - width;
            win->grab_y = event->y_root - height;
            lypad_note_window_grab (win, GTK_WIDGET (win));
            return TRUE;
    }

    return GTK_WIDGET_CLASS (lypad_note_window_parent_class)->button_press_event (widget, event);
}

static gboolean lypad_note_window_button_release (GtkWidget *widget, GdkEventButton *event)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (widget);
    if (win->is_resizing && event->button == GDK_BUTTON_PRIMARY)
    {
        lypad_note_window_stop_resize (win, FALSE);
        return TRUE;
    }
    return GTK_WIDGET_CLASS (lypad_note_window_parent_class)->button_release_event (widget, event);
}

static gboolean lypad_note_window_key_press (GtkWidget *widget, GdkEventKey *event)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (widget);
    if (event->keyval == GDK_KEY_Escape && win->is_resizing)
    {
        lypad_note_window_stop_resize (win, TRUE);
        return TRUE;
    }
    return GTK_WIDGET_CLASS (lypad_note_window_parent_class)->key_press_event (widget, event);
}

static void on_lock_button_toggled (LypadToggleButton *button, gboolean active, gpointer user_data)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (user_data);
    lypad_note_set_locked (win->note, active);
}

static void on_note_lock_changed (LypadNote *note, gpointer user_data)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (user_data);
    g_return_if_fail (win->layout_data != NULL);

    gboolean locked = lypad_note_get_locked (win->note);
    if (locked)
    {
        if (win->layout_data->title_view)
        {
            gtk_text_view_set_editable (GTK_TEXT_VIEW (win->layout_data->title_view), FALSE);
            gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (win->layout_data->title_view), FALSE);
        }
        gtk_text_view_set_editable (GTK_TEXT_VIEW (win->layout_data->content_view), FALSE);
        gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (win->layout_data->content_view), FALSE);
    }
    else
    {
        if (win->layout_data->title_view)
        {
            gtk_text_view_set_editable (GTK_TEXT_VIEW (win->layout_data->title_view), TRUE);
            gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (win->layout_data->title_view), TRUE);
        }
        gtk_text_view_set_editable (GTK_TEXT_VIEW (win->layout_data->content_view), TRUE);
        gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (win->layout_data->content_view), TRUE);
    }
    if (win->layout_data->lock_button)
        lypad_toggle_button_set_active (win->layout_data->lock_button, locked);
}

static void on_menu_button_clicked (LypadButton *button, gpointer user_data)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (user_data);
    gtk_popover_popup (GTK_POPOVER (win->menu_popover));
}

static void on_menu_popover_closed (GtkPopover *popover, gpointer user_data)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (user_data);
    lypad_toggle_button_set_active (win->layout_data->menu_button, FALSE);
}

static void lypad_note_window_init (LypadNoteWindow *win)
{
    gtk_window_set_type_hint (GTK_WINDOW (win), GDK_WINDOW_TYPE_HINT_DESKTOP);

	GtkWindowGroup *window_group = gtk_window_group_new ();
	gtk_window_group_add_window (window_group, GTK_WINDOW (win));
    g_object_unref (window_group);

    GtkStyleContext *context = gtk_widget_get_style_context (GTK_WIDGET (win));
    gtk_style_context_add_class (context, "LypadNoteWindow");

    win->note = NULL;
    win->layout_data = NULL;
    win->menu_popover = NULL;

    win->is_moving = FALSE;
    win->grab_cursor = NULL;
    win->grabbed_widget = NULL;

    win->color_provider = gtk_css_provider_new ();
    win->note_destroy_handler = 0;

    gtk_widget_add_events (GTK_WIDGET (win), GDK_POINTER_MOTION_MASK | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_KEY_PRESS_MASK);

    lypad_note_window_enable_transparency (win);
    gtk_widget_show_all (GTK_WIDGET (win));

    g_action_map_add_action_entries (G_ACTION_MAP (win), action_entries, G_N_ELEMENTS (action_entries), win);
    g_signal_connect (win, "button-press-event", G_CALLBACK (on_button_press), win);
    g_signal_connect (win, "button-release-event", G_CALLBACK (on_button_press), win);
    g_signal_connect (win, "motion-notify-event", G_CALLBACK (on_drag_widget_motion), win);
}

static void lypad_note_window_set_theme (LypadNoteWindow *win, LypadTheme *theme)
{
    g_return_if_fail (LYPAD_IS_THEME (theme));
    if (win->theme)
    {
        gtk_widget_destroy (win->layout_data->layout_base);
        lypad_theme_destroy_layout (win->theme, win->layout_data);
        g_object_unref (win->theme);
        //win->layout_data = NULL;
        //win->theme = NULL;
    }
    win->theme = g_object_ref (theme);
    win->layout_data = lypad_theme_build_layout (theme, win->note);
    gtk_container_add (GTK_CONTAINER (win), win->layout_data->layout_base);
    gtk_widget_set_margin_bottom (win->layout_data->layout_base, RESIZE_GRIP_WIDTH);

    if (gtk_widget_get_direction (GTK_WIDGET (win)) == GTK_TEXT_DIR_RTL)
        gtk_widget_set_margin_start (win->layout_data->layout_base, RESIZE_GRIP_WIDTH);
    else
        gtk_widget_set_margin_end (win->layout_data->layout_base, RESIZE_GRIP_WIDTH);

    if (win->layout_data->drag_widget)
    {
        gtk_widget_add_events(GTK_WIDGET (win->layout_data->drag_widget),
                              GDK_POINTER_MOTION_MASK | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_KEY_PRESS_MASK);

        g_signal_connect (win->layout_data->drag_widget, "motion-notify-event", G_CALLBACK (on_drag_widget_motion), win);
        g_signal_connect (win->layout_data->drag_widget, "button-press-event", G_CALLBACK (on_drag_widget_button_press), win);
        g_signal_connect (win->layout_data->drag_widget, "button-release-event", G_CALLBACK (on_drag_widget_button_press), win);
        g_signal_connect (win->layout_data->drag_widget, "key-press-event", G_CALLBACK (on_drag_widget_key_press), win);
    }

    g_signal_connect (win->layout_data->content_view, "button-press-event", G_CALLBACK (on_button_press), win);
    if (win->layout_data->title_view)
        g_signal_connect (win->layout_data->title_view, "button-press-event", G_CALLBACK (on_button_press), win);

    GtkStyleContext *style_context = gtk_widget_get_style_context (win->layout_data->layout_base);
    gtk_style_context_add_provider (style_context, GTK_STYLE_PROVIDER (win->color_provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

    if (win->layout_data->title_view)
    {
        style_context = gtk_widget_get_style_context (GTK_WIDGET (win->layout_data->title_view));
        gtk_style_context_add_provider (style_context, GTK_STYLE_PROVIDER (win->color_provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    }

    style_context = gtk_widget_get_style_context (GTK_WIDGET (win->layout_data->content_view));
    gtk_style_context_add_provider (style_context, GTK_STYLE_PROVIDER (win->color_provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

    on_note_lock_changed (win->note, win);
    if (win->layout_data->lock_button)
    {
        g_signal_connect (win->layout_data->lock_button, "toggled", G_CALLBACK (on_lock_button_toggled), win);
    }

    if (win->layout_data->menu_button)
    {
        LypadApplication *app = LYPAD_APPLICATION (gtk_window_get_application (GTK_WINDOW (win)));
        win->menu_popover = gtk_popover_new_from_model (GTK_WIDGET (win->layout_data->menu_button),
                                                        lypad_application_get_menu (app, LYPAD_NOTE_WINDOW_MENU));
        g_signal_connect (win->layout_data->menu_button, "clicked", G_CALLBACK (on_menu_button_clicked), win);
        g_signal_connect (win->menu_popover, "closed", G_CALLBACK (on_menu_popover_closed), win);
    }

    gtk_widget_show_all (win->layout_data->layout_base);
}

static void on_note_theme_changed (LypadNote *note, gpointer user_data)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (user_data);
    lypad_note_window_set_theme (win, lypad_note_get_theme (win->note));
}

static void lypad_note_window_set_note (LypadNoteWindow *win, LypadNote *note)
{
    g_return_if_fail (win->note == NULL);
    win->note = g_object_ref (note);

    lypad_note_window_set_color (win, lypad_note_get_color (note));
    win->note_color_changed_handler = g_signal_connect (note, "color-changed", G_CALLBACK (on_note_color_changed), win);
    win->note_theme_changed_handler = g_signal_connect (note, "theme-changed", G_CALLBACK (on_note_theme_changed), win);
    win->note_theme_lock_handler = g_signal_connect (note, "lock-changed", G_CALLBACK (on_note_lock_changed), win);
    win->note_destroy_handler = g_signal_connect (note, "destroy", G_CALLBACK (on_note_destroy), win);

    int x, y;
    lypad_note_get_position (win->note, &x, &y);
    gtk_window_move (GTK_WINDOW (win), x, y);
    lypad_note_get_size (win->note, &x, &y);
    gtk_window_resize (GTK_WINDOW (win), x, y);

    lypad_note_window_set_theme (win, lypad_note_get_theme (note));
}

static void lypad_note_window_destroy (GtkWidget *widget)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (widget);
    if (win->note)
    {
        lypad_theme_destroy_layout (lypad_note_get_theme (win->note), win->layout_data);
        g_signal_handler_disconnect (win->note, win->note_color_changed_handler);
        g_signal_handler_disconnect (win->note, win->note_theme_changed_handler);
        g_signal_handler_disconnect (win->note, win->note_theme_lock_handler);
        g_signal_handler_disconnect (win->note, win->note_destroy_handler);
        g_object_unref (win->theme);
        g_object_unref (win->note);
        win->note = NULL;
    }

    GTK_WIDGET_CLASS (lypad_note_window_parent_class)->destroy (widget);
}

static void lypad_note_window_finalize (GObject *object)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (object);
    g_object_unref (win->color_provider);
    G_OBJECT_CLASS (lypad_note_window_parent_class)->finalize (object);
}

static void lypad_note_window_realize (GtkWidget *widget)
{
    GTK_WIDGET_CLASS (lypad_note_window_parent_class)->realize (widget);

    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (widget);
    GdkDisplay *display = gtk_widget_get_display (widget);
    win->grab_cursor = gdk_cursor_new_from_name (display, "grabbing");
}

static void lypad_note_window_unrealize (GtkWidget *widget)
{
    LypadNoteWindow *win = LYPAD_NOTE_WINDOW (widget);
    g_clear_object (&win->grab_cursor);

    GTK_WIDGET_CLASS (lypad_note_window_parent_class)->unrealize (widget);
}

static void lypad_note_window_class_init (LypadNoteWindowClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

    object_class->finalize             = lypad_note_window_finalize;
    widget_class->realize              = lypad_note_window_realize;
    widget_class->unrealize            = lypad_note_window_unrealize;
    widget_class->motion_notify_event  = lypad_note_window_motion_notify;
    widget_class->button_press_event   = lypad_note_window_button_press;
    widget_class->button_release_event = lypad_note_window_button_release;
    widget_class->key_press_event      = lypad_note_window_key_press;
    widget_class->destroy              = lypad_note_window_destroy;
}

LypadNoteWindow *lypad_note_window_new (LypadApplication *app, LypadNote *note)
{
    LypadNoteWindow *win = g_object_new (LYPAD_TYPE_NOTE_WINDOW, "application", app, NULL);
    lypad_note_window_set_note (win, note);
    return win;
}
