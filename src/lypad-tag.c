/*
 *  lypad-tag.c
 *  This file is part of Lypad.
 *
 *  Copyright © 2021 José M. Clavijo
 *
 *  Lypad is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Lypad is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "lypad-tag.h"
#include "lypad-note.h"

struct _LypadTag
{
    GObject  parent;
    GdkRGBA  color;
    char    *name;
    int      note_ref_count;
};

G_DEFINE_TYPE (LypadTag, lypad_tag, G_TYPE_OBJECT)

enum
{
    SIGNAL_DESTROY,
    SIGNAL_TAG_CREATED,
    SIGNAL_TAG_ADDED,
    SIGNAL_TAG_REMOVED,
    SIGNAL_LAST
};

static guint signals[SIGNAL_LAST] = {0};

enum
{
    PROP_0,
    PROP_NAME,
    PROP_COLOR,
    N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES] = {NULL};

static GList *tag_list = NULL;

LypadTag *lypad_tag_ref_by_note (LypadTag *tag, LypadNote *note)
{
    tag->note_ref_count += 1;
    g_signal_emit (tag, signals[SIGNAL_TAG_ADDED], 0, note);
    return g_object_ref (tag);
}

void lypad_tag_unref_by_note (LypadTag *tag, LypadNote *note)
{
    tag->note_ref_count -= 1;
    if (tag->note_ref_count == 0)
        lypad_tag_destroy (tag);
    else
        g_signal_emit (tag, signals[SIGNAL_TAG_REMOVED], 0, note);
    g_object_unref (tag);
}

static int lypad_tag_compare (gconstpointer a, gconstpointer b)
{
    const LypadTag *tag_a = a;
    const LypadTag *tag_b = b;
    return g_ascii_strcasecmp (tag_a->name, tag_b->name);
}

GSList *lypad_tag_get_all ()
{
    GSList *tags = NULL;
    for (GList *l = tag_list; l != NULL; l = l->next)
        tags = g_slist_prepend (tags, l->data);
    return g_slist_reverse (tags);
}

static void lypad_tag_init (LypadTag *tag)
{
    tag->name = NULL;
    tag->color.red   = 0.9;
    tag->color.green = 0.9;
    tag->color.blue  = 0.9;
    tag->color.alpha = 1;
    tag->note_ref_count = 0;
}

void lypad_tag_set_color (LypadTag *tag, GdkRGBA *color)
{
    g_return_if_fail (LYPAD_IS_TAG (tag));
    tag->color = *color;
    g_object_notify_by_pspec (G_OBJECT (tag), properties[PROP_COLOR]);
}

void lypad_tag_get_color (LypadTag *tag, GdkRGBA *color)
{
    g_return_if_fail (LYPAD_IS_TAG (tag));
    *color = tag->color;
}

const char *lypad_tag_get_name (LypadTag *tag)
{
    g_return_val_if_fail (LYPAD_IS_TAG (tag), NULL);
    return tag->name;
}

void lypad_tag_destroy (LypadTag *tag)
{
    g_signal_emit (tag, signals[SIGNAL_DESTROY], 0);
}

char *lypad_tag_serialize (LypadTag *tag)
{
    g_return_val_if_fail (LYPAD_IS_TAG (tag), NULL);
    GString *buffer = g_string_new (NULL);

    // Escape name
    for (char *c = tag->name; *c != '\0'; ++c)
    {
        if (*c == '&')
            g_string_append (buffer, "&a");
        else
            g_string_append_c (buffer, *c);
    }
    //

    g_string_append (buffer, "&&");
    char *color_str = gdk_rgba_to_string (&tag->color);
    g_string_append (buffer, color_str);
    g_free (color_str);
    return g_string_free (buffer, FALSE);
}

LypadTag *lypad_tag_parse (const char *str)
{
    char *name = NULL;
    GdkRGBA color;
    for (int i = 0; str[i] != '\0'; ++i)
    {
        if (str[i] == '&' && str[i + 1] == '&')
        {
            if (!gdk_rgba_parse (&color, str + i + 2))
                return NULL;
            name = g_strndup (str, i);
        }
    }
    if (!name)
        return NULL;

    // Unescape name
    for (int i = 0; name[i] != '\0'; ++i)
    {
        if (name[i] == '&' && name[i + 1] == 'a')
            for (int j = i + 1; name[j] != '\0'; ++j)
                name[j] = name[j + 1];
    }
    //

    LypadTag *tag = g_object_new (LYPAD_TYPE_TAG, "name", name, "color", &color, NULL);
    g_free (name);
    return tag;
}

static void lypad_tag_finalize (GObject *object)
{
    LypadTag *tag = LYPAD_TAG (object);
    tag_list = g_list_remove (tag_list, tag);
    g_clear_pointer (&tag->name, g_free);
    G_OBJECT_CLASS (lypad_tag_parent_class)->finalize (object);
}

static void lypad_tag_get_property (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
    LypadTag *tag = LYPAD_TAG (object);
    switch (prop_id)
    {
        case PROP_NAME:
            g_value_set_string (value, tag->name);
            break;

        case PROP_COLOR:
            g_value_set_boxed (value, &tag->color);
            break;
    }
}

static void lypad_tag_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
    LypadTag *tag = LYPAD_TAG (object);
    switch (prop_id)
    {
        case PROP_NAME:
            g_return_if_fail (tag->name == NULL);
            tag->name = g_strdup (g_value_get_string (value));
            break;

        case PROP_COLOR:
            tag->color = *((GdkRGBA *) g_value_get_boxed (value));
            break;
    }
}

static GObject *lypad_tag_constructor (GType type, guint n_construct_params, GObjectConstructParam *construct_params)
{
    GObject *tag = NULL;
    const char *name = NULL;
    for (guint i = 0; i < n_construct_params; ++i)
    {
        if (construct_params[i].pspec == properties[PROP_NAME])
            name = g_value_get_string (construct_params[i].value);
    }
    g_warn_if_fail (name != NULL);

    for (GList *l = tag_list; l != NULL; l = l->next)
    {
        if (g_strcmp0 (name, LYPAD_TAG (l->data)->name) == 0)
        {
            tag = g_object_ref (l->data);
            break;
        }
    }
    if (!tag)
    {
        tag = G_OBJECT_CLASS (lypad_tag_parent_class)->constructor (type, n_construct_params, construct_params);
        tag_list = g_list_insert_sorted (tag_list, tag, lypad_tag_compare);
        GObject *signaler = lypad_tag_get_signaler ();
        g_signal_emit (signaler, signals[SIGNAL_TAG_CREATED], 0, tag);
    }
    return tag;
}

LypadTag *lypad_tag_find (const char *name)
{
    for (GList *l = tag_list; l != NULL; l = l->next)
        if (g_strcmp0 (name, LYPAD_TAG (l->data)->name) == 0)
            return l->data;
    return NULL;
}

static void lypad_tag_class_init (LypadTagClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    object_class->constructor  = lypad_tag_constructor;
    object_class->get_property = lypad_tag_get_property;
    object_class->set_property = lypad_tag_set_property;
    object_class->finalize     = lypad_tag_finalize;

    properties[PROP_NAME] = g_param_spec_string ("name", "Name",
                            "The text of the tag", "", G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

    properties[PROP_COLOR] = g_param_spec_boxed ("color", "Color",
                            "The color of the tag", GDK_TYPE_RGBA, G_PARAM_READWRITE);

    g_object_class_install_properties (object_class, N_PROPERTIES, properties);

    signals[SIGNAL_DESTROY] = g_signal_new ("destroy", G_TYPE_FROM_CLASS (klass),
                                            G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                            g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

    signals[SIGNAL_TAG_ADDED] = g_signal_new ("added", G_TYPE_FROM_CLASS (klass),
                                              G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                              g_cclosure_marshal_VOID__OBJECT, G_TYPE_NONE, 1, LYPAD_TYPE_NOTE);

    signals[SIGNAL_TAG_REMOVED] = g_signal_new ("removed", G_TYPE_FROM_CLASS (klass),
                                                G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                                g_cclosure_marshal_VOID__OBJECT, G_TYPE_NONE, 1, LYPAD_TYPE_NOTE);
}

LypadTag *lypad_tag_new (const char *name)
{
    return g_object_new (LYPAD_TYPE_TAG, "name", name, NULL);
}

/* Signaler */

typedef GObject LypadTagSignaler;
typedef GObjectClass LypadTagSignalerClass;

static GType lypad_tag_signaler_get_type (void);

G_DEFINE_TYPE (LypadTagSignaler, lypad_tag_signaler, G_TYPE_OBJECT)

GObject *lypad_tag_get_signaler ()
{
    static GObject *signaler = NULL;
    if (G_UNLIKELY (!signaler))
        signaler = g_object_new (lypad_tag_signaler_get_type (), NULL);

    return signaler;
}

static void lypad_tag_signaler_init (LypadTagSignaler *signaler)
{
}

static void lypad_tag_signaler_class_init (LypadTagSignalerClass *klass)
{
    signals[SIGNAL_TAG_CREATED] = g_signal_new ("tag-created", G_TYPE_FROM_CLASS (klass),
                                                G_SIGNAL_RUN_FIRST, 0, NULL, NULL,
                                                g_cclosure_marshal_VOID__OBJECT, G_TYPE_NONE, 1, LYPAD_TYPE_TAG);
}

/************/
